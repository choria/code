#!/bin/bash

# variables
project=choria
version=$(grep "GAME_VERSION=.*" -o ../CMakeLists.txt | sed 's/[^0-9.]//g')
gitver=$(git rev-parse --short HEAD)
