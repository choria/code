#!/bin/bash

# change to script dir
cd "$(dirname "${BASH_SOURCE[0]}")" || exit

# test for version files
if [ ! -f "../src/version.cpp" ] || [ ! -f "../src/version.h" ]; then
	echo "Version files missing! Game must be built first!"
	exit 1
fi

# test for stats.db
if [ ! -f "../working/stats/stats.db" ]; then
	echo "stats.db missing! Game must be built first!"
	exit 1
fi

# includes
source common.inc

# variables
base="$project-$version-$gitver"
pkg="$base-src.tar"

# prepare
mkdir -p out
rm -f "out/$pkg.gz"

echo "Making $pkg"

# build package
tar --transform "s|^|$base/|" -cvf "out/$pkg" -C ../ \
--exclude="$pkg" \
--exclude=*.swp \
--exclude=.git \
--exclude=working/"$project"* \
--exclude=working/scripts \
ext/ \
src/ \
working/ \
deployment/choria{,.desktop,.png,.xml} \
cmake/ \
build.sh \
CMakeLists.txt \
README \
CHANGELOG \
LICENSE

# add source scripts
tar --append --transform "s|^|$base/working/|" -f "out/$pkg" -C "../assets/source/" scripts
gzip "out/$pkg"

# output
echo -e "\nMade $pkg.gz"
