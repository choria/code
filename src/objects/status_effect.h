/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <sstream>
#include <vector>
#include <cstdint>

// Forward Declarations
class _Buff;
class _Object;
class _Stats;

namespace ae {
	class _Buffer;
	class _Element;
}

// Classes
class _StatusEffect {

	public:

		struct _Stack {
			_Stack() { }
			_Stack(_StatusEffect *StatusEffect) : Duration(StatusEffect->Duration), Level(StatusEffect->Level) { }
			double Duration{0.0};
			int64_t Level{0};
		};

		~_StatusEffect();

		void Serialize(ae::_Buffer &Data);
		void Unserialize(ae::_Buffer &Data, const _Stats *Stats);

		bool Update(double FrameTime, bool InBattle);
		bool ShowWarning();

		ae::_Element *CreateElement(ae::_Element *Parent);
		void Render(ae::_Element *Element, double Timer, bool InBattle);

		std::vector<_Stack> Stacks;
		ae::_Element *BattleElement{nullptr};
		ae::_Element *HUDElement{nullptr};
		const _Buff *Buff{nullptr};
		_Object *Source{nullptr};
		double Time{0.0};
		double Duration{0.0};
		double MaxDuration{0.0};
		int64_t Level{0};
		int Priority{0};
		int Modifier{0};
		int TargetCount{0};
		bool Infinite{false};
		bool Deleted{false};

	private:

		static void FormatDuration(std::stringstream &Buffer, double Duration);
};
