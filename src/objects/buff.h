/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <string>
#include <cstdint>

// Forward Declarations
class _Scripting;
class _StatChange;
class _StatusEffect;

namespace ae {
	class _Texture;
}

// Classes
class _Buff {

	public:

		void DrawTooltip(_Scripting *Scripting, const _StatusEffect *Effect, int DismissLevel) const;
		void ExecuteScript(_Scripting *Scripting, const std::string &Function, int64_t Level, _StatChange &StatChange) const;

		std::string Name;
		std::string Script;
		const ae::_Texture *Texture;
		double WarningTime;
		uint32_t ID;
		uint32_t SwapBuffID;
		int Rank;
		int ShowLevel;
		int Dismiss;
		int Modifier;
		bool PauseDuringBattle;
		bool Summon;
		bool Overwrite;
		bool Stack;
		bool Extend;
		bool Hide;
		bool PassOn;

	private:

};
