/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <constants.h>
#include <string>
#include <unordered_map>
#include <cmath>

// Forward Declarations
class _Object;

namespace ae {
	template<class T> class _Manager;
	class _Buffer;
	class _Font;
}

enum class StatValueType : int {
	BOOLEAN,
	INTEGER,
	INTEGER64,
	DOUBLE,
	PERCENT,
	TIME,
	POINTER,
	PERCENT64,
	PERCENT_DOUBLE,
};

enum class StatUpdateType : int {
	NONE,
	ADD,
	SET,
	MULTIPLICATIVE,
};

// Holds a value union
struct _Value {

	double Mult() const { return Int * 0.01; }
	int Multiplicative(int Current) { return std::ceil(Int * (100 - Current) * 0.01); }
	static const _Value &Print(const std::unordered_map<std::string, _Value> &Map, const char *Key);

	union {
		int64_t Int;
		double Double;
		void *Pointer;
	};
};

// Stat changes
class _StatChange {

	public:

		void Reset() { Object = nullptr; Values.clear(); }
		bool HasStat(const std::string &Name) const { return Values.find(Name) != Values.end(); }

		void Serialize(ae::_Buffer &Data);
		void Unserialize(ae::_Buffer &Data, ae::_Manager<_Object> *Manager);

		void Log();

		// Owner
		_Object *Object{nullptr};

		// Data
		std::unordered_map<std::string, _Value> Values;
};

// Graphical stat change
class _StatChangeUI {

	public:

		_StatChangeUI() { }

		void Render(double BlendFactor);
		void SetText(const glm::vec4 &NegativeColor, const glm::vec4 &PositiveColor);

		const ae::_Font *Font{nullptr};
		std::string Text;
		glm::vec4 Color{1.0f};
		glm::vec2 StartPosition{0.0f};
		glm::vec2 LastPosition{0.0f};
		glm::vec2 Position{0.0f};
		float Direction{-1.0f};
		double Time{0.0};
		double Timeout{HUD_STATCHANGE_TIMEOUT};
		double Change{0.0};
		bool Battle{false};

};
