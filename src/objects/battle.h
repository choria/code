/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <ae/baseobject.h>
#include <objects/action.h>

// Forward Declarations
class _Scripting;
class _Server;

namespace ae {
	class _ClientNetwork;
	class _Element;
}

struct _BattleResult {
	int64_t TotalBounty{0};
	int64_t TotalGoldStolen{0};
	int64_t ExperiencePerCharacter{0};
	int64_t GoldPerCharacter{0};
	int64_t GoldStolenPerCharacter{0};
	int64_t TotalExperienceGiven{0};
	int64_t TotalGoldGiven{0};
	int AliveCount{0};
	int PlayerCount{0};
	int AlivePlayerCount{0};
	int AliveMonsterCount{0};
	int MonsterCount{0};
	int JoinedCount{0};
	bool Dead{true};
};

// Classes
class _Battle : public ae::_BaseObject {

	public:

		_Battle();
		~_Battle() override;

		// Objects
		void AddObject(_Object *Object, uint8_t Side, bool Join=false);
		void RemoveObject(_Object *RemoveObject);
		void GetSeparateObjectList(uint8_t Side, std::vector<_Object *> &Allies, std::vector<_Object *> &Enemies);
		void GetObjectList(int Side, std::vector<_Object *> &SideObjects);
		_Object *SetTargets(_Object *SourceObject, const _Action &Action, _Object *InitialTarget, std::vector<_Object *> &Targets);
		size_t GetPotentialTargets(int Side, const _Item *Item, _Object *SourceObject, _Object *StartObject, std::vector<_Object *> &Targets);
		int GetPeerCount();

		// Updates
		void Update(double FrameTime) override;
		void Render(double BlendFactor);

		// Network
		void Serialize(ae::_Buffer &Data);
		void Unserialize(ae::_Buffer &Data);
		void BroadcastPacket(ae::_Buffer &Data);
		void BroadcastStatusEffects(_Object *UpdatedObject);

		// Setup
		void ServerEndBattle();

		// Input
		bool ClientHandleInput(size_t Action, bool MouseCombat=false);
		void ClientHandlePlayerAction(ae::_Buffer &Data);
		void ClientSetAction(uint8_t ActionBarSlot);
		void ClientChangeTarget(int Direction, bool NextColumn, bool SideDirection);

		// Pointers
		const _Stats *Stats{nullptr};
		_Server *Server{nullptr};
		_Scripting *Scripting{nullptr};
		ae::_ClientNetwork *ClientNetwork{nullptr};
		_Object *ClientPlayer{nullptr};
		std::vector<_Object *> ClientTargets;
		ae::_Manager<_Object> *Manager{nullptr};

		// Objects
		std::vector<_Object *> Objects;
		std::vector<_ActionResult> ActionResults;

		// Attributes
		std::string Name;
		double Time{0.0};
		double Duration{0.0};
		double Cooldown{0.0};
		uint32_t Zone{0};
		int64_t Difficulty{0};
		int SideCount[2]{0, 0};
		double Multiplier{1.0};
		float BountyEarned{0.0f};
		float BountyClaimed{0.0f};
		bool PVP{false};
		bool Boss{false};

	private:

		void GetBattleOffset(int SideIndex, _Object *Object);
		void AdjustBattleElements(int SideIndex, _Object *Object);
		void CreateBattleElements(int SideIndex, _Object *Object);

		void RenderActionResults(_ActionResult &ActionResult, double BlendFactor);

		// State
		double WaitTimer{0.0};

		// UI
		ae::_Element *BattleElement{nullptr};
		ae::_Element *TimerElement{nullptr};
		ae::_Element *DifficultyElement{nullptr};

};
