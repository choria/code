/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <objects/map.h>
#include <ae/actions.h>
#include <ae/assets.h>
#include <ae/atlas.h>
#include <ae/camera.h>
#include <ae/clientnetwork.h>
#include <ae/font.h>
#include <ae/framebuffer.h>
#include <ae/graphics.h>
#include <ae/peer.h>
#include <ae/program.h>
#include <ae/servernetwork.h>
#include <ae/texture.h>
#include <hud/hud.h>
#include <objects/components/character.h>
#include <objects/components/controller.h>
#include <objects/components/inventory.h>
#include <objects/battle.h>
#include <objects/item.h>
#include <objects/object.h>
#include <states/editor.h>
#include <states/play.h>
#include <actiontype.h>
#include <packet.h>
#include <scripting.h>
#include <server.h>
#include <stats.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>
#include <zlib/zfstream.h>
#include <algorithm>
#include <iomanip>

// Color overlays for zones
const glm::vec4 ZoneColors[] = {
	{ 1.0f, 0.0f, 0.0f, 0.4f },
	{ 0.0f, 1.0f, 0.0f, 0.4f },
	{ 0.0f, 0.0f, 1.0f, 0.4f },
	{ 1.0f, 1.0f, 0.0f, 0.4f },
	{ 0.0f, 1.0f, 1.0f, 0.4f },
	{ 1.0f, 0.0f, 1.0f, 0.4f },
};

// Colors of each time cycle
const std::vector<glm::vec4> DayCycles = {
	{ 0.0625f, 0.0625f, 0.375f,  1 },
	{ 0.125f,  0.125f,  0.125f,  1 },
	{ 0.75f,   0.75f,   0.5625f, 1 },
	{ 0.6875f, 0.5625f, 0.375f,  1 },
	{ 0.625f,  0.5f,    0.375f,  1 },
};

// Colors of each time cycle during blood moon
const std::vector<glm::vec4> DayCyclesBlood = {
	{ 1.0f,    0.0f,    0.0f,    1 },
	{ 0.5f,    0.125f,  0.125f,  1 },
	{ 0.75f,   0.75f,   0.5625f, 1 },
	{ 1.0f,    0.5625f, 0.575f,  1 },
	{ 1.0f,    0.3f,    0.275f,  1 },
};

// Time of each cycle change
const std::vector<double> DayCyclesTime = {
	0.0  * 60.0,
	6.0  * 60.0,
	12.5 * 60.0,
	16.5 * 60.0,
	18.0 * 60.0,
};

// Constructor
_Map::_Map() : MaxZoneColors(sizeof(ZoneColors) / sizeof(glm::vec4)), CurrentZoneColors(MaxZoneColors) {
}

// Destructor
_Map::~_Map() {

	// Delete path finding
	delete Pather;

	// Delete background layer
	delete BackgroundMap;

	// Delete atlas
	if(!Headless)
		CloseAtlas();

	// Delete map data
	FreeMap();

	// Update objects
	for(auto &Object : Objects)
		Object->Map = nullptr;

	// Delete objects managed by map
	for(auto &Object : StaticObjects)
		delete Object;
}

// Allocates memory for the map
void _Map::AllocateMap() {
	if(Tiles)
		return;

	Tiles = new _Tile*[(uint32_t)Size.x];
	for(int i = 0; i < Size.x; i++) {
		Tiles[i] = new _Tile[(uint32_t)Size.y];
	}
}

// Resize tile data
void _Map::ResizeMap(glm::ivec2 Offset, glm::ivec2 NewSize) {

	// Create new map
	_Tile **NewTiles = new _Tile*[(uint32_t)NewSize.x];
	for(int i = 0; i < NewSize.x; i++) {
		NewTiles[i] = new _Tile[(uint32_t)NewSize.y];
	}

	// Copy data
	glm::ivec2 TileIndex;
	for(int j = 0; j < Size.y; j++) {
		TileIndex.y = j - Offset.y;
		if(TileIndex.y < 0 || TileIndex.y >= NewSize.y)
			continue;

		for(int i = 0; i < Size.x; i++) {
			TileIndex.x = i - Offset.x;
			if(TileIndex.x < 0 || TileIndex.x >= NewSize.x)
				continue;

			NewTiles[TileIndex.x][TileIndex.y] = Tiles[i][j];
		}
	}

	// Save old texture atlas name
	std::string OldTextureAtlas = "";
	if(TileAtlas)
		OldTextureAtlas = TileAtlas->Texture->Name;

	// Delete data
	CloseAtlas();
	FreeMap();

	// Init new data
	Tiles = NewTiles;
	Size = NewSize;
	if(OldTextureAtlas != "")
		InitAtlas(OldTextureAtlas);

	// Update index
	IndexEvents();

	// Update static objects
	for(auto &Object : StaticObjects)
		Object->Position -= Offset;
}

// Initialize the texture atlas
void _Map::InitAtlas(const std::string AtlasPath, bool Static) {
	const ae::_Texture *AtlasTexture = ae::Assets.Textures[AtlasPath];
	if(!AtlasTexture)
		throw std::runtime_error("Can't find atlas: " + AtlasPath);

	TileAtlas = new ae::_Atlas(AtlasTexture, glm::ivec2(MAP_TILE_WIDTH, MAP_TILE_HEIGHT), 1);

	GLuint TileVertexCount = (GLuint)(4 * Size.x * Size.y);
	GLuint TileFaceCount = (GLuint)(2 * Size.x * Size.y);

	TileFaces = new glm::u32vec3[TileFaceCount];

	int FaceIndex = 0;
	int VertexIndex = 0;
	for(int j = 0; j < Size.y; j++) {
		for(int i = 0; i < Size.x; i++) {
			TileFaces[FaceIndex++] = { VertexIndex + 2, VertexIndex + 1, VertexIndex + 0 };
			TileFaces[FaceIndex++] = { VertexIndex + 2, VertexIndex + 3, VertexIndex + 1 };
			VertexIndex += 4;
		}
	}

	// Create a static vbo
	if(Static) {
		uint32_t VertexIndex = 0;
		TileVertices[0] = new glm::vec4[TileVertexCount];
		TileVertices[1] = new glm::vec4[TileVertexCount];

		for(int k = 0; k < 2; k++) {
			VertexIndex = 0;
			for(int j = 0; j < Size.y; j++) {
				for(int i = 0; i < Size.x; i++) {
					glm::vec4 TextureCoords = TileAtlas->GetTextureCoords(Tiles[i][j].TextureIndex[k]);
					TileVertices[k][VertexIndex++] = { i + 0.0f, j + 0.0f, TextureCoords[0], TextureCoords[1] };
					TileVertices[k][VertexIndex++] = { i + 1.0f, j + 0.0f, TextureCoords[2], TextureCoords[1] };
					TileVertices[k][VertexIndex++] = { i + 0.0f, j + 1.0f, TextureCoords[0], TextureCoords[3] };
					TileVertices[k][VertexIndex++] = { i + 1.0f, j + 1.0f, TextureCoords[2], TextureCoords[3] };
				}
			}

			glGenBuffers(1, &TileVertexBufferID[k]);
			glBindBuffer(GL_ARRAY_BUFFER, TileVertexBufferID[k]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4) * TileVertexCount, TileVertices[k], GL_STATIC_DRAW);
		}

		glGenBuffers(1, &TileElementBufferID);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, TileElementBufferID);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(glm::u32vec3) * TileFaceCount, TileFaces, GL_STATIC_DRAW);
	}
	else {
		TileVertices[0] = new glm::vec4[TileVertexCount];

		glGenBuffers(1, &TileVertexBufferID[0]);
		glBindBuffer(GL_ARRAY_BUFFER, TileVertexBufferID[0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4) * TileVertexCount, nullptr, GL_DYNAMIC_DRAW);

		glGenBuffers(1, &TileElementBufferID);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, TileElementBufferID);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(glm::u32vec3) * TileFaceCount, nullptr, GL_DYNAMIC_DRAW);
	}
}

// Free memory used by texture atlas
void _Map::CloseAtlas() {
	delete TileAtlas;
	delete[] TileVertices[0];
	delete[] TileVertices[1];
	delete[] TileFaces;
	glDeleteBuffers(1, &TileVertexBufferID[0]);
	glDeleteBuffers(1, &TileVertexBufferID[1]);
	glDeleteBuffers(1, &TileElementBufferID);

	TileVertexBufferID[0] = 0;
	TileVertexBufferID[1] = 0;
	TileElementBufferID = 0;
	TileAtlas = nullptr;
	TileVertices[0] = nullptr;
	TileVertices[1] = nullptr;
	TileFaces = nullptr;
}

// Free memory used by the tiles
void _Map::FreeMap() {
	if(Tiles) {
		for(int i = 0; i < Size.x; i++)
			delete[] Tiles[i];
		delete[] Tiles;

		Tiles = nullptr;
	}
}

// Updates the map and sends object updates
void _Map::Update(double FrameTime) {

	// Update clock
	Clock += FrameTime * MAP_CLOCK_SPEED;
	if(Clock >= MAP_DAY_LENGTH)
		Clock -= MAP_DAY_LENGTH;
}

// Check for events
bool _Map::CheckEvents(_Object *Object, int Mode, bool Wait) const {

	// Get tile
	const _Tile *Tile = &Tiles[Object->Position.x][Object->Position.y];

	// Only check for manual trigger events
	if(Mode == 0) {
		Object->Character->OnTrigger = false;

		// Wait for server when using the use command on certain events
		if(Wait) {
			switch(Tile->Event.Type) {
				case _Map::EVENT_SPAWN:
				case _Map::EVENT_MAPCHANGE:
				case _Map::EVENT_VENDOR:
				case _Map::EVENT_TRADER:
				case _Map::EVENT_PORTAL:
				case _Map::EVENT_BLACKSMITH:
				case _Map::EVENT_MINIGAME:
				case _Map::EVENT_ENCHANTER:
				case _Map::EVENT_STASH:
				case _Map::EVENT_DISENCHANTER:
					Object->Controller->WaitForServer = true;
				break;
				case _Map::EVENT_JUMP:
					glm::ivec2 Position;
					if(FindEvent(_Event(Tile->Event.Type, Tile->Event.Data + 1), Position))
						Object->Controller->WaitForServer = true;
				break;
			}
		}

		// Update trigger state
		if(Tile->Event.Type == _Map::EVENT_SCRIPT)
			RunEventScript(Tile, Object, Mode);

		// Show use text
		if(Tile->Event.Type == _Map::EVENT_SPAWN && !Object->Character->CompareSpawn(NetworkID, Tile->Event.Data))
			HUD.UseText = "set spawn point";

		return Tile->Event.Type;
	}

	// Handle non-trigger events
	switch(Tile->Event.Type) {
		case _Map::EVENT_SPAWN:
			if(Server) {

				// Check for manual trigger
				if(Mode == 2) {
					Object->Character->SpawnMapID = NetworkID;
					Object->Character->SpawnPoint = Tile->Event.Data;
					Server->SendSpawnPoint(Object);
				}
			}
			else if(!Object->Character->CompareSpawn(NetworkID, Tile->Event.Data))
				HUD.UseText = "set spawn point";
		break;
		case _Map::EVENT_MAPCHANGE:
			if(Server)
				Server->SpawnPlayer(Object, (ae::NetworkIDType)Tile->Event.Data, _Map::EVENT_MAPENTRANCE);
			else
				Object->Controller->WaitForServer = true;
		break;
		case _Map::EVENT_VENDOR:
		case _Map::EVENT_TRADER:
		case _Map::EVENT_BLACKSMITH:
		case _Map::EVENT_ENCHANTER:
		case _Map::EVENT_DISENCHANTER:
		case _Map::EVENT_MINIGAME:
		case _Map::EVENT_STASH:
			if(Server)
				StartEvent(Object, Tile->Event, false);
			else
				Object->Controller->WaitForServer = true;
		break;
		case _Map::EVENT_SCRIPT:
			RunEventScript(Tile, Object, Mode);
		break;
		case _Map::EVENT_PORTAL:
			if(Server) {

				// Find matching even/odd event
				FindEvent(_Event(Tile->Event.Type, Tile->Event.Data ^ 1), Object->Position);
				Server->SendPlayerPosition(Object->Peer);
			}
			else
				Object->Controller->WaitForServer = true;
		break;
		case _Map::EVENT_JUMP: {

			// Find next jump
			glm::ivec2 Position;
			if(FindEvent(_Event(Tile->Event.Type, Tile->Event.Data + 1), Position)) {
				if(Server) {
					Object->Position = Position;
					Server->SendPlayerPosition(Object->Peer);
				}
				else
					Object->Controller->WaitForServer = true;
			}
		} break;
		default:
			if(Server) {
				Object->Character->Vendor = nullptr;
				Object->Character->Trader = nullptr;
				CheckBattle(Object, Tile);
			}
		break;
	}

	if(Server && Object->QueuedMapChange) {
		Server->SpawnPlayer(Object, Object->QueuedMapChange, _Map::EVENT_MAPENTRANCE);
		Object->QueuedMapChange = 0;
	}

	return Tile->Event.Type;
}

// Check for next battle
void _Map::CheckBattle(_Object *Object, const _Tile *Tile) const {
	if(!Server || !Tile->Zone)
		return;

	if(Object->Character->IsAlive() && Object->Character->NextBattle <= 0) {
		auto Iterator = Stats->Zones.find(Tile->Zone);
		if(Iterator == Stats->Zones.end())
			return;

		const _Zone &Zone = Iterator->second;
		Server->QueueBattle(Object, Zone.Name, Zone.ID, false, false, false, 0.0f, 0.0f, 0.0);
	}
}

// Run event script
void _Map::RunEventScript(const _Tile *Tile, _Object *Object, int Mode) const {

	// Find script
	auto Iterator = Stats->Scripts.find(Tile->Event.Data);
	if(Iterator == Stats->Scripts.end())
		return;

	// Check for manually triggered events
	const _Script &Script = Iterator->second;
	if(Script.Manual) {
		if(Script.Name == "Script_Boss") {
			uint32_t ZoneID = (uint32_t)Script.Level;
			if(Object->Character->IsZoneOnCooldown(ZoneID)) {
				if(Server)
					SendBossCooldownMessage(Object, Object->Character->BossCooldowns[ZoneID]);
			}
			else
				Object->Character->OnTrigger = true;
		}

		if(Server)
			Server->UpdateEventCount();

		if(Mode != 2)
			return;
	}

	// Only check triggers
	if(!Mode)
		return;

	// Call script
	_StatChange StatChange;
	StatChange.Object = Object;
	if(Object->Scripting->StartMethodCall(Script.Name, "Activate")) {
		Object->Scripting->PushInt(Script.Level);
		Object->Scripting->PushObject(StatChange.Object);
		Object->Scripting->PushStatChange(&StatChange);
		Object->Scripting->MethodCall(3, 1);
		Object->Scripting->GetStatChange(1, Stats, StatChange);
		Object->Scripting->FinishMethodCall();

		if(Server) {
			StatChange.Values["Manual"].Int = (Mode == 2);
			Object->UpdateStats(StatChange, Object);
			StatChange.Values.erase("Manual");

			// Update stat on client
			if(Object->Peer) {
				ae::_Buffer Packet;
				Packet.Write<PacketType>(PacketType::STAT_CHANGE);
				StatChange.Serialize(Packet);
				Server->Network->SendPacket(Packet, Object->Peer);
			}

			CheckBattle(Object, Tile);
		}
		else {

			// Stop moving on client
			if(StatChange.HasStat("MapChange"))
				Object->Controller->WaitForServer = true;

			// Play sound on client
			if(Object->Scripting->StartMethodCall(Script.Name, "PlaySound")) {
				Object->Scripting->PushObject(Object);
				Object->Scripting->MethodCall(1, 0);
				Object->Scripting->FinishMethodCall();
			}
		}
	}
}

// Build indexed events list
void _Map::IndexEvents() {
	IndexedEvents.clear();

	// Build event index
	for(int j = 0; j < Size.y; j++) {
		for(int i = 0; i < Size.x; i++) {
			const _Tile &Tile = Tiles[i][j];
			if(Tile.Event.Type != EVENT_NONE) {
				IndexedEvents[Tile.Event].push_back(glm::ivec2(i, j));
			}
		}
	}
}

// Convert clock time to text
void _Map::GetClockAsString(std::stringstream &Buffer, bool Clock24Hour) const {
	int Hours = (int)(Clock / 60.0);
	int Minutes = (int)std::fmod(Clock, 60.0);
	if(!Clock24Hour) {
		if(Hours == 0)
			Hours = 12;
		else if(Hours > 12)
			Hours -= 12;

		Buffer << Hours << ":" << std::setfill('0') << std::setw(2) << Minutes;
		if(Clock < MAP_DAY_LENGTH / 2)
			Buffer << " AM";
		else
			Buffer << " PM";
	}
	else {
		Buffer << std::setfill('0') << std::setw(2) << Hours << ":" << std::setfill('0') << std::setw(2) << Minutes;
	}
}

// Set ambient light for map
void _Map::SetAmbientLightByClock() {
	if(!OutsideFlag)
		return;

	// Find index by time
	size_t NextCycle = DayCyclesTime.size();
	for(size_t i = 0; i < DayCyclesTime.size(); i++) {
		if(Clock < DayCyclesTime[i]) {
			NextCycle = i;
			break;
		}
	}

	// Get indices for current and next cycle
	size_t CurrentCycle = NextCycle - 1;
	if(CurrentCycle >= DayCyclesTime.size())
		CurrentCycle = 0;
	if(NextCycle >= DayCyclesTime.size())
		NextCycle = 0;

	// Get current time diff
	double Diff = Clock - DayCyclesTime[CurrentCycle];
	if(Diff < 0)
		Diff += MAP_DAY_LENGTH;

	// Get length of cycle
	double Length = DayCyclesTime[NextCycle] - DayCyclesTime[CurrentCycle];
	if(Length < 0)
		Length += MAP_DAY_LENGTH;

	// Get percent to next cycle
	float Percent = (float)(Diff / Length);

	// Get cycle light
	const auto &Cycles = PlayState.BloodMoonActive ? DayCyclesBlood : DayCycles;

	// Set color
	glm::vec4 OutdoorLight = glm::mix(Cycles[CurrentCycle], Cycles[NextCycle], Percent);
	if(OutsideFlag == 2)
		AmbientLight = LightFilter * OutdoorLight;
	else
		AmbientLight = OutdoorLight;
}

// Start event for an object and send packet
void _Map::StartEvent(_Object *Object, _Event Event, bool Remote) const {
	if(!Server)
		return;

	// Handle event types
	try {
		switch(Event.Type) {
			case _Map::EVENT_TRADER:
				Object->Character->Trader = &Server->Stats->Traders.at(Event.Data);
				if(!Object->Character->Trader->ID)
					return;
			break;
			case _Map::EVENT_VENDOR:
				Object->Character->Vendor = &Server->Stats->Vendors.at(Event.Data);
				if(!Object->Character->Vendor->ID)
					return;
			break;
			case _Map::EVENT_BLACKSMITH:
				Object->Character->Blacksmith = &Server->Stats->Blacksmiths.at(Event.Data);
				if(!Object->Character->Blacksmith->ID)
					return;
			break;
			case _Map::EVENT_ENCHANTER:
				Object->Character->Enchanter = &Server->Stats->Enchanters.at(Event.Data);
				if(!Object->Character->Enchanter->ID)
					return;
			break;
			case _Map::EVENT_DISENCHANTER:
				Object->Character->Disenchanter = &Server->Stats->Disenchanters.at(Event.Data);
				if(!Object->Character->Disenchanter->ID)
					return;
			break;
			case _Map::EVENT_MINIGAME:
				Object->Character->Minigame = &Server->Stats->Minigames.at(Event.Data);
				if(!Object->Character->Minigame->ID)
					return;
			break;
			case _Map::EVENT_STASH:
				Object->Character->ViewingStash = 1 + (int)Remote;
			break;
			default:
				return;
			break;
		}
	}
	catch(std::exception &Error) {
		return;
	}

	// Notify client
	if(Object->Peer->ENetPeer) {
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::EVENT_START);
		Packet.WriteBit(Remote);
		Packet.Write<uint32_t>(Event.Type);
		Packet.Write<uint32_t>(Event.Data);
		Packet.Write<glm::ivec2>(Object->Position);
		Server->Network->SendPacket(Packet, Object->Peer);
	}

	// Generate seed
	if(Event.Type == _Map::EVENT_MINIGAME)
		Object->SendSeed();
}
// Send boss cooldown message to client
void _Map::SendBossCooldownMessage(_Object *Object, double Duration) const {
	if(!Server)
		return;

	// Get message
	uint8_t MessageID;
	if(Duration < 0)
		MessageID = 0;
	else if(Duration <= 10)
		MessageID = 1;
	else if(Duration <= 60)
		MessageID = 2;
	else if(Duration <= 300)
		MessageID = 3;
	else if(Duration <= 600)
		MessageID = 4;
	else if(Duration <= 1800)
		MessageID = 5;
	else if(Duration <= 3600)
		MessageID = 6;
	else if(Duration <= GAME_BOSS_STRONGER_THRESHOLD)
		MessageID = 7;
	else if(Duration <= GAME_BOSS_BANISHED_THRESHOLD)
		MessageID = 8;
	else
		MessageID = 9;

	// Send packet
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::PLAYER_BOSSCOOLDOWN_MESSAGE);
	Packet.Write<uint8_t>(MessageID);
	Server->Network->SendPacket(Packet, Object->Peer);
}

// Determine if a position is in a pvp zone
bool _Map::IsPVPZone(const glm::ivec2 &Position) const {
	if(!IsValidPosition(Position))
		return false;

	return GetTile(Position)->PVP;
}

// Renders the map
void _Map::Render(ae::_Camera *Camera, ae::_Framebuffer *Framebuffer, _Object *ClientPlayer, double BlendFactor, int RenderFlags) {

	// Set lights for editor
	if(RenderFlags & MAP_RENDER_EDITOR_AMBIENT) {
		Framebuffer->Clear();
		ae::_Framebuffer::Unbind();
		glm::vec4 AmbientLightEditor(1.0f);
		ae::Assets.Programs["map"]->AmbientLight = AmbientLightEditor;
		ae::Assets.Programs["pos_uv_static"]->AmbientLight = AmbientLightEditor;
	}
	else {

		// Setup day night cycle
		SetAmbientLightByClock();

		// Setup lights
		ae::Assets.Programs["map"]->AmbientLight = AmbientLight;

		// Add lights
		Framebuffer->Clear();
		ae::Graphics.EnableParticleBlending();
		AddLights(ClientPlayer, Objects, ae::Assets.Programs["pos_uv"], Camera->AABB);
		AddLights(nullptr, StaticObjects, ae::Assets.Programs["pos_uv"], Camera->AABB);
		ae::Graphics.DisableParticleBlending();
		ae::_Framebuffer::Unbind();
	}

	// Draw background map
	if(!(RenderFlags & MAP_RENDER_NOBACKGROUND) && BackgroundMap) {
		BackgroundMap->Clock = Clock;
		BackgroundMap->SetAmbientLightByClock();
		ae::Assets.Programs["pos_uv_static"]->AmbientLight = BackgroundMap->AmbientLight * BackgroundMap->AmbientBackground;
		if(RenderFlags & MAP_RENDER_EDITOR_AMBIENT)
			ae::Assets.Programs["pos_uv_static"]->AmbientLight = glm::vec4(1.0f);

		// Get camera position
		glm::vec3 DrawPosition;
		Camera->GetDrawPosition(BlendFactor, DrawPosition);
		DrawPosition -= BackgroundOffset;

		float Width = DrawPosition.z * ae::Graphics.AspectRatio;
		float Height = DrawPosition.z;

		// Get render bounds of background tiles
		glm::vec4 BackgroundBounds;
		BackgroundBounds[0] = glm::clamp(-Width + DrawPosition.x, 0.0f, (float)BackgroundMap->Size.x);
		BackgroundBounds[1] = glm::clamp(-Height + DrawPosition.y, 0.0f, (float)BackgroundMap->Size.y);
		BackgroundBounds[2] = glm::clamp(Width + DrawPosition.x, 0.0f, (float)BackgroundMap->Size.x);
		BackgroundBounds[3] = glm::clamp(Height + DrawPosition.y, 0.0f, (float)BackgroundMap->Size.y);

		BackgroundMap->RenderLayer("pos_uv_static", BackgroundBounds, BackgroundOffset, 0, true);
		BackgroundMap->RenderLayer("pos_uv_static", BackgroundBounds, BackgroundOffset, 1, true);
	}

	// Get render bounds
	glm::vec4 Bounds = Camera->AABB;
	Bounds[0] = glm::clamp(Bounds[0], 0.0f, (float)Size.x);
	Bounds[1] = glm::clamp(Bounds[1], 0.0f, (float)Size.y);
	Bounds[2] = glm::clamp(Bounds[2], 0.0f, (float)Size.x);
	Bounds[3] = glm::clamp(Bounds[3], 0.0f, (float)Size.y);

	// Set framebuffer texture
	if(Framebuffer) {
		ae::Assets.Programs["map"]->Use();
		ae::_Framebuffer::Unbind();
		ae::Graphics.SetActiveTexture(1);
		Framebuffer->BindTexture();
		ae::Graphics.SetActiveTexture(0);
	}

	// Draw layers
	RenderLayer("map", Bounds, glm::vec3(0.0f), 0);
	if(!(RenderFlags & MAP_RENDER_NOFOREGROUND))
		RenderLayer("map", Bounds, glm::vec3(0.0f), 1);

	// Render static objects
	for(const auto &Object : StaticObjects) {
		if(Object->Light || Object->Character)
			continue;

		Object->Render(Bounds, ClientPlayer);
	}

	// Clear object name map
	ObjectNames.clear();

	// Render players
	for(const auto &Object : Objects) {
		if(Object != ClientPlayer)
			Object->Render(Bounds, ClientPlayer);
	}

	// Render client player last
	if(ClientPlayer)
		ClientPlayer->Render(Bounds, ClientPlayer);

	// Check for flags
	if(!RenderFlags)
		return;

	// Draw map boundaries
	if(RenderFlags & MAP_RENDER_BOUNDARY) {
		ae::Graphics.SetProgram(ae::Assets.Programs["pos"]);
		ae::Graphics.SetColor(ae::Assets.Colors["red"]);
		ae::Graphics.DrawRectangle3D(glm::vec2(0), glm::vec2(Size), false);
	}

	// Draw boundary where map edge is almost visible
	if((RenderFlags & MAP_RENDER_EDGE_BOUNDARY) && Size.x > 32 && Size.y > 20) {
		ae::Graphics.SetProgram(ae::Assets.Programs["pos"]);
		ae::Graphics.SetColor(ae::Assets.Colors["editor_edge"]);
		ae::Graphics.DrawRectangle3D(glm::vec2(16, 10), glm::vec2(Size.x - 16, Size.y - 10), false);
	}

	// Draw zone overlays
	if(RenderFlags & MAP_RENDER_ZONE) {
		ae::Graphics.SetProgram(ae::Assets.Programs["pos"]);
		for(int j = (int)Bounds[1]; j < Bounds[3]; j++) {
			for(int i = (int)Bounds[0]; i < Bounds[2]; i++) {
				_Tile *Tile = &Tiles[i][j];

				// Draw zone color
				if(!Tile->Wall && Tile->Zone > 0) {
					ae::Graphics.SetColor(ZoneColors[Tile->Zone % CurrentZoneColors]);
					ae::Graphics.DrawRectangle(glm::vec2(i, j), glm::vec2(i+1, j+1), true);
				}
			}
		}
	}

	if(RenderFlags & MAP_RENDER_MAP_VIEW)
		return;

	// Draw text overlay
	ae::Assets.Fonts["hud_medium"]->SetupProgram();
	float Scale = 1.0f / (UI_TILE_SIZE.x * ae::_Element::GetUIScale());
	for(int ColorIndex = 0; ColorIndex < 3; ColorIndex++) {
		for(int j = (int)Bounds[1]; j < Bounds[3]; j++) {
			for(int i = (int)Bounds[0]; i < Bounds[2]; i++) {
				_Tile *Tile = &Tiles[i][j];
				glm::vec3 DrawPosition = glm::vec3(i, j, 0) + glm::vec3(0.5f, 0.5f, 0);

				// Draw wall
				if(Tile->Wall) {
					if(ColorIndex == 0 && (RenderFlags & MAP_RENDER_WALL))
						ae::Assets.Fonts["hud_medium"]->DrawText("W", glm::vec2(DrawPosition), ae::CENTER_MIDDLE, glm::vec4(1.0f), Scale, false);
				}
				else {

					// Draw zone number
					if(ColorIndex == 0 && (RenderFlags & MAP_RENDER_ZONE) && Tile->Zone > 0)
						ae::Assets.Fonts["hud_medium"]->DrawText(std::to_string(Tile->Zone), glm::vec2(DrawPosition), ae::CENTER_MIDDLE, glm::vec4(1.0f), Scale, false);

					// Draw PVP
					if(ColorIndex == 1 && (RenderFlags & MAP_RENDER_PVP) && Tile->PVP)
						ae::Assets.Fonts["hud_medium"]->DrawText("PVP", glm::vec2(DrawPosition) - glm::vec2(0, 0.25), ae::CENTER_MIDDLE, ae::Assets.Colors["red"], Scale, false);
				}

				// Draw event info
				if(ColorIndex == 2 && Tile->Event.Type > 0) {
					std::string EventText = Stats->EventNames[Tile->Event.Type].ShortName + std::string(" ") + std::to_string(Tile->Event.Data);
					ae::Assets.Fonts["hud_medium"]->DrawText(EventText, glm::vec2(DrawPosition) - glm::vec2(0, -0.25), ae::CENTER_MIDDLE, ae::Assets.Colors["cyan"], Scale, false);
				}
			}
		}

		// Render text
		ae::_Font::Draw();
	}
}

// Render use help text
void _Map::RenderUseText(ae::_Camera *Camera, _Object *ClientPlayer) {
	if(!ClientPlayer || HUD.UseText.empty())
		return;

	// Get text
	std::stringstream Buffer;
	Buffer << "Press [c yellow]" << ae::Actions.GetInputNameForAction(Action::GAME_USE) << "[c white] to " << HUD.UseText;

	// Render
	RenderText(Camera, ClientPlayer, Buffer);
}

// Render event text
void _Map::RenderEventText(ae::_Camera *Camera, _Object *ClientPlayer) {
	if(!ClientPlayer)
		return;

	if(!ClientPlayer->ShowTriggerMessage())
		return;

	// Get text
	std::stringstream Buffer;
	Buffer << "Press [c yellow]" << ae::Actions.GetInputNameForAction(Action::GAME_USE) << "[c white] to begin battle (" << ClientPlayer->Character->TriggerCount << "/" << BATTLE_MAX_OBJECTS_PER_SIDE << " players)";

	// Render
	RenderText(Camera, ClientPlayer, Buffer);
}

// Render text in center of screen
void _Map::RenderText(ae::_Camera *Camera, _Object *ClientPlayer, std::stringstream &Buffer) {

	// Get render position
	glm::vec2 ScreenPosition;
	Camera->ConvertWorldToScreen(glm::vec2(ClientPlayer->Position) + glm::vec2(0.5f, 1.6f), ScreenPosition);

	// Get text dimensions
	ae::_Font *NameFont = ae::Assets.Fonts["hud_medium"];
	ae::_TextBounds TextBounds;
	NameFont->GetStringDimensions(Buffer.str(), TextBounds, true);

	// Draw background
	float Padding = 10 * ae::_Element::GetUIScale();
	ae::_Bounds BackgroundBounds(glm::vec4(
		ScreenPosition.x - Padding - TextBounds.Width * 0.5f,
		ScreenPosition.y - Padding - TextBounds.AboveBase,
		ScreenPosition.x + Padding + TextBounds.Width * 0.5f,
		ScreenPosition.y + Padding + TextBounds.BelowBase
	));
	ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos"]);
	ae::Graphics.SetColor(ae::Assets.Colors["event_bg"]);
	ae::Graphics.DrawRectangle(BackgroundBounds, true);

	// Draw text
	NameFont->DrawTextFormatted(Buffer.str(), glm::ivec2(ScreenPosition), ae::CENTER_BASELINE);
}

// Render player names
void _Map::RenderPlayerNames(ae::_Camera *Camera, _Object *ClientPlayer) {

	// Draw stack of names
	ae::_Font *NameFont = ae::Assets.Fonts["hud_medium"];
	for(const auto &Pair : ObjectNames) {
		glm::vec2 TilePosition = IndexToPosition(Pair.first);
		float OffsetY = -0.1f;
		for(const auto &Name : Pair.second) {

			// Get render position
			glm::vec2 ScreenPosition;
			Camera->ConvertWorldToScreen(glm::vec2(TilePosition) + glm::vec2(0.5f, OffsetY), ScreenPosition);

			// Draw name
			NameFont->DrawTextFormatted(Name, glm::ivec2(ScreenPosition), ae::CENTER_BASELINE);
			OffsetY -= 0.36f;
		}
	}
}

// Render either floor or foreground texture tiles
void _Map::RenderLayer(const std::string &Program, glm::vec4 &Bounds, const glm::vec3 &Offset, int Layer, bool Static) {
	ae::Graphics.SetProgram(ae::Assets.Programs[Program]);
	ae::Graphics.SetColor(glm::vec4(1.0f));
	glUniformMatrix4fv(ae::Assets.Programs[Program]->ModelTransformID, 1, GL_FALSE, glm::value_ptr(glm::translate(glm::mat4(1.0f), Offset)));
	glUniformMatrix4fv(ae::Assets.Programs[Program]->TextureTransformID, 1, GL_FALSE, glm::value_ptr(glm::mat4(1.0f)));

	uint32_t VertexIndex = 0;
	int FaceIndex = 0;

	if(!Static) {
		for(int j = (int)Bounds[1]; j < Bounds[3]; j++) {
			for(int i = (int)Bounds[0]; i < Bounds[2]; i++) {
				glm::vec4 TextureCoords = TileAtlas->GetTextureCoords(Tiles[i][j].TextureIndex[Layer]);
				TileVertices[0][VertexIndex++] = { i + 0.0f, j + 0.0f, TextureCoords[0], TextureCoords[1] };
				TileVertices[0][VertexIndex++] = { i + 1.0f, j + 0.0f, TextureCoords[2], TextureCoords[1] };
				TileVertices[0][VertexIndex++] = { i + 0.0f, j + 1.0f, TextureCoords[0], TextureCoords[3] };
				TileVertices[0][VertexIndex++] = { i + 1.0f, j + 1.0f, TextureCoords[2], TextureCoords[3] };

				FaceIndex += 2;
			}
		}
		Layer = 0;
	}
	else {
		VertexIndex = (uint32_t)(Size.x * Size.y * 4);
		FaceIndex = Size.x * Size.y * 2;
	}

	GLsizeiptr VertexBufferSize = VertexIndex * sizeof(glm::vec4);
	GLsizeiptr ElementBufferSize = FaceIndex * (int)sizeof(glm::u32vec3);

	ae::Graphics.SetTextureID(TileAtlas->Texture->ID);
	ae::Graphics.SetAttribLevel(2);

	glBindBuffer(GL_ARRAY_BUFFER, TileVertexBufferID[Layer]);
	if(!Static)
		glBufferSubData(GL_ARRAY_BUFFER, 0, VertexBufferSize, TileVertices[Layer]);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec4), (void *)nullptr);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec4), (void *)sizeof(glm::vec2));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, TileElementBufferID);
	if(!Static)
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, ElementBufferSize, TileFaces);
	glDrawElements(GL_TRIANGLES, FaceIndex * 3, GL_UNSIGNED_INT, nullptr);

	ae::Graphics.ResetState();
}

// Add lights from objects
int _Map::AddLights(_Object *ClientPlayer, const std::vector<_Object *> &ObjectList, const ae::_Program *Program, glm::vec4 AABB) {
	ae::Graphics.SetProgram(Program);
	glUniformMatrix4fv(Program->TextureTransformID, 1, GL_FALSE, glm::value_ptr(glm::mat4(1.0f)));

	// Iterate over objects
	int LightCount = 0;
	for(const auto &Object : ObjectList) {
		if(!Object->Light)
			continue;

		// Check for valid light
		const auto &Iterator = Stats->Lights.find((uint32_t)Object->Light);
		if(Iterator == Stats->Lights.end())
			continue;

		const _LightType &LightType = Iterator->second;

		// Check to see if light is in frustum
		glm::vec2 Point(Object->Position.x + 0.5f, Object->Position.y + 0.5f);
		if(Point.x < AABB[0])
			Point.x = AABB[0];
		if(Point.y < AABB[1])
			Point.y = AABB[1];
		if(Point.x > AABB[2])
			Point.x = AABB[2];
		if(Point.y > AABB[3])
			Point.y = AABB[3];

		// Compare distances
		float DistanceSquared = glm::distance2(Point, glm::vec2(Object->Position) + glm::vec2(0.5f));
		if(DistanceSquared >= (LightType.Radius + 0.5f) * (LightType.Radius + 0.5f))
			continue;

		// Get light color
		glm::vec4 LightColor(LightType.Color, 1);
		glm::vec4 FinalLightColor;
		if(PlayState.LightBlendTime) {
			glm::vec4 StartColor = PlayState.BloodMoonActive ? GAME_BLOODMOON_LIGHTCOLOR : LightColor;
			glm::vec4 EndColor = PlayState.BloodMoonActive ? LightColor : GAME_BLOODMOON_LIGHTCOLOR;
			FinalLightColor = glm::mix(StartColor, EndColor, PlayState.LightBlendTime);
		}
		else
			FinalLightColor = PlayState.BloodMoonActive ? GAME_BLOODMOON_LIGHTCOLOR : LightColor;

		// Draw light
		glm::vec3 Position = glm::vec3(Object->Position, 0) + glm::vec3(0.5f, 0.5f, 0);
		glm::vec2 Scale(LightType.Radius * 2.0f);
		ae::Graphics.SetColor(FinalLightColor);
		ae::Graphics.DrawSprite(Position, ae::Assets.Textures["textures/lights/light0.webp"], 0.0f, Scale);

		LightCount++;
	}

	return LightCount;
}

// Load map
void _Map::Load(const _MapStat *MapStat, bool Static) {

	// Load file
	gzifstream File(MapStat->File.c_str());
	if(!File)
		throw std::runtime_error("Cannot load map: " + MapStat->File);

	// Save map stats
	if(Server)
		Headless = true;
	AmbientLight = LightFilter = MapStat->AmbientLight;
	AmbientBackground = MapStat->AmbientBackground;
	Music = MapStat->Music;

	// 0: static ambient light 1: use daytime cycle 2: multiply ambient with daytime cycle
	OutsideFlag = MapStat->Outside;

	// Load background map
	if(!Headless && MapStat->BackgroundMapID) {
		BackgroundOffset = MapStat->BackgroundOffset;

		BackgroundMap = new _Map();
		try {
			BackgroundMap->Load(&Stats->Maps.at(MapStat->BackgroundMapID), true);
		}
		catch(std::exception &Error) {
			delete BackgroundMap;
			BackgroundMap = nullptr;
		}
	}

	// Load tiles
	_Tile *Tile = nullptr;
	_Object *Object = nullptr;
	glm::ivec2 TileCoordinate;
	int TileIndex = 0;
	while(!File.eof() && File.peek() != EOF) {

		// Read chunk type
		char ChunkType;
		File >> ChunkType;

		switch(ChunkType) {
			// Map version
			case 'V': {
				int FileVersion;
				File >> FileVersion;
				if(FileVersion != MAP_VERSION)
					throw std::runtime_error("Level version mismatch: " + std::to_string(FileVersion));
			} break;
			// Map size
			case 'S': {
				File >> Size.x >> Size.y;
				FreeMap();
				AllocateMap();
			} break;
			// Tile
			case 'T': {
				TileCoordinate.x = TileIndex % Size.x;
				TileCoordinate.y = TileIndex / Size.x;
				Tile = &Tiles[TileCoordinate.x][TileCoordinate.y];
				TileIndex++;
			} break;
			// Texture index
			case 'b': {
				if(Tile)
					File >> Tile->TextureIndex[0];
			} break;
			// Foreground texture index
			case 'f': {
				if(Tile)
					File >> Tile->TextureIndex[1];
			} break;
			// Zone
			case 'z': {
				if(Tile)
					File >> Tile->Zone;
			} break;
			// Event
			case 'e': {
				if(Tile) {
					File >> Tile->Event.Type >> Tile->Event.Data;

					// Create static objects for boss events
					if(!Headless && Stats && Tile->Event.Type == EVENT_SCRIPT) {
						const _Script &Script = Stats->Scripts.at(Tile->Event.Data);
						if(Script.Name == "Script_Boss") {
							_Object *BossObject = new _Object();
							BossObject->Position = TileCoordinate;
							BossObject->BossZoneID = (uint32_t)Script.Level;
							if(!Script.Data.empty())
								BossObject->ModelTexture = ae::Assets.Textures.at(Script.Data);
							StaticObjects.push_back(BossObject);
						}
					}
				}
			} break;
			// Wall
			case 'w': {
				if(Tile)
					File >> Tile->Wall;
			} break;
			// PVP
			case 'p': {
				if(Tile)
					File >> Tile->PVP;
			} break;
			// Object
			case 'O': {
				glm::ivec2 Coordinate;
				File >> Coordinate.x >> Coordinate.y;
				if(!Headless) {
					Object = new _Object();
					Object->Position = Coordinate;
					StaticObjects.push_back(Object);
				}
				else
					Object = nullptr;
			} break;
			// Object light
			case 'l': {
				if(Object)
					File >> Object->Light;
			} break;
			default:
				File.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			break;
		}
	}

	File.close();

	// Index events
	IndexEvents();

	// Initialize 2d tile rendering
	if(!Headless)
		InitAtlas(MapStat->Atlas, Static);

	// Initialize path finding
	Pather = new micropather::MicroPather(this, (unsigned)(Size.x * Size.y), 4);
}

// Saves the level to a file
bool _Map::Save(const std::string &Path) {
	if(Path == "")
		return false;

	// Open file
	gzofstream Output(Path.c_str());
	if(!Output)
		throw std::runtime_error("Cannot create file: " + Path);

	// Header
	Output << "V " << MAP_VERSION << '\n';
	Output << "S " << Size.x << " " << Size.y << '\n';

	// Write tile map
	for(int j = 0; j < Size.y; j++) {
		for(int i = 0; i < Size.x; i++) {
			const _Tile &Tile = Tiles[i][j];
			Output << "T" << '\n';
			if(Tile.TextureIndex[0])
				Output << "b " << Tile.TextureIndex[0] << '\n';
			if(Tile.TextureIndex[1])
				Output << "f " << Tile.TextureIndex[1] << '\n';
			if(Tile.Zone)
				Output << "z " << Tile.Zone << '\n';
			if(Tile.Event.Type)
				Output << "e " << Tile.Event.Type << " " << Tiles[i][j].Event.Data << '\n';
			if(Tile.Wall)
				Output << "w " << Tile.Wall << '\n';
			if(Tile.PVP)
				Output << "p " << Tile.PVP << '\n';
		}
	}

	// Write static objects
	for(auto &Object : StaticObjects) {
		if(!Object->Light)
			continue;

		Output << "O " << Object->Position.x << ' ' << Object->Position.y << '\n';
		Output << "l " << Object->Light << '\n';
	}

	Output.close();

	return true;
}

// Determines if a square can be moved to
bool _Map::CanMoveTo(const glm::ivec2 &Position, _Object *Object) {

	// Check bounds
	if(Position.x < 0 || Position.x >= Size.x || Position.y < 0 || Position.y >= Size.y)
		return false;

	// Check events
	const _Tile *Tile = &Tiles[Position.x][Position.y];
	switch(Tile->Event.Type) {
		case _Map::EVENT_KEY: {

			// Search for item in key chain
			if(Object->Inventory->GetBag(BagType::KEYS).HasItemID(Tile->Event.Data) != NOSLOT)
				return true;

			// Search for item in equipment
			if(Object->Inventory->GetBag(BagType::EQUIPMENT).HasItemID(Tile->Event.Data) != NOSLOT)
				return true;

			// Automatically add key to keychain on use if found in inventory bag
			size_t FoundIndex = Object->Inventory->GetBag(BagType::INVENTORY).HasItemID(Tile->Event.Data);
			if(FoundIndex != NOSLOT) {

				// Check for client
				if(!Server) {

					// Only add keys
					try {
						const _Item *Item = Stats->Items.at(Tile->Event.Data);
						if(Item->Type == ItemType::KEY) {

							// Send use command
							_Slot Slot(BagType::INVENTORY, FoundIndex);
							ae::_Buffer Packet;
							Packet.Write<PacketType>(PacketType::INVENTORY_USE);
							Packet.WriteBit(0);
							Slot.Serialize(Packet);
							PlayState.Network->SendPacket(Packet);

							// Add message
							if(!Server)
								HUD.AddChatMessage(_Message("Used " + Item->Name, ae::Assets.Colors["yellow"], PlayState.Time));
						}
					}
					catch(std::exception &Error) {
					}
				}

				return true;
			}

			// Set message for client
			if(!Server) {
				const _Item *Item = Object->Stats->Items.at(Tile->Event.Data);
				if(Item && Item->Name.size())
					HUD.SetMessage("You need the " + Item->Name, HUD_MESSAGE_SHORT_TIMEOUT);
			}

			return Object->Character->GhostMode;
		} break;
		case _Map::EVENT_TERRAIN: {
			if(!Tile->Event.Data)
				break;

			if(Stats->Terrains.find(Tile->Event.Data) == Stats->Terrains.end())
				break;

			const _Terrain &Terrain = Stats->Terrains.at(Tile->Event.Data);
			if(!Object->Character->Attributes[Terrain.Attribute].Int) {
				if(!Server)
					HUD.SetMessage("You need " + Stats->Attributes.at(Terrain.Attribute).Label + " to pass " + Terrain.Name, HUD_MESSAGE_SHORT_TIMEOUT);

				return Object->Character->GhostMode;
			}

			return true;
		} break;
	}

	return Object->Character->GhostMode || !Tile->Wall;
}

// Removes an object from the map
void _Map::RemoveObject(const _Object *RemoveObject) {

	// Notify peers
	if(Server) {

		// Create packet
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::WORLD_DELETEOBJECT);
		Packet.Write<ae::NetworkIDType>(RemoveObject->NetworkID);

		// Send to everyone
		BroadcastPacket(Packet);
	}

	// Remove object
	auto Iterator = std::find(Objects.begin(), Objects.end(), RemoveObject);
	if(Iterator != Objects.end())
		Objects.erase(Iterator);
}

// Adds an object to the map
void _Map::AddObject(_Object *Object) {
	if(Server) {

		// Create packet for the new object
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::WORLD_CREATEOBJECT);
		Object->SerializeCreate(Packet);

		// Notify other players of the new object
		BroadcastPacket(Packet);
	}

	// Add object to map
	Objects.push_back(Object);
}

// Returns a list of players close to a player that can battle
void _Map::GetPotentialBattlePlayers(const _Object *Player, float DistanceSquared, size_t Max, bool Manual, std::vector<_Object *> &Players) const {

	// Players must be standing on manually triggered boss fights
	if(Manual)
		DistanceSquared = 0.0f;

	// Search objects
	bool HitLevelRestriction = false;
	for(const auto &Object : Objects) {

		// Check online state
		if(!Object->Peer)
			continue;

		// Check interaction
		if(!Player->CanInteractWith(Object, true, HitLevelRestriction))
			continue;

		// Check party name
		if(Player->Character->PartyName != Object->Character->PartyName)
			continue;

		// Check if player can battle
		if(!Object->Character->CanBattle(Manual))
			continue;

		// Check distance
		glm::vec2 Delta = Object->Position - Player->Position;
		if(glm::dot(Delta, Delta) <= DistanceSquared) {
			Players.push_back(Object);
			if(Players.size() >= Max)
				return;
		}
	}
}

// Returns a battle instance close to a player
_Battle *_Map::GetCloseBattle(const _Object *Player, bool &HitPrivateParty, bool &HitFullBattle, bool &HitLevelRestriction, bool &HitBossBattle) {

	// Search objects
	for(const auto &Object : Objects) {

		// Check for battle
		if(!Object->Battle)
			continue;

		// Check for alive
		if(!Object->Character->IsAlive())
			continue;

		// Can't join PVP battle
		if(Object->Battle->PVP)
			continue;

		// Compare distance
		glm::vec2 Delta = Object->Position - Player->Position;
		if(glm::dot(Delta, Delta) > BATTLE_JOIN_DISTANCE)
			continue;

		// Check interaction
		if(!Player->CanInteractWith(Object, true, HitLevelRestriction))
			continue;

		// Can't join boss battles
		if(Object->Battle->Boss) {
			HitBossBattle = true;
			continue;
		}

		if(Object->Battle->SideCount[0] >= BATTLE_MAX_OBJECTS_PER_SIDE) {
			HitFullBattle = true;
			continue;
		}

		if(Object->Character->PartyName == "" || Object->Character->PartyName == Player->Character->PartyName)
			return Object->Battle;
		else
			HitPrivateParty = true;
	}

	return nullptr;
}

// Returns target players appropriate for pvp
void _Map::GetPVPPlayers(const _Object *Attacker, std::vector<_Object *> &Players, bool UsePVPZone) {

	// Attacker must be in PVP zone
	if(UsePVPZone && !IsPVPZone(Attacker->Position))
		return;

	bool HitLevelRestriction = false;
	for(const auto &Object : Objects) {

		// Check interaction
		if(!Attacker->CanInteractWith(Object, true, HitLevelRestriction))
			continue;

		// Check if target can PVP
		if(!Object->Character->CanPVP())
			continue;

		// Target must be in PVP zone
		if(UsePVPZone && !IsPVPZone(Object->Position))
			continue;

		// Can only bounty hunt players with a bounty
		if(!UsePVPZone && !Object->Character->Attributes["Bounty"].Int)
			continue;

		// Can't attack same party member
		if(Object->Character->PartyName != "" && Object->Character->PartyName == Attacker->Character->PartyName)
			continue;

		// Check distance
		glm::vec2 Delta = Object->Position - Attacker->Position;
		if(glm::dot(Delta, Delta) <= BATTLE_PVP_DISTANCE)
			Players.push_back(Object);
	}
}

// Returns the closest player
_Object *_Map::FindTradePlayer(const _Object *Player, float MaxDistanceSquared) {

	_Object *ClosestPlayer = nullptr;
	float ClosestDistanceSquared = HUGE_VAL;
	bool HitLevelRestriction = false;
	for(const auto &Object : Objects) {

		// Check online state
		if(!Object->Peer)
			continue;

		// Check interaction
		if(!Player->CanInteractWith(Object, true, HitLevelRestriction))
			continue;

		// Check charisma
		if(Player->Character->Attributes["EternalCharisma"].Int != Object->Character->Attributes["EternalCharisma"].Int)
			continue;

		if(!Object->Character->WaitingForTrade)
			continue;

		if(Object->Character->TradePlayer)
			continue;

		glm::vec2 Delta = Object->Position - Player->Position;
		float DistanceSquared = glm::dot(Delta, Delta);
		if(DistanceSquared <= MaxDistanceSquared && DistanceSquared < ClosestDistanceSquared) {
			ClosestDistanceSquared = DistanceSquared;
			ClosestPlayer = Object;
		}
	}

	return ClosestPlayer;
}

// Find a player that's dead in the world
_Object *_Map::FindDeadPlayer(const _Object *Player, float MaxDistanceSquared) {

	_Object *ClosestPlayer = nullptr;
	float ClosestDistanceSquared = HUGE_VAL;
	bool HitLevelRestriction = false;
	for(const auto &Object : Objects) {

		// Check interaction
		if(!Player->CanInteractWith(Object, false, HitLevelRestriction))
			continue;

		if(Object->Character->IsAlive())
			continue;

		if(Object->Character->Hardcore)
			continue;

		if(Object->Battle)
			continue;

		glm::vec2 Delta = Object->Position - Player->Position;
		float DistanceSquared = glm::dot(Delta, Delta);
		if(DistanceSquared <= MaxDistanceSquared && DistanceSquared < ClosestDistanceSquared) {
			ClosestDistanceSquared = DistanceSquared;
			ClosestPlayer = Object;
		}
	}

	return ClosestPlayer;
}

// Find closest event on the map, returns true on found
bool _Map::FindEvent(const _Event &Event, glm::ivec2 &Position) const {

	// Find event
	auto Iterator = IndexedEvents.find(Event);
	if(Iterator == IndexedEvents.end())
		return false;

	// Return closest position
	glm::ivec2 StartPosition = Position;
	float ClosestDistanceSquared = HUGE_VAL;
	for(const auto &CheckPosition : Iterator->second) {
		glm::vec2 Delta = StartPosition - CheckPosition;
		float DistanceSquared = glm::dot(Delta, Delta);
		if(DistanceSquared < ClosestDistanceSquared) {
			ClosestDistanceSquared = DistanceSquared;
			Position = CheckPosition;
		}
	}

	return true;
}

// Delete static objects at the position
void _Map::DeleteLight(const glm::ivec2 &Position) {

	// Update objects
	for(auto Iterator = StaticObjects.begin(); Iterator != StaticObjects.end(); ) {
		_Object *Object = *Iterator;
		if(Object->Light && Position == Object->Position) {
			Iterator = StaticObjects.erase(Iterator);
			delete Object;
		}
		else {
			++Iterator;
		}
	}
}

// Send complete object list to player
void _Map::SendObjectList(ae::_Peer *Peer) {
	if(!Server)
		return;

	if(!Peer->Object)
		return;

	// Create packet
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::WORLD_OBJECTLIST);
	Packet.Write<ae::NetworkIDType>(Peer->Object->NetworkID);

	// Write object data
	Packet.Write<ae::NetworkIDType>((ae::NetworkIDType)Objects.size());
	for(auto &Object : Objects)
		Object->SerializeCreate(Packet);

	Server->Network->SendPacket(Packet, Peer);
}

// Sends object position information to all the clients in the map
void _Map::SendObjectUpdates() {

	// See if at least one object was changed
	bool Changed = false;
	for(auto &Object : Objects) {
		if(Object->Changed) {
			Object->Changed = false;
			Changed = true;
		}
	}

	// Increment update id
	if(Changed)
		UpdateID++;

	// Create packet
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::WORLD_OBJECTUPDATES);
	Packet.Write<uint8_t>(UpdateID);
	Packet.Write<uint8_t>((uint8_t)NetworkID);

	// Write object count
	Packet.Write<ae::NetworkIDType>((ae::NetworkIDType)Objects.size());

	// Iterate over objects
	for(const auto &Object : Objects) {
		Object->SerializeUpdate(Packet);
	}

	// Send packet to players in map
	for(auto &Object : Objects) {
		if(!Object->Deleted && Object->Peer && Object->Peer->ENetPeer && Object->UpdateID != UpdateID) {
			Server->Network->SendPacket(Packet, Object->Peer, ae::_Network::SEND_UNSEQUENCED, 1);
		}
	}
}

// Broadcast a packet to all peers in the map
void _Map::BroadcastPacket(ae::_Buffer &Buffer, ae::_Network::SendType Type) {
	if(!Server)
		return;

	// Send packet to peers
	for(auto &Object : Objects) {
		if(!Object->Deleted && Object->Peer && Object->Peer->ENetPeer)
			Server->Network->SendPacket(Buffer, Object->Peer, Type, Type == ae::_Network::SEND_UNSEQUENCED);
	}
}

// Get a valid position within the grid
glm::vec2 _Map::GetValidPosition(const glm::vec2 &Position) const {
	return glm::clamp(Position, glm::vec2(0.0f), glm::vec2(Size));
}

// Get a valid position within the grid
glm::ivec2 _Map::GetValidCoord(const glm::ivec2 &Position) const {
	return glm::clamp(Position, glm::ivec2(0), Size - 1);
}

// Distance between two points
float _Map::LeastCostEstimate(void *StateStart, void *StateEnd) {
	glm::ivec2 StartPosition;
	NodeToPosition(StateStart, StartPosition);

	glm::ivec2 EndPosition;
	NodeToPosition(StateStart, EndPosition);

	return std::abs(StartPosition.x - EndPosition.x) + std::abs(StartPosition.y - EndPosition.y);
}

// Generate successors from a state
void _Map::AdjacentCost(void *State, std::vector<micropather::StateCost> *Neighbors) {
	glm::ivec2 Position;
	NodeToPosition(State, Position);

	glm::ivec2 Directions[4] = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };
	for(int i = 0; i < 4; i++) {
		glm::ivec2 NewPosition = Position + Directions[i];
		const _Tile &Tile = Tiles[NewPosition.x][NewPosition.y];
		float Cost = (Tile.Wall || Tile.Event.Type == EVENT_TERRAIN) ? FLT_MAX : 1.0f;
		micropather::StateCost NodeCost = { PositionToNode(NewPosition), Cost };
		Neighbors->push_back(NodeCost);
	}
}
