/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <objects/object.h>
#include <ae/assets.h>
#include <ae/font.h>
#include <ae/graphics.h>
#include <ae/input.h>
#include <ae/manager.h>
#include <ae/peer.h>
#include <ae/program.h>
#include <ae/random.h>
#include <ae/servernetwork.h>
#include <ae/texture.h>
#include <ae/ui.h>
#include <ae/util.h>
#include <hud/hud.h>
#include <objects/components/character.h>
#include <objects/components/controller.h>
#include <objects/components/fighter.h>
#include <objects/components/inventory.h>
#include <objects/components/monster.h>
#include <objects/battle.h>
#include <objects/buff.h>
#include <objects/item.h>
#include <objects/map.h>
#include <objects/status_effect.h>
#include <config.h>
#include <packet.h>
#include <save.h>
#include <scripting.h>
#include <server.h>
#include <stats.h>
#include <glm/gtc/type_ptr.hpp>
#include <json/reader.h>
#include <SDL_keycode.h>
#include <algorithm>
#include <iomanip>

// Destructor
_Object::~_Object() {
	if(Fighter)
		Fighter->RemoveBattleElement();

	if(Map) {
		Map->RemoveObject(this);
		Map = nullptr;
	}

	if(Character) {
		if(Battle) {
			Battle->RemoveObject(this);
			Battle = nullptr;
		}

		if(Character->Bot) {
			delete Peer;
			Peer = nullptr;
		}
	}

	if(Server)
		Server->RemoveObject(this);

	delete Monster;
	delete Controller;
	delete Fighter;
	delete Character;
	delete Inventory;
}

// Create object components
void _Object::CreateComponents() {
	Inventory = new _Inventory(this);
	Character = new _Character(this);
	Fighter = new _Fighter(this);
	Controller = new _Controller(this);
	Monster = new _Monster(this);
}

// Updates the player
void _Object::Update(double FrameTime) {

	// Update time client has waited for server
	if(!Server && Controller->WaitForServer && Character->TeleportTime == 0.0)
		Controller->WaitTime += FrameTime;
	else
		Controller->WaitTime = 0.0;

	// Update bots
	if(Server && Character->Bot)
		UpdateBot(FrameTime);

	// Update player position
	Controller->DirectionMoved = Move();

	// Handle move
	int CheckEvent = 0;
	if(Controller->DirectionMoved) {
		if(Server && Character->OnTrigger)
			Server->UpdateEventCount();
		if(!Server)
			HUD.UseText = "";

		Character->OnTrigger = false;
		Character->TriggerCount = 0;
		CheckEvent = 1;

		// Remove node from pathfinding
		if(Character->Bot && Character->Path.size())
			Character->Path.erase(Character->Path.begin());
	}

	// Update actions and battle
	if(Character->IsAlive()) {

		// Update monster AI
		if(Server && Server->AI && Battle && IsMonster())
			UpdateMonsterAI(FrameTime);

		// Check turn timer
		if(Battle) {
			if(Character->Attributes["Stunned"].Int)
				Fighter->TurnTimer += FrameTime * (1.0 / BATTLE_DEFAULTATTACKPERIOD) * BATTLE_STUNNED_BATTLESPEED * 0.01;
			else
				Fighter->TurnTimer += FrameTime * (1.0 / Character->BaseAttackPeriod) * Character->Attributes["BattleSpeed"].Mult();
		}
		else
			Fighter->TurnTimer = 1.0;

		// Resolve action
		if(Fighter->TurnTimer >= 1.0) {
			Fighter->TurnTimer = 1.0;

			if(Server && !Character->Attributes["Stunned"].Int && Character->Action.Item) {
				ScopeType Scope = ScopeType::WORLD;
				if(Battle)
					Scope = ScopeType::BATTLE;

				ae::_Buffer Packet;
				if(Character->Action.Resolve(Packet, this, Scope))
					SendPacket(Packet);
				else
					SendActionClear();

				Character->Action.Clear();
			}
		}
	}
	else
		Fighter->TurnTimer = 0.0;

	// Update status effects
	if(Character) {
		Character->Update(FrameTime);

		// Update playtime
		if(!Offline) {
			Character->Attributes["PlayTime"].Double += FrameTime;
			if(Character->Attributes["Rebirths"].Int || Character->Attributes["Evolves"].Int || Character->Attributes["Transforms"].Int)
				Character->Attributes["RebirthTime"].Double += FrameTime;
			if(Character->Attributes["Evolves"].Int || Character->Attributes["Transforms"].Int)
				Character->Attributes["EvolveTime"].Double += FrameTime;
			if(Character->Attributes["Transforms"].Int)
				Character->Attributes["TransformTime"].Double += FrameTime;
			if(Battle)
				Character->Attributes["BattleTime"].Double += FrameTime;
		}

		if(Server)
			Character->IdleTime += FrameTime;
	}

	// Update teleport time
	if(Character->TeleportTime > 0.0) {
		Character->Status = _Character::STATUS_TELEPORT;
		Character->TeleportTime -= FrameTime;

		// Handle spawn
		if(Character->TeleportTime <= 0.0) {
			Character->TeleportTime = 0.0;
			if(Server)
				Server->SpawnPlayer(this, Character->SpawnMapID, _Map::EVENT_SPAWN);
		}
	}

	// Update timers
	Controller->MoveTime += FrameTime;

	// Player hit the use button
	if(Controller->UseCommand) {
		CheckEvent = 2;
		Controller->UseCommand = false;
	}

	// Check events
	if(Map && CheckEvent)
		Map->CheckEvents(this, CheckEvent);

	if(Server) {

		// Update status
		if(Character && !Monster->DatabaseID)
			Character->Status = Character->GetStatus();

		// Test for changed state
		if(OldOffline != Offline)
			Changed = true;
		if(Position != OldPosition)
			Changed = true;
		if(Character->Status != OldStatus)
			Changed = true;
		if(OldInvisible != Character->Invisible)
			Changed = true;
		if(Light != OldLight)
			Changed = true;
		if(Character->Attributes["Bounty"].Int != OldBounty)
			Changed = true;

		// Save old state
		OldPosition = Position;
		OldStatus = Character->Status;
		OldInvisible = Character->Invisible;
		OldBounty = Character->Attributes["Bounty"].Int;
		OldLight = Light;
		OldOffline = Offline;
	}
}

// Update bot AI
void _Object::UpdateBot(double FrameTime) {

	// Call ai script
	if(!Battle && Scripting->StartMethodCall("Bot_Server", "Update")) {
		Scripting->PushReal(FrameTime);
		Scripting->PushObject(this);
		Scripting->MethodCall(2, 0);
		Scripting->FinishMethodCall();
	}

	// Set input
	if(Character->AcceptingMoveInput()) {
		Character->IdleTime = 0.0;
		int InputState = 0;

		// Call ai input script
		if(Scripting->StartMethodCall("Bot_Server", "GetInputState")) {
			Scripting->PushObject(this);
			Scripting->MethodCall(1, 1);
			InputState = Scripting->GetInt(1);
			Scripting->FinishMethodCall();
		}

		Controller->InputStates.clear();
		if(InputState)
			Controller->InputStates.push_back(InputState);
	}

	// Update battle
	if(Battle) {
		if(Fighter->TurnTimer >= 1.0 && !Character->Action.Item) {

			// Set skill used
			size_t ActionBarIndex = 0;
			if(!Character->GetActionFromActionBar(Character->Action, ActionBarIndex)) {
				return;
			}

			// Check that the action can be used
			_ActionResult ActionResult;
			ActionResult.Source.Object = this;
			ActionResult.Scope = ScopeType::BATTLE;
			ActionResult.ActionUsed = Character->Action;
			if(!Character->Action.Item->CanUse(Scripting, ActionResult))
				Character->Action.Item = nullptr;

			// Separate object list
			std::vector<_Object *> Allies, Enemies;
			Allies.reserve(BATTLE_MAX_OBJECTS_PER_SIDE);
			Enemies.reserve(BATTLE_MAX_OBJECTS_PER_SIDE);
			Battle->GetSeparateObjectList(Fighter->BattleSide, Allies, Enemies);

			// Call lua script
			if(Enemies.size()) {
				if(Scripting->StartMethodCall("AI_Smart", "Update")) {
					Scripting->PushObject(this);
					Scripting->PushObjectList(Enemies);
					Scripting->PushObjectList(Allies);
					Scripting->MethodCall(3, 0);
					Scripting->FinishMethodCall();
				}
			}
		}
	}
}

// Update monster AI during battle
void _Object::UpdateMonsterAI(double FrameTime) {
	if(!Monster->AI.length())
		return;

	// Call AI script to get action
	if(Fighter->TurnTimer >= 1.0 && !Character->Action.Item) {

		// Separate object list
		std::vector<_Object *> Allies, Enemies;
		Allies.reserve(BATTLE_MAX_OBJECTS_PER_SIDE);
		Enemies.reserve(BATTLE_MAX_OBJECTS_PER_SIDE);
		Battle->GetSeparateObjectList(Fighter->BattleSide, Allies, Enemies);

		// Call lua script
		if(Scripting->StartMethodCall(Monster->AI, "Update")) {
			Scripting->PushObject(this);
			Scripting->PushObjectList(Enemies);
			Scripting->PushObjectList(Allies);
			Scripting->PushReal(Battle->Time);
			Scripting->MethodCall(4, 0);
			Scripting->FinishMethodCall();
		}
	}
}

// Renders the player while walking around the world
void _Object::Render(glm::vec4 &ViewBounds, const _Object *ClientPlayer) {
	if(!ModelTexture)
		return;

	// Check view bounds with padding for object name
	if(ViewBounds[2] < Position.x - 2 || ViewBounds[0] > Position.x + 3)
		return;
	if(ViewBounds[3] < Position.y - 2 || ViewBounds[1] > Position.y + 3)
		return;

	// Setup shader
	ae::Graphics.SetProgram(ae::Assets.Programs["map"]);
	glUniformMatrix4fv(ae::Assets.Programs["map"]->TextureTransformID, 1, GL_FALSE, glm::value_ptr(glm::mat4(1)));
	glUniformMatrix4fv(ae::Assets.Programs["map"]->ModelTransformID, 1, GL_FALSE, glm::value_ptr(glm::mat4(1)));

	// Draw debug server position
	glm::vec3 DrawPosition;
	if(Character && HUD.ShowDebug) {
		DrawPosition = glm::vec3(ServerPosition, 0.0f) + glm::vec3(0.5f, 0.5f, 0);
		ae::Graphics.SetColor(glm::vec4(1, 0, 0, 1));
		ae::Graphics.DrawSprite(DrawPosition, ModelTexture);
	}

	// Set alpha
	float Alpha = 1.0f;
	if(Character && Character->Invisible)
		Alpha *= PLAYER_INVIS_ALPHA;
	if(Offline)
		Alpha *= PLAYER_OFFLINE_ALPHA;

	// Get position and color
	DrawPosition = glm::vec3(Position, 0.0f) + glm::vec3(0.5f, 0.5f, 0);
	ae::Graphics.SetColor(glm::vec4(1.0f, 1.0f, 1.0f, Alpha));

	// Get model texture
	const ae::_Texture *DrawTexture = ModelTexture;
	if(BossZoneID && ClientPlayer && ClientPlayer->Character->IsZoneOnCooldown(BossZoneID))
		DrawTexture = ae::Assets.Textures["textures/objects/dead_boss.webp"];

	// Draw model
	ae::Graphics.DrawSprite(DrawPosition, DrawTexture);
	if(!Character)
		return;

	// Draw status
	if(Character->StatusTexture)
		ae::Graphics.DrawSprite(DrawPosition, Character->StatusTexture);

	// Draw offline status
	if(Offline)
		ae::Graphics.DrawSprite(DrawPosition, ae::Assets.Textures["textures/status/offline.webp"]);

	// Don't draw name when invisible
	if(Character->Invisible)
		return;

	// Get name color
	std::string Color;
	if(this == ClientPlayer) {
		Color = "green";
	}
	else {
		if(Offline)
			Color = "offline";
		else if(ClientPlayer->Character->PartyName != "" && ClientPlayer->Character->PartyName == Character->PartyName)
			Color = "light_yellow";
		else
			Color = "white";
	}

	// Set prefix
	std::stringstream Buffer;
	_HUD::GetPlayerPrefix(Buffer, Character->Attributes["Rebirths"].Int, Character->Attributes["Evolves"].Int, Character->Attributes["Transforms"].Int);

	// Set color
	Buffer << "[c " << Color << "]" << Name << "[c white]";

	// Add bounty text
	if(Character->Attributes["Bounty"].Int > 0) {
		Buffer << " ([c cyan]";
		ae::FormatSI<int64_t>(Buffer, Character->Attributes["Bounty"].Int);
		Buffer << "[c white])";
	}

	// Accumulate object names on tile
	Map->ObjectNames[Map->PositionToIndex(Position)].push_back(Buffer.str());
}

// Renders the object during a battle
void _Object::RenderBattle(_Object *ClientPlayer, double Time, bool ShowLevel) {
	std::stringstream Buffer;

	// Set color
	glm::vec4 GlobalColor(glm::vec4(1.0f));
	GlobalColor.a = 1.0f;
	if(!Character->IsAlive())
		GlobalColor.a = 0.2f;
	Fighter->BattleElement->Fade = GlobalColor.a;

	// Set slot style
	const ae::_Texture *HealthBarEmpty;
	const ae::_Texture *HealthBarFull;
	const ae::_Texture *ManaBarEmpty;
	const ae::_Texture *ManaBarFull;
	const ae::_Texture *StaminaBarEmpty;
	const ae::_Texture *StaminaBarFull;
	glm::vec4 PortraitBackgroundColor;
	glm::vec4 PortraitOutlineColor;
	if(Fighter->Corpse <= 0) {
		PortraitBackgroundColor = ae::Assets.Colors["gray_alpha"];
		PortraitOutlineColor = ae::Assets.Colors["gray_outline"];
		HealthBarEmpty = ae::Assets.Textures["textures/hud_repeat/dead_empty.webp"];
		HealthBarFull = ae::Assets.Textures["textures/hud_repeat/dead_full.webp"];
		ManaBarEmpty = ae::Assets.Textures["textures/hud_repeat/dead_empty.webp"];
		ManaBarFull = ae::Assets.Textures["textures/hud_repeat/dead_empty.webp"];
		StaminaBarEmpty = ae::Assets.Textures["textures/hud_repeat/dead_empty.webp"];
		StaminaBarFull = ae::Assets.Textures["textures/hud_repeat/dead_empty.webp"];
	}
	else {
		PortraitBackgroundColor = (Fighter->BattleSide == 0) ? ae::Assets.Colors["green_alpha"] : ae::Assets.Colors["red_alpha"];
		PortraitOutlineColor = (Fighter->BattleSide == 0) ? ae::Assets.Colors["green"] : ae::Assets.Colors["red"];
		HealthBarEmpty = ae::Assets.Textures["textures/hud_repeat/health_empty.webp"];
		HealthBarFull = ae::Assets.Textures["textures/hud_repeat/health_full.webp"];
		ManaBarEmpty = ae::Assets.Textures["textures/hud_repeat/mana_empty.webp"];
		ManaBarFull = ae::Assets.Textures["textures/hud_repeat/mana_full.webp"];
		StaminaBarEmpty = ae::Assets.Textures["textures/hud_repeat/stamina_empty.webp"];
		StaminaBarFull = ae::Assets.Textures["textures/hud_repeat/stamina_full.webp"];
	}
	PortraitBackgroundColor.a *= GlobalColor.a;
	PortraitOutlineColor.a *= GlobalColor.a;

	// Get background for items used
	const ae::_Texture *ItemBackTexture = ae::Assets.Textures["textures/hud/item_back.webp"];

	// Get slot center
	glm::vec2 SlotPosition = Fighter->BattleElement->Bounds.Start;

	// Get health/mana bar positions
	glm::vec2 BarSize = glm::vec2(BATTLE_HEALTHBAR_WIDTH, BATTLE_HEALTHBAR_HEIGHT) * ae::_Element::GetUIScale();
	glm::vec2 BarOffset((10 + UI_PORTRAIT_SIZE.x) * ae::_Element::GetUIScale(), 0);
	float BarPaddingY = 6 * ae::_Element::GetUIScale();

	// Get bar element bounds
	ae::_Bounds BarBounds;
	BarBounds.Start = SlotPosition + glm::vec2(0, 0) + BarOffset;
	BarBounds.End = SlotPosition + glm::vec2(BarSize.x, BarSize.y) + BarOffset;
	glm::vec2 BarCenter = (BarBounds.Start + BarBounds.End) / 2.0f;
	float BarEndX = BarBounds.End.x;

	// Save positions
	Fighter->ResultPosition = Fighter->BattleElement->Bounds.Start + UI_PORTRAIT_SIZE * ae::_Element::GetUIScale() * 0.5f;
	Fighter->StatPosition = Fighter->ResultPosition + glm::vec2((Character->Portrait->Size.x/2 + 10 + BATTLE_HEALTHBAR_WIDTH/2), 0) * ae::_Element::GetUIScale();

	// Get highlight bounds
	ae::_Bounds HighlightBounds;
	HighlightBounds.Start = Fighter->BattleElement->Bounds.Start;
	HighlightBounds.End = glm::vec2(BarBounds.End.x, Fighter->BattleElement->Bounds.End.y);

	// Draw potential action to use
	auto &Action = ClientPlayer->Character->Action;
	if(Action.Unconfirmed()) {
		auto &Targets = ClientPlayer->Battle->ClientTargets;
		for(auto &Target : Targets) {
			if(Target != this)
				continue;

			// Highlight
			ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos"]);
			ae::Graphics.SetColor(glm::vec4(0.5, 0.5, 0.5, 0.25));
			ae::Graphics.DrawRectangle(HighlightBounds, true);

			// Make icon flash
			glm::vec4 Color(glm::vec4(1.0f));
			double FastTime = Time * 2;
			if(FastTime - (int)FastTime < 0.5)
				Color.a = 0.75f;

			// Draw background icon
			ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos_uv"]);
			glm::vec2 DrawPosition = glm::ivec2(BarEndX + 12 * ae::_Element::GetUIScale(), SlotPosition.y + Fighter->BattleElement->Size.y/2);
			if(Action.Item && !Action.Item->IsSkill()) {
				DrawPosition.x += UI_SLOT_SIZE.x/2 * ae::_Element::GetUIScale();
				ae::Graphics.DrawScaledImage(DrawPosition, ItemBackTexture, UI_SLOT_SIZE, Color);
			}
			else
				DrawPosition.x += UI_SLOT_SIZE.x/2 * ae::_Element::GetUIScale();

			// Draw item
			ae::Graphics.DrawScaledImage(DrawPosition, Action.Item->Texture,  UI_SLOT_SIZE, Color);

			// Draw target index
			if((Fighter->TargetIndex == 1 && Action.GetTargetType() == TargetType::ENEMY_CORPSE_AOE) || (Targets.size() > 1 && Action.Item->Attributes.at("BuffPriority").Int)) {
				glm::vec4 Color = Fighter->TargetIndex == 1 ? ae::Assets.Colors["light_green"] : ae::Assets.Colors["darker_green"];
				ae::Assets.Fonts["hud_small"]->DrawText(std::to_string(Fighter->TargetIndex), DrawPosition + glm::vec2(-28, 28) * ae::_Element::GetUIScale(), ae::LEFT_BASELINE, Color);
			}
		}
	}

	// Set color
	ae::Graphics.SetColor(GlobalColor);

	// Name
	if(!Character->Invisible) {
		Buffer << Name;
		if(ShowLevel && !Monster->DatabaseID)
			Buffer << " (" << Character->Level << ")" << std::endl;

		// Get name color
		glm::vec4 NameColor;
		if(ClientPlayer == this)
			NameColor = ae::Assets.Colors["green"];
		else if(Character->PartyName.size() && Character->PartyName == ClientPlayer->Character->PartyName)
			NameColor = ae::Assets.Colors["light_yellow"];
		else
			NameColor = glm::vec4(1.0f);
		NameColor.a *= GlobalColor.a;

		// Draw name
		ae::Assets.Fonts["hud_medium"]->DrawText(Buffer.str(), SlotPosition + glm::vec2(0, -12) * ae::_Element::GetUIScale(), ae::LEFT_BASELINE, NameColor);
		Buffer.str("");
	}

	// Portrait
	if(Character->Portrait) {
		ae::_Bounds PortraitBounds(glm::ivec2(SlotPosition), glm::ivec2(SlotPosition + UI_PORTRAIT_SIZE * ae::_Element::GetUIScale()));
		ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos"]);
		ae::Graphics.SetColor(PortraitBackgroundColor);
		ae::Graphics.DrawRectangle(PortraitBounds, true);
		ae::Graphics.SetColor(PortraitOutlineColor);
		ae::Graphics.DrawRectangle(PortraitBounds, false);
		ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos_uv"]);
		ae::Graphics.DrawScaledImage(SlotPosition + UI_PORTRAIT_SIZE * 0.5f * ae::_Element::GetUIScale(), Character->Portrait, UI_PORTRAIT_SIZE, GlobalColor);
	}

	// Draw empty bar
	ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos_uv"]);
	ae::Graphics.DrawImage(BarBounds, HealthBarEmpty);

	// Draw full bar
	BarBounds.End = SlotPosition + glm::vec2(BarSize.x * Character->GetHealthPercent(), BarSize.y) + BarOffset;
	ae::Graphics.DrawImage(BarBounds, HealthBarFull);

	// Draw health text
	Buffer << std::fixed << std::setprecision(2);
	ae::_Font *DisplayFont = Character->FormatCurrentMax(Buffer, "Health");
	int TextOffsetY = (DisplayFont->MaxAbove - DisplayFont->MaxBelow) / 2 + (int)(2.5 * ae::_Element::GetUIScale());
	DisplayFont->DrawText(Buffer.str(), glm::ivec2(BarCenter) + glm::ivec2(0, TextOffsetY), ae::CENTER_BASELINE, GlobalColor);
	Buffer.str("");

	// Draw mana
	if(Character->Attributes["MaxMana"].Int > 0) {
		float ManaPercent = Character->Attributes["MaxMana"].Int > 0 ? Character->Attributes["Mana"].Int / (float)Character->Attributes["MaxMana"].Int : 0;

		// Get ui size
		BarOffset.y += BarSize.y + BarPaddingY;
		BarBounds.Start = SlotPosition + glm::vec2(0, 0) + BarOffset;
		BarBounds.End = SlotPosition + glm::vec2(BarSize.x, BarSize.y) + BarOffset;
		BarCenter = (BarBounds.Start + BarBounds.End) / 2.0f;

		// Draw empty bar
		ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos_uv"]);
		ae::Graphics.DrawImage(BarBounds, ManaBarEmpty);

		// Draw full bar
		BarBounds.End = SlotPosition + glm::vec2(BarSize.x * ManaPercent, BarSize.y) + BarOffset;
		ae::Graphics.DrawImage(BarBounds, ManaBarFull);

		// Draw mana text
		DisplayFont = Character->FormatCurrentMax(Buffer, "Mana");
		int TextOffsetY = (DisplayFont->MaxAbove - DisplayFont->MaxBelow) / 2 + (int)(2.5 * ae::_Element::GetUIScale());
		DisplayFont->DrawText(Buffer.str(), glm::ivec2(BarCenter) + glm::ivec2(0, TextOffsetY), ae::CENTER_BASELINE, GlobalColor);
		Buffer.str("");
	}

	// Draw turn timer
	BarOffset.y += BarSize.y + BarPaddingY;
	BarSize.y = 8 * ae::_Element::GetUIScale();
	BarBounds.Start = SlotPosition + glm::vec2(0, 0) + BarOffset;
	BarBounds.End = SlotPosition + glm::vec2(BarSize.x, BarSize.y) + BarOffset;

	// Draw empty bar
	ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos_uv"]);
	ae::Graphics.DrawImage(BarBounds, StaminaBarEmpty);

	// Draw full bar
	BarBounds.End = SlotPosition + glm::vec2(BarSize.x * Fighter->TurnTimer, BarSize.y) + BarOffset;
	ae::Graphics.DrawImage(BarBounds, StaminaBarFull);

	// Draw the action used
	bool ActionUsed = (ClientPlayer != this && Character->Action.Item) || (Character->Action.Item && Character->Action.Confirmed);
	if(ClientPlayer->Fighter->BattleSide == Fighter->BattleSide && ActionUsed) {
		glm::vec2 ItemUsingPosition = SlotPosition + glm::vec2((-UI_SLOT_SIZE.x/2 - 16) * ae::_Element::GetUIScale(), Fighter->BattleElement->Size.y/2);
		ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos_uv"]);
		if(!Character->Action.Item->IsSkill())
			ae::Graphics.DrawScaledImage(ItemUsingPosition, ItemBackTexture, UI_SLOT_SIZE, GlobalColor);
		ae::Graphics.DrawScaledImage(ItemUsingPosition, Character->Action.Item->Texture, UI_SLOT_SIZE, GlobalColor);
	}

	// Draw status effects
	glm::vec2 Offset(0, Fighter->BattleElement->BaseSize.y + 2);
	for(auto &Effect : Character->StatusEffects) {
		if(!Effect->BattleElement)
			continue;

		// Hide certain status effects unless holding alt or flashing
		if(Effect->Buff->Hide && !ae::Input.ModKeyDown(KMOD_ALT) && !Effect->ShowWarning())
			continue;

		Effect->BattleElement->BaseOffset = Offset;
		Effect->BattleElement->CalculateBounds();
		Effect->Render(Effect->BattleElement, Time, true);
		Offset.x += UI_BUFF_SIZE.x + 2;
	}
}

// Convert summons buff map to status effects and notify client
void _Object::UpdateSummonStatusEffects(std::map<_SummonKey, int> &Summons) {

	// Handle summon updates
	for(auto &Summon : Summons) {

		// Add or update summon buff
		_StatChange Summons;
		Summons.Object = Summon.first.first;
		Summons.Values["Buff"].Pointer = (void *)Summon.first.second;
		Summons.Values["BuffLevel"].Int = std::max(1, Summon.second);
		Summons.Values["BuffDuration"].Double = -1.0;
		Summons.Object->UpdateStats(Summons, Summons.Object);

		// Sync status effects
		if(Summons.Object->Server && Summons.Object->Peer)
			Summons.Object->Server->SendStatusEffects(Summons.Object);
	}
}

// Serialize attributes for saving
void _Object::SerializeSaveData(Json::Value &Data) const {

	// Write stats
	Json::Value StatsNode;
	StatsNode["Log"] = Logging;
	StatsNode["Hardcore"] = Character->Hardcore;
	StatsNode["MapX"] = Position.x;
	StatsNode["MapY"] = Position.y;
	StatsNode["SpawnMapID"] = Character->SpawnMapID;
	StatsNode["SpawnPoint"] = Character->SpawnPoint;
	StatsNode["BuildID"] = Character->BuildID;
	StatsNode["PortraitID"] = Character->PortraitID;
	StatsNode["ModelID"] = ModelID;
	StatsNode["BeltSize"] = Character->BeltSize;
	StatsNode["SkillBarSize"] = Character->SkillBarSize;
	StatsNode["SkillPointsUnlocked"] = Character->SkillPointsUnlocked;
	StatsNode["NextBattle"] = Character->NextBattle;
	StatsNode["Priority"] = Character->Priority;
	StatsNode["Seed"] = Character->Seed;
	StatsNode["PartyName"] = Character->PartyName.c_str();

	// Save attributes
	for(const auto &Attribute : Stats->Attributes) {
		if(!Attribute.second.Save)
			continue;

		const _Value &AttributeStorage = Character->Attributes.at(Attribute.second.Name);
		switch(Attribute.second.Type) {
			case StatValueType::BOOLEAN:
			case StatValueType::INTEGER:
			case StatValueType::PERCENT:
			case StatValueType::INTEGER64:
			case StatValueType::PERCENT64:
				StatsNode[Attribute.second.Name] = (Json::Value::Int64)AttributeStorage.Int;
			break;
			case StatValueType::DOUBLE:
			case StatValueType::TIME:
			case StatValueType::PERCENT_DOUBLE:
				StatsNode[Attribute.second.Name] = AttributeStorage.Double;
			break;
			default:
				throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " Unsupported save type: " + Attribute.second.Name);
			break;
		}
	}
	Data["stats"] = StatsNode;

	// Write items
	Json::Value ItemsNode;
	for(auto &Bag : Inventory->GetBags()) {
		if(Bag.Type == BagType::NONE)
			continue;

		// Write bag contents
		Json::Value BagNode;
		for(size_t i = 0; i < Bag.Slots.size(); i++) {
			const _InventorySlot &InventorySlot = Bag.Slots[i];
			if(InventorySlot.Item) {
				Json::Value ItemNode;
				ItemNode["slot"] = (Json::Value::UInt64)i;
				ItemNode["id"] = InventorySlot.Item->ID;
				ItemNode["upgrades"] = InventorySlot.Upgrades;
				ItemNode["count"] = InventorySlot.Count;
				BagNode.append(ItemNode);
			}
		}
		ItemsNode[std::to_string((int)Bag.Type)] = BagNode;
	}
	Data["items"] = ItemsNode;

	// Write skills
	Json::Value SkillsNode;
	for(auto &Skill : Character->Skills) {
		Json::Value SkillNode;
		SkillNode["id"] = Skill.first;
		SkillNode["level"] = Skill.second;
		SkillsNode.append(SkillNode);
	}
	Data["skills"] = SkillsNode;

	// Write min skill levels
	Json::Value MinSkillLevelsNode;
	for(auto &MinSkillLevel : Character->MinSkillLevels) {
		if(MinSkillLevel.second <= GAME_DEFAULT_MAX_SKILL_LEVEL)
			continue;

		Json::Value MinSkillLevelNode;
		MinSkillLevelNode["id"] = MinSkillLevel.first;
		MinSkillLevelNode["level"] = MinSkillLevel.second;
		MinSkillLevelsNode.append(MinSkillLevelNode);
	}
	Data["min_skill_levels"] = MinSkillLevelsNode;

	// Write max skill levels
	Json::Value MaxSkillLevelsNode;
	for(auto &MaxSkillLevel : Character->MaxSkillLevels) {
		Json::Value MaxSkillLevelNode;
		MaxSkillLevelNode["id"] = MaxSkillLevel.first;
		MaxSkillLevelNode["level"] = MaxSkillLevel.second;
		MaxSkillLevelsNode.append(MaxSkillLevelNode);
	}
	Data["max_skill_levels"] = MaxSkillLevelsNode;

	// Write action bar
	Json::Value ActionBarNode;
	for(size_t i = 0; i < Character->ActionBar.size(); i++) {
		if(Character->ActionBar[i].Item) {
			Json::Value ActionNode;
			ActionNode["slot"] = (Json::Value::UInt64)i;
			ActionNode["id"] = Character->ActionBar[i].Item ? Character->ActionBar[i].Item->ID : 0;
			ActionBarNode.append(ActionNode);
		}
	}
	Data["actionbar"] = ActionBarNode;

	// Write status effects
	Json::Value StatusEffectsNode;
	for(auto &StatusEffect : Character->StatusEffects) {
		Json::Value StatusEffectNode;
		StatusEffectNode["id"] = StatusEffect->Buff->ID;
		StatusEffectNode["level"] = StatusEffect->Level;
		if(StatusEffect->Source == this)
			StatusEffectNode["target_count"] = StatusEffect->TargetCount;
		if(StatusEffect->Infinite) {
			StatusEffectNode["infinite"] = StatusEffect->Infinite;
		}
		else {
			StatusEffectNode["duration"] = StatusEffect->Duration;
			StatusEffectNode["maxduration"] = StatusEffect->MaxDuration;
			Json::Value &StacksNode = StatusEffectNode["stacks"];
			for(auto &Stack : StatusEffect->Stacks) {
				Json::Value StackNode;
				StackNode.append(Stack.Level);
				StackNode.append(Stack.Duration);
				StacksNode.append(StackNode);
			}
		}
		StatusEffectsNode.append(StatusEffectNode);
	}
	Data["statuseffects"] = StatusEffectsNode;

	// Write unlocks
	Json::Value UnlocksNode;
	for(auto &Unlock : Character->Unlocks) {
		Json::Value UnlockNode;
		UnlockNode["id"] = Unlock.first;
		UnlockNode["level"] = Unlock.second.Level;
		UnlocksNode.append(UnlockNode);
	}
	Data["unlocks"] = UnlocksNode;

	// Write cooldowns
	Json::Value CooldownsNode;
	for(auto &Cooldown : Character->Cooldowns) {
		Json::Value CooldownNode;
		CooldownNode["id"] = Cooldown.first;
		CooldownNode["duration"] = Cooldown.second.Duration;
		CooldownNode["maxduration"] = Cooldown.second.MaxDuration;
		CooldownsNode.append(CooldownNode);
	}
	Data["cooldowns"] = CooldownsNode;

	// Write boss cooldowns
	Json::Value BossCooldownsNode;
	for(auto &BossCooldown : Character->BossCooldowns) {
		Json::Value BossCooldownNode;
		BossCooldownNode["id"] = BossCooldown.first;
		BossCooldownNode["duration"] = BossCooldown.second;
		BossCooldownsNode.append(BossCooldownNode);
	}
	Data["bosscooldowns"] = BossCooldownsNode;

	// Write boss kills
	Json::Value BossKillsNode;
	for(auto &BossKill : Character->BossKills) {
		Json::Value BossKillNode;
		BossKillNode["id"] = BossKill.first;
		BossKillNode["count"] = BossKill.second;
		BossKillsNode.append(BossKillNode);
	}
	Data["bosskills"] = BossKillsNode;

	// Write last privilege items
	Json::Value PrivilegeItemsNode;
	for(auto &PrivilegeItem : Character->LastPrivilege) {
		if(!PrivilegeItem.Item)
			continue;

		Json::Value Node;
		Node["id"] = PrivilegeItem.Item->ID;
		Node["count"] = PrivilegeItem.Count;
		PrivilegeItemsNode.append(Node);
	}
	Data["privilege"] = PrivilegeItemsNode;
}

// Unserialize attributes from string
void _Object::UnserializeSaveData(const std::string &JsonString) {

	// Parse JSON
	Json::CharReaderBuilder Reader;
	Json::Value Data;
	std::istringstream Stream(JsonString);
	std::string Errors;
	if(!Json::parseFromStream((Json::CharReader::Factory const &)Reader, Stream, &Data, &Errors))
		throw std::runtime_error("_Object::UnserializeSaveData: " + Errors);

	// Get stats
	Json::Value StatsNode = Data["stats"];
	Character->LoadMapID = (ae::NetworkIDType)StatsNode["MapID"].asUInt();
	Position.x = StatsNode["MapX"].asInt();
	Position.y = StatsNode["MapY"].asInt();
	Character->SpawnMapID = (ae::NetworkIDType)StatsNode["SpawnMapID"].asUInt();
	Character->SpawnPoint = StatsNode["SpawnPoint"].asUInt();
	Logging = StatsNode["Log"].asBool();
	Character->Hardcore = StatsNode["Hardcore"].asBool();
	Character->BuildID = StatsNode["BuildID"].asUInt();
	Character->PortraitID = StatsNode["PortraitID"].asUInt();
	ModelID = StatsNode["ModelID"].asUInt();
	Character->BeltSize = StatsNode["BeltSize"].asInt();
	Character->SkillBarSize = StatsNode["SkillBarSize"].asInt();
	Character->SkillPointsUnlocked = StatsNode["SkillPointsUnlocked"].asInt();
	Character->NextBattle = StatsNode["NextBattle"].asInt();
	Character->Priority = StatsNode["Priority"].asInt();
	Character->Seed = StatsNode["Seed"].asUInt();
	Character->PartyName = StatsNode["PartyName"].asString();

	// Load attributes
	for(const auto &Attribute : Stats->Attributes) {
		if(!Attribute.second.Save)
			continue;

		switch(Attribute.second.Type) {
			case StatValueType::BOOLEAN:
				Character->Attributes[Attribute.second.Name].Int = StatsNode[Attribute.second.Name].asBool();
			break;
			case StatValueType::INTEGER:
			case StatValueType::PERCENT:
			case StatValueType::INTEGER64:
			case StatValueType::PERCENT64:
				Character->Attributes[Attribute.second.Name].Int = StatsNode[Attribute.second.Name].asInt64();
			break;
			case StatValueType::DOUBLE:
			case StatValueType::TIME:
			case StatValueType::PERCENT_DOUBLE:
				Character->Attributes[Attribute.second.Name].Double = StatsNode[Attribute.second.Name].asDouble();
			break;
			default:
				throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " Unsupported save type: " + Attribute.second.Name);
			break;
		}
	}

	if(!Character->BeltSize)
		Character->BeltSize = ACTIONBAR_DEFAULT_BELTSIZE;

	if(!Character->SkillBarSize)
		Character->SkillBarSize = ACTIONBAR_DEFAULT_SKILLBARSIZE;

	if(!Character->Seed)
		Character->Seed = ae::GetRandomInt((uint32_t)1, std::numeric_limits<uint32_t>::max());

	if(!Character->BuildID)
		Character->BuildID = 1;

	// Set items
	for(Json::ValueIterator BagNode = Data["items"].begin(); BagNode != Data["items"].end(); BagNode++) {
		for(const Json::Value &ItemNode : *BagNode) {
			uint32_t ItemID = ItemNode["id"].asUInt();
			if(Stats->Items.find(ItemID) == Stats->Items.end())
				continue;

			_InventorySlot InventorySlot;
			InventorySlot.Item = Stats->Items.at(ItemID);
			InventorySlot.Upgrades = std::clamp(ItemNode["upgrades"].asInt(), 0, InventorySlot.Item->MaxLevel);
			InventorySlot.Count = ItemNode["count"].asInt();
			BagType Bag = (BagType)std::stoul(BagNode.name());
			if(Inventory->GetBag(Bag).StaticSize)
				Inventory->GetBag(Bag).Slots[ItemNode["slot"].asUInt64()] = InventorySlot;
			else
				Inventory->GetBag(Bag).Slots.push_back(InventorySlot);
		}
	}

	// Set skills
	for(const Json::Value &SkillNode : Data["skills"]) {
		uint32_t ItemID = SkillNode["id"].asUInt();
		Character->Skills[ItemID] = std::min(SkillNode["level"].asInt(), Stats->Items.at(ItemID)->MaxLevel);
	}

	// Set min skill levels
	for(const Json::Value &MinSkillLevelNode : Data["min_skill_levels"]) {
		uint32_t ItemID = MinSkillLevelNode["id"].asUInt();
		Character->MinSkillLevels[ItemID] = std::min(MinSkillLevelNode["level"].asInt(), Stats->Items.at(ItemID)->MaxLevel);
	}

	// Set max skill levels
	for(const Json::Value &MaxSkillLevelNode : Data["max_skill_levels"]) {
		uint32_t ItemID = MaxSkillLevelNode["id"].asUInt();
		Character->MaxSkillLevels[ItemID] = std::min(MaxSkillLevelNode["level"].asInt(), Stats->Items.at(ItemID)->MaxLevel);
	}

	// Set actionbar
	for(const Json::Value &ActionNode : Data["actionbar"]) {
		uint32_t Slot = ActionNode["slot"].asUInt();
		if(Slot < Character->ActionBar.size()) {
			if(Slot < ACTIONBAR_MAX_SKILLBARSIZE && Slot >= (uint32_t)Character->SkillBarSize)
				continue;

			if(Slot >= ACTIONBAR_BELT_STARTS && Slot >= (uint32_t)(Character->BeltSize + ACTIONBAR_BELT_STARTS))
				continue;

			const _Item *Item = Stats->Items.at(ActionNode["id"].asUInt());
			if(Item->IsSkill() && Slot >= ACTIONBAR_MAX_SKILLBARSIZE)
				continue;

			if(!Item->IsSkill() && Slot < ACTIONBAR_BELT_STARTS)
				continue;

			Character->ActionBar[Slot].Item = Item;
			Character->ActionBar[Slot].ActionBarSlot = (int)Slot;
		}
	}

	// Set status effects
	for(const Json::Value &StatusEffectNode : Data["statuseffects"]) {
		_StatusEffect *StatusEffect = new _StatusEffect();
		StatusEffect->Buff = Stats->Buffs.at(StatusEffectNode["id"].asUInt());
		StatusEffect->Level = StatusEffectNode["level"].asInt64();
		StatusEffect->TargetCount = StatusEffectNode["target_count"].asInt();
		if(StatusEffect->TargetCount)
			StatusEffect->Source = this;
		StatusEffect->Infinite = StatusEffectNode["infinite"].asBool();
		if(!StatusEffect->Infinite) {
			StatusEffect->Duration = StatusEffectNode["duration"].asDouble();
			StatusEffect->MaxDuration = StatusEffectNode["maxduration"].asDouble();
			StatusEffect->Time = 1.0 - (StatusEffect->Duration - (int)StatusEffect->Duration);
			for(const Json::Value &StackNode : StatusEffectNode["stacks"]) {
				_StatusEffect::_Stack Stack;
				Stack.Level = StackNode[0].asInt64();
				Stack.Duration = StackNode[1].asDouble();
				StatusEffect->Stacks.push_back(Stack);
			}
		}
		Character->StatusEffects.push_back(StatusEffect);
	}

	// Set unlocks
	for(const Json::Value &UnlockNode : Data["unlocks"])
		Character->Unlocks[UnlockNode["id"].asUInt()].Level = UnlockNode["level"].asInt();

	// Set cooldowns
	for(const Json::Value &CooldownNode : Data["cooldowns"]) {
		uint32_t CooldownID = CooldownNode["id"].asUInt();
		Character->Cooldowns[CooldownID].Duration = CooldownNode["duration"].asDouble();
		Character->Cooldowns[CooldownID].MaxDuration = CooldownNode["maxduration"].asDouble();
	}

	// Set boss cooldowns
	for(const Json::Value &BossCooldownNode : Data["bosscooldowns"])
		Character->BossCooldowns[BossCooldownNode["id"].asUInt()] = BossCooldownNode["duration"].asDouble();

	// Set boss kills
	for(const Json::Value &BossKillNode: Data["bosskills"])
		Character->BossKills[BossKillNode["id"].asUInt()] = BossKillNode["count"].asInt();

	// Set privilege items
	Character->LastPrivilege.clear();
	for(const Json::Value &Node: Data["privilege"]) {
		_PrivilegeItem PrivilegeItem;
		PrivilegeItem.Item = Stats->Items.at(Node["id"].asUInt());
		if(!PrivilegeItem.Item)
			continue;

		PrivilegeItem.Count = Node["count"].asInt();

		Character->LastPrivilege.push_back(PrivilegeItem);
	}
}

// Serialize for ObjectCreate
void _Object::SerializeCreate(ae::_Buffer &Data) {
	Data.Write<ae::NetworkIDType>(NetworkID);
	Data.Write<glm::ivec2>(Position);
	Data.WriteString(Name.c_str());
	Data.WriteString(Character->PartyName.c_str());
	if(Character)
		Data.Write<uint8_t>(Character->PortraitID);
	else
		Data.Write<uint8_t>(0);
	Data.Write<uint8_t>(ModelID);
	Data.Write<uint32_t>(Character->Attributes["Rebirths"].Int);
	Data.Write<uint32_t>(Character->Attributes["Evolves"].Int);
	Data.Write<uint32_t>(Character->Attributes["Transforms"].Int);
	Data.Write<uint8_t>(Light);
	Data.Write<uint8_t>(Character->GetStatus());
	Data.WriteBit(Offline);
	Data.WriteBit(Character->Invisible);
}

// Unserialize for ObjectCreate
void _Object::UnserializeCreate(ae::_Buffer &Data) {
	Position = Data.Read<glm::ivec2>();
	Name = Data.ReadString();
	Character->PartyName = Data.ReadString();
	uint32_t PortraitID = Data.Read<uint8_t>();
	if(PortraitID && Character)
		Character->PortraitID = PortraitID;
	ModelID = Data.Read<uint8_t>();
	Character->Attributes["Rebirths"].Int = Data.Read<uint32_t>();
	Character->Attributes["Evolves"].Int = Data.Read<uint32_t>();
	Character->Attributes["Transforms"].Int = Data.Read<uint32_t>();
	Light = Data.Read<uint8_t>();
	Character->Status = Data.Read<uint8_t>();
	Offline = Data.ReadBit();
	Character->Invisible = Data.ReadBit();
	Character->Portrait = Stats->GetPortraitImage(Character->PortraitID);

	ModelTexture = Stats->Models.at(ModelID).Texture;
}

// Serialize for ObjectUpdate
void _Object::SerializeUpdate(ae::_Buffer &Data) {
	Data.Write<ae::NetworkIDType>(NetworkID);
	Data.Write<uint8_t>(Position.x);
	Data.Write<uint8_t>(Position.y);
	Data.Write<uint8_t>(Character->Status);
	Data.WriteBit(Offline);
	Data.WriteBit(Light);
	Data.WriteBit(Character->Invisible);
	Data.WriteBit(Character->Attributes["Bounty"].Int);
	if(Character->Attributes["Bounty"].Int)
		Data.Write<int64_t>(Character->Attributes["Bounty"].Int);
	if(Light)
		Data.Write<uint8_t>(Light);
}

// Serialize object stats
void _Object::SerializeStats(ae::_Buffer &Data) {
	Data.WriteString(Name.c_str());
	Data.Write<uint8_t>(Light);
	Data.Write<uint32_t>(ModelID);
	Data.Write<uint32_t>(Character->PortraitID);
	Data.Write<uint32_t>(Character->SpawnMapID);
	Data.WriteString(Character->PartyName.c_str());
	Data.Write<uint8_t>(Character->BeltSize);
	Data.Write<uint8_t>(Character->SkillBarSize);
	Data.Write<int>(Character->SkillPointsUnlocked);
	Data.Write<int>(Character->Priority);
	Data.WriteBit(Character->Invisible);
	Data.WriteBit(Character->Hardcore);
	Data.WriteBit(Character->GhostMode);
	Data.WriteBit(Character->LastPrivilege.size());

	// Serialize attributes
	for(const auto &AttributeName : Stats->AttributeRank) {
		const _Attribute &Attribute = Stats->Attributes.at(AttributeName);
		if(!Attribute.Network)
			continue;

		_Value &AttributeStorage = Character->Attributes[AttributeName];
		switch(Attribute.Type) {
			case StatValueType::BOOLEAN:
			case StatValueType::INTEGER:
			case StatValueType::PERCENT:
				Data.Write<int>((int)AttributeStorage.Int);
			break;
			case StatValueType::INTEGER64:
			case StatValueType::PERCENT64:
				Data.Write<int64_t>(AttributeStorage.Int);
			break;
			case StatValueType::DOUBLE:
			case StatValueType::TIME:
			case StatValueType::PERCENT_DOUBLE:
				Data.Write<float>(AttributeStorage.Double);
			break;
			default:
				throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " Unsupported network type: " + Attribute.Name);
			break;
		}
	}

	// Write inventory
	Inventory->Serialize(Data);

	// Write skills
	Data.Write<uint32_t>((uint32_t)Character->Skills.size());
	for(const auto &Skill : Character->Skills) {
		Data.Write<uint32_t>(Skill.first);
		Data.Write<int>(Skill.second);
	}

	// Write min skill levels
	Data.Write<uint32_t>((uint32_t)Character->MinSkillLevels.size());
	for(const auto &Skill : Character->MinSkillLevels) {
		Data.Write<uint32_t>(Skill.first);
		Data.Write<int>(Skill.second);
	}

	// Write max skill levels
	Data.Write<uint32_t>((uint32_t)Character->MaxSkillLevels.size());
	for(const auto &Skill : Character->MaxSkillLevels) {
		Data.Write<uint32_t>(Skill.first);
		Data.Write<int>(Skill.second);
	}

	// Write action bar
	for(size_t i = 0; i < ACTIONBAR_MAX_SIZE; i++)
		Character->ActionBar[i].Serialize(Data);

	// Write unlocks
	Data.Write<uint32_t>((uint32_t)Character->Unlocks.size());
	for(const auto &Unlock : Character->Unlocks) {
		Data.Write<uint32_t>(Unlock.first);
		Data.Write<int>(Unlock.second.Level);
	}

	// Write cooldowns
	Data.Write<uint32_t>((uint32_t)Character->Cooldowns.size());
	for(const auto &Cooldown : Character->Cooldowns) {
		Data.Write<uint32_t>(Cooldown.first);
		Data.Write<float>(Cooldown.second.Duration);
		Data.Write<float>(Cooldown.second.MaxDuration);
	}

	// Write boss cooldowns
	SerializeBossCooldowns(Data);

	// Write status effects
	SerializeStatusEffects(Data);
}

// Unserialize object stats
void _Object::UnserializeStats(ae::_Buffer &Data) {
	Name = Data.ReadString();
	Light = Data.Read<uint8_t>();
	ModelID = Data.Read<uint32_t>();
	Character->PortraitID = Data.Read<uint32_t>();
	Character->SpawnMapID = Data.Read<uint32_t>();
	Character->PartyName = Data.ReadString();
	Character->BeltSize = Data.Read<uint8_t>();
	Character->SkillBarSize = Data.Read<uint8_t>();
	Character->SkillPointsUnlocked = Data.Read<int>();
	Character->Priority = Data.Read<int>();
	Character->Invisible = Data.ReadBit();
	Character->Hardcore = Data.ReadBit();
	Character->GhostMode = Data.ReadBit();
	Character->ShowMoveItems = Data.ReadBit();

	// Serialize attributes
	for(const auto &AttributeName : Stats->AttributeRank) {
		const _Attribute &Attribute = Stats->Attributes.at(AttributeName);
		if(!Attribute.Network)
			continue;

		_Value &AttributeStorage = Character->Attributes[AttributeName];
		switch(Attribute.Type) {
			case StatValueType::BOOLEAN:
			case StatValueType::INTEGER:
			case StatValueType::PERCENT:
				AttributeStorage.Int = Data.Read<int>();
			break;
			case StatValueType::INTEGER64:
			case StatValueType::PERCENT64:
				AttributeStorage.Int = Data.Read<int64_t>();
			break;
			case StatValueType::DOUBLE:
			case StatValueType::TIME:
			case StatValueType::PERCENT_DOUBLE:
				AttributeStorage.Double = Data.Read<float>();
			break;
			default:
				throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " Unsupported network type: " + Attribute.Name);
			break;
		}
	}

	ModelTexture = Stats->Models.at(ModelID).Texture;

	// Read inventory
	Inventory->Unserialize(Data, Stats);

	// Read skills
	Character->Skills.clear();
	uint32_t SkillCount = Data.Read<uint32_t>();
	for(uint32_t i = 0; i < SkillCount; i++) {
		uint32_t SkillID = Data.Read<uint32_t>();
		int Points = Data.Read<int>();
		Character->Skills[SkillID] = Points;
	}

	// Read min skill levels
	Character->MinSkillLevels.clear();
	uint32_t MinSkillLevelCount = Data.Read<uint32_t>();
	for(uint32_t i = 0; i < MinSkillLevelCount; i++) {
		uint32_t SkillID = Data.Read<uint32_t>();
		int Level = Data.Read<int>();
		Character->MinSkillLevels[SkillID] = Level;
	}

	// Read max skill levels
	Character->MaxSkillLevels.clear();
	uint32_t MaxSkillLevelCount = Data.Read<uint32_t>();
	for(uint32_t i = 0; i < MaxSkillLevelCount; i++) {
		uint32_t SkillID = Data.Read<uint32_t>();
		int Level = Data.Read<int>();
		Character->MaxSkillLevels[SkillID] = Level;
	}

	// Read action bar
	for(size_t i = 0; i < ACTIONBAR_MAX_SIZE; i++)
		Character->ActionBar[i].Unserialize(Data, Stats);

	// Read unlocks
	Character->Unlocks.clear();
	uint32_t UnlockCount = Data.Read<uint32_t>();
	for(uint32_t i = 0; i < UnlockCount; i++) {
		uint32_t UnlockID = Data.Read<uint32_t>();
		int Level = Data.Read<int>();
		Character->Unlocks[UnlockID].Level = Level;
	}

	// Read cooldowns
	Character->Cooldowns.clear();
	uint32_t CooldownCount = Data.Read<uint32_t>();
	for(uint32_t i = 0; i < CooldownCount; i++) {
		uint32_t CooldownID = Data.Read<uint32_t>();
		Character->Cooldowns[CooldownID].Duration = Data.Read<float>();
		Character->Cooldowns[CooldownID].MaxDuration = Data.Read<float>();
	}

	// Read boss cooldowns
	UnserializeBossCooldowns(Data);

	// Read status effects
	UnserializeStatusEffects(Data);

	// Calculate final stats
	Character->RefreshActionBarCount();
	Character->CalculateStats();
}

// Serialize object for battle
void _Object::SerializeBattle(ae::_Buffer &Data) {

	// Send players' battlespeed before buffs
	int BattleSpeed = Monster->DatabaseID ? (int)Character->BaseAttributes.at("BattleSpeed").Int : Character->BattleSpeedBeforeBuffs;

	Data.Write<ae::NetworkIDType>(NetworkID);
	Data.Write<uint32_t>(Monster->DatabaseID);
	Data.Write<glm::ivec2>(Position);
	Data.Write<int>(Character->Level);
	Data.Write<int64_t>(Character->Attributes["Health"].Int);
	Data.Write<int64_t>(Character->Attributes["MaxHealth"].Int);
	Data.Write<int64_t>(Character->Attributes["Mana"].Int);
	Data.Write<int64_t>(Character->Attributes["MaxMana"].Int);
	Data.Write<int>(BattleSpeed);
	Data.Write<float>(Fighter->TurnTimer);
	Data.Write<uint8_t>(Fighter->BattleSide);
	Data.Write<uint8_t>(Fighter->Corpse);

	SerializeStatusEffects(Data);
}

// Unserialize battle stats
void _Object::UnserializeBattle(ae::_Buffer &Data, bool IsSelf) {
	Controller->InputStates.clear();

	// Get object type
	Position = ServerPosition = Data.Read<glm::ivec2>();
	Character->Level = Data.Read<int>();
	Character->Attributes["Health"].Int = Data.Read<int64_t>();
	Character->BaseAttributes["MaxHealth"].Int = Data.Read<int64_t>();
	Character->Attributes["Mana"].Int = Data.Read<int64_t>();
	Character->BaseAttributes["MaxMana"].Int = Data.Read<int64_t>();
	int BattleSpeed = Data.Read<int>();
	if(!IsSelf)
		Character->BaseAttributes.at("BattleSpeed").Int = BattleSpeed;

	Fighter->TurnTimer = Data.Read<float>();
	Fighter->BattleSide = Data.Read<uint8_t>();
	Fighter->Corpse = Data.Read<uint8_t>();

	UnserializeStatusEffects(Data, false);
	Character->CalculateStats();
}

// Serialize status effects
void _Object::SerializeStatusEffects(ae::_Buffer &Data) {
	Data.Write<uint8_t>((uint8_t)Character->StatusEffects.size());
	for(auto &StatusEffect : Character->StatusEffects)
		StatusEffect->Serialize(Data);
}

// Serialize boss cooldowns
void _Object::SerializeBossCooldowns(ae::_Buffer &Data) {
	Data.Write<uint32_t>((uint32_t)Character->BossCooldowns.size());
	for(const auto &BossCooldown : Character->BossCooldowns) {
		Data.Write<uint32_t>(BossCooldown.first);
		Data.Write<float>(BossCooldown.second);
	}
}

// Unserialize boss cooldowns
void _Object::UnserializeBossCooldowns(ae::_Buffer &Data) {
	Character->BossCooldowns.clear();
	uint32_t BossCooldownCount = Data.Read<uint32_t>();
	for(uint32_t i = 0; i < BossCooldownCount; i++) {
		uint32_t BossCooldownID = Data.Read<uint32_t>();
		Character->BossCooldowns[BossCooldownID] = Data.Read<float>();
	}
}

// Unserialize status effects
void _Object::UnserializeStatusEffects(ae::_Buffer &Data, bool CreateHUDElements) {
	if(!Character)
		return;

	Character->DeleteStatusEffects();
	int StatusEffectCount = Data.Read<uint8_t>();
	for(int i = 0; i < StatusEffectCount; i++) {
		_StatusEffect *StatusEffect = new _StatusEffect();
		StatusEffect->Unserialize(Data, Stats);
		Character->StatusEffects.push_back(StatusEffect);
	}

	if(CreateHUDElements)
		Character->CreateAllStatusEffectElements();
}

// Update stats
void _Object::UpdateStats(_StatChange &StatChange, _Object *Source, int TargetCount) {

	// Server only
	if(Server) {
		if(StatChange.HasStat("BloodMoonTime"))
			Server->Save->BloodMoonTime = StatChange.Values["BloodMoonTime"].Int;

		if(StatChange.HasStat("Rebirth")) {
			for(size_t i = 0; i < Stats->RebirthBonusNames.size(); i++) {
				if(StatChange.HasStat(Stats->RebirthBonusNames[i]))
					Server->QueueRebirth(this, 0, Stats->RebirthBonusNames[i], StatChange.Values[Stats->RebirthBonusNames[i]].Int);
			}

			return;
		}

		if(StatChange.HasStat("Evolve")) {
			for(size_t i = 0; i < Stats->RebirthBonusNames.size(); i++) {
				if(StatChange.HasStat(Stats->RebirthBonusNames[i]))
					Server->QueueRebirth(this, 1, Stats->RebirthBonusNames[i], StatChange.Values[Stats->RebirthBonusNames[i]].Int);
			}

			return;
		}

		if(StatChange.HasStat("Transform")) {
			for(size_t i = 0; i < Stats->RebirthBonusNames.size(); i++) {
				if(StatChange.HasStat(Stats->RebirthBonusNames[i]))
					Server->QueueRebirth(this, 2, Stats->RebirthBonusNames[i], StatChange.Values[Stats->RebirthBonusNames[i]].Int);
			}

			return;
		}

		if(StatChange.HasStat("DestroyCursed")) {

			// Search for cursed items
			for(auto &InventorySlot : Inventory->GetBag(BagType::EQUIPMENT).Slots) {
				if(InventorySlot.Item && InventorySlot.Item->IsCursed())
					InventorySlot.Reset();
			}
			Character->CalculateStats();

			// Send inventory
			ae::_Buffer Packet;
			Packet.Write<PacketType>(PacketType::INVENTORY);
			Inventory->Serialize(Packet);
			Server->Network->SendPacket(Packet, Peer);
		}

		if(StatChange.HasStat("Event")) {
			_Event Event;
			Event.Type = (uint32_t)StatChange.Values["Event"].Int;
			Event.Data = 0;
			if(Map)
				Map->StartEvent(this, Event, true);
		}
	}

	// Add buffs
	if(StatChange.HasStat("Buff")) {
		if(TargetCount == 0)
			TargetCount = BATTLE_MAX_OBJECTS_PER_SIDE;

		_StatusEffect *StatusEffect = new _StatusEffect();
		StatusEffect->Buff = (const _Buff *)StatChange.Values["Buff"].Pointer;
		StatusEffect->Level = StatChange.Values["BuffLevel"].Int;
		StatusEffect->MaxDuration = StatusEffect->Duration = StatChange.Values["BuffDuration"].Double;
		StatusEffect->Priority = (int)StatChange.Values["BuffPriority"].Int;
		StatusEffect->Modifier = (int)StatChange.Values["BuffModifier"].Int;
		StatusEffect->TargetCount = TargetCount;
		StatusEffect->Source = Source;
		if(StatusEffect->Duration < 0.0) {
			StatusEffect->Infinite = true;
			StatusEffect->Duration = 0.0;
		}

		if(!Character->AddStatusEffect(StatusEffect))
			delete StatusEffect;

		Character->CalculateStats();
	}

	// Clear buff
	if(Server && StatChange.HasStat("ClearBuff")) {
		_Buff *ClearBuff = (_Buff *)StatChange.Values["ClearBuff"].Pointer;

		// Remove status effects
		if(Character->ClearBuff(ClearBuff->ID))
			Server->ClearBuff(this, ClearBuff->ID);
	}

	// Update gold
	if(StatChange.HasStat("Gold")) {
		int64_t GoldUpdate = StatChange.Values["Gold"].Int;
		if(Battle && GoldUpdate < 0 && Fighter->GoldStolen) {
			Fighter->GoldStolen += GoldUpdate;
			if(Fighter->GoldStolen < 0)
				Fighter->GoldStolen = 0;
		}

		Character->UpdateGold(GoldUpdate);
	}

	// Update gold stolen
	if(StatChange.HasStat("GoldStolen")) {
		int64_t Amount = StatChange.Values["GoldStolen"].Int;
		Fighter->GoldStolen += Amount;
		if(Fighter->GoldStolen > PLAYER_MAX_GOLD)
			Fighter->GoldStolen = PLAYER_MAX_GOLD;

		Character->UpdateGold(Amount);
	}

	// Update experience
	if(StatChange.HasStat("Experience")) {
		Character->UpdateExperience(StatChange.Values["Experience"].Int);
		Character->CalculateStats();
	}

	// Update gold lost
	if(StatChange.HasStat("GoldLost"))
		Character->UpdateGoldLost(StatChange.Values["GoldLost"].Int);

	// Update health
	bool WasAlive = Character->IsAlive();
	if(StatChange.HasStat("Health"))
		Character->UpdateHealth(StatChange.Values["Health"].Int);

	// Just died
	if(WasAlive && !Character->IsAlive()) {
		Character->Action.Clear();
		Character->ResetUIState(false);

		// If not in battle apply penalty immediately
		if(!Battle) {

			// Apply penalty
			ApplyDeathPenalty(false, PLAYER_DEATH_GOLD_PENALTY, 0);
		}
	}

	// Send HUD to character revived outside of battle
	if(Server && !WasAlive && Character->IsAlive() && !Battle) {
		Server->SendHUD(Peer);
	}

	// Mana change
	if(StatChange.HasStat("Mana"))
		Character->UpdateMana(StatChange.Values["Mana"].Int);

	// Stamina change
	if(StatChange.HasStat("Stamina")) {
		Fighter->TurnTimer += StatChange.Values["Stamina"].Double;
		Fighter->TurnTimer = glm::clamp(Fighter->TurnTimer, 0.0, 1.0);
	}

	// Skill bar upgrade
	if(StatChange.HasStat("SkillBarSize")) {
		Character->SkillBarSize += StatChange.Values["SkillBarSize"].Int;
		if(Character->SkillBarSize >= ACTIONBAR_MAX_SKILLBARSIZE)
			Character->SkillBarSize = ACTIONBAR_MAX_SKILLBARSIZE;
	}

	// Belt size upgrade
	if(StatChange.HasStat("BeltSize")) {
		Character->BeltSize += StatChange.Values["BeltSize"].Int;
		if(Character->BeltSize >= ACTIONBAR_MAX_BELTSIZE)
			Character->BeltSize = ACTIONBAR_MAX_BELTSIZE;
	}

	// Skill point unlocked
	if(StatChange.HasStat("SkillPoint")) {
		Character->SkillPointsUnlocked += StatChange.Values["SkillPoint"].Int;
		Character->CalculateStats();
	}

	// Boss cooldowns
	if(StatChange.HasStat("CurrentBossCooldowns")) {
		for(auto &BattleCooldown : Character->BossCooldowns) {
			if(BattleCooldown.second > 0.0)
				BattleCooldown.second *= 1.0 - StatChange.Values["CurrentBossCooldowns"].Mult();
		}
	}

	// Rites
	for(const auto &RiteName : Stats->RiteStatNames) {
		if(StatChange.HasStat(RiteName)) {
			Character->Attributes[RiteName].Int += StatChange.Values[RiteName].Int;
			Character->CalculateStats();
		}
	}

	// Reset skills
	if(StatChange.HasStat("Respec")) {
		for(const auto &SkillLevel : Character->Skills) {
			const _Item *Skill = Stats->Items.at(SkillLevel.first);
			if(Skill && SkillLevel.second > 0) {

				// Set action bar skill to 1
				bool SetToZero = true;
				for(int i = 0; i < Character->SkillBarSize; i++) {
					if(Character->ActionBar[(size_t)i].Item == Skill) {
						Character->Skills[SkillLevel.first] = 1;
						SetToZero = false;
						break;
					}
				}

				if(SetToZero)
					Character->Skills[SkillLevel.first] = 0;
			}
		}

		Character->CalculateStats();
	}

	// Flee from battle
	if(StatChange.HasStat("Flee")) {
		if(Fighter) {
			Fighter->FleeBattle = true;
			if(Server && Battle) {
				Server->Log
					<< "[BATTLE] Player " << Name << " flees zone " << Battle->Name
					<< " ("
					<< " character_id=" << Character->ID
					<< " zone_id=" << Battle->Zone
					<< " time=" << Battle->Time
					<< " pvp=" << Battle->PVP
					<< " gold_stolen=" << Fighter->GoldStolen
					<< " )"
					<< std::endl;
			}
		}
	}

	// Use corpse
	if(StatChange.HasStat("Corpse")) {
		Fighter->Corpse += StatChange.Values["Corpse"].Int;
		if(Fighter->Corpse < 0)
			Fighter->Corpse = 0;
		else if(Fighter->Corpse > 1)
			Fighter->Corpse = 1;
	}

	// Run server only commands
	if(Server) {
		if(!Battle) {

			// Start teleport
			if(StatChange.HasStat("Teleport"))
				Server->StartTeleport(this, StatChange.Values["Teleport"].Double);

			// Start battle
			if(StatChange.HasStat("Battle")) {
				uint32_t ZoneID = (uint32_t)StatChange.Values["Battle"].Int;

				// Check zone
				auto Iterator = Stats->Zones.find(ZoneID);
				if(Iterator != Stats->Zones.end()) {
					const _Zone &Zone = Iterator->second;
					if(Zone.Boss == 0)
						Character->GenerateNextBattle();

					Server->QueueBattle(this, Zone.Name, Zone.ID, Zone.Boss, false, StatChange.Values["Manual"].Int, 0.0f, 0.0f, Zone.Duration);
				}
			}

			// Start PVP
			if(StatChange.HasStat("Hunt"))
				Server->QueueBattle(this, "pvp_hunt", 0, false, true, false, StatChange.Values["Hunt"].Double, 0.0f, 0.0);
			if(StatChange.HasStat("BountyHunt"))
				Server->QueueBattle(this, "pvp_bountyhunt", 0, false, true, false, 0.0f, StatChange.Values["BountyHunt"].Double, 0.0);
		}

		// Set clock
		if(StatChange.HasStat("Clock"))
			Server->SetClock(StatChange.Values["Clock"].Double);

		// Map Change
		if(StatChange.HasStat("MapChange"))
			QueuedMapChange = (uint32_t)StatChange.Values["MapChange"].Int;
	}
}

// Moves the player and returns direction moved
int _Object::Move() {
	if(Controller->WaitForServer || Battle || Controller->InputStates.size() == 0 || !Character->IsAlive()) {
		Controller->InputStates.clear();
		return 0;
	}

	// Check timer
	if(Controller->MoveTime < PLAYER_MOVETIME / Character->Attributes["MoveSpeed"].Mult())
		return 0;

	if(Character->TeleportTime > 0.0) {
		Controller->InputStates.clear();
		return 0;
	}

	Controller->MoveTime = 0;
	int InputState = Controller->InputStates.front();
	Controller->InputStates.pop_front();

	// Check for use command
	Controller->UseCommand = InputState & _Object::INPUT_USE;

	// Get new position
	glm::ivec2 Direction(0, 0);
	bool HasDirection = GetDirectionFromInput(InputState, Direction);

	// Log movement
	if(Logging && Server)
		Log << "[INPUT] time=" << std::fixed << std::setprecision(2) << Server->Time << " mapid=" << Map->NetworkID << " x=" << Position.x << " y=" << Position.y << " input=" << InputState << std::endl;

	// Check for moving
	if(!HasDirection)
		return 0;

	// Test movement
	bool Moved = false;
	if(!Map->CanMoveTo(Position + Direction, this)) {

		// Check for moving diagonally
		if(Direction.x != 0 && Direction.y != 0) {

			// Try moving horizontally
			glm::ivec2 TestDirection(Direction.x, 0);
			if(Map->CanMoveTo(Position + TestDirection, this)) {
				Direction = TestDirection;
				Moved = true;
			}
			else {

				// Try moving vertically
				TestDirection.x = 0;
				TestDirection.y = Direction.y;
				if(Map->CanMoveTo(Position + TestDirection, this)) {
					Direction = TestDirection;
					Moved = true;
				}
			}
		}
	}
	else
		Moved = true;

	// Move player
	if(Moved) {
		Position += Direction;
		if(GetTile()->Zone > 0 && !Character->Invisible && !Character->GhostMode)
			Character->NextBattle--;

		return InputState;
	}

	return 0;
}

// Return true if the object can respec
bool _Object::CanRespec() const {
	if(Map && Map->IsValidPosition(Position) && GetTile()->Event.Type == _Map::EVENT_SPAWN)
		return true;

	return false;
}

// Gets the tile that the player is currently standing on
const _Tile *_Object::GetTile() const {

	return Map->GetTile(Position);
}

// Get map id, return 0 if none
ae::NetworkIDType _Object::GetMapID() const {
	if(!Map)
		return 0;

	return Map->NetworkID;
}

// Stop a battle
void _Object::StopBattle() {
	Battle = nullptr;
	Fighter->RemoveBattleElement();
}

// Call update function for buff
void _Object::CallBuffUpdate(_StatusEffect *StatusEffect) {
	if(!Server)
		return;

	// Call function
	_StatChange StatChange;
	StatChange.Object = this;
	StatusEffect->Buff->ExecuteScript(Scripting, "Update", StatusEffect->Level, StatChange);
	StatChange.Object->UpdateStats(StatChange);

	// Build packet
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::STAT_CHANGE);
	StatChange.Serialize(Packet);

	// Send packet to player
	SendPacket(Packet);
}

// Add summon key to summons map
void _Object::AddSummonKey(std::map<_SummonKey, int> &Summons) {
	if(!Monster->Owner)
		return;

	if(Monster->Owner->IsMonster())
		return;

	if(!Monster->SummonBuff)
		return;

	// Pair summon buff to owner
	_Object::_SummonKey SummonKey(Monster->Owner, Monster->SummonBuff);

	// Add to map if summon is alive
	if(Character->IsAlive())
		Summons[SummonKey]++;
}

// Update death count and gold loss
void _Object::ApplyDeathPenalty(bool InBattle, double Penalty, int64_t BountyLoss) {
	int64_t GoldPenalty = BountyLoss + (int64_t)(std::abs(Character->Attributes["Gold"].Int) * Penalty + 0.5);
	int64_t ClaimedBounty = Character->Attributes["Bounty"].Int;

	// Update stats
	Character->UpdateGold(-GoldPenalty);
	Character->UpdateGoldLost(GoldPenalty);
	Character->Attributes["Deaths"].Int++;
	Character->Attributes["Bounty"].Int -= BountyLoss;
	if(Character->Attributes["Bounty"].Int < 0)
		Character->Attributes["Bounty"].Int = 0;

	// Send message
	if(Server) {
		if(BountyLoss > 0 && Character->Attributes["Bounty"].Int == 0) {
			std::stringstream Buffer;
			Buffer << "Player " + Name + "'s bounty of ";
			ae::FormatSI<int64_t>(Buffer, ClaimedBounty);
			Buffer << " gold has been claimed!";
			Server->BroadcastMessage(nullptr, Buffer.str(), "cyan");
			Server->Log << "[BOUNTY] " << Buffer.str() << std::endl;
		}

		// Notify
		if(Peer) {
			std::stringstream Buffer;
			Buffer.imbue(std::locale(Config.Locale));

			Buffer << "You died ";
			if(InBattle)
				Buffer << "in battle ";

			Buffer << "and lost ";
			ae::FormatSI<int64_t>(Buffer, GoldPenalty);
			Buffer << " gold";
			Server->SendPlayerPosition(Peer);
			Server->SendMessage(Peer, Buffer.str(), "red");
			Server->Log << "[DEATH] Player " << Name << " died and lost "
				<< std::to_string(GoldPenalty)<< " gold "
				<< "( character_id=" << Character->ID
				<< " gold=" << Character->Attributes["Gold"].Int
				<< " deaths=" << Character->Attributes["Deaths"].Int
				<< " hardcore=" << Character->Hardcore
				<< " )"	<< std::endl;
		}
	}
}

// Determine whether to show manual trigger event messages
bool _Object::ShowTriggerMessage() {
	if(!Character)
		return false;

	if(!Character->IsAlive())
		return false;

	if(Battle)
		return false;

	return Character->OnTrigger && Character->TriggerCount;
}

// Set action and targets, return false if no action set
bool _Object::SetActionUsing(ae::_Buffer &Data, ae::_Manager<_Object> *ObjectManager) {

	// Check for existing action set
	if(Character->Action.Item)
		return true;

	// Read packet
	bool WasInBattle = Data.ReadBit();
	uint8_t ActionBarSlot = Data.Read<uint8_t>();
	ae::NetworkIDType TargetID = Data.Read<ae::NetworkIDType>();

	// Check if action use is consistent with player state
	if(WasInBattle != !!Battle)
		return false;

	// Get action
	if(!Character->GetActionFromActionBar(Character->Action, ActionBarSlot))
		return false;

	// Set initial target
	Character->Action.Target = ObjectManager->GetObject(TargetID);

	// Check for using resurrect outside of battle
	if(!Battle && !Character->Action.Item->TargetAlive && Map)
		Character->Action.Target = Map->FindDeadPlayer(this, 1.0f);

	return Character->Action.Target;
}

// Determine if a player can interact with another
bool _Object::CanInteractWith(const _Object *Object, bool UseLevelRange, bool &HitLevelRestriction) const {

	// Check for deleted state
	if(Object->Deleted)
		return false;

	// Check for character
	if(!Object->Character)
		return false;

	// Skip self target
	if(Object == this)
		return false;

	// Check hardcore
	if(Object->Character->Hardcore != Character->Hardcore)
		return false;

	// Check rebirths
	if(Object->Character->Attributes["Rebirths"].Int != Character->Attributes["Rebirths"].Int) {
		HitLevelRestriction = true;
		return false;
	}

	// Check evolves
	if(Object->Character->Attributes["Evolves"].Int != Character->Attributes["Evolves"].Int) {
		HitLevelRestriction = true;
		return false;
	}

	// Check transforms
	if(Object->Character->Attributes["Transforms"].Int != Character->Attributes["Transforms"].Int) {
		HitLevelRestriction = true;
		return false;
	}

	// Check level
	if(!UseLevelRange)
		return true;

	// Check level range
	const _Level *Level = Stats->GetLevel(Character->Level);
	if(Object->Character->Level < Level->MinInteract || Object->Character->Level > Level->MaxInteract) {
		HitLevelRestriction = true;
		return false;
	}

	return true;
}

// Move last privileged items into trade bag
void _Object::MovePrivilegeItems(std::vector<_Slot> &SlotsUpdated, bool SearchStash) {
	for(const auto &PrivilegeItem : Character->LastPrivilege) {
		if(!PrivilegeItem.Item || !PrivilegeItem.Item->Tradable)
			continue;

		int SearchCount = PrivilegeItem.Item->IsEquippable() ? PrivilegeItem.Count : INVENTORY_SIZE;
		for(int i = 0; i < SearchCount; i++) {

			// Find item
			_Slot SourceSlot;
			Inventory->FindBestItem(PrivilegeItem.Item, SourceSlot, SearchStash);
			if(SourceSlot.Type == BagType::NONE)
				continue;

			// Move stack
			if(!Inventory->Transfer(SourceSlot, BagType::TRADE, SlotsUpdated))
				break;
		}
	}
}

// Generate and send seed to client
void _Object::SendSeed() {
	if(!Server)
		return;

	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::MINIGAME_SEED);
	Packet.Write<uint32_t>(Character->Seed);
	Server->Network->SendPacket(Packet, Peer);
}

// Convert input state bitfield to direction, return true if non-zero direction
bool _Object::GetDirectionFromInput(int InputState, glm::ivec2 &Direction) {

	// Get new position
	if(InputState & INPUT_UP)
		Direction.y += -1;
	if(InputState & INPUT_DOWN)
		Direction.y += 1;
	if(InputState & INPUT_LEFT)
		Direction.x += -1;
	if(InputState & INPUT_RIGHT)
		Direction.x += 1;

	// Check for diagonal movement
	if(!Character->Attributes["DiagonalMovement"].Int && Direction.x != 0 && Direction.y != 0)
		Direction.x = 0;

	return Direction.x != 0 || Direction.y != 0;
}

// Send packet to player or broadcast during battle
void _Object::SendPacket(ae::_Buffer &Packet, bool Broadcast) {
	if(Broadcast && Battle)
		Battle->BroadcastPacket(Packet);
	else if(Peer)
		Server->Network->SendPacket(Packet, Peer);
}

// Send action clear packet to client
void _Object::SendActionClear() {
	if(!Server)
		return;

	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::ACTION_CLEAR);
	Packet.Write<ae::NetworkIDType>(NetworkID);
	SendPacket(Packet);
}

// Set logging
void _Object::SetLogging(bool Value) {
	Logging = Value;
	if(Logging) {
		Log.Open((Config.LogDataPath + "char_" + std::to_string(Character->ID) + ".log").c_str());
		Log.ToStdOut = false;
	}
	else
		Log.Close();
}

// Check if object is a monster
bool _Object::IsMonster() const {
	return Monster->DatabaseID != 0;
}

// Create list of nodes to destination
bool _Object::Pathfind(const glm::ivec2 &StartPosition, const glm::ivec2 &EndPosition) {
	if(!Map || !Map->Pather)
		return false;

	if(Character->Path.size())
		return true;

	float TotalCost;
	std::vector<void *> PathFound;
	int Result = Map->Pather->Solve(Map->PositionToNode(StartPosition), Map->PositionToNode(EndPosition), &PathFound, &TotalCost);
	if(Result == micropather::MicroPather::SOLVED) {

		// Convert vector to list
		Character->Path.clear();
		for(auto &Node : PathFound)
			Character->Path.push_back(Node);

		return true;
	}

	return false;
}

// Return an input state from the next node in the path list
int _Object::GetInputStateFromPath() {
	int InputState = 0;

	// Find current position in list
	for(auto Iterator = Character->Path.begin(); Iterator != Character->Path.end(); ++Iterator) {
		glm::ivec2 NodePosition;
		Map->NodeToPosition(*Iterator, NodePosition);

		if(Position == NodePosition) {
			auto NextIterator = std::next(Iterator, 1);
			if(NextIterator == Character->Path.end()) {
				Character->Path.clear();
				return 0;
			}

			// Get next node position
			Map->NodeToPosition(*NextIterator, NodePosition);

			// Get direction to next node
			glm::ivec2 Direction = NodePosition - Position;
			if(Direction.x < 0)
				InputState = _Object::INPUT_LEFT;
			else if(Direction.x > 0)
				InputState = _Object::INPUT_RIGHT;
			else if(Direction.y < 0)
				InputState = _Object::INPUT_UP;
			else if(Direction.y > 0)
				InputState = _Object::INPUT_DOWN;

			return InputState;
		}
	}

	return 0;
}
