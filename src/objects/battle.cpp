/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <objects/battle.h>
#include <ae/actions.h>
#include <ae/assets.h>
#include <ae/clientnetwork.h>
#include <ae/database.h>
#include <ae/font.h>
#include <ae/graphics.h>
#include <ae/input.h>
#include <ae/manager.h>
#include <ae/random.h>
#include <ae/servernetwork.h>
#include <ae/ui.h>
#include <ae/util.h>
#include <hud/hud.h>
#include <objects/components/character.h>
#include <objects/components/controller.h>
#include <objects/components/fighter.h>
#include <objects/components/inventory.h>
#include <objects/components/monster.h>
#include <objects/item.h>
#include <objects/object.h>
#include <objects/status_effect.h>
#include <actiontype.h>
#include <config.h>
#include <framework.h>
#include <packet.h>
#include <scripting.h>
#include <server.h>
#include <stats.h>
#include <glm/gtc/type_ptr.hpp>
#include <SDL_keycode.h>
#include <algorithm>

// Constructor
_Battle::_Battle() {
	Objects.reserve(BATTLE_MAX_OBJECTS_PER_SIDE * 2);
}

// Destructor
_Battle::~_Battle() {
	if(BattleElement)
		BattleElement->SetActive(false);

	// Remove objects
	for(auto &Object : Objects) {
		if(Object->IsMonster())
			Object->Deleted = true;
		Object->StopBattle();
	}

	// Remove entry from battle table
	if(Server)
		Scripting->DeleteBattle(this);
}

// Update battle
void _Battle::Update(double FrameTime) {

	// Check for end
	if(Server) {

		// Count alive objects for each side
		int AliveCount[2] = { 0, 0 };
		for(auto &Object : Objects) {
			if(Object->Character->IsAlive())
				AliveCount[Object->Fighter->BattleSide]++;
		}

		// Check for end conditions
		if(AliveCount[0] == 0 || AliveCount[1] == 0) {
			WaitTimer += FrameTime;
			if(WaitTimer >= BATTLE_WAITDEADTIME)
				ServerEndBattle();
		}
	}
	else {

		// Update battle clock
		if(TimerElement) {
			double DisplayTime = Duration ? std::max(Duration - Time, 0.0) : Time;
			std::stringstream Buffer;
			ae::FormatTime(Buffer, DisplayTime);
			TimerElement->Text = Buffer.str();
		}

		// Update action results
		for(auto Iterator = ActionResults.begin(); Iterator != ActionResults.end(); ) {
			_ActionResult &ActionResult = *Iterator;

			// Update ui
			if(BattleElement && ActionResult.Source.Object && ActionResult.Target.Object) {

				// Find start position
				glm::vec2 StartPosition = ActionResult.Source.Object->Fighter->ResultPosition - glm::vec2(UI_PORTRAIT_SIZE.x/2 + UI_SLOT_SIZE.x/2 + 10, 0) * ae::_Element::GetUIScale();
				ActionResult.LastPosition = ActionResult.Position;

				// Get target position for action icon
				glm::vec2 TargetPosition = ActionResult.Target.Object->Fighter->ResultPosition + (UI_PORTRAIT_SIZE * ActionResult.PortraitOffset + UI_SLOT_SIZE * ActionResult.SlotOffset) * 0.5f * ae::_Element::GetUIScale();

				// Interpolate between start and end position of action used
				ActionResult.Position = glm::mix(StartPosition, TargetPosition, std::min(ActionResult.Time * ActionResult.Speed / ActionResult.Timeout, 1.0));
				if(ActionResult.Time == 0.0) {
					ActionResult.LastPosition = ActionResult.Position;
				}
				else if(!ActionResult.Short) {
					double TimeLeft = ActionResult.Timeout - ActionResult.Time;
					if(TimeLeft < HUD_ACTIONRESULT_FADETIME)
						ActionResult.Position = glm::mix(TargetPosition + glm::vec2(0.0f, -UI_PORTRAIT_SIZE.y) * ae::_Element::GetUIScale(), TargetPosition, TimeLeft);
				}
			}

			// Update timer
			ActionResult.Time += FrameTime;
			if(ActionResult.Time >= ActionResult.Timeout)
				Iterator = ActionResults.erase(Iterator);
			else
				++Iterator;
		}
	}

	Time += FrameTime;
}

// Render the battle screen
void _Battle::Render(double BlendFactor) {
	BattleElement->Render();

	// Show battle timer
	if(Config.ShowBattleStats || Duration || ae::Input.ModKeyDown(KMOD_ALT)) {
		TimerElement->Active = true;
		TimerElement->Render();
		TimerElement->Active = false;
	}

	// Show battle difficulty
	if(Config.ShowBattleStats || ae::Input.ModKeyDown(KMOD_ALT)) {
		DifficultyElement->Active = true;
		DifficultyElement->Render();
		DifficultyElement->Active = false;
	}

	// Draw battle elements
	for(auto &Object : Objects)
		Object->RenderBattle(ClientPlayer, Time, ae::Input.ModKeyDown(KMOD_ALT));

	// Draw action results
	for(auto &ActionResult : ActionResults)
		RenderActionResults(ActionResult, BlendFactor);
}

// Render results of an action
void _Battle::RenderActionResults(_ActionResult &ActionResult, double BlendFactor) {
	if(!ActionResult.Target.Object || !ActionResult.Source.Object)
		return;

	// Get alpha
	double TimeLeft = ActionResult.Timeout - ActionResult.Time;
	float AlphaPercent = 1.0f;
	if(TimeLeft < HUD_ACTIONRESULT_FADETIME)
		AlphaPercent = (float)(TimeLeft / HUD_ACTIONRESULT_FADETIME);

	// Get final draw position
	glm::vec2 DrawPosition = glm::mix(ActionResult.LastPosition, ActionResult.Position, BlendFactor);

	// Draw icon
	glm::vec4 WhiteAlpha = glm::vec4(0.5f, 0.5f, 0.5f, AlphaPercent);
	ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos_uv"]);
	if(ActionResult.ActionUsed.Item && !ActionResult.ActionUsed.Item->IsSkill()) {
		WhiteAlpha = glm::vec4(1.0f, 1.0f, 1.0f, AlphaPercent);
		ae::Graphics.DrawScaledImage(DrawPosition, ae::Assets.Textures["textures/hud/item_back.webp"], UI_SLOT_SIZE, WhiteAlpha);
	}
	ae::Graphics.DrawScaledImage(DrawPosition, ActionResult.Texture, UI_SLOT_SIZE, WhiteAlpha);

	// Get damage value and color
	glm::vec4 TextColor = glm::vec4(1.0f);
	std::stringstream Buffer;
	if(ActionResult.Target.HasStat("Health")) {

		if(ActionResult.Target.HasStat("DamageType")) {
			uint32_t DamageTypeID = (uint32_t)ActionResult.Target.Values["DamageType"].Int;
			TextColor = Stats->DamageTypes.at(DamageTypeID).Color;
		}

		ae::FormatSI<int64_t>(Buffer, std::abs(ActionResult.Target.Values["Health"].Int), ae::RoundDown1);
	}
	else if(ActionResult.Target.HasStat("Miss"))
		Buffer << "miss";

	// Change color
	if(ActionResult.Target.HasStat("Health") && ActionResult.Target.Values["Health"].Int > 0)
		TextColor = ae::Assets.Colors["green"];
	else if(ActionResult.Target.HasStat("Crit") && ActionResult.Target.Values["Crit"].Int)
		TextColor = ae::Assets.Colors["yellow"];

	// Get offset for mana damage
	bool HasManaDamage = ActionResult.Target.HasStat("Mana") && ActionResult.Target.Values["Mana"].Int < 0;
	glm::vec2 HealthDamageOffset(0, HasManaDamage ? -3 : 0);

	// Draw damage dealt
	TextColor.a = AlphaPercent;
	ae::_Font *Font = _ActionResult::GetFont(Buffer.str());
	Font->DrawText(Buffer.str(), glm::ivec2(DrawPosition + HealthDamageOffset * ae::_Element::GetUIScale()), HasManaDamage ? ae::CENTER_BOTTOM : ae::CENTER_MIDDLE, TextColor);

	// Draw mana damage
	if(HasManaDamage) {
		Buffer.str("");
		ae::FormatSI<int64_t>(Buffer, std::abs(ActionResult.Target.Values["Mana"].Int), ae::RoundDown1);

		TextColor = ae::Assets.Colors["light_blue"];
		TextColor.a = AlphaPercent;

		ae::_Font *Font = _ActionResult::GetFont(Buffer.str());
		Font->DrawText(Buffer.str(), glm::ivec2(DrawPosition + glm::vec2(0, 3) * ae::_Element::GetUIScale()), ae::CENTER_TOP, TextColor);
	}
}

// Sends an action selection to the server
void _Battle::ClientSetAction(uint8_t ActionBarSlot) {

	// Get aliases
	auto &ClientCharacter = ClientPlayer->Character;
	auto &ClientFighter = ClientPlayer->Fighter;
	auto &ClientLastTarget = ClientFighter->LastTarget;
	auto &Action = ClientCharacter->Action;

	// Check slot
	if(ActionBarSlot >= ClientCharacter->ActionBar.size())
		return;

	// Check if alive
	if(!ClientCharacter->IsAlive())
		return;

	// Player already locked in
	if(Action.Confirmed)
		return;

	// Get action from action bar
	_Action BarAction;
	ClientCharacter->GetActionFromActionBar(BarAction, ActionBarSlot);
	if(!BarAction.Item)
		return;

	// Check for changing an action
	bool ChangingAction = Action.Item && BarAction != Action;

	// Action has been chosen, select target
	if(!Action.Item || ChangingAction) {

		// Check if item can be used
		_ActionResult ActionResult;
		ActionResult.Source.Object = ClientPlayer;
		ActionResult.Scope = ScopeType::BATTLE;
		ActionResult.ActionUsed = BarAction;
		if(!BarAction.Item->CanUse(Scripting, ActionResult))
			return;

		// Show tutorial message
		if(Config.ShowTutorial && ClientCharacter->Level == 1)
			HUD.SetMessage("Press up/down or use mouse to change targets. Press " + ae::Actions.GetInputNameForAction(Action::GAME_SKILL1 + ActionBarSlot) + " again to confirm.", HUD_MESSAGE_TIMEOUT);

		// Get side to start action on
		int StartingSide = ClientFighter->GetStartingSide(BarAction.Item);

		// Set initial target to self when targeting ally/self
		if(StartingSide == ClientFighter->BattleSide && !ClientLastTarget[StartingSide])
			ClientLastTarget[StartingSide] = ClientPlayer;

		// Set targets
		Action.Item = BarAction.Item;
		Action.ActionBarSlot = ActionBarSlot;
		Action.Target = SetTargets(ClientPlayer, Action, ClientLastTarget[StartingSide], ClientTargets);
		ClientLastTarget[StartingSide] = Action.Target;

		// No target
		if(!Action.Target)
			Action.Clear();
	}
	// Confirm action
	else if(Action.Target) {

		// Update HUD
		if(Config.ShowTutorial && ClientCharacter->Level == 1)
			HUD.SetMessage("", 0.0);

		// Check if action can be used
		_ActionResult ActionResult;
		ActionResult.Source.Object = ClientPlayer;
		ActionResult.Scope = ScopeType::BATTLE;
		ActionResult.ActionUsed = BarAction;
		if(!Action.Item->CanUse(Scripting, ActionResult)) {
			Action.Clear();
			ClientTargets.clear();
			return;
		}

		// Remember target
		ClientLastTarget[Action.Target->Fighter->BattleSide] = Action.Target;

		// Notify server
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::ACTION_USE);
		Packet.WriteBit(1);
		Packet.Write<uint8_t>(ActionBarSlot);
		Packet.Write<ae::NetworkIDType>(Action.Target->NetworkID);
		ClientNetwork->SendPacket(Packet);

		// Set state
		Action.Confirmed = true;
	}
}

// Set targets for an object and return first target
_Object *_Battle::SetTargets(_Object *SourceObject, const _Action &Action, _Object *InitialTarget, std::vector<_Object *> &Targets) {
	_Fighter *SourceFighter = SourceObject->Fighter;

	// Clear existing targets
	Targets.clear();

	// Add self target
	if(Action.GetTargetType() == TargetType::SELF) {
		Targets.push_back(SourceObject);
		return SourceObject;
	}

	// Get battle side
	int Side = InitialTarget ? InitialTarget->Fighter->BattleSide : SourceFighter->GetStartingSide(Action.Item);

	// Get list of objects on target side
	std::vector<_Object *> SideObjects;
	SideObjects.reserve(BATTLE_MAX_OBJECTS_PER_SIDE);
	GetObjectList(Side, SideObjects);
	if(SideObjects.empty())
	   return nullptr;

	// Get iterator to initial target
	auto Iterator = SideObjects.end();
	if(InitialTarget && Action.Item->CanTarget(Scripting, SourceObject, InitialTarget))
		Iterator = std::find(SideObjects.begin(), SideObjects.end(), InitialTarget);

	// Start at beginning
	if(Iterator == SideObjects.end())
		Iterator = SideObjects.begin();

	// Handle corpse AOE target type
	bool CorpseAOE = false;
	int TargetCount = 0;
	if(Action.GetTargetType() == TargetType::ENEMY_CORPSE_AOE) {
		CorpseAOE = true;
		TargetCount++;
	}

	// Get target count
	if(Action.TargetCountOverride)
		TargetCount = Action.TargetCountOverride;
	else
		TargetCount += Action.Item->GetTargetCount(Scripting, SourceObject);

	// Find targets
	int TargetIndex = 1;
	bool IsFirstTarget = true;
	for(size_t i = 0; i < SideObjects.size(); i++) {

		// First Corpse AOE target needs to be dead, the rest need to be alive
		bool ForceTargetAlive = false;
		if(CorpseAOE && !IsFirstTarget)
			ForceTargetAlive = true;

		// Check for valid target
		_Object *Target = *Iterator;
		if(Action.Item->CanTarget(Scripting, SourceObject, Target, ForceTargetAlive)) {
			IsFirstTarget = false;

			// Add to list of targets
			Targets.push_back(Target);
			Target->Fighter->TargetIndex = TargetIndex++;

			// Update count
			TargetCount--;
			if(TargetCount <= 0)
				break;
		}

		// Update target
		++Iterator;
		if(Iterator == SideObjects.end())
			Iterator = SideObjects.begin();
	}

	// Return first target
	if(Targets.size())
		return Targets.front();

	return nullptr;
}

// Change targets on the client in a direction by a certain amount, or change battle sides
void _Battle::ClientChangeTarget(int Direction, bool NextColumn, bool ChangeSides) {
	if(!ClientNetwork)
	   return;

	_Character *ClientCharacter = ClientPlayer->Character;
	_Fighter *ClientFighter = ClientPlayer->Fighter;
	auto &Action = ClientCharacter->Action;

	// Check for action confirmed
	if(!Action.Unconfirmed())
		return;

	// Check if client is alive
	if(!ClientCharacter->IsAlive())
		return;

	// Check for targets
	if(ClientTargets.empty())
		return;

	// Can't change self targeting actions
	if(Action.Item->TargetID == TargetType::SELF)
		return;

	// Get current target side
	int TargetSide = Action.Target->Fighter->BattleSide;

	// Change sides
	if(Action.Item->TargetID == TargetType::ANY && ChangeSides)
		TargetSide = !TargetSide;

	// Get list of available targets
	std::vector<_Object *> Targets;
	Targets.reserve(BATTLE_MAX_OBJECTS_PER_SIDE);
	size_t StartIndex = GetPotentialTargets(TargetSide, Action.Item, ClientPlayer, ClientFighter->LastTarget[TargetSide], Targets);
	if(Targets.empty())
		return;

	// Get valid target index
	if(StartIndex >= Targets.size())
		StartIndex = 0;

	// Move cursor
	auto Iterator = Targets.begin() + StartIndex;
	auto PreviousIterator = Iterator;
	glm::vec2 &StartOffset = (*Iterator)->Fighter->BattleBaseOffset;
	int NextColumnCount = 0;
	for(size_t i = 0; i < Targets.size(); i++) {

		// Update iterator
		PreviousIterator = Iterator;
		if(NextColumn || Direction > 0) {
			++Iterator;
			if(Iterator == Targets.end())
				Iterator = Targets.begin();
		}
		else if(Direction < 0) {
			if(Iterator == Targets.begin())
				Iterator = Targets.end();
			--Iterator;
		}

		// Move once unless cursor is moving to next column
		if(!NextColumn)
			break;

		// Check fighters on next column
		glm::vec2 &FighterOffset = (*Iterator)->Fighter->BattleBaseOffset;
		if(StartOffset.x != FighterOffset.x) {

			// Exit when cursor finds same row
			if(FighterOffset.y == StartOffset.y)
				break;

			// Exit when cursor moves past current row
			if(FighterOffset.y > StartOffset.y) {

				// Prefer fighter above current row
				if(NextColumnCount)
					Iterator = PreviousIterator;

				break;
			}

			NextColumnCount++;
		}
		// Exit when coming back to starting column
		else if(NextColumnCount) {
			Iterator = PreviousIterator;
			break;
		}
	}

	// Set new targets
	Action.Target = SetTargets(ClientPlayer, Action, *Iterator, ClientTargets);
	ClientFighter->LastTarget[TargetSide] = Action.Target;
}

// Add an object to the battle
void _Battle::AddObject(_Object *Object, uint8_t Side, bool Join) {
	Object->Battle = this;
	Object->Fighter->BattleSide = Side;
	Object->Fighter->LastTarget.clear();
	Object->Character->Action.Clear();
	Object->Character->ResetUIState();
	Object->Fighter->JoinedBattle = Join;
	Object->Fighter->GoldStolen = 0;
	Object->Fighter->ActionResultCount = -1;
	if(Server) {
		Object->Character->GenerateNextBattle();
		Object->Fighter->TurnTimer = std::clamp(ae::GetRandomReal(0, BATTLE_MAX_START_TURNTIMER) + Object->Character->Attributes["Initiative"].Mult(), 0.0, 1.0);

		// Send player join packet to current objects
		if(Join) {
			ae::_Buffer Packet;
			Packet.Write<PacketType>(PacketType::BATTLE_JOIN);
			Object->SerializeBattle(Packet);
			BroadcastPacket(Packet);
		}
	}

	// Count objects and set slots
	SideCount[Side]++;
	Objects.push_back(Object);

	// Object joining on the client
	if(!Server && Join) {

		// Adjust existing battle elements and create new one
		int SideIndex = 0;
		for(auto &AdjustObject : Objects) {
			if(AdjustObject->Fighter->BattleSide == Side) {
				if(AdjustObject == Object)
					CreateBattleElements(SideIndex, AdjustObject);
				else
					AdjustBattleElements(SideIndex, AdjustObject);

				SideIndex++;
			}
		}
	}
}

// Get a list of objects from a side
void _Battle::GetObjectList(int Side, std::vector<_Object *> &SideObjects) {
	for(auto &Object : Objects) {
		if(Object->Fighter->BattleSide == Side)
			SideObjects.push_back(Object);
	}
}

// Get list of objects on a side that an object can target and return index to existing or next available target
size_t _Battle::GetPotentialTargets(int Side, const _Item *Item, _Object *SourceObject, _Object *StartObject, std::vector<_Object *> &Targets) {

	// Build target list
	size_t Index = 0;
	size_t ExistingIndex = (size_t)-1;
	bool GetNext = false;
	for(auto Iterator = Objects.begin(); Iterator != Objects.end(); ++Iterator) {
		_Object *Object = *Iterator;

		// Check side
		if(Object->Fighter->BattleSide != Side)
			continue;

		// Check target
		if(!Item->CanTarget(Scripting, SourceObject, Object))
			continue;

		// Keep track of next object
		if(Object == StartObject)
			GetNext = true;

		// Save index
		if(GetNext) {
			ExistingIndex = Index;
			GetNext = false;
		}

		// Add to list
		Targets.push_back(Object);
		Index++;
	}

	return ExistingIndex;
}

// Starts the battle and notifies the players
void _Battle::Serialize(ae::_Buffer &Data) {

	// Write flags
	Data.WriteBit(Boss);

	// Write object count
	Data.Write<uint8_t>((uint8_t)Objects.size());

	// Write stats
	Data.Write<float>(Time);
	Data.Write<float>(Duration);
	Data.Write<uint32_t>(Zone);
	Data.Write<int64_t>(Difficulty);

	// Write object information
	for(auto &Object : Objects)
		Object->SerializeBattle(Data);
}

// Unserialize for network
void _Battle::Unserialize(ae::_Buffer &Data) {

	// Read flags
	Boss = Data.ReadBit();

	// Get object count
	int ObjectCount = Data.Read<uint8_t>();

	// Get stats
	Time = Data.Read<float>();
	Duration = Data.Read<float>();
	Zone = Data.Read<uint32_t>();
	Difficulty = Data.Read<int64_t>();

	// Get UI elements
	BattleElement = ae::Assets.Elements["element_battle"];
	TimerElement = ae::Assets.Elements["label_battle_timer"];
	DifficultyElement = ae::Assets.Elements["label_battle_difficulty"];
	if(BattleElement)
		BattleElement->SetActive(true);
	if(TimerElement)
		TimerElement->SetActive(false);
	if(DifficultyElement) {
		DifficultyElement->SetActive(false);

		// Set text
		std::stringstream Buffer;
		Buffer << "Difficulty ";
		ae::FormatSI(Buffer, Difficulty);
		Buffer << "%";
		DifficultyElement->Text = Buffer.str();
	}

	// Get object information
	for(int i = 0; i < ObjectCount; i++) {

		// Get object data
		ae::NetworkIDType NetworkID = Data.Read<ae::NetworkIDType>();
		uint32_t DatabaseID = Data.Read<uint32_t>();

		// Get object pointers
		_Object *Object = nullptr;
		if(DatabaseID) {
			Object = Manager->CreateWithID(NetworkID);
			Object->Stats = Stats;
			Object->CreateComponents();
			Object->Monster->DatabaseID = DatabaseID;
			Stats->SetMonsterStats(Object);
		}
		else {
			Object = Manager->GetObject(NetworkID);
			Object->Stats = Stats;
		}

		// Get battle stats
		Object->Scripting = Scripting;
		Object->UnserializeBattle(Data, ClientPlayer == Object);

		// Add object
		AddObject(Object, Object->Fighter->BattleSide);
	}

	// Log
	if(Config.LogStatChanges) {
		Framework.Log << "BattleStart" << std::endl;
		Framework.Log << " Difficulty = " << Difficulty << std::endl;
		Framework.Log << " Fighters" << std::endl;
	}

	// Set object position offsets and create ui elements
	int SideIndex[2] = { 0, 0 };
	for(auto &Object : Objects) {
		CreateBattleElements(SideIndex[Object->Fighter->BattleSide], Object);
		SideIndex[Object->Fighter->BattleSide]++;

		// Log
		if(Config.LogStatChanges)
			Framework.Log << "  " << (int)Object->Fighter->BattleSide << ": " << Object->Name << "(" << Object->NetworkID << ")" << std::endl;
	}
}

// End battle and give rewards
void _Battle::ServerEndBattle() {

	// Get statistics for each side
	_BattleResult SideStats[2];
	std::vector<_Object *> SideObjects[2];
	SideObjects[0].reserve(BATTLE_MAX_OBJECTS_PER_SIDE);
	SideObjects[1].reserve(BATTLE_MAX_OBJECTS_PER_SIDE);
	for(int Side = 0; Side < 2; Side++) {

		// Get a list of objects that are still in the battle
		GetObjectList(Side, SideObjects[Side]);

		// Loop through objects
		for(auto &Object : SideObjects[Side]) {

			// Keep track of players
			if(!Object->IsMonster()) {
				if(Object->Character->IsAlive())
					SideStats[Side].AlivePlayerCount++;
				SideStats[Side].PlayerCount++;
			}
			else {
				if(Object->Character->IsAlive())
					SideStats[Side].AliveMonsterCount++;
				SideStats[Side].MonsterCount++;
			}

			if(Object->Fighter->JoinedBattle)
				SideStats[Side].JoinedCount++;

			// Tally alive objects
			if(Object->Character->IsAlive()) {
				SideStats[Side].AliveCount++;
				SideStats[Side].Dead = false;
			}

			// Sum experience and gold
			SideStats[Side].TotalExperienceGiven += Object->Monster->ExperienceGiven;

			// Calculate gold based on monster or player
			SideStats[Side].TotalGoldStolen += Object->Fighter->GoldStolen;
			SideStats[Side].TotalBounty += Object->Character->Attributes["Bounty"].Int;
			if(Object->IsMonster())
				SideStats[Side].TotalGoldGiven += Object->Monster->GoldGiven;
			else
				SideStats[Side].TotalGoldGiven += Object->Character->Attributes["Bounty"].Int + (int64_t)(Object->Character->Attributes["Gold"].Int * BountyEarned + 0.5f);
		}

		SideStats[Side].TotalExperienceGiven = std::ceil(SideStats[Side].TotalExperienceGiven);
		SideStats[Side].TotalGoldGiven = std::ceil(SideStats[Side].TotalGoldGiven);
	}

	// Get winning side
	int WinningSide;
	if(SideStats[0].Dead && SideStats[1].Dead)
		WinningSide = -1;
	else if(SideStats[0].Dead)
		WinningSide = 1;
	else
		WinningSide = 0;

	// Check for a winning side
	if(WinningSide != -1 && SideObjects[WinningSide].size()) {

		// Divide up rewards
		for(int Side = 0; Side < 2; Side++) {
			int OtherSide = !Side;
			int DivideCount = SideStats[Side].AlivePlayerCount;
			if(DivideCount <= 0)
				continue;

			// Divide experience up
			if(SideStats[OtherSide].TotalExperienceGiven > 0) {
				SideStats[Side].ExperiencePerCharacter = SideStats[OtherSide].TotalExperienceGiven / DivideCount;
				if(SideStats[Side].ExperiencePerCharacter <= 0)
					SideStats[Side].ExperiencePerCharacter = 1;
			}

			// Divide gold up
			if(SideStats[OtherSide].TotalGoldGiven > 0) {
				SideStats[Side].GoldPerCharacter = SideStats[OtherSide].TotalGoldGiven / DivideCount;
				SideStats[Side].GoldStolenPerCharacter = SideStats[OtherSide].TotalGoldStolen / DivideCount;
				if(SideStats[Side].GoldPerCharacter <= 0)
					SideStats[Side].GoldPerCharacter = 1;
			}
		}

		// Get list of objects that get rewards
		std::vector<_Object *> RewardObjects;
		RewardObjects.reserve(BATTLE_MAX_OBJECTS_PER_SIDE);
		for(auto &Object : SideObjects[WinningSide]) {
			if(Object->Character->IsAlive() && !Object->IsMonster())
				RewardObjects.push_back(Object);
		}

		// Check for reward recipients
		if(RewardObjects.size() && !PVP) {

			// Boss drops aren't divided up and only come from zonedrop
			if(Boss) {
				std::vector<std::pair<uint32_t, int>> ItemDrops;
				ItemDrops.reserve(10);

				// Get items from zonedrops
				Stats->Database->PrepareQuery("SELECT item_id, count FROM zonedrop WHERE zone_id = @zone_id");
				Stats->Database->BindInt(1, Zone);
				while(Stats->Database->FetchRow()) {
					uint32_t ItemID = Stats->Database->GetInt<uint32_t>("item_id");
					int Count = Stats->Database->GetInt<int>("count");
					ItemDrops.push_back(std::pair(ItemID, Count));
				}
				Stats->Database->CloseQuery();

				// Hand out drops
				for(auto &ItemDrop : ItemDrops) {
					for(auto &Object : RewardObjects) {

						// Give drops to players that don't have the boss on cooldown
						if(Object->Character->IsZoneOnCooldown(Zone))
							continue;

						int Count = ItemDrop.second * (Object->Character->Attributes["DropRate"].Mult() + 0.001) * Multiplier;
						for(int i = 0; i < Count; i++)
							Object->Fighter->ItemDropsReceived.push_back(ItemDrop.first);
					}
				}
			}
			else {

				// Get shuffled copy of reward objects
				std::vector<_Object *> ShuffledRewardObjects { std::begin(RewardObjects), std::end(RewardObjects) };
				std::shuffle(ShuffledRewardObjects.begin(), ShuffledRewardObjects.end(), ae::RandomGenerator);

				// Iterate through monsters
				size_t PlayerIndex = 0;
				for(auto &Object : SideObjects[!WinningSide]) {
					if(!Object->IsMonster())
						continue;

					_Object *Player = ShuffledRewardObjects[PlayerIndex++];
					if(PlayerIndex >= ShuffledRewardObjects.size())
						PlayerIndex = 0;

					// Generate item
					std::vector<uint32_t> ItemDrops;
					ItemDrops.reserve(10);
					Stats->GenerateItemDrops(Object->Monster->DatabaseID, 1, ItemDrops, Multiplier * Player->Character->Attributes["DropRate"].Mult());
					for(auto &ItemID : ItemDrops)
						Player->Fighter->ItemDropsReceived.push_back(ItemID);
				}
			}
		}
	}

	// Build results
	std::map<_Object::_SummonKey, int> PlayerSummons;
	for(auto &Object : Objects) {
		Object->Controller->InputStates.clear();
		Object->Character->Action.Clear();

		// Get rewards
		int64_t ExperienceEarned = 0;
		int64_t GoldEarned = 0;
		if(Object->Character->IsAlive()) {
			double ExperienceMultiplier = Object->Character->Attributes["ExperienceGained"].Mult();
			double GoldMultiplier = Object->Character->Attributes["GoldGained"].Mult();

			// Get rewards if boss isn't on cooldown
			if(!Object->Character->IsZoneOnCooldown(Zone)) {
				ExperienceEarned = SideStats[WinningSide].ExperiencePerCharacter;
				GoldEarned = SideStats[WinningSide].GoldPerCharacter;

				// Boost xp/gold gain
				if(!PVP) {
					if(double(ExperienceEarned) * ExperienceMultiplier > GAME_MAX_NUMBER)
						ExperienceEarned = GAME_MAX_NUMBER;
					else
						ExperienceEarned *= ExperienceMultiplier;

					if(double(GoldEarned) * GoldMultiplier > GAME_MAX_NUMBER)
						GoldEarned = GAME_MAX_NUMBER;
					else
						GoldEarned *= GoldMultiplier;
				}

				if(Zone) {

					// Start cooldown timer
					if(Cooldown > 0.0)
						Object->Character->BossCooldowns[Zone] = std::max(Cooldown * Object->Character->Attributes["BossCooldowns"].Mult(), GAME_BOSS_MIN_COOLDOWN) / Multiplier;

					// Add to kill count
					if(Boss) {
						if(Cooldown > 0.0) {
							Object->Character->BossKills[Zone]++;
							if(Cooldown <= GAME_BOSS_STRONGER_THRESHOLD)
								Server->SendMessage(Object->Peer, std::string("The soul grows stronger"), "yellow");
						}
						else if(Cooldown < 0.0) {
							Object->Character->BossCooldowns[Zone] = -1.0;
							Server->SendMessage(Object->Peer, std::string("The soul rests for eternity"), "yellow");
						}
						else
							Object->Character->BossKills[Zone] = 0;
					}
				}
			}

			// Handle pickpocket
			GoldEarned += SideStats[WinningSide].GoldStolenPerCharacter;

			Object->Character->Attributes["PlayerKills"].Int += SideStats[!WinningSide].PlayerCount;
			Object->Character->Attributes["MonsterKills"].Int += SideStats[!WinningSide].MonsterCount;
			if(PVP && Object->Fighter->BattleSide == BATTLE_PVP_ATTACKER_SIDE) {
				if(BountyEarned) {
					Object->Character->Attributes["Bounty"].Int += GoldEarned;
					if(Object->Character->Attributes["Bounty"].Int) {
						std::stringstream Buffer;
						Buffer << "Player " << Object->Name << " now has a bounty of ";
						ae::FormatSI<int64_t>(Buffer, Object->Character->Attributes["Bounty"].Int);
						Buffer << " gold!";
						Server->BroadcastMessage(nullptr, Buffer.str(), "cyan");
						Server->Log << "[BOUNTY] " << Buffer.str() << std::endl;
					}
				}
			}
		}
		else {

			// Give stolen gold back if dead
			Object->Character->UpdateGold(-Object->Fighter->GoldStolen);

			if(PVP) {

				// Fugitive was bounty hunted, apply regular death penalty
				double AttackPenalty = BountyEarned;
				if(BountyEarned == 0.0)
					AttackPenalty = PLAYER_DEATH_GOLD_PENALTY;

				// Both sides died
				if(WinningSide == -1) {

					// Victim applies penalty first, then gets bounty share
					if(Object->Fighter->BattleSide == BATTLE_PVP_VICTIM_SIDE) {
						Object->ApplyDeathPenalty(true, PLAYER_DEATH_GOLD_PENALTY, 0);
						GoldEarned = SideStats[BATTLE_PVP_ATTACKER_SIDE].TotalBounty / SideStats[BATTLE_PVP_VICTIM_SIDE].PlayerCount;
					}
					// Attacker loses contract
					else {
						Object->ApplyDeathPenalty(true, AttackPenalty, Object->Character->Attributes["Bounty"].Int);
					}
				}
				else {
					Object->ApplyDeathPenalty(true, AttackPenalty, Object->Character->Attributes["Bounty"].Int);
				}
			}
			else
				Object->ApplyDeathPenalty(true, PLAYER_DEATH_GOLD_PENALTY, 0);
		}

		// Log battle
		if(Object->Character->Bot || Object->Peer) {
			Server->Log
				<< "[BATTLE] Player " << Object->Name << (Object->Character->IsAlive() ? " lives" : " dies")
				<< " zone " << Name
				<< " ("
				<< " character_id=" << Object->Character->ID
				<< " zone_id=" << Zone
				<< " boss=" << Boss
				<< " diff=" << Difficulty
				<< " mult=" << Multiplier
				<< " time=" << Time
				<< " pvp=" << PVP
				<< " exp=" << ExperienceEarned
				<< " gold=" << GoldEarned
				<< " stole=" << Object->Fighter->GoldStolen
				<< " )"
				<< std::endl;
		}

		// Update stats
		int CurrentLevel = Object->Character->Level;
		Object->Fighter->GoldStolen = 0;
		Object->Character->UpdateExperience(ExperienceEarned);
		Object->Character->UpdateGold(GoldEarned);
		Object->Character->CalculateStats();
		int NewLevel = Object->Character->Level;
		if(NewLevel > CurrentLevel) {
			if(Object->Peer)
				Server->SendMessage(Object->Peer, std::string("You are now level " + std::to_string(NewLevel) + "!"), "yellow");

			Object->Character->Attributes["Health"].Int = Object->Character->Attributes["MaxHealth"].Int;
			Object->Character->Attributes["Mana"].Int = Object->Character->Attributes["MaxMana"].Int;
		}

		// Add summon to map of players and their summon buffs
		Object->AddSummonKey(PlayerSummons);

		// Get boss cooldown state
		bool BossOnCooldown = Object->Character->BossCooldowns.find(Zone) != Object->Character->BossCooldowns.end();

		// Sort item drops
		std::unordered_map<uint32_t, int> SortedItems;
		for(auto &ItemID : Object->Fighter->ItemDropsReceived)
			SortedItems[ItemID]++;

		Object->Fighter->ItemDropsReceived.clear();

		// Update bot goal
		if(Object->Character->Bot && Scripting->StartMethodCall("Bot_Server", "DetermineNextGoal")) {
			Scripting->PushObject(Object);
			Scripting->MethodCall(1, 0);
			Scripting->FinishMethodCall();
		}

		// Notify client
		if(!Object->Peer)
			continue;

		// Write results
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::BATTLE_END);
		Packet.Write<float>(Time);
		Packet.Write<int>((int)Object->Character->Attributes["PlayerKills"].Int);
		Packet.Write<int>((int)Object->Character->Attributes["MonsterKills"].Int);
		Packet.Write<int64_t>(Object->Character->Attributes["GoldLost"].Int);
		Packet.Write<int64_t>(Object->Character->Attributes["Bounty"].Int);
		Packet.Write<int64_t>(ExperienceEarned);
		Packet.Write<int64_t>(GoldEarned);
		Packet.Write<uint8_t>(BossOnCooldown);

		// Write item count
		size_t ItemCount = SortedItems.size();
		Packet.Write<uint8_t>((uint8_t)ItemCount);

		// Write items
		for(auto &Iterator : SortedItems) {
			Packet.Write<uint32_t>(Iterator.first);
			Packet.Write<uint8_t>(0);
			Packet.Write<uint8_t>((uint8_t)Iterator.second);
			const _Item *Item = Stats->Items.at(Iterator.first);
			int Added = Object->Inventory->AddItem(Item, 0, Iterator.second, true);
			Server->SendInventoryFullMessage(Object->Peer, Item, Added);
		}

		// Send data to client
		Server->Network->SendPacket(Packet, Object->Peer);
		Server->SendHUD(Object->Peer);
	}

	// Handle summon updates
	_Object::UpdateSummonStatusEffects(PlayerSummons);

	// Update event
	if(Boss)
		Server->UpdateEventCount();

	Deleted = true;
}

// Calculates a screen position for a slot
void _Battle::GetBattleOffset(int SideIndex, _Object *Object) {
	if(!BattleElement)
		return;

	// Get column index
	int Column = SideIndex / BATTLE_ROWS_PER_SIDE;

	// Check sides
	if(Object->Fighter->BattleSide == 0)
		Object->Fighter->BattleBaseOffset.x = -240 - Column * BATTLE_COLUMN_SPACING;
	else
		Object->Fighter->BattleBaseOffset.x = 100  + Column * BATTLE_COLUMN_SPACING;

	// Get row count for a given column
	float RowCount = (float)SideCount[Object->Fighter->BattleSide] / BATTLE_ROWS_PER_SIDE - Column;
	if(RowCount >= 1)
		RowCount = BATTLE_ROWS_PER_SIDE;
	else
		RowCount *= BATTLE_ROWS_PER_SIDE;

	// Divide space into RowCount parts, then divide that by 2
	int SpacingY = (int)((BattleElement->BaseSize.y / RowCount) / 2);

	// Place slots in between main divisions
	Object->Fighter->BattleBaseOffset.y = SpacingY * (2 * (SideIndex % BATTLE_ROWS_PER_SIDE) + 1) - BattleElement->BaseSize.y/2;
}

// Adjust existing battle elements
void _Battle::AdjustBattleElements(int SideIndex, _Object *Object) {

	// Get position on screen
	GetBattleOffset(SideIndex, Object);

	// Update position
	if(Object->Fighter->BattleElement) {
		Object->Fighter->BattleElement->BaseOffset = Object->Fighter->BattleBaseOffset;
		Object->Fighter->BattleElement->CalculateBounds();
	}
}

// Create battle element for an object
void _Battle::CreateBattleElements(int SideIndex, _Object *Object) {

	// Get position on screen
	GetBattleOffset(SideIndex, Object);

	// Create ui element
	if(BattleElement)
		Object->Fighter->CreateBattleElement(BattleElement);

	// Add status effect ui elements
	Object->Character->CreateAllStatusEffectElements();
}

// Removes an object from battle
void _Battle::RemoveObject(_Object *RemoveObject) {

	// Remove action results
	for(auto Iterator = ActionResults.begin(); Iterator != ActionResults.end(); ) {
		_ActionResult &ActionResult = *Iterator;
		if(ActionResult.Source.Object == RemoveObject || ActionResult.Target.Object == RemoveObject) {
			Iterator = ActionResults.erase(Iterator);
		}
		else
			++Iterator;
	}

	// Remove object from other objects
	for(auto &Object : Objects) {
		if(!Object)
			continue;

		if(Object->Fighter) {

			// Remove object from each fighter's last target
			for(auto &Entry : Object->Fighter->LastTarget) {
				if(Entry.second == RemoveObject)
					Object->Fighter->LastTarget[Entry.first] = nullptr;
			}
		}

		if(Object->Character) {

			// Remove source in status effect
			for(auto &StatusEffect : Object->Character->StatusEffects) {
				if(RemoveObject == StatusEffect->Source)
					StatusEffect->Source = nullptr;
			}

			// Remove from action target
			if(Object->Character->Action.Target == RemoveObject)
				Object->Character->Action.Target = nullptr;

			// Remove from client targets
			for(auto Iterator = ClientTargets.begin(); Iterator != ClientTargets.end(); ) {
				if(*Iterator == RemoveObject)
					Iterator = ClientTargets.erase(Iterator);
				else
					++Iterator;
			}
		}
	}

	// Remove object and their summons from battle
	for(auto Iterator = Objects.begin(); Iterator != Objects.end(); ) {
		_Object *Object = *Iterator;
		if(Object != RemoveObject && Object->Monster->Owner != RemoveObject) {
			++Iterator;
			continue;
		}

		// Broadcast object leaving
		if(Server) {
			ae::_Buffer Packet;
			Packet.Write<PacketType>(PacketType::BATTLE_LEAVE);
			Packet.Write<ae::NetworkIDType>(Object->NetworkID);
			BroadcastPacket(Packet);
		}

		// Update battle
		SideCount[Object->Fighter->BattleSide]--;
		Object->StopBattle();
		if(Object->IsMonster())
			Object->Deleted = true;

		// Remove
		Iterator = Objects.erase(Iterator);
	}
}

// Get list of allies and enemies from object list
void _Battle::GetSeparateObjectList(uint8_t Side, std::vector<_Object *> &Allies, std::vector<_Object *> &Enemies) {
	for(const auto &Object : Objects) {
		if(Object->Deleted)
			continue;

		if(Object->Fighter->BattleSide == Side)
			Allies.push_back(Object);
		else if(Object->Character->IsAlive())
			Enemies.push_back(Object);
	}
}

// Get number of peers in battle
int _Battle::GetPeerCount() {
	int PeerCount = 0;
	for(auto &Object : Objects) {
		if(Object->Peer)
			PeerCount++;
	}

	return PeerCount;
}

// Handle player input, return true if mouse combat should be enabled
bool _Battle::ClientHandleInput(size_t Action, bool MouseCombat) {

	switch(Action) {
		case Action::GAME_SKILL1:
		case Action::GAME_SKILL2:
		case Action::GAME_SKILL3:
		case Action::GAME_SKILL4:
		case Action::GAME_SKILL5:
		case Action::GAME_SKILL6:
		case Action::GAME_SKILL7:
		case Action::GAME_SKILL8:
			ClientSetAction((uint8_t)(Action - Action::GAME_SKILL1));
			return MouseCombat;
		break;
		case Action::GAME_ITEM1:
		case Action::GAME_ITEM2:
		case Action::GAME_ITEM3:
		case Action::GAME_ITEM4:
			ClientSetAction((uint8_t)(Action - Action::GAME_ITEM1 + ACTIONBAR_BELT_STARTS));
			return MouseCombat;
		break;
		case Action::GAME_UP:
			ClientChangeTarget(-1, false, false);
			return false;
		break;
		case Action::GAME_DOWN:
			ClientChangeTarget(1, false, false);
			return false;
		break;
		case Action::GAME_LEFT:
			ClientChangeTarget(-1, true, false);
			return false;
		break;
		case Action::GAME_RIGHT:
			ClientChangeTarget(1, true, false);
			return false;
		break;
		case Action::GAME_USE:
			ClientChangeTarget(0, false, true);
			return false;
		break;
	}

	return false;
}

// Handle action from another player
void _Battle::ClientHandlePlayerAction(ae::_Buffer &Data) {

	ae::NetworkIDType NetworkID = Data.Read<ae::NetworkIDType>();
	uint32_t ItemID = Data.Read<uint32_t>();

	_Object *Object = Manager->GetObject(NetworkID);
	if(Object)
		Object->Character->Action.Item = Stats->Items.at(ItemID);
}

// Send a packet to all players
void _Battle::BroadcastPacket(ae::_Buffer &Data) {

	// Send packet to all players
	for(auto &Object : Objects) {
		if(!Object->Deleted && Object->Peer) {
			Server->Network->SendPacket(Data, Object->Peer);
		}
	}
}

// Broadcast an object's current list of status effects
void _Battle::BroadcastStatusEffects(_Object *UpdatedObject) {
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::PLAYER_STATUSEFFECTS);
	Packet.Write<ae::NetworkIDType>(UpdatedObject->NetworkID);
	UpdatedObject->SerializeStatusEffects(Packet);

	// Send packet to all players
	for(auto &Object : Objects) {
		if(UpdatedObject != Object && !Object->Deleted && Object->Peer)
			Server->Network->SendPacket(Packet, Object->Peer);
	}
}
