/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <ae/baseobject.h>
#include <ae/log.h>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <map>
#include <vector>

// Forward Declarations
class _Battle;
class _Buff;
class _Character;
class _Controller;
class _Fighter;
class _Inventory;
class _Map;
class _Monster;
class _Save;
class _Scripting;
class _Server;
class _StatChange;
class _Stats;
class _StatusEffect;
struct _Slot;
struct _Tile;

namespace ae {
	template<class T> class _Manager;
	class _Buffer;
	class _Element;
	class _Peer;
	class _Texture;
}

namespace Json {
	class Value;
}

// Classes
class _Object : public ae::_BaseObject {

	public:

		enum InputStateType {
			INPUT_UP    = (1 << 0),
			INPUT_DOWN  = (1 << 1),
			INPUT_LEFT  = (1 << 2),
			INPUT_RIGHT = (1 << 3),
			INPUT_USE   = (1 << 4),
		};

		// Pair object to summon buff
		typedef std::pair<_Object *, const _Buff *> _SummonKey;

		~_Object() override;

		// Components
		void CreateComponents();

		// Updates
		void Update(double FrameTime) override;
		void UpdateBot(double FrameTime);
		void Render(glm::vec4 &ViewBounds, const _Object *ClientPlayer=nullptr);
		void RenderBattle(_Object *ClientPlayer, double Time, bool ShowLevel);

		// Save
		void SerializeSaveData(Json::Value &Data) const;
		void UnserializeSaveData(const std::string &JsonString);

		// Network
		void SerializeCreate(ae::_Buffer &Data);
		void SerializeUpdate(ae::_Buffer &Data);
		void SerializeStats(ae::_Buffer &Data);
		void SerializeBattle(ae::_Buffer &Data);
		void SerializeStatusEffects(ae::_Buffer &Data);
		void SerializeBossCooldowns(ae::_Buffer &Data);
		void UnserializeCreate(ae::_Buffer &Data);
		void UnserializeStats(ae::_Buffer &Data);
		void UnserializeBattle(ae::_Buffer &Data, bool IsSelf);
		void UnserializeStatusEffects(ae::_Buffer &Data, bool CreateHUDElements=true);
		void UnserializeBossCooldowns(ae::_Buffer &Data);
		void SendPacket(ae::_Buffer &Packet, bool Broadcast=true);
		void SendActionClear();

		// Logging
		void SetLogging(bool Value);

		// Stats
		bool IsMonster() const;
		void UpdateStats(_StatChange &StatChange, _Object *Source=nullptr, int TargetCount=0);
		void ApplyDeathPenalty(bool InBattle, double Penalty, int64_t BountyLoss);
		bool ShowTriggerMessage();

		// Battles
		void UpdateMonsterAI(double FrameTime);
		void CreateBattleElement(ae::_Element *Parent);
		void RemoveBattleElement();
		void StopBattle();

		// Status effects
		void CallBuffUpdate(_StatusEffect *StatusEffect);
		void AddSummonKey(std::map<_Object::_SummonKey, int> &Summons);
		static void UpdateSummonStatusEffects(std::map<_Object::_SummonKey, int> &Summons);

		// Actions
		bool SetActionUsing(ae::_Buffer &Data, ae::_Manager<_Object> *ObjectManager);
		bool CanInteractWith(const _Object *Object, bool UseLevelRange, bool &HitLevelRestriction) const;
		void MovePrivilegeItems(std::vector<_Slot> &SlotsUpdated, bool SearchStash=false);

		// Movement
		bool GetDirectionFromInput(int InputState, glm::ivec2 &Direction);
		int Move();

		// Minigames
		void SendSeed();

		// Map
		bool CanRespec() const;
		const _Tile *GetTile() const;
		ae::NetworkIDType GetMapID() const;

		// Path finding
		bool Pathfind(const glm::ivec2 &StartPosition, const glm::ivec2 &EndPosition);
		int GetInputStateFromPath();

		// Base
		std::string Name;
		ae::_LogFile Log;
		bool Offline{false};
		bool Logging{false};
		bool Muted{false};

		// Components
		_Character *Character{nullptr};
		_Inventory *Inventory{nullptr};
		_Fighter *Fighter{nullptr};
		_Controller *Controller{nullptr};
		_Monster *Monster{nullptr};

		// Pointers
		const _Stats *Stats{nullptr};
		_Battle *Battle{nullptr};
		_Map *Map{nullptr};
		_Scripting *Scripting{nullptr};
		_Server *Server{nullptr};
		ae::_Peer *Peer{nullptr};
		uint32_t QueuedMapChange{0};

		// Movement
		glm::ivec2 Position{0};
		glm::ivec2 ServerPosition{0};

		// Render
		const ae::_Texture *ModelTexture{nullptr};
		uint32_t ModelID{0};
		uint32_t BossZoneID{0};
		int Light{0};

		// Compression
		uint8_t UpdateID{0};
		bool Changed{false};
		glm::ivec2 OldPosition{0};
		int64_t OldBounty{0};
		bool OldOffline{false};
		int OldStatus{0};
		int OldInvisible{0};
		int OldLight{0};

	private:

};
