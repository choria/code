/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <objects/action.h>
#include <ae/assets.h>
#include <ae/buffer.h>
#include <ae/random.h>
#include <objects/components/character.h>
#include <objects/components/fighter.h>
#include <objects/components/inventory.h>
#include <objects/components/monster.h>
#include <objects/battle.h>
#include <objects/item.h>
#include <objects/object.h>
#include <packet.h>
#include <server.h>
#include <stats.h>

// Serialize action
void _Action::Serialize(ae::_Buffer &Data) {
	uint32_t ItemID = 0;
	if(Item)
		ItemID = Item->ID;

	Data.Write<uint32_t>(ItemID);
}

// Unserialize action
void _Action::Unserialize(ae::_Buffer &Data, const _Stats *Stats) {
	uint32_t ItemID = Data.Read<uint32_t>();

	Item = Stats->Items.at(ItemID);
}

// Resolve action
bool _Action::Resolve(ae::_Buffer &Data, _Object *Source, ScopeType Scope) {
	if(!Item || (!Target && !LearningSkill))
		return false;

	// Build action result
	_ActionResult ActionResult;
	ActionResult.Source.Object = Source;
	ActionResult.Scope = Scope;
	ActionResult.ActionUsed = *this;

	// Check if item can be used
	if(!Item->CanUse(Source->Scripting, ActionResult))
		return false;

	// Add targets
	auto &Battle = Source->Battle;
	std::vector<_Object *> Targets;
	int TargetCount = 1;
	if(!LearningSkill) {
		if(Battle)
			Battle->SetTargets(Source, *this, Target, Targets);
		else if(Item->CanTarget(Source->Scripting, Source, Target))
			Targets.push_back(Target);

		// Check targets
		if(Targets.empty())
			return false;

		// Get original target count to store in any new status effects
		TargetCount = TargetCountOverride ? TargetCountOverride : Item->GetTargetCount(Source->Scripting, Source);
	}

	bool SkillUnlocked = false;
	bool ItemUnlocked = false;
	bool KeyUnlocked = false;
	bool DecrementItem = false;

	// Apply skill cost
	if(Item->IsSkill() && Source->Character->HasLearned(Item) && InventorySlot == -1) {
		Item->ApplyCost(Source->Scripting, ActionResult);
	}
	// Apply item cost
	else {
		size_t Index;
		if(!Source->Inventory->FindItem(Item, Index, (size_t)InventorySlot))
			return false;

		// Handle different item types
		int ConsumeRoll = 0;
		if(Item->IsSkill()) {

			// Learn skill
			Source->Character->Skills[Item->ID] = 0;
			Source->Character->MinSkillLevels[Item->ID] = Source->Character->MaxSkillLevels[Item->ID] = std::min(Item->MaxLevel, GAME_DEFAULT_MAX_SKILL_LEVEL);
			SkillUnlocked = true;
		}
		else if(Item->IsUnlockable()) {
			Source->Character->Unlocks[Item->UnlockID].Level = 1;
			ItemUnlocked = true;
		}
		else if(Item->IsKey()) {
			Source->Inventory->GetBag(BagType::KEYS).Slots.push_back(_InventorySlot(Item, 1));
			KeyUnlocked = true;
		}
		else {
			if(Battle)
				ConsumeRoll = ae::GetRandomInt(1, 100);
		}

		// Roll to consume item
		if(ConsumeRoll <= Source->Character->Attributes["ConsumeChance"].Int) {
			Source->Inventory->UpdateItemCount(_Slot(BagType::INVENTORY, Index), -1);
			DecrementItem = true;
		}
	}

	// Set cooldown
	if(!SkillUnlocked && std::abs(Item->Cooldown) > 0.0) {
		double Reduction = Item->Cooldown > 0.0 ? Source->Character->Attributes["Cooldowns"].Mult() : 1.0;
		double Duration = std::max(GAME_MIN_COOLDOWN, std::abs(Item->Cooldown) * Reduction);
		Source->Character->Cooldowns[Item->ID].Duration = Duration;
		Source->Character->Cooldowns[Item->ID].MaxDuration = Duration;
	}

	// Update stats
	Source->UpdateStats(ActionResult.Source);
	Source->Fighter->TurnTimer = 0.0;

	// Build packet for results
	Data.Write<PacketType>(PacketType::ACTION_RESULTS);
	Data.WriteBit(DecrementItem);
	Data.WriteBit(SkillUnlocked);
	Data.WriteBit(ItemUnlocked);
	Data.WriteBit(KeyUnlocked);

	// Write action used
	Data.Write<uint32_t>(Item->ID);
	Data.Write<int8_t>((int8_t)ActionResult.ActionUsed.InventorySlot);

	// Write source updates
	ActionResult.Source.Serialize(Data);

	// Update each target
	Data.Write<uint8_t>(Targets.size());
	int Priority = 1;
	for(auto &Object : Targets) {

		// Set objects
		ActionResult.Source.Reset();
		ActionResult.Target.Reset();
		ActionResult.Source.Object = Source;
		ActionResult.Target.Object = Object;

		// Call Use script
		if(!SkillUnlocked)
			Item->Use(Source->Scripting, ActionResult, Priority);

		// Update objects
		ActionResult.Source.Object->UpdateStats(ActionResult.Source, nullptr, TargetCount);
		ActionResult.Target.Object->UpdateStats(ActionResult.Target, ActionResult.Source.Object, TargetCount);
		HandleSummons(ActionResult);

		// Write stat changes
		ActionResult.Source.Serialize(Data);
		ActionResult.Target.Serialize(Data);
		Priority++;
	}

	// Reset object
	Source->Character->Action.Clear();

	// Remove object from battle
	if(Source->Fighter->FleeBattle) {

		// Give summons to player
		if(!Source->IsMonster()) {

			// Build map of players and their summon buffs
			std::map<_Object::_SummonKey, int> PlayerSummons;
			for(auto &Object : Source->Battle->Objects)
				Object->AddSummonKey(PlayerSummons);

			// Handle summon updates
			_Object::UpdateSummonStatusEffects(PlayerSummons);
		}

		Source->Battle->RemoveObject(Source);
		Source->Server->UpdateEventCount();
		Source->Fighter->FleeBattle = false;
	}

	return true;
}

// Handle summon actions
void _Action::HandleSummons(_ActionResult &ActionResult) {
	if(ActionResult.Summons.empty())
		return;

	_Object *SourceObject = ActionResult.Source.Object;
	_Battle *Battle = SourceObject->Battle;
	if(!Battle)
		return;

	for(const auto &Summon : ActionResult.Summons) {

		// Check for existing summons and get count of fighters on player's side
		std::vector<_Object *>ExistingSummons;
		ExistingSummons.reserve(BATTLE_MAX_OBJECTS_PER_SIDE);
		int SideCount = 0;
		for(auto &Object : Battle->Objects) {
			if(Object->Fighter->BattleSide == SourceObject->Fighter->BattleSide) {
				if(Object->Monster->Owner == SourceObject && Object->Monster->SpellID == Summon.SpellID)
					ExistingSummons.push_back(Object);

				SideCount++;
			}
		}

		// Create new summon if below limit
		if(SideCount < BATTLE_MAX_OBJECTS_PER_SIDE && (int)ExistingSummons.size() < Summon.Limit) {

			// Create object
			_Object *Object = SourceObject->Server->CreateSummon(0, false, SourceObject, Summon);

			// Create packet for new object
			ae::_Buffer Packet;
			Packet.Write<PacketType>(PacketType::WORLD_CREATEOBJECT);
			Object->SerializeCreate(Packet);
			Battle->BroadcastPacket(Packet);

			// Add monster to battle
			Battle->AddObject(Object, SourceObject->Fighter->BattleSide, true);
		}
		// Heal existing summon if already at limit
		else if(ExistingSummons.size()) {

			// Get lowest health summon
			int64_t LowestHealth = ExistingSummons[0]->Character->Attributes["Health"].Int;
			_Object *LowestHealthSummon = ExistingSummons[0];
			for(auto &ExistingSummon : ExistingSummons) {
				if(ExistingSummon->Character->Attributes["Health"].Int < LowestHealth) {
					LowestHealth = ExistingSummon->Character->Attributes["Health"].Int;
					LowestHealthSummon = ExistingSummon;
				}
			}

			_StatChange Heal;
			Heal.Object = LowestHealthSummon;
			Heal.Values["Health"].Int = LowestHealthSummon->Character->Attributes["MaxHealth"].Int;
			Heal.Values["Mana"].Int = LowestHealthSummon->Character->Attributes["MaxMana"].Int;
			LowestHealthSummon->UpdateStats(Heal);

			ae::_Buffer Packet;
			Packet.Write<PacketType>(PacketType::STAT_CHANGE);
			Heal.Serialize(Packet);
			Battle->BroadcastPacket(Packet);
		}
	}
}

// Reset action
void _Action::Clear() {
	Item = nullptr;
	Target = nullptr;
	Count = 0;
	Duration = 0.0;
	Level = 0;
	TargetCountOverride = 0;
	InventorySlot = -1;
	ActionBarSlot = -1;
	Confirmed = false;
	LearningSkill = false;
}

// Return target type of action used
TargetType _Action::GetTargetType() const {
	if(Item)
		return Item->TargetID;

	return TargetType::NONE;
}

// Get suitable font for string
ae::_Font *_ActionResult::GetFont(const std::string &Text) {
	if(Text.length() >= 6)
		return ae::Assets.Fonts["hud_tiny"];
	else if(Text.length() >= 4)
		return ae::Assets.Fonts["hud_small"];

	return ae::Assets.Fonts["hud_medium"];
}
