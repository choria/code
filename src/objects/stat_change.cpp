/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <objects/stat_change.h>
#include <ae/buffer.h>
#include <ae/font.h>
#include <ae/manager.h>
#include <ae/util.h>
#include <objects/buff.h>
#include <objects/object.h>
#include <framework.h>
#include <stats.h>
#include <glm/gtc/type_ptr.hpp>

const _Value &_Value::Print(const std::unordered_map<std::string, _Value> &Map, const char *Key) { return Map.at(Key); }

// Serialize network
void _StatChange::Serialize(ae::_Buffer &Data) {
	if(!Object)
		throw std::runtime_error("_StatChange::Serialize: Object is null!");

	// Write header
	Data.Write<ae::NetworkIDType>(Object->NetworkID);
	Data.Write<uint8_t>(Values.size());

	// Iterate over statchanges
	for(auto &Iterator : Values) {

		// Write type
		const _Attribute &Attribute = Object->Stats->Attributes.at(Iterator.first);
		Data.Write<uint8_t>(Attribute.ID);

		// Write data
		switch(Attribute.Type) {
			case StatValueType::POINTER: {
				_Buff *Buff = (_Buff *)(Iterator.second.Pointer);
				Data.Write<uint32_t>(Buff->ID);
			} break;
			case StatValueType::INTEGER64:
			case StatValueType::PERCENT64:
				Data.Write<int64_t>(Iterator.second.Int);
			break;
			case StatValueType::TIME:
			case StatValueType::DOUBLE:
				Data.Write<float>(Iterator.second.Double);
			break;
			default:
				Data.Write<int>((int)Iterator.second.Int);
			break;
		}
	}
}

// Unserialize network
void _StatChange::Unserialize(ae::_Buffer &Data, ae::_Manager<_Object> *Manager) {
	Reset();

	// Get object id
	ae::NetworkIDType NetworkID = Data.Read<ae::NetworkIDType>();
	Object = Manager->GetObject(NetworkID);
	if(!Object)
		throw std::runtime_error("_StatChange::Unserialize BadObject NetworkID=" + std::to_string(NetworkID));

	// Get change count
	int Count = Data.Read<uint8_t>();
	if(!Count)
		return;

	// Get values
	for(int i = 0; i < Count; i++) {

		// Get type
		uint8_t AttributeID = Data.Read<uint8_t>();
		const _Attribute &Attribute = Object->Stats->Attributes.at(Object->Stats->AttributeRank[AttributeID]);

		// Get data
		switch(Attribute.Type) {
			case StatValueType::POINTER: {
				uint32_t BuffID = Data.Read<uint32_t>();
				if(Object)
					Values[Attribute.Name].Pointer = (void *)Object->Stats->Buffs.at(BuffID);
			} break;
			case StatValueType::INTEGER64:
			case StatValueType::PERCENT64:
				Values[Attribute.Name].Int = Data.Read<int64_t>();
			break;
			case StatValueType::TIME:
			case StatValueType::DOUBLE:
				Values[Attribute.Name].Double = Data.Read<float>();
			break;
			default:
				Values[Attribute.Name].Int = Data.Read<int>();
			break;
		}
	}
}

// Log values
void _StatChange::Log() {

	// Print object
	Framework.Log << "StatChange for " << Object->Name << "(" << Object->NetworkID << ")" << std::endl;

	// Print values
	for(const auto &Pair : Values) {

		// Get attribute
		const _Attribute &Attribute = Object->Stats->Attributes.at(Pair.first);

		// Log name
		Framework.Log << " " << Attribute.Name << " = ";

		// Log value
		switch(Attribute.Type) {
			case StatValueType::INTEGER:
			case StatValueType::PERCENT:
			case StatValueType::BOOLEAN:
			case StatValueType::INTEGER64:
			case StatValueType::PERCENT64:
				Framework.Log << Pair.second.Int;
			break;
			case StatValueType::DOUBLE:
			case StatValueType::TIME:
			case StatValueType::PERCENT_DOUBLE:
				Framework.Log << Pair.second.Double;
			break;
			case StatValueType::POINTER:
				Framework.Log << ((_Buff *)(Pair.second.Pointer))->Name;
			break;
		}

		Framework.Log << std::endl;
	}
}

// Render stat change
void _StatChangeUI::Render(double BlendFactor) {
	if(Change == 0.0)
		return;

	// Get alpha
	double TimeLeft = Timeout - Time;
	Color.a = 1.0f;
	if(TimeLeft < HUD_STATCHANGE_FADETIME)
		Color.a = (float)(TimeLeft / HUD_STATCHANGE_FADETIME);

	// Get final draw position
	glm::ivec2 DrawPosition = glm::mix(LastPosition, Position, BlendFactor);

	// Draw text
	Font->DrawText(Text, DrawPosition, ae::CENTER_MIDDLE, Color);
}

// Set text and color
void _StatChangeUI::SetText(const glm::vec4 &NegativeColor, const glm::vec4 &PositiveColor) {

	// Get text color and sign
	std::stringstream Buffer;
	if(Change > 0.0) {
		Color = PositiveColor;
		Buffer << "+";
	}
	else if(Change < 0.0) {
		Color = NegativeColor;
		Buffer << "-";
	}
	else {
		Color = glm::vec4(1.0);
		Buffer << " ";
	}

	// Set text
	ae::FormatSI<int64_t>(Buffer, std::abs(Change), ae::RoundDown1);
	Text = Buffer.str();
}
