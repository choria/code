/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <objects/status_effect.h>
#include <ae/assets.h>
#include <ae/buffer.h>
#include <ae/font.h>
#include <ae/graphics.h>
#include <ae/ui.h>
#include <ae/util.h>
#include <objects/buff.h>
#include <stats.h>
#include <iomanip>

// Destructor
_StatusEffect::~_StatusEffect() {
	if(BattleElement) {
		if(BattleElement->Parent)
			BattleElement->Parent->RemoveChild(BattleElement);

		delete BattleElement;
	}

	if(HUDElement) {
		if(HUDElement->Parent)
			HUDElement->Parent->RemoveChild(HUDElement);

		delete HUDElement;
	}
}

// Serialize for network
void _StatusEffect::Serialize(ae::_Buffer &Data) {
	Data.Write<uint32_t>(Buff->ID);
	Data.WriteBit(Deleted);
	Data.WriteBit(Infinite);
	Data.Write<uint8_t>(Priority);
	Data.Write<int16_t>(Modifier);
	Data.Write<int64_t>(Level);
	if(!Infinite) {
		Data.Write<int>(Stacks.size());
		Data.Write<float>((float)Duration);
		Data.Write<float>((float)MaxDuration);
		for(auto &Stack : Stacks) {
			Data.Write<int64_t>(Stack.Level);
			Data.Write<float>((float)Stack.Duration);
		}
	}
}

// Unserialize from network
void _StatusEffect::Unserialize(ae::_Buffer &Data, const _Stats *Stats) {
	uint32_t BuffID = Data.Read<uint32_t>();
	Buff = Stats->Buffs.at(BuffID);
	Deleted = Data.ReadBit();
	Infinite = Data.ReadBit();
	Priority = Data.Read<uint8_t>();
	Modifier = Data.Read<int16_t>();
	Level = Data.Read<int64_t>();
	if(!Infinite) {
		int StackSize = Data.Read<int>();
		Duration = Data.Read<float>();
		MaxDuration = Data.Read<float>();
		for(int i = 0; i < StackSize; i++) {
			_Stack Stack;
			Stack.Level = Data.Read<int64_t>();
			Stack.Duration = Data.Read<float>();
			Stacks.push_back(Stack);
		}
	}
}

// Update status effect, return true when buff's update function should be called
bool _StatusEffect::Update(double FrameTime, bool InBattle) {
	bool CallUpdate = false;

	// Update time
	Time += FrameTime;

	// Call buff update every second
	if(Time >= 1.0) {
		Time -= 1.0;
		CallUpdate = true;
	}

	// Update duration if not infinite
	if(!Infinite) {

		// Don't update during battle if it pauses
		if(!Buff->PauseDuringBattle || (Buff->PauseDuringBattle && !InBattle)) {
			Duration -= FrameTime;

			// Update stacks
			for(auto Iterator = Stacks.begin(); Iterator != Stacks.end(); ) {
				auto &Stack = *Iterator;

				Stack.Duration -= FrameTime;
				if(Stack.Duration <= 0) {
					Level -= Stack.Level;

					Iterator = Stacks.erase(Iterator);
				}
				else
					++Iterator;
			}
		}

		// Delete
		if(Duration <= 0.0)
			Deleted = true;
	}

	return CallUpdate;
}

// Create element for hud
ae::_Element *_StatusEffect::CreateElement(ae::_Element *Parent) {
	ae::_Element *Element = new ae::_Element();
	Element->BaseSize = UI_BUFF_SIZE;
	Element->Alignment = ae::LEFT_TOP;
	Element->Active = true;
	Element->Index = 0;
	Element->UserData = (void *)this;
	Element->Clickable = true;
	Element->Parent = Parent;
	Parent->Children.push_back(Element);

	return Element;
}

// Render element
void _StatusEffect::Render(ae::_Element *Element, double Timer, bool InBattle) {

	// Warning flash
	glm::vec4 Color(glm::vec4(1.0f));
	if(ShowWarning()) {
		double FastTime = Timer * 4;
		if(FastTime - (int)FastTime < 0.5)
			Color *= 0.5f;
	}

	// Draw buff icon
	ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos_uv"]);
	ae::Graphics.SetColor(Color);
	ae::Graphics.DrawImage(Element->Bounds, Buff->Texture);

	// Set up graphics
	ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos"]);
	ae::Graphics.SetColor(glm::vec4(0, 0, 0, 0.7f));

	// Draw dark percentage bg
	if(MaxDuration) {
		float OverlayHeight = (Duration / MaxDuration) * (Element->Bounds.End.y - Element->Bounds.Start.y);
		ae::Graphics.DrawRectangle(Element->Bounds.Start + glm::vec2(0, OverlayHeight), Element->Bounds.End, true);
	}

	// Get position to draw text
	glm::ivec2 TextPosition = glm::ivec2((Element->Bounds.Start.x + Element->Bounds.End.x) / 2, 0);

	// Draw level
	int OffsetY = Buff->ShowLevel == 1 ? 3 : 33;
	if(Buff->ShowLevel & 1) {
		TextPosition.y = Element->Bounds.End.y - OffsetY * ae::_Element::GetUIScale();
		std::stringstream Buffer;
		ae::FormatSI<int64_t>(Buffer, Level, ae::RoundDown1);
		if(Buffer.str().length())
			ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), TextPosition, ae::CENTER_BASELINE);
	}

	// Draw count
	if(Stacks.size() > 1) {
		TextPosition.y = Element->Bounds.End.y - 18 * ae::_Element::GetUIScale();
		std::stringstream Buffer;
		Buffer << Stacks.size();
		Color = Buff->Modifier ? ae::Assets.Colors["light_green"] : ae::Assets.Colors["yellow"];
		ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), TextPosition, ae::CENTER_BASELINE, Color);
	}
	// Draw priority or modifier
	else if(InBattle && (Buff->Modifier || Priority)) {
		TextPosition.y = Element->Bounds.End.y - 18 * ae::_Element::GetUIScale();
		std::stringstream Buffer;
		int DisplayValue = Buff->Modifier ? Modifier : Priority;
		if(DisplayValue) {
			Buffer << DisplayValue;
			ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), TextPosition, ae::CENTER_BASELINE, ae::Assets.Colors["light_green"]);
		}
	}

	// Draw duration
	if(Buff->ShowLevel & 2) {
		TextPosition.y = Element->Bounds.End.y - 3 * ae::_Element::GetUIScale();
		std::stringstream Buffer;
		_StatusEffect::FormatDuration(Buffer, Duration);
		ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), TextPosition, ae::CENTER_BASELINE);
	}
}

// Format duration
void _StatusEffect::FormatDuration(std::stringstream &Buffer, double Duration) {
	Buffer << std::fixed << std::setprecision(1);

	if(Duration < 60)
		Buffer << Duration << "s";
	else if(Duration < 3600)
		Buffer << Duration / 60 << "m";
	else
		Buffer << Duration / 3600 << "h";
}

// Return true if status effect should flash
bool _StatusEffect::ShowWarning() {
	return Buff->WarningTime > 0 && Duration <= Buff->WarningTime;
}
