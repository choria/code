/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <objects/components/character.h>
#include <ae/assets.h>
#include <ae/buffer.h>
#include <ae/database.h>
#include <ae/random.h>
#include <ae/util.h>
#include <hud/hud.h>
#include <objects/components/controller.h>
#include <objects/components/fighter.h>
#include <objects/components/inventory.h>
#include <objects/components/monster.h>
#include <objects/buff.h>
#include <objects/item.h>
#include <objects/map.h>
#include <objects/object.h>
#include <objects/status_effect.h>
#include <states/play.h>
#include <packet.h>
#include <scripting.h>
#include <stats.h>
#include <algorithm>

// Constructor
_Character::_Character(_Object *Object) : Object(Object) {
	ActionBar.resize(ACTIONBAR_MAX_SIZE);
	for(size_t i = 0; i < ActionBar.size(); i++)
		ActionBar[i].ActionBarSlot = (int)i;

	// Set base attributes
	BaseAttributes["BattleSpeed"].Int = 100;
}

// Destructor
_Character::~_Character() {
	DeleteStatusEffects();
}

// Initialize attributes
void _Character::InitAttributes() {
	if(!Object->Stats)
		throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " Object->Stats is null");

	// Set attributes
	for(const auto &Attribute : Object->Stats->Attributes)
		Attributes[Attribute.second.Name].Int = 0;
}

// Update
void _Character::Update(double FrameTime) {

	// Update stats
	UpdateTimer += FrameTime;
	if(UpdateTimer >= 1.0) {
		UpdateTimer -= 1.0;

		// Update stats on server
		if(Object->Server && IsAlive()) {
			_StatChange StatChange;
			StatChange.Object = Object;

			// Update regen
			if(((Attributes["Health"].Int < Attributes["MaxHealth"].Int) && Attributes["HealthRegen"].Int > 0) || Attributes["HealthRegen"].Int < 0)
				StatChange.Values["Health"].Int = Attributes["HealthRegen"].Int;
			if(((Attributes["Mana"].Int < Attributes["MaxMana"].Int) && Attributes["ManaRegen"].Int > 0) || Attributes["ManaRegen"].Int < 0)
				StatChange.Values["Mana"].Int = Attributes["ManaRegen"].Int;

			// Update object
			if(StatChange.Values.size() != 0) {
				Object->UpdateStats(StatChange);

				// Build packet
				ae::_Buffer Packet;
				Packet.Write<PacketType>(PacketType::STAT_CHANGE);
				StatChange.Serialize(Packet);

				// Send packet to player
				Object->SendPacket(Packet);
			}
		}
	}

	// Update status effects
	for(auto Iterator = StatusEffects.begin(); Iterator != StatusEffects.end(); ) {
		_StatusEffect *StatusEffect = *Iterator;

		// Update
		bool CallUpdate = StatusEffect->Update(FrameTime, !!Object->Battle);

		// Call buff update
		if(CallUpdate && Object->Server && IsAlive())
			Object->CallBuffUpdate(StatusEffect);

		// Clean up
		if(StatusEffect->Deleted || (!IsAlive() && !StatusEffect->Buff->Summon)) {

			// Delete status effect
			delete StatusEffect;
			Iterator = StatusEffects.erase(Iterator);

			// Update stats
			CalculateStats();
		}
		else
			++Iterator;
	}

	// Update cooldowns
	for(auto Iterator = Cooldowns.begin(); Iterator != Cooldowns.end(); ) {
		Iterator->second.Duration -= FrameTime;

		// Remove cooldown
		if(Iterator->second.Duration <= 0.0)
			Iterator = Cooldowns.erase(Iterator);
		else
			++Iterator;
	}

	// Update boss cooldowns
	if(Object->Server && Object->Peer) {
		bool SendBossCooldownUpdate = false;
		if(IdleTime <= PLAYER_IDLE_TIME) {
			for(auto Iterator = BossCooldowns.begin(); Iterator != BossCooldowns.end(); ) {

				// Skip infinite cooldowns
				if(Iterator->second < 0.0) {
					++Iterator;
					continue;
				}

				// Update time
				Iterator->second -= FrameTime;

				// Remove cooldown
				if(Iterator->second <= 0.0) {
					SendBossCooldownUpdate = true;
					Iterator = BossCooldowns.erase(Iterator);
				}
				else
					++Iterator;
			}
		}

		// Notify client of boss being off cooldown
		if(SendBossCooldownUpdate) {
			ae::_Buffer Packet;
			Packet.Write<PacketType>(PacketType::PLAYER_BOSSCOOLDOWNS);
			Object->SerializeBossCooldowns(Packet);
			Object->SendPacket(Packet, false);
			if(Object->Map)
				Object->Map->CheckEvents(Object, 0);
		}
	}
}

// Update health
void _Character::UpdateHealth(int64_t &Value) {
	if(Object->Server && Value > 0)
		Value *= Attributes["HealthUpdateMultiplier"].Mult();

	Attributes["Health"].Int = std::clamp(Attributes["Health"].Int + Value, (int64_t)0, Attributes["MaxHealth"].Int);
}

// Update mana
void _Character::UpdateMana(int64_t Value) {
	Attributes["Mana"].Int = std::clamp(Attributes["Mana"].Int + Value, (int64_t)0, Attributes["MaxMana"].Int);
}

// Update gold amount
void _Character::UpdateGold(int64_t Value) {
	Attributes["Gold"].Int = std::clamp(Attributes["Gold"].Int + Value, -PLAYER_MAX_GOLD, PLAYER_MAX_GOLD);
}

// Update gold amount
void _Character::UpdateGoldLost(int64_t Value) {
	Attributes["GoldLost"].Int = std::clamp(Attributes["GoldLost"].Int + Value, (int64_t)0, PLAYER_MAX_GOLD);
}

// Update experience
void _Character::UpdateExperience(int64_t Value) {
	Attributes["Experience"].Int = std::max(Attributes["Experience"].Int + Value, (int64_t)0);
}

// Update all resistances
void _Character::UpdateAllResist(int Value, const std::string &Prefix) {
	for(const auto &Name : _Stats::ResistNames)
		Attributes[Prefix + Name].Int += Value;
}

// Update elemental resistances
void _Character::UpdateElementalResist(int Value, const std::string &Prefix) {
	Attributes[Prefix + "FireResist"].Int += Value;
	Attributes[Prefix + "ColdResist"].Int += Value;
	Attributes[Prefix + "LightningResist"].Int += Value;
}

// Calculates all of the player stats
void _Character::CalculateStats() {
	_Scripting *Scripting = Object->Scripting;

	// Set default values
	for(const auto &Attribute : Object->Stats->Attributes) {
		if(!Attribute.second.Calculate)
			continue;

		Attributes[Attribute.second.Name].Int = Attribute.second.Default.Int;
	}

	// Get base stats from level
	CalculateLevelStats();

	// Set base stats
	for(const auto &Attribute : BaseAttributes)
		Attributes[Attribute.first].Int = Attribute.second.Int;

	// Update monster's power by difficulty
	if(Object->Monster->DatabaseID) {
		int64_t AddedPower = Object->Monster->Difficulty - 100;
		Attributes["HealPower"].Int += AddedPower;
		Attributes["BleedPower"].Int += AddedPower * BATTLE_DIFFICULTY_POWER;
		Attributes["PoisonPower"].Int += AddedPower * BATTLE_DIFFICULTY_POWER;
	}

	SkillPoints += SkillPointsUnlocked;
	Object->Light = 0;
	Invisible = 0;
	Attributes["Difficulty"].Int += Attributes["EternalPain"].Int + Attributes["Rebirths"].Int * GAME_REBIRTH_DIFFICULTY + Attributes["Evolves"].Int * GAME_EVOLVE_DIFFICULTY + Attributes["Transforms"].Int * GAME_TRANSFORM_DIFFICULTY;
	Attributes["RebirthTier"].Int = Level / std::max(0.1, 5.0 - Attributes["RebirthProgress"].Int * 0.1) + Attributes["RebirthPower"].Int + Attributes["Evolves"].Int + Attributes["Transforms"].Int * GAME_TRANSFORM_REBIRTH_TIERS;
	Attributes["EvolveTier"].Int = Attributes["Transforms"].Int + Attributes["Rebirths"].Int / 10;
	Attributes["TransformTier"].Int = Attributes["Evolves"].Int / 10;

	// Eternal Strength
	Attributes["PhysicalPower"].Int += Attributes["EternalStrength"].Int;
	Attributes["FirePower"].Int += Attributes["EternalStrength"].Int;
	Attributes["ColdPower"].Int += Attributes["EternalStrength"].Int;
	Attributes["LightningPower"].Int += Attributes["EternalStrength"].Int;
	Attributes["BleedPower"].Int += Attributes["EternalStrength"].Int;
	Attributes["PoisonPower"].Int += Attributes["EternalStrength"].Int;
	Attributes["SummonPower"].Int += Attributes["EternalStrength"].Int;

	// Eternal Guard
	if(Attributes["EternalGuard"].Int) {
		Attributes["DamageBlock"].Int += Attributes["EternalGuard"].Int;
		Attributes["Armor"].Int += Attributes["EternalGuard"].Int / GAME_ETERNAL_GUARD_DIVISOR;
	}

	// Eternal Protection
	if(Attributes["EternalProtection"].Int)
		UpdateAllResist((int)(Attributes["EternalProtection"].Int / GAME_ETERNAL_PROTECTION_DIVISOR));

	// Eternal Fortitude
	Attributes["HealthBonus"].Int += Attributes["EternalFortitude"].Int;
	Attributes["HealPower"].Int += Attributes["EternalFortitude"].Int;

	// Eternal Spirit
	Attributes["ManaBonus"].Int += Attributes["EternalSpirit"].Int;
	Attributes["ManaPower"].Int += Attributes["EternalSpirit"].Int;

	// Eternal Wisdom
	Attributes["ExperienceGained"].Int += Attributes["EternalWisdom"].Int;

	// Eternal Wealth
	Attributes["GoldGained"].Int += Attributes["EternalWealth"].Int;

	// Eternal Knowledge
	SkillPoints += Attributes["EternalKnowledge"].Int / GAME_ETERNAL_KNOWLEDGE_DIVISOR;

	// Eternal Alacrity
	Attributes["BattleSpeed"].Int += Attributes["EternalAlacrity"].Int * GAME_ETERNAL_ALACRITY_SCALE;

	// Eternal Command
	Attributes["SummonBattleSpeed"].Int += Attributes["EternalCommand"].Int * GAME_ETERNAL_COMMAND_SCALE;

	// Eternal Ward
	if(Attributes["EternalWard"].Int)
		UpdateAllResist(Attributes["EternalWard"].Int * GAME_ETERNAL_WARD_SCALE, "Max");

	// Eternal Impatience
	Attributes["Cooldowns"].Int -= Attributes["EternalImpatience"].Int * GAME_ETERNAL_IMPATIENCE_SCALE;
	Attributes["BossCooldowns"].Int -= Attributes["RebirthSoul"].Int;

	// Eternal Charisma
	Attributes["VendorDiscount"].Int += Attributes["EternalCharisma"].Int * GAME_ETERNAL_CHARISMA_SCALE;

	// Eternal Hell
	Attributes["DifficultyMultiplier"].Double += Attributes["EternalHell"].Int * GAME_ETERNAL_HELL_SCALE;

	// Eternal Malice
	Attributes["TargetCount"].Int += Attributes["EternalMalice"].Int * GAME_ETERNAL_MALICE_SCALE;

	// Eternal Deceit
	Attributes["SetLimit"].Int += Attributes["EternalDeceit"].Int * GAME_ETERNAL_DECEIT_SCALE;

	// Get item stats
	Sets.clear();
	std::vector<int64_t> ItemMinDamage(Object->Stats->DamageTypes.size(), 0);
	std::vector<int64_t> ItemMaxDamage(Object->Stats->DamageTypes.size(), 0);
	std::unordered_map<uint32_t, std::vector<int>> SetPieceLevels;
	_Bag &EquipmentBag = Object->Inventory->GetBag(BagType::EQUIPMENT);
	for(size_t i = 0; i < EquipmentBag.Slots.size(); i++) {
		const _Item *Item = EquipmentBag.Slots[i].Item;
		if(!Item)
			continue;

		// Get upgrade count
		int Upgrades = EquipmentBag.Slots[i].Upgrades;

		// Get stat bonuses
		_ActionResult ActionResult;
		ActionResult.ActionUsed.Level = Upgrades;
		ActionResult.Source.Object = Object;
		Item->GetStats(Scripting, ActionResult, 0, 0);
		CalculateStatBonuses(ActionResult.Source);

		// Add damage
		if(Item->Type != ItemType::SHIELD) {
			ItemMinDamage[Item->DamageTypeID] += std::floor(Item->GetAttribute("MinDamage", Upgrades));
			ItemMaxDamage[Item->DamageTypeID] += std::floor(Item->GetAttribute("MaxDamage", Upgrades));
		}

		// Stat changes
		Attributes["Armor"].Int += std::floor(Item->GetAttribute("Armor", Upgrades));
		Attributes["DamageBlock"].Int += std::floor(Item->GetAttribute("DamageBlock", Upgrades));
		Attributes["Pierce"].Int += std::floor(Item->GetAttribute("Pierce", Upgrades));
		Attributes["MaxHealth"].Int += (int64_t)std::floor(Item->GetAttribute("MaxHealth", Upgrades));
		Attributes["MaxMana"].Int += (int64_t)std::floor(Item->GetAttribute("MaxMana", Upgrades));
		Attributes["HealthRegen"].Int += std::floor(Item->GetAttribute("HealthRegen", Upgrades));
		Attributes["ManaRegen"].Int += std::floor(Item->GetAttribute("ManaRegen", Upgrades));
		Attributes["BattleSpeed"].Int += std::floor(Item->GetAttribute("BattleSpeed", Upgrades));
		Attributes["MoveSpeed"].Int += std::floor(Item->GetAttribute("MoveSpeed", Upgrades));
		Attributes["AllSkills"].Int += std::floor(Item->GetAttribute("AllSkills", Upgrades));
		Attributes["SpellDamage"].Int += std::floor(Item->GetAttribute("SpellDamage", Upgrades));
		Attributes["Cooldowns"].Int += std::floor(Item->GetCooldownReduction(Upgrades));
		Attributes["ExperienceGained"].Int += std::floor(Item->GetAttribute("ExperienceGained", Upgrades));
		Attributes["GoldGained"].Int += std::floor(Item->GetAttribute("GoldGained", Upgrades));
		Attributes["AttackPower"].Int += std::floor(Item->GetAttribute("AttackPower", Upgrades));
		Attributes["SummonPower"].Int += std::floor(Item->GetAttribute("SummonPower", Upgrades));
		Attributes["Initiative"].Int += std::floor(Item->GetAttribute("Initiative", Upgrades));
		Attributes["Evasion"].Int = Attributes["Evasion"].Multiplicative(std::floor(Item->GetAttribute("Evasion", Upgrades)));

		// Add max resists
		for(const auto &Name : _Stats::ResistNames)
			Attributes["Max" + Name].Int += std::floor(Item->GetAttribute("Max" + Name, Upgrades));

		// Handle all resist
		if(Item->ResistanceTypeID == 1)
			UpdateAllResist(std::floor(Item->GetAttribute("Resist", Upgrades)));
		else if(Item->ResistanceTypeID == 9)
			UpdateElementalResist(std::floor(Item->GetAttribute("Resist", Upgrades)));
		else
			Attributes[Object->Stats->DamageTypes.at(Item->ResistanceTypeID).Name + "Resist"].Int += std::floor(Item->GetAttribute("Resist", Upgrades));

		// Increment set count
		if(Item->SetID) {
			auto SetIterator = Sets.find(Item->SetID);
			if(SetIterator == Sets.end()) {
				Sets[Item->SetID].EquippedCount = 1;
				Sets[Item->SetID].MaxLevel = Item->MaxLevel;
			}
			else {
				SetIterator->second.EquippedCount += Item->SetAdd;
				SetIterator->second.MaxLevel = std::min(SetIterator->second.MaxLevel, Item->MaxLevel);
			}

			for(int j = 0; j < Item->SetAdd; j++)
				SetPieceLevels[Item->SetID].push_back(Upgrades);
		}

		// Add to networth
		if(Item->Type != ItemType::RELIC && !Item->IsCursed())
			Attributes["EquippedNetworth"].Int += Item->GetPrice(Scripting, Object, nullptr, 1, false, Upgrades);
	}

	// Bag networths
	std::unordered_map<BagType, const char *> NetworthContainers;
	NetworthContainers[BagType::INVENTORY] = "InventoryNetworth";
	NetworthContainers[BagType::TRADE] = "TradeNetworth";
	NetworthContainers[BagType::STASH] = "StashNetworth";
	for(const auto &InventoryBag : Object->Inventory->GetBags()) {
		if(NetworthContainers.find(InventoryBag.Type) == NetworthContainers.end())
			continue;

		_Value &Value = Attributes[NetworthContainers[InventoryBag.Type]];
		for(size_t i = 0; i < InventoryBag.Slots.size(); i++) {
			const _Item *Item = InventoryBag.Slots[i].Item;
			if(!Item)
				continue;

			if(Item->Type != ItemType::RELIC) {
				int64_t Price = Item->GetPrice(Scripting, Object, nullptr, InventoryBag.Slots[i].Count, false, InventoryBag.Slots[i].Upgrades);
				if(Value.Int + Price > PLAYER_MAX_GOLD)
					Value.Int = PLAYER_MAX_GOLD;
				else
					Value.Int += Price;
			}
		}
	}

	// Add set bonus
	for(auto &SetData : Sets) {

		// Check for completed set
		const _Set &Set = Object->Stats->Sets.at(SetData.first);
		int SetLimit = std::max(1, Set.Count + (int)Attributes["SetLimit"].Int);
		if(SetData.second.EquippedCount < SetLimit) {
			SetData.second.Level = 0;
			continue;
		}
		SetData.second.Complete = true;

		// Get set level from nth highest level piece, where n is the set count
		auto SetPieceLevelsArray = SetPieceLevels[SetData.first];
		std::sort(SetPieceLevelsArray.begin(), SetPieceLevelsArray.end(), std::greater<int>());
		SetData.second.Level = SetPieceLevelsArray[(size_t)SetLimit-1];

		// Get stat bonuses when set is complete
		_StatChange StatChange;
		StatChange.Object = Object;
		if(Scripting->StartMethodCall(Set.Script, "SetStats")) {
			Scripting->PushObject(StatChange.Object);
			Scripting->PushInt(SetData.second.Level);
			Scripting->PushInt(SetData.second.MaxLevel);
			Scripting->PushStatChange(&StatChange);
			Scripting->MethodCall(4, 1);
			Scripting->GetStatChange(1, Object->Stats, StatChange);
			Scripting->FinishMethodCall();

			CalculateStatBonuses(StatChange);
		}
	}

	// Get added set bonus
	for(size_t i = 0; i < EquipmentBag.Slots.size(); i++) {
		const _Item *Item = EquipmentBag.Slots[i].Item;
		if(!Item || !Item->SetID)
			continue;

		const _SetData &SetData = Sets.at(Item->SetID);
		const _Set &Set = Object->Stats->Sets.at(Item->SetID);
		int SetLimit = std::max(1, Set.Count + (int)Attributes["SetLimit"].Int);
		if(SetData.EquippedCount < SetLimit)
			continue;

		_StatChange StatChange;
		StatChange.Object = Object;
		if(Scripting->StartMethodCall(Item->Script, "SetStats")) {
			const _SetData &SetData = Sets.at(Item->SetID);

			Scripting->PushObject(StatChange.Object);
			Scripting->PushInt(SetData.Level);
			Scripting->PushInt(SetData.MaxLevel);
			Scripting->PushStatChange(&StatChange);
			Scripting->MethodCall(4, 1);
			Scripting->GetStatChange(1, Object->Stats, StatChange);
			Scripting->FinishMethodCall();

			CalculateStatBonuses(StatChange);
		}
	}

	// Set max skill levels if necessary
	if(CalcLevelStats)
		ValidateSkillLevels();

	// Calculate skills points used
	SkillPointsUsed = 0;
	for(const auto &SkillLevel : Skills) {
		const _Item *Skill = Object->Stats->Items.at(SkillLevel.first);
		if(Skill)
			SkillPointsUsed += SkillLevel.second;
	}

	// Get skill bonus
	for(int i = 0; i < SkillBarSize; i++) {
		_ActionResult ActionResult;
		ActionResult.Source.Object = Object;
		if(GetActionFromActionBar(ActionResult.ActionUsed, (size_t)i)) {
			const _Item *Skill = ActionResult.ActionUsed.Item;
			if(Skill->IsSkill() && Skill->TargetID == TargetType::NONE) {

				// Get passive stat changes
				Skill->GetStats(Object->Scripting, ActionResult, 0, 0);
				CalculateStatBonuses(ActionResult.Source);
			}
		}
	}

	// Get speed before buffs
	BattleSpeedBeforeBuffs = std::min((int)Attributes["BattleSpeed"].Int, GAME_MAX_BATTLE_SPEED);

	// Get buff stats
	for(const auto &StatusEffect : StatusEffects) {
		_StatChange StatChange;
		StatChange.Object = Object;
		StatusEffect->Buff->ExecuteScript(Scripting, "Stats", StatusEffect->Level, StatChange);
		CalculateStatBonuses(StatChange);
	}

	// Get damage
	if(!Object->Monster->DatabaseID) {
		bool HasWeaponDamage = false;
		for(size_t i = 0; i < ItemMinDamage.size(); i++) {
			if(ItemMinDamage[i] != 0 || ItemMaxDamage[i] != 0)
				HasWeaponDamage = true;

			Attributes["MinDamage"].Int += std::round(ItemMinDamage[i] * Attributes["AttackPower"].Mult() * GetDamagePowerMultiplier((uint32_t)i));
			Attributes["MaxDamage"].Int += std::round(ItemMaxDamage[i] * Attributes["AttackPower"].Mult() * GetDamagePowerMultiplier((uint32_t)i));
		}

		// Add fist damage
		if(!HasWeaponDamage) {
			Attributes["MinDamage"].Int = Level;
			Attributes["MaxDamage"].Int = Level + 1;
		}
	}
	else {
		Attributes["MinDamage"].Int *= Attributes["AttackPower"].Mult();
		Attributes["MaxDamage"].Int *= Attributes["AttackPower"].Mult();
	}

	// Cap resistances
	for(const auto &Name : _Stats::ResistNames) {

		// Cap max resistances
		if(Object->Monster->DatabaseID && !Object->Monster->SummonBuff)
			Attributes["Max" + Name].Int = 100;
		else
			Attributes["Max" + Name].Int = std::clamp(Attributes["Max" + Name].Int, (int64_t)GAME_MIN_RESISTANCE, (int64_t)GAME_MAX_RESISTANCE);

		// Cap resistances
		Attributes[Name].Int = std::clamp(Attributes[Name].Int, (int64_t)GAME_MIN_RESISTANCE, Attributes["Max" + Name].Int);
	}

	// Get physical resistance from armor
	double ArmorResist = Attributes["Armor"].Int / (30.0 + std::abs(Attributes["Armor"].Int));

	// Physical resist comes solely from armor
	Attributes["PhysicalResist"].Double += ArmorResist * 100.0;

	// Cap stats
	Attributes["Evasion"].Int = std::clamp(100 - Attributes["Evasion"].Int, (int64_t)0, (int64_t)GAME_MAX_EVASION);
	Attributes["MoveSpeed"].Int = std::max(Attributes["MoveSpeed"].Int, (int64_t)PLAYER_MIN_MOVESPEED);
	Attributes["BattleSpeed"].Int = std::clamp(Attributes["BattleSpeed"].Int, (int64_t)GAME_MIN_BATTLE_SPEED, (int64_t)GAME_MAX_BATTLE_SPEED);
	Attributes["SummonBattleSpeed"].Int = std::clamp(Attributes["SummonBattleSpeed"].Int, (int64_t)GAME_MIN_BATTLE_SPEED, (int64_t)GAME_MAX_BATTLE_SPEED);
	Attributes["Cooldowns"].Int = std::max(Attributes["Cooldowns"].Int, (int64_t)0);
	Attributes["FallResist"].Int = std::min(Attributes["FallResist"].Int, (int64_t)100);

	Attributes["MinDamage"].Int = std::max(Attributes["MinDamage"].Int, (int64_t)0);
	Attributes["MaxDamage"].Int = std::max(Attributes["MaxDamage"].Int, (int64_t)0);
	Attributes["Pierce"].Int = std::max(Attributes["Pierce"].Int, (int64_t)0);
	Attributes["DamageBlock"].Int = std::max(Attributes["DamageBlock"].Int, (int64_t)0);
	Attributes["ConsumeChance"].Int = std::clamp(Attributes["ConsumeChance"].Int, (int64_t)0, (int64_t)100);
	Attributes["ManaShield"].Int = std::clamp(Attributes["ManaShield"].Int, (int64_t)0, (int64_t)100);

	Attributes["MaxHealth"].Int *= Attributes["HealthBonus"].Mult();
	Attributes["MaxMana"].Int *= Attributes["ManaBonus"].Mult();
	Attributes["Health"].Int = std::min(Attributes["Health"].Int, Attributes["MaxHealth"].Int);
	Attributes["Mana"].Int = std::min(Attributes["Mana"].Int, Attributes["MaxMana"].Int);
	if(Attributes["HealthRegen"].Int > 0)
		Attributes["HealthRegen"].Int *= Attributes["HealPower"].Mult();
	if(Attributes["ManaRegen"].Int > 0)
		Attributes["ManaRegen"].Int *= Attributes["ManaPower"].Mult();
	Attributes["BossCooldowns"].Int = std::clamp(Attributes["BossCooldowns"].Int, (int64_t)0, (int64_t)100);
	Attributes["VendorDiscount"].Int = std::clamp(Attributes["VendorDiscount"].Int, (int64_t)0, (int64_t)GAME_MAX_VENDOR_DISCOUNT);

	RefreshActionBarCount();

	if(!Object->Server)
		HUD.RefreshRelicWheel();
}

// Calculate base level stats
void _Character::CalculateLevelStats() {
	if(!Object->Stats || !CalcLevelStats)
		return;

	// Cap experience
	const _Level *MaxLevelStat = Object->Stats->GetLevel(Object->Stats->GetMaxLevel());
	Attributes["Experience"].Int = std::clamp(Attributes["Experience"].Int, (int64_t)0, MaxLevelStat->Experience);

	// Find current level
	const _Level *LevelStat = Object->Stats->FindLevel(Attributes["Experience"].Int);
	Level = LevelStat->Level;
	ExperienceNextLevel = LevelStat->NextLevel;
	ExperienceNeeded = (Level == Object->Stats->GetMaxLevel()) ? 0 : LevelStat->NextLevel - (Attributes["Experience"].Int - LevelStat->Experience);

	// Set base attributes
	BaseAttributes["MaxHealth"].Int = LevelStat->Health;
	BaseAttributes["MaxMana"].Int = LevelStat->Mana;
	BaseAttributes["MinDamage"].Int = 0;
	BaseAttributes["MaxDamage"].Int = 0;
	BaseAttributes["Armor"].Int = 0;
	BaseAttributes["DamageBlock"].Int = 0;
	SkillPoints = LevelStat->SkillPoints;
}

// Update an object's stats from a statchange
void _Character::CalculateStatBonuses(_StatChange &StatChange) {

	// Update attributes
	for(const auto &Update : StatChange.Values) {
		const auto &Attribute = Object->Stats->Attributes.at(Update.first);
		switch(Attribute.UpdateType) {
			case StatUpdateType::NONE:
				continue;
			break;
			case StatUpdateType::ADD: {
				switch(Attribute.Type) {
					case StatValueType::INTEGER:
					case StatValueType::PERCENT:
					case StatValueType::INTEGER64:
					case StatValueType::PERCENT64:
						Attributes[Update.first].Int += Update.second.Int;
					break;
					case StatValueType::DOUBLE:
					case StatValueType::TIME:
					case StatValueType::PERCENT_DOUBLE:
						Attributes[Update.first].Double += Update.second.Double;
					break;
					default:
						throw std::runtime_error("Bad update type: " + Update.first);
					break;
				}
			} break;
			case StatUpdateType::SET:
				Attributes[Update.first].Int = Update.second.Int;
			break;
			case StatUpdateType::MULTIPLICATIVE:
				Attributes[Update.first].Int = Attributes[Update.first].Multiplicative((int)Update.second.Int);
			break;
		}
	}

	if(StatChange.HasStat("Invisible"))
		Invisible = (int)StatChange.Values["Invisible"].Int;

	if(StatChange.HasStat("Light"))
		Object->Light = (int)StatChange.Values["Light"].Int;

	if(StatChange.HasStat("AllResist"))
		UpdateAllResist((int)StatChange.Values["AllResist"].Int);

	if(StatChange.HasStat("MaxAllResist"))
		UpdateAllResist((int)StatChange.Values["MaxAllResist"].Int, "Max");

	if(StatChange.HasStat("ElementalResist"))
		UpdateElementalResist((int)StatChange.Values["ElementalResist"].Int);

	if(StatChange.HasStat("MaxElementalResist"))
		UpdateElementalResist((int)StatChange.Values["MaxElementalResist"].Int, "Max");
}

// Get percentage to next level
float _Character::GetNextLevelPercent() const {
	float Percent = 0;

	if(ExperienceNextLevel > 0)
		Percent = 1.0f - (float)ExperienceNeeded / ExperienceNextLevel;

	return Percent;
}

// Get adjusted item cost
int64_t _Character::GetItemCost(int64_t ItemCost) {
	if(Attributes["VendorDiscount"].Int > 0) {
		ItemCost *= (100 - Attributes["VendorDiscount"].Int) * 0.01;
		if(ItemCost <= 0)
			ItemCost = 1;
	}

	return ItemCost;
}

// Determines if the player can accept movement keys held down
bool _Character::AcceptingMoveInput() {
	if(Object->Battle)
		return false;

	if(Object->Controller->WaitForServer)
		return false;

	if(Vendor)
		return false;

	if(Trader)
		return false;

	if(Blacksmith)
		return false;

	if(Enchanter)
		return false;

	if(Disenchanter)
		return false;

	if(Minigame)
		return false;

	if(!IsAlive())
		return false;

	return true;
}

// Determine if player can battle
bool _Character::CanBattle(bool Manual) const {
	if(Object->Battle)
		return false;

	if(!IsAlive())
		return false;

	if(Manual)
		return true;

	return Status == STATUS_NONE && !Invisible;
}

// Generates the number of moves until the next battle
void _Character::GenerateNextBattle() {
	if(Attributes["Attractant"].Int)
		NextBattle = (int)Attributes["Attractant"].Int;
	else
		NextBattle = ae::GetRandomInt(BATTLE_MINSTEPS, BATTLE_MAXSTEPS);
}

// Generate damage
int64_t _Character::GenerateDamage() {
	return ae::GetRandomInt(Attributes["MinDamage"].Int, Attributes["MaxDamage"].Int);
}

// Get damage power from a type
double _Character::GetDamagePowerMultiplier(uint32_t DamageTypeID, bool Consumable) {
	double Multiplier = Consumable ? Attributes["ConsumePower"].Mult() : 1.0;

	switch(DamageTypeID) {
		case 2: Multiplier *= Attributes["PhysicalPower"].Mult(); break;
		case 3: Multiplier *= Attributes["FirePower"].Mult(); break;
		case 4: Multiplier *= Attributes["ColdPower"].Mult(); break;
		case 5: Multiplier *= Attributes["LightningPower"].Mult(); break;
		case 6: Multiplier *= Attributes["PoisonPower"].Mult(); break;
		case 7: Multiplier *= Attributes["BleedPower"].Mult(); break;
	}

	return Multiplier;
}

// Update counts on action bar
void _Character::RefreshActionBarCount() {
	SkillPointsOnActionBar = 0;
	for(size_t i = 0; i < ActionBar.size(); i++) {
		const _Item *Item = ActionBar[i].Item;
		if(Item) {
			if(Item->IsSkill() && HasLearned(Item))
				SkillPointsOnActionBar += Skills[Item->ID];
			else
				ActionBar[i].Count = Object->Inventory->CountItem(Item);
		}
		else
			ActionBar[i].Count = 0;
	}
}

// Return an action struct from an action bar slot
bool _Character::GetActionFromActionBar(_Action &ReturnAction, size_t Slot) {
	if(Slot >= ActionBar.size())
		return false;

	// Set item
	ReturnAction.Item = ActionBar[Slot].Item;
	if(!ReturnAction.Item)
		return false;

	// Determine if item is a skill, then look at object's skill levels
	if(ReturnAction.Item->IsSkill() && HasLearned(ReturnAction.Item)) {
		ReturnAction.Level = Skills[ReturnAction.Item->ID];
		if(ReturnAction.Level > 0) {
			ReturnAction.Level += Attributes["AllSkills"].Int;
			if(MaxSkillLevels.find(ReturnAction.Item->ID) != MaxSkillLevels.end())
				ReturnAction.Level = std::min(ReturnAction.Level, MaxSkillLevels.at(ReturnAction.Item->ID));
		}
	}
	else
		ReturnAction.Level = ReturnAction.Item->Level;

	// Set duration for certain items
	ReturnAction.Duration = ReturnAction.Item->Duration;

	return true;
}

// Get a vector of summon structs from the character's list of status effects
void _Character::GetSummonsFromBuffs(std::vector<std::pair<_Summon, _StatusEffect *> > &Summons) {
	std::unordered_map<_Item *, int> SummonLimits;
	for(auto &StatusEffect : StatusEffects) {

		// See if the buff has a summon skill attached to it
		_Item *SummonSkill = nullptr;
		if(Object->Scripting->StartMethodCall(StatusEffect->Buff->Script, "GetSummonSkill")) {
			Object->Scripting->MethodCall(0, 1);
			SummonSkill = (_Item *)Object->Scripting->GetPointer(1);
			Object->Scripting->FinishMethodCall();
		}

		// Check for skill
		if(!SummonSkill)
			continue;

		// Find summon skill on action bar
		for(auto &Action : ActionBar) {
			if(Action.Item != SummonSkill)
				continue;

			// Get level of skill
			_ActionResult ActionResult;
			ActionResult.Summons.reserve(BATTLE_MAX_OBJECTS_PER_SIDE);
			if(!GetActionFromActionBar(ActionResult.ActionUsed, (size_t)Action.ActionBarSlot))
				break;

			// Get summon stats
			ActionResult.Source.Object = Object;
			ActionResult.SummonBuff = StatusEffect->Buff;
			Action.Item->Use(Object->Scripting, ActionResult);

			// Get summon limit for skill
			if(SummonLimits.find(SummonSkill) == SummonLimits.end())
				SummonLimits[SummonSkill] = ActionResult.Summons[0].Limit;

			// Add summons to list
			if(ActionResult.Summons[0].ID) {
				int AllowedSummons = std::min((int)StatusEffect->Level, SummonLimits[SummonSkill]);
				for(int i = 0; i < AllowedSummons; i++) {
					SummonLimits[SummonSkill]--;
					Summons.push_back(std::pair<_Summon, _StatusEffect *>(ActionResult.Summons[0], StatusEffect));
				}
			}
		}
	}
}

// Determine if a skill can be equipped from the skill screen
bool _Character::CanEquipSkill(const _Item *Skill) {

	// Find existing action
	for(size_t i = 0; i < (size_t)SkillBarSize; i++) {
		if(ActionBar[i].Item == Skill)
			return false;
	}

	// Find an empty slot
	for(size_t i = 0; i < (size_t)SkillBarSize; i++) {
		if(!ActionBar[i].Item)
			return true;
	}

	return false;
}

// Return true if the object has the skill unlocked
bool _Character::HasLearned(const _Item *Skill) const {
	if(!Skill)
		return false;

	if(Skills.find(Skill->ID) != Skills.end())
		return true;

	return false;
}

// Updates a skill level
void _Character::AdjustSkillLevel(uint32_t SkillID, int Amount, bool SoftMax, bool SoftMin) {
	if(SkillID == 0)
		return;

	const _Item *Skill = Object->Stats->Items.at(SkillID);
	if(Skill == nullptr)
		return;

	// Increasing
	if(Amount > 0) {

		// Get max level for skill
		int MaxLevel = std::min(MaxSkillLevels[SkillID], Skill->MaxLevel);

		// Get number of points until max level is hit
		int PointsToMax = MaxLevel - Skills[SkillID];
		if(SoftMax) {
			PointsToMax -= Attributes["AllSkills"].Int;

			// All skills brings it over the max, so give one point at level 0 only
			if(PointsToMax <= 0)
				PointsToMax = Skills[SkillID] == 0 ? 1 : 0;
		}

		// Cap points to spend
		int PointsToSpend = std::min(GetSkillPointsAvailable(), Amount);
		PointsToSpend = std::min(PointsToSpend, PointsToMax);

		// Update level
		Skills[SkillID] += PointsToSpend;
	}
	// Decreasing
	else if(Amount < 0) {

		// SoftMin flag sets skill to 1 instead of 0
		Amount = std::max((int)SoftMin - Skills[SkillID], Amount);

		// Update level
		Skills[SkillID] += Amount;
		if(Skills[SkillID] < 0)
			Skills[SkillID] = 0;

		// Update action bar
		if(Skills[SkillID] == 0) {
			for(size_t i = 0; i < (size_t)SkillBarSize; i++) {
				if(ActionBar[i].Item == Skill) {
					ActionBar[i].Clear();
					break;
				}
			}
		}
	}
}

// Initialize max skill levels based on learned skills and clamp skill levels
void _Character::ValidateSkillLevels() {
	for(auto &Skill : Skills) {
		const _Item *SkillItem = Object->Stats->Items.at(Skill.first);

		// Set up min skill level
		auto MinSkillLevelIterator = MinSkillLevels.find(Skill.first);
		if(MinSkillLevelIterator == MinSkillLevels.end())
			MinSkillLevels[Skill.first] = std::min(GAME_DEFAULT_MAX_SKILL_LEVEL, SkillItem->MaxLevel);

		// Set up max skill level
		auto MaxSkillLevelIterator = MaxSkillLevels.find(Skill.first);
		if(MaxSkillLevelIterator != MaxSkillLevels.end())
			Skill.second = std::min(MaxSkillLevelIterator->second, Skill.second);
		else
			MaxSkillLevels[Skill.first] = std::min(GAME_DEFAULT_MAX_SKILL_LEVEL, SkillItem->MaxLevel);
	}
}

// Update status effects' levels given a buff ID
bool _Character::UpdateBuffLevel(uint32_t BuffID, int64_t Level) {
	bool Updated = false;
	for(auto &StatusEffect : StatusEffects) {
		if(StatusEffect->Buff->ID == BuffID) {
			StatusEffect->Level = Level;
			Updated = true;
		}
	}

	return Updated;
}

// Delete status effects given a buff ID, return true if at least one deleted
bool _Character::ClearBuff(uint32_t BuffID) {
	bool Deleted = false;
	for(auto &StatusEffect : StatusEffects) {
		if(StatusEffect->Buff->ID == BuffID) {
			StatusEffect->Deleted = true;
			Deleted = true;
		}
	}

	return Deleted;
}

// Determine if character has unlocked trading
bool _Character::CanTrade() const {
	return Object->ModelID == 7 || Level >= GAME_TRADING_LEVEL;
}

// Reset ui state variables
void _Character::ResetUIState(bool ResetMenuState) {
	InventoryOpen = false;
	SkillsOpen = false;
	Vendor = nullptr;
	Trader = nullptr;
	Blacksmith = nullptr;
	Enchanter = nullptr;
	Disenchanter = nullptr;
	Minigame = nullptr;
	TeleportTime = 0.0;
	if(ResetMenuState)
		MenuOpen = false;
}

// Create UI elements for all status effects
void _Character::CreateAllStatusEffectElements() {
	if(Object->Server)
		return;

	for(auto &StatusEffect : StatusEffects)
		CreateStatusEffectElements(StatusEffect);
}

// Create UI elements for a status effect
void _Character::CreateStatusEffectElements(_StatusEffect *StatusEffect) {
	if(Object->Server)
		return;

	// Add to client's hud
	if(PlayState.Player && PlayState.Player->Character == this) {
		if(StatusEffect->HUDElement)
			throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " StatusEffect->HUDElement already exists!");

		StatusEffect->HUDElement = StatusEffect->CreateElement(HUD.StatusEffectsElement);
	}

	// Add to battle element
	if(Object->Battle && Object->Fighter->BattleElement) {
		if(StatusEffect->BattleElement)
			throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " StatusEffect->BattleElement already exists!");

		StatusEffect->BattleElement = StatusEffect->CreateElement(Object->Fighter->BattleElement);
	}
}

// Add status effect, return true if added
bool _Character::AddStatusEffect(_StatusEffect *StatusEffect) {
	if(!StatusEffect)
		return false;

	// Reduce duration of stun/slow with resist
	if(StatusEffect->Buff->Name == "Stunned" || StatusEffect->Buff->Name == "Slowed" || StatusEffect->Buff->Name == "Taunted") {
		StatusEffect->Duration *= 1.0 - Attributes["StunResist"].Mult();
		StatusEffect->MaxDuration *= 1.0 - Attributes["StunResist"].Mult();
	}

	// Look for existing buff to update
	for(auto &ExistingEffect : StatusEffects) {
		if(StatusEffect->Buff != ExistingEffect->Buff)
			continue;

		// Add to level of summon buff
		if(StatusEffect->Buff->Summon) {
			ExistingEffect->Level += StatusEffect->Level;
		}
		// Overwrite buff
		else if(StatusEffect->Buff->Overwrite) {
			ExistingEffect->Time = 0.0;
			ExistingEffect->Duration = StatusEffect->Duration;
			ExistingEffect->MaxDuration = StatusEffect->MaxDuration;
			ExistingEffect->Level = StatusEffect->Level;
		}
		// Stack
		else if(StatusEffect->Buff->Stack) {

			// Stack levels
			ExistingEffect->Level += StatusEffect->Level;

			// Take larger duration
			ExistingEffect->Duration = std::max(ExistingEffect->Duration, StatusEffect->Duration);
			ExistingEffect->MaxDuration = std::max(ExistingEffect->MaxDuration, StatusEffect->MaxDuration);

			// Add to stacks
			ExistingEffect->Stacks.push_back(StatusEffect);
		}
		// Extend duration
		else if(StatusEffect->Buff->Extend) {
			ExistingEffect->Duration += StatusEffect->Duration;
			ExistingEffect->MaxDuration += StatusEffect->MaxDuration;
		}
		// Update duration and take highest level
		else {

			// Take larger duration
			if(StatusEffect->Duration > ExistingEffect->Duration) {
				ExistingEffect->Duration = StatusEffect->Duration;
				ExistingEffect->MaxDuration = StatusEffect->MaxDuration;
			}
			else {
				ExistingEffect->Duration = std::max(ExistingEffect->Duration, StatusEffect->Duration);
				ExistingEffect->MaxDuration = std::max(ExistingEffect->MaxDuration, StatusEffect->MaxDuration);
			}

			// Take highest level
			ExistingEffect->Level = std::max(ExistingEffect->Level, StatusEffect->Level);
		}

		// Update attributes
		ExistingEffect->Source = StatusEffect->Source;
		ExistingEffect->TargetCount = StatusEffect->TargetCount;
		ExistingEffect->Priority = StatusEffect->Priority;
		ExistingEffect->Modifier = std::max(ExistingEffect->Modifier, StatusEffect->Modifier);

		return false;
	}

	// Set first stack
	if(StatusEffect->Buff->Stack)
		StatusEffect->Stacks.push_back(StatusEffect);

	// Add to list of status effects according to rank
	auto Iterator = std::lower_bound(StatusEffects.begin(), StatusEffects.end(), StatusEffect, [](const _StatusEffect *Left, const _StatusEffect *Right) {
		return Left->Buff->Rank < Right->Buff->Rank;
	});
	StatusEffects.insert(Iterator, StatusEffect);

	// Add to HUD if necessary
	CreateStatusEffectElements(StatusEffect);

	return true;
}

// Delete memory used by status effects
void _Character::DeleteStatusEffects() {
	for(auto &StatusEffect : StatusEffects)
		delete StatusEffect;

	StatusEffects.clear();
}

// Update status of character
uint8_t _Character::GetStatus() {
	if(!IsAlive())
		return STATUS_DEAD;
	else if(Object->Battle)
		return STATUS_BATTLE;
	else if(WaitingForTrade)
		return STATUS_TRADE;
	else if(Vendor)
		return STATUS_VENDOR;
	else if(Trader)
		return STATUS_TRADER;
	else if(Blacksmith)
		return STATUS_BLACKSMITH;
	else if(Enchanter)
		return STATUS_SKILLS;
	else if(Disenchanter)
		return STATUS_SKILLS;
	else if(Minigame)
		return STATUS_MINIGAME;
	else if(InventoryOpen)
		return STATUS_INVENTORY;
	else if(SkillsOpen)
		return STATUS_SKILLS;
	else if(MenuOpen)
		return STATUS_MENU;
	else if(TeleportTime > 0)
		return STATUS_TELEPORT;

	return STATUS_NONE;
}

// Update status texture from status
void _Character::UpdateStatusTexture() {

	switch(Status) {
		case _Character::STATUS_NONE:
			Object->Character->StatusTexture = nullptr;
		break;
		case _Character::STATUS_MENU:
			Object->Character->StatusTexture = ae::Assets.Textures["textures/status/pause.webp"];
		break;
		case _Character::STATUS_INVENTORY:
			Object->Character->StatusTexture = ae::Assets.Textures["textures/status/bag.webp"];
		break;
		case _Character::STATUS_VENDOR:
			Object->Character->StatusTexture = ae::Assets.Textures["textures/status/vendor.webp"];
		break;
		case _Character::STATUS_SKILLS:
			Object->Character->StatusTexture = ae::Assets.Textures["textures/status/skills.webp"];
		break;
		case _Character::STATUS_TRADE:
			Object->Character->StatusTexture = ae::Assets.Textures["textures/status/trade.webp"];
		break;
		case _Character::STATUS_TRADER:
			Object->Character->StatusTexture = ae::Assets.Textures["textures/status/vendor.webp"];
		break;
		case _Character::STATUS_BLACKSMITH:
			Object->Character->StatusTexture = ae::Assets.Textures["textures/status/vendor.webp"];
		break;
		case _Character::STATUS_MINIGAME:
			Object->Character->StatusTexture = ae::Assets.Textures["textures/status/vendor.webp"];
		break;
		case _Character::STATUS_TELEPORT:
			Object->Character->StatusTexture = ae::Assets.Textures["textures/status/teleport.webp"];
		break;
		case _Character::STATUS_BATTLE:
			Object->Character->StatusTexture = ae::Assets.Textures["textures/status/battle.webp"];
		break;
		case _Character::STATUS_DEAD:
			Object->Character->StatusTexture = ae::Assets.Textures["textures/status/dead.webp"];
		break;
		default:
		break;
	}
}

// Format an int64 value for display and return type of font that should be used
int _Character::FormatValue(std::stringstream &Buffer, const std::string &AttributeName) {

	int64_t Value = Attributes[AttributeName].Int;
	if(Value > 9999) {
		ae::FormatSI<int64_t>(Buffer, Attributes[AttributeName].Int, ae::RoundDown2);
		return 1;
	}

	Buffer << Value;

	return 0;
}

// Format display of current / max stat
ae::_Font *_Character::FormatCurrentMax(std::stringstream &Buffer, const std::string &AttributeName) {

	// Get current value
	int Size = FormatValue(Buffer, AttributeName);
	Buffer << " / ";

	// Get max value
	Size = std::max(Size, FormatValue(Buffer, "Max" + AttributeName));

	// Pick font
	ae::_Font *DisplayFont = Size ? ae::Assets.Fonts["hud_bar"] : ae::Assets.Fonts["hud_char"];

	return DisplayFont;
}

// Clear player unlocks
void _Character::ClearUnlocks() {
	Unlocks.clear();
	SkillPointsUnlocked = 0;
	BeltSize = ACTIONBAR_DEFAULT_BELTSIZE;
	SkillBarSize = ACTIONBAR_DEFAULT_SKILLBARSIZE;
	for(size_t i = 0; i < ActionBar.size(); i++)
		ActionBar[i].Clear();
}

// Unlock items based on search term and count. Return sum of levels
int _Character::UnlockBySearch(const std::string &Search, int Count) {

	std::vector<uint32_t> UnlockIDs;
	UnlockIDs.reserve((size_t)Count);

	// Get unlock ids
	ae::_Database *Database = Object->Stats->Database;
	Database->PrepareQuery("SELECT id FROM unlock WHERE name LIKE @search ORDER BY id LIMIT @limit");
	Database->BindString(1, Search);
	Database->BindInt(2, Count);
	while(Database->FetchRow()) {
		uint32_t ID = Database->GetInt<uint32_t>("id");
		UnlockIDs.push_back(ID);

		// Unlock for character
		Unlocks[ID].Level = 1;
	}
	Database->CloseQuery();

	// Get level
	int Sum = 0;
	for(const auto &UnlockID : UnlockIDs) {
		Database->PrepareQuery("SELECT level FROM item WHERE unlock_id = @unlock_id");
		Database->BindInt(1, UnlockID);
		if(Database->FetchRow()) {
			Sum += Database->GetInt<int>("level");
		}
		Database->CloseQuery();
	}

	return Sum;
}

// Return true if the object has the item unlocked
bool _Character::HasUnlocked(const _Item *Item) const {
	if(!Item)
		return false;

	if(Unlocks.find(Item->UnlockID) != Unlocks.end())
		return true;

	return false;
}

// Reset eternal levels
void _Character::ResetEternal(bool Evolves) {
	Attributes["EternalStrength"].Int = 0;
	Attributes["EternalGuard"].Int = 0;
	Attributes["EternalFortitude"].Int = 0;
	Attributes["EternalSpirit"].Int = 0;
	Attributes["EternalWisdom"].Int = 0;
	Attributes["EternalWealth"].Int = 0;
	Attributes["EternalKnowledge"].Int = 0;
	Attributes["EternalPain"].Int = 0;
	Attributes["EternalProtection"].Int = 0;
	if(Evolves) {
		Attributes["EternalAlacrity"].Int = 0;
		Attributes["EternalCommand"].Int = 0;
		Attributes["EternalWard"].Int = 0;
		Attributes["EternalImpatience"].Int = 0;
		Attributes["EternalCharisma"].Int = 0;
	}
}

// Reset rites
void _Character::ResetRites() {
	for(const auto &RiteName : Object->Stats->RiteStatNames)
		Attributes[RiteName].Int = 0;
}

// Determine if character can engage in pvp
bool _Character::CanPVP() const {
	return !Object->Battle && IsAlive();
}

// Determine if character can open trade window
bool _Character::CanOpenTrade() const {
	return IsAlive() && !Object->Battle;
}

// Determine if character can open skill screen
bool _Character::CanOpenSkills() const {
	return IsAlive() && !Object->Battle;
}

// Determine if character can open inventory screen
bool _Character::CanOpenInventory() const {
	return IsAlive() && !Object->Battle;
}

// Determine if character can open relic wheel
bool _Character::CanOpenRelicWheel() const {
	return IsAlive() && !Object->Battle;
}

// Determine if character can open party screen
bool _Character::CanOpenParty() const {
	return IsAlive() && !Object->Battle;
}

// Determine if character can teleport
bool _Character::CanTeleport() const {
	return IsAlive() && !Object->Battle;
}

// Get scope for character in current state
ScopeType _Character::GetScope() {
	return Object->Battle ? ScopeType::BATTLE : ScopeType::WORLD;
}
