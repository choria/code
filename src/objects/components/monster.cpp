/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <objects/components/monster.h>
#include <constants.h>

// Constructor
_Monster::_Monster(_Object *Object) : Object(Object) {
}

// Get difficulty multiplier
double _Monster::GetDifficultyMultiplier() const {
	return Difficulty * 0.01;
}

// Get difficulty damage multiplier
double _Monster::GetDamageMultiplier() const {
	return 1.0 + (Difficulty - 100) * BATTLE_DIFFICULTY_DAMAGE * 0.01;
}
