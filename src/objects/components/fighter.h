/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <objects/action.h>

// Forward Declarations
class _Object;

namespace ae {
	class _Element;
}

// Classes
class _Fighter {

	public:

		_Fighter(_Object *Object) : Object(Object) { }

		// Battle
		int GetStartingSide(const _Item *Item);

		// UI
		void CreateBattleElement(ae::_Element *Parent);
		void RemoveBattleElement();

		// Base
		_Object *Object{nullptr};

		// Render
		ae::_Element *BattleElement{nullptr};
		glm::vec2 BattleBaseOffset{0.0f};
		glm::vec2 ResultPosition{0.0f};
		glm::vec2 StatPosition{0.0f};

		// Targets
		std::unordered_map<uint8_t, _Object *> LastTarget;

		// State
		std::vector<uint32_t> ItemDropsReceived;
		double TurnTimer{0.0};
		int64_t GoldStolen{0};
		int ActionResultCount{-1};
		int Corpse{1};
		int TargetIndex{0};
		bool JoinedBattle{false};
		bool FleeBattle{false};
		uint8_t BattleSide{0};

	private:

};
