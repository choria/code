/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <objects/itemtype.h>
#include <string>
#include <vector>

const size_t NOSLOT = (size_t)-1;

// Forward Declarations
class _Item;
class _Object;
class _Stats;
struct _Trader;

namespace ae {
	class _Buffer;
}

enum EquipmentType : size_t {
	HEAD,
	BODY,
	LEGS,
	HAND1,
	HAND2,
	RING1,
	RING2,
	AMULET,
	RELIC,
	COUNT,
};

enum class BagType : size_t {
	NONE,
	EQUIPMENT,
	INVENTORY,
	TRADE,
	KEYS,
	STASH,
	COUNT,
};

// Single inventory slot
struct _InventorySlot {

	_InventorySlot() { }
	_InventorySlot(const _Item *Item, int Count) : Item(Item), Count(Count) { }

	void Serialize(ae::_Buffer &Data);
	void Unserialize(ae::_Buffer &Data, const _Stats *Stats);
	void Reset() { Item = nullptr; Upgrades = 0; Count = 0; }

	const _Item *Item{nullptr};
	int Upgrades{0};
	int Count{0};
};

// Bags contain multiple slots
struct _Bag {

	void Serialize(ae::_Buffer &Data);
	void Unserialize(ae::_Buffer &Data, const _Stats *Stats);

	size_t HasItemID(uint32_t ItemID);

	std::vector<_InventorySlot> Slots;
	std::string Name;
	BagType Type{BagType::NONE};
	bool StaticSize{true};
};

// Slots point to a bag and index in the bag
struct _Slot {

	_Slot() { }
	_Slot(BagType BagType, size_t Index) : Type(BagType), Index(Index) { }

	bool operator==(const _Slot &Slot) const { return this->Index == Slot.Index && this->Type == Slot.Type; }
	bool operator!=(const _Slot &Slot) const { return !(*this == Slot); }

	void Serialize(ae::_Buffer &Data) const;
	void Unserialize(ae::_Buffer &Data);
	void Reset() { Type = BagType::NONE; Index = NOSLOT; }

	BagType Type{BagType::NONE};
	size_t Index{NOSLOT};
};

// Classes
class _Inventory {

	friend class _Save;
	friend class _Stats;
	friend class _Server;

	public:

		_Inventory(_Object *Object);

		// Network
		void Serialize(ae::_Buffer &Data);
		void SerializeSlot(ae::_Buffer &Data, const _Slot &Slot);
		void Unserialize(ae::_Buffer &Data, const _Stats *Stats);
		void UnserializeSlot(ae::_Buffer &Data, const _Stats *Stats);

		bool FindItem(const _Item *Item, size_t &Slot, size_t StartSlot);
		void FindBestItem(const _Item *Item, _Slot &Slot, bool SearchStash);
		int CountItem(const _Item *Item);
		bool IsValidSlot(const _Slot &Slot) { return Slot.Type > BagType::NONE && Slot.Type < BagType::COUNT && Slot.Index < GetBag(Slot.Type).Slots.size(); }
		_InventorySlot &GetSlot(const _Slot &Slot) { return GetBag(Slot.Type).Slots[Slot.Index]; }
		int GetMaxCount(BagType BagType, ItemType ItemType);

		bool MoveInventory(ae::_Buffer &Data, const _Slot &NewSlot, const _Slot &OldSlot);
		int UpdateItemCount(const _Slot &Slot, int Amount);
		void SpendItems(const _Item *Item, int Count);
		_Slot FindSlotForItem(const _Item *Item, int Upgrades, int Count, bool TryStash);
		_Slot FindSlotForItemInBag(BagType BagType, const _Item *Item, int Upgrades, int Count);
		int AddItem(const _Item *Item, int Upgrades, int Count, bool TryStash, _Slot TargetSlot=_Slot());
		void MoveTradeToInventory();
		void UnequipItems();
		bool SplitStack(ae::_Buffer &Data, const _Slot &Slot, int Count);
		int Transfer(const _Slot &SourceSlot, BagType TargetBagType, std::vector<_Slot> &SlotsUpdated);
		bool IsTradable(const _Item *Item);
		bool IsStashable(const _Item *Item);

		// Vendors
		bool CanUpgradeSet(int BlacksmithMaxLevel);
		bool GetRequiredItemSlots(const _Trader *Trader, std::vector<_Slot> &RequiredItemSlots, _Slot &RewardItemSlot);

		// Bags
		_Bag &GetBag(BagType Bag) { return Bags[(size_t)Bag]; }
		std::vector<_Bag> &GetBags() { return Bags; }

		// Statics
		static std::vector<EquipmentType> UpgradeSetSlotTypes;

	private:

		bool CanEquipItem(size_t Slot, const _Item *Item);
		void SwapItem(const _Slot &NewSlot, const _Slot &OldSlot);
		bool CanSwap(const _Slot &NewSlot, const _Slot &OldSlot);

		// Objects
		_Object *Object{nullptr};

		// Items
		std::vector<_Bag> Bags;

};
