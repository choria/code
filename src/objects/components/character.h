/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <ae/type.h>
#include <objects/action.h>
#include <list>
#include <map>
#include <unordered_map>

// Forward Declarations
struct _Blacksmith;
struct _Disenchanter;
struct _Enchanter;
struct _MinigameType;
struct _Trader;
struct _Vendor;

// Structures
struct _Unlock {
	int Level{0};
};

struct _PrivilegeItem {
	_PrivilegeItem() { }
	_PrivilegeItem(const _Item *Item, int Count) : Item(Item), Count(Count) { }

	const _Item *Item{nullptr};
	int Count{0};
};

struct _SetData {
	int EquippedCount{0};
	int MaxLevel{0};
	int Level{0};
	bool Complete{false};
};

struct _Cooldown {
	double Duration{0.0};
	double MaxDuration{0.0};
};

// Classes
class _Character {

	public:

		enum StatusImageType {
			STATUS_NONE,
			STATUS_MENU,
			STATUS_INVENTORY,
			STATUS_VENDOR,
			STATUS_SKILLS,
			STATUS_TRADE,
			STATUS_TRADER,
			STATUS_BLACKSMITH,
			STATUS_MINIGAME,
			STATUS_BATTLE,
			STATUS_TELEPORT,
			STATUS_DEAD,
		};

		_Character(_Object *Object);
		~_Character();

		// Initialize
		void InitAttributes();

		// Updates
		void Update(double FrameTime);
		void UpdateHealth(int64_t &Value);
		void UpdateMana(int64_t Value);
		void UpdateGold(int64_t Value);
		void UpdateGoldLost(int64_t Value);
		void UpdateExperience(int64_t Value);
		void UpdateAllResist(int Value, const std::string &Prefix="");
		void UpdateElementalResist(int Value, const std::string &Prefix="");

		// Stats
		void CalculateStats();
		void CalculateLevelStats();
		float GetNextLevelPercent() const;
		bool IsAlive() const { return Attributes.at("Health").Int > 0; }
		float GetHealthPercent() const { return Attributes.at("MaxHealth").Int > 0 ? Attributes.at("Health").Int / (float)Attributes.at("MaxHealth").Int : 0; }
		float GetManaPercent() const { return Attributes.at("MaxMana").Int > 0 ? Attributes.at("Mana").Int / (float)Attributes.at("MaxMana").Int : 0; }
		int64_t GetItemCost(int64_t ItemCost);

		// Input
		bool AcceptingMoveInput();
		bool CanOpenTrade() const;
		bool CanOpenSkills() const;
		bool CanOpenInventory() const;
		bool CanOpenRelicWheel() const;
		bool CanOpenParty() const;
		bool CanTeleport() const;
		bool CanBattle(bool Manual) const;
		bool CanPVP() const;

		// Battle
		bool IsZoneOnCooldown(uint32_t Zone) { return BossCooldowns.find(Zone) != BossCooldowns.end(); }
		void GenerateNextBattle();
		int64_t GenerateDamage();
		double GetAverageDamage() const { return (Attributes.at("MinDamage").Int + Attributes.at("MaxDamage").Int) / 2.0; }
		double GetDamagePowerMultiplier(uint32_t DamageTypeID, bool Consumable=false);

		// Actions
		ScopeType GetScope();
		void RefreshActionBarCount();
		bool GetActionFromActionBar(_Action &ReturnAction, size_t Slot);
		void GetSummonsFromBuffs(std::vector<std::pair<_Summon, _StatusEffect *> > &Summons);
		bool CanEquipSkill(const _Item *Skill);

		// Skills
		bool HasLearned(const _Item *Skill) const;
		int GetSkillPointsAvailable() const { return SkillPoints - SkillPointsUsed; }
		void AdjustSkillLevel(uint32_t SkillID, int Amount, bool SoftMax, bool SoftMin);
		void ValidateSkillLevels();

		// Status effects
		bool UpdateBuffLevel(uint32_t BuffID, int64_t Level);
		bool ClearBuff(uint32_t BuffID);

		// UI
		void CreateAllStatusEffectElements();
		bool IsTrading() { return WaitingForTrade || TradePlayer; }
		bool CanTrade() const;
		void ResetUIState(bool ResetMenuState=true);
		bool AddStatusEffect(_StatusEffect *StatusEffect);
		void DeleteStatusEffects();
		uint8_t GetStatus();
		void UpdateStatusTexture();
		int FormatValue(std::stringstream &Buffer, const std::string &AttributeName);
		ae::_Font *FormatCurrentMax(std::stringstream &Buffer, const std::string &AttributeName);

		// Unlocks
		void ClearUnlocks();
		int UnlockBySearch(const std::string &Search, int Count);
		bool HasUnlocked(const _Item *Item) const;

		// Rebirth
		void ResetEternal(bool Evolves);
		void ResetRites();

		// Events
		bool CompareSpawn(ae::NetworkIDType MapID, uint32_t Point) { return SpawnMapID == MapID && Point == SpawnPoint; }

		// Base
		_Object *Object{nullptr};
		uint32_t ID{0};
		uint32_t AccountID{0};
		uint32_t BuildID{1};
		double UpdateTimer{0.0};

		// Render
		const ae::_Texture *StatusTexture{nullptr};
		const ae::_Texture *Portrait{nullptr};
		uint32_t PortraitID{0};

		// State
		std::unordered_map<uint32_t, double> BossCooldowns;
		std::unordered_map<uint32_t, int> BossKills;
		std::string PartyName;
		double IdleTime{0.0};
		int NextBattle{0};
		int Invisible{0};
		int TriggerCount{0};
		int Priority{0};
		bool Hardcore{false};
		bool OnTrigger{false};
		uint8_t Status{0};

		// Levels
		bool CalcLevelStats{true};
		int Level{0};
		int64_t ExperienceNeeded{0};
		int64_t ExperienceNextLevel{0};

		// Base attributes
		double BaseAttackPeriod{BATTLE_DEFAULTATTACKPERIOD};

		// Final attributes
		std::unordered_map<std::string, _Value> BaseAttributes;
		std::unordered_map<std::string, _Value> Attributes;
		std::unordered_map<uint32_t, _SetData> Sets;
		int BattleSpeedBeforeBuffs;

		// Status effects
		std::vector<_StatusEffect *> StatusEffects;

		// Unlocks
		std::unordered_map<uint32_t, _Unlock> Unlocks;

		// Skills
		std::unordered_map<uint32_t, int> Skills;
		std::unordered_map<uint32_t, int> MinSkillLevels;
		std::unordered_map<uint32_t, int> MaxSkillLevels;
		std::unordered_map<uint32_t, _Cooldown> Cooldowns;
		int SkillPoints{0};
		int SkillPointsUnlocked{0};
		int SkillPointsUsed{0};
		int SkillPointsOnActionBar{0};

		// Action bar
		std::vector<_Action> ActionBar;
		int BeltSize{ACTIONBAR_DEFAULT_BELTSIZE};
		int SkillBarSize{ACTIONBAR_DEFAULT_SKILLBARSIZE};

		// Events
		const _Vendor *Vendor{nullptr};
		const _Trader *Trader{nullptr};
		const _Blacksmith *Blacksmith{nullptr};
		const _Enchanter *Enchanter{nullptr};
		const _Disenchanter *Disenchanter{nullptr};
		const _MinigameType *Minigame{nullptr};
		int ViewingStash{0};
		uint32_t Seed{0};
		uint32_t PaidSeed{0};

		// Trading
		std::vector<_PrivilegeItem> LastPrivilege;
		_Object *TradePlayer{nullptr};
		int64_t TradeGold{0};
		bool WaitingForTrade{false};
		bool TradeAccepted{false};

		// Actions
		_Action Action;

		// Map
		ae::NetworkIDType LoadMapID{0};
		ae::NetworkIDType SpawnMapID{1};
		uint32_t SpawnPoint{0};
		double TeleportTime{0.0};

		// HUD
		bool MenuOpen{false};
		bool InventoryOpen{false};
		bool SkillsOpen{false};
		bool ShowMoveItems{false};

		// Bots
		bool Bot{false};
		std::list<void *> Path;

		// Debug
		bool GhostMode{false};

	private:

		void CreateStatusEffectElements(_StatusEffect *StatusEffect);
		void CalculateStatBonuses(_StatChange &StatChange);

};
