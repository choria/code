/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <list>

// Forward Declarations
class _Object;

// Classes
class _Controller {

	public:

		_Controller(_Object *Object) : Object(Object) { }

		// Base
		_Object *Object{nullptr};

		// Attributes
		std::list<int> InputStates;
		double MoveTime{0.0};
		double WaitTime{0.0};
		int DirectionMoved{0};
		bool UseCommand{false};
		bool WaitForServer{false};

	private:

};
