/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <ae/baseobject.h>
#include <ae/network.h>
#include <constants.h>
#include <glm/vec3.hpp>
#include <path/micropather.h>
#include <map>
#include <string>
#include <unordered_map>

// Forward Declarations
class _Battle;
class _Object;
class _Scripting;
class _Server;
class _Stats;
struct _MapStat;

namespace ae {
	class _Atlas;
	class _Buffer;
	class _Camera;
	class _Framebuffer;
	class _Peer;
	class _Program;
	class _Texture;
}

// Structures
struct _Event {

	_Event() { }
	_Event(uint32_t Type, uint32_t Data) : Type(Type), Data(Data) { }

	bool operator==(const _Event &Event) const { return Event.Type == Type && Event.Data == Data; }
	bool operator<(const _Event &Event) const { return std::tie(Event.Type, Event.Data) < std::tie(Type, Data); }

	uint32_t Type{0};
	uint32_t Data{0};
};

struct _Tile {
	uint32_t TextureIndex[2]{0, 0};
	uint32_t Zone{0};
	_Event Event;
	bool Wall{false};
	bool PVP{false};
};

// Classes
class _Map : public ae::_BaseObject, public micropather::Graph {

	public:

		enum EventType {
			EVENT_NONE,
			EVENT_SPAWN,
			EVENT_MAPENTRANCE,
			EVENT_MAPCHANGE,
			EVENT_VENDOR,
			EVENT_TRADER,
			EVENT_KEY,
			EVENT_SCRIPT,
			EVENT_PORTAL,
			EVENT_JUMP,
			EVENT_BLACKSMITH,
			EVENT_MINIGAME,
			EVENT_ENCHANTER,
			EVENT_TERRAIN,
			EVENT_STASH,
			EVENT_DISENCHANTER,
			EVENT_COUNT
		};

		_Map();
		~_Map() override;

		void AllocateMap();
		void ResizeMap(glm::ivec2 Offset, glm::ivec2 NewSize);
		void InitAtlas(const std::string AtlasPath, bool Static=false);
		void CloseAtlas();

		void Update(double FrameTime) override;

		// Events
		bool CheckEvents(_Object *Object, int Mode, bool Wait=false) const;
		void CheckBattle(_Object *Object, const _Tile *Tile) const;
		void RunEventScript(const _Tile *Tile, _Object *Object, int Mode) const;
		void IndexEvents();
		void GetClockAsString(std::stringstream &Buffer, bool Clock24Hour) const;
		void SetAmbientLightByClock();
		void StartEvent(_Object *Object, _Event Event, bool Remote) const;
		void SendBossCooldownMessage(_Object *Object, double Duration) const;
		bool IsPVPZone(const glm::ivec2 &Position) const;

		// Graphics
		void Render(ae::_Camera *Camera, ae::_Framebuffer *Framebuffer, _Object *ClientPlayer, double BlendFactor, int RenderFlags=0);
		void RenderUseText(ae::_Camera *Camera, _Object *ClientPlayer);
		void RenderEventText(ae::_Camera *Camera, _Object *ClientPlayer);
		void RenderText(ae::_Camera *Camera, _Object *ClientPlayer, std::stringstream &Buffer);
		void RenderPlayerNames(ae::_Camera *Camera, _Object *ClientPlayer);
		void RenderLayer(const std::string &Program, glm::vec4 &Bounds, const glm::vec3 &Offset, int Layer, bool Static=false);
		int AddLights(_Object *ClientPlayer, const std::vector<_Object *> &ObjectList, const ae::_Program *Program, glm::vec4 AABB);

		// Collision
		bool CanMoveTo(const glm::ivec2 &Position, _Object *Object);

		// Peer management
		void BroadcastPacket(ae::_Buffer &Buffer, ae::_Network::SendType Type=ae::_Network::SEND_RELIABLE);

		// Object management
		void SendObjectUpdates();
		void AddObject(_Object *Object);
		void RemoveObject(const _Object *RemoveObject);
		void SendObjectList(ae::_Peer *Peer);
		void GetPotentialBattlePlayers(const _Object *Player, float DistanceSquared, size_t Max, bool Manual, std::vector<_Object *> &Players) const;
		_Battle *GetCloseBattle(const _Object *Player, bool &HitPrivateParty, bool &HitFullBattle, bool &HitLevelRestriction, bool &HitBossBattle);
		void GetPVPPlayers(const _Object *Attacker, std::vector<_Object *> &Players, bool UsePVPZone);
		_Object *FindTradePlayer(const _Object *Player, float MaxDistanceSquared);
		_Object *FindDeadPlayer(const _Object *Player, float MaxDistanceSquared);
		bool FindEvent(const _Event &Event, glm::ivec2 &Position) const;
		void DeleteLight(const glm::ivec2 &Position);

		// Map editing
		bool IsValidPosition(const glm::ivec2 &Position) const { return Position.x >= 0 && Position.y >= 0 && Position.x < Size.x && Position.y < Size.y; }
		glm::vec2 GetValidPosition(const glm::vec2 &Position) const;
		glm::ivec2 GetValidCoord(const glm::ivec2 &Position) const;

		void GetTile(const glm::ivec2 &Position, _Tile &Tile) const { Tile = Tiles[Position.x][Position.y]; }
		const _Tile *GetTile(const glm::ivec2 &Position) const { return &Tiles[Position.x][Position.y]; }
		void SetTile(const glm::ivec2 &Position, const _Tile *Tile) { Tiles[Position.x][Position.y] = *Tile; }

		// File IO
		void Load(const _MapStat *MapStat, bool Static=false);
		bool Save(const std::string &Path);

		// Path finding
		void NodeToPosition(void *Node, glm::ivec2 &Position) {
			int Index = (int)(intptr_t)Node;
			Position.y = Index / Size.x;
			Position.x = Index - Position.y * Size.x;
		}

		void *PositionToNode(const glm::ivec2 &Position) { return (void *)(intptr_t)(Position.y * Size.x + Position.x); }
		int PositionToIndex(const glm::ivec2 &Position) { return Position.y * Size.x + Position.x; }
		glm::ivec2 IndexToPosition(int Index) { return glm::ivec2(Index % Size.x, Index / Size.x); }

		// Map data
		_Tile **Tiles{nullptr};
		glm::ivec2 Size{0};
		std::map<_Event, std::vector<glm::ivec2>> IndexedEvents;
		std::unordered_map<int, std::vector<std::string>> ObjectNames;

		// Graphics
		const ae::_Atlas *TileAtlas{nullptr};
		glm::vec4 AmbientLight{MAP_AMBIENT_LIGHT};
		glm::vec4 LightFilter;
		float AmbientBackground{1.0f};
		int OutsideFlag{1};
		double Clock{0.0};
		bool Headless{false};

		// Background
		glm::vec3 BackgroundOffset{0.0f};
		_Map *BackgroundMap{nullptr};

		// Objects
		std::vector<_Object *> Objects;
		std::vector<_Object *> StaticObjects;
		uint8_t UpdateID{0};

		// Stats
		const _Stats *Stats{nullptr};

		// Audio
		std::string Music;

		// Network
		_Server *Server{nullptr};

		// Editor
		uint32_t MaxZoneColors{0};
		uint32_t CurrentZoneColors{0};

		// Path finding
		micropather::MicroPather *Pather{nullptr};

	private:

		void FreeMap();

		// Path finding
		float LeastCostEstimate(void *StateStart, void *StateEnd) override;
		void AdjacentCost(void *State, std::vector<micropather::StateCost> *Neighbors) override;
		void PrintStateInfo(void *State) override { }

		// Rendering
		uint32_t TileVertexBufferID[2]{0, 0};
		uint32_t TileElementBufferID{0};
		glm::vec4 *TileVertices[2]{nullptr, nullptr};
		glm::u32vec3 *TileFaces{nullptr};

};
