/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <objects/item.h>
#include <ae/assets.h>
#include <ae/font.h>
#include <ae/graphics.h>
#include <ae/input.h>
#include <ae/texture.h>
#include <ae/util.h>
#include <hud/hud.h>
#include <hud/inventory_screen.h>
#include <hud/skill_screen.h>
#include <objects/components/character.h>
#include <objects/components/fighter.h>
#include <objects/components/inventory.h>
#include <objects/object.h>
#include <states/play.h>
#include <config.h>
#include <scripting.h>
#include <stats.h>
#include <SDL_keycode.h>
#include <iomanip>

// Category names
const std::string SkillCategories[5] = {
	"Passive Skill",
	"Attack Skill",
	"Spell",
	"Summon Spell",
	"Skill",
};

// Draw tooltip container
void _Item::DrawTooltip(const glm::vec2 &Position, _Object *Player, const _Cursor &Tooltip, const _Slot &CompareSlot) const {

	// Get final height of tooltip
	float Height = DrawTooltip(false, 0.0f, Position, Player, Tooltip, CompareSlot);

	// Draw
	DrawTooltip(true, Height, Position, Player, Tooltip, CompareSlot);
}

// Draw tooltip
float _Item::DrawTooltip(bool Render, float Height, const glm::vec2 &Position, _Object *Player, const _Cursor &Tooltip, const _Slot &CompareSlot) const {
	if(!Player)
		return 0.0f;

	// Set up UI element
	ae::_Element *TooltipElement = ae::Assets.Elements["element_item_tooltip"];
	TooltipElement->SetActive(Render);

	// Set up padding
	float SidePadding = 36 * ae::_Element::GetUIScale();
	float SpacingY = 36 * ae::_Element::GetUIScale();
	float SubtitleSpacingY = 27 * ae::_Element::GetUIScale();
	float ControlSpacingY = 28 * ae::_Element::GetUIScale();
	float LargeSpacingY = 56 * ae::_Element::GetUIScale();
	glm::vec2 Spacing = glm::vec2(10, 0) * ae::_Element::GetUIScale();

	// Set up window size
	glm::vec2 Size;
	Size.x = INVENTORY_TOOLTIP_WIDTH * ae::_Element::GetUIScale();
	Size.y = Height + 10 * ae::_Element::GetUIScale();

	// Increase size for long prices
	if(Player->Character->Vendor) {
		if(Tooltip.Cost >= 1e12)
			Size.x += 90 * ae::_Element::GetUIScale();
	}

	// Increase width for long names
	ae::_TextBounds TextBounds;
	ae::Assets.Fonts["hud_medium"]->GetStringDimensions(Name, TextBounds);
	Size.x = std::max(Size.x, (float)TextBounds.Width / ae::_Element::GetUIScale()) + SidePadding * 2;

	// Calculate bounds of window
	TooltipElement->Offset = glm::vec2(0, 0);
	TooltipElement->Size = Size;
	TooltipElement->CalculateBounds(false);

	// Position window
	glm::vec2 WindowOffset = Position;

	// Center vertically
	if(Position.y < 0) {
		WindowOffset.y = (ae::Graphics.CurrentSize.y - Size.y) / 2;
	}
	else {
		WindowOffset.x += INVENTORY_TOOLTIP_OFFSET * ae::_Element::GetUIScale();
		WindowOffset.y += -(TooltipElement->Bounds.End.y - TooltipElement->Bounds.Start.y) / 2;
	}

	// Reposition window if out of bounds
	if(WindowOffset.x + Size.x > ae::Graphics.Element->Bounds.End.x - INVENTORY_TOOLTIP_PADDING)
		WindowOffset.x -= Size.x + INVENTORY_TOOLTIP_OFFSET + INVENTORY_TOOLTIP_PADDING;
	if(WindowOffset.y + Size.y > ae::Graphics.Element->Bounds.End.y - INVENTORY_TOOLTIP_PADDING)
		WindowOffset.y -= (WindowOffset.y + Size.y) - (ae::Graphics.Element->Bounds.End.y - INVENTORY_TOOLTIP_PADDING);
	if(WindowOffset.y < 0)
		WindowOffset.y = 0;

	// Get final bounds with offset
	TooltipElement->Offset = WindowOffset;
	TooltipElement->CalculateBounds(false);

	// Render tooltip background
	TooltipElement->Render();
	TooltipElement->SetActive(false);

	// Set draw position to center of window
	glm::vec2 DrawPosition = glm::vec2((int)(TooltipElement->Size.x / 2 + WindowOffset.x), (int)TooltipElement->Bounds.Start.y);

	// Start at zero when calculating final window height
	if(!Render)
		DrawPosition.y = 0.0f;

	// Draw title
	DrawPosition.y += 48 * ae::_Element::GetUIScale();
	if(Render)
		ae::Assets.Fonts["menu_buttons"]->DrawText(Name, glm::ivec2(DrawPosition), ae::CENTER_BASELINE);

	// Get type text
	std::string TypeText;
	glm::vec4 TypeColor(1.0f);
	if(Category && Category <= 5)
		TypeText = SkillCategories[Category-1];
	else if(Type != ItemType::NONE) {
		TypeText = Player->Stats->ItemTypes.at((uint32_t)Type);
		if(Type == ItemType::CONSUMABLE && Scope == ScopeType::BATTLE) {
			TypeText = "Battle " + TypeText;
		}
		else if(IsCursed()) {
			TypeText = "Cursed " + TypeText;
			TypeColor = ae::Assets.Colors["red"];
		}
	}

	// Draw item type
	DrawPosition.y += 32 * ae::_Element::GetUIScale();
	if(Render)
		ae::Assets.Fonts["hud_small"]->DrawText(TypeText, glm::ivec2(DrawPosition), ae::CENTER_BASELINE, TypeColor);

	// Draw map
	if(Type == ItemType::MAP) {
		if(AltTexture == nullptr)
			return DrawPosition.y;

		// Restrict viewing
		if(Tooltip.Window == _HUD::WINDOW_VENDOR || Tooltip.Window == _HUD::WINDOW_TRADER || Tooltip.Window == _HUD::WINDOW_TRADETHEIRS) {
			DrawPosition.y += LargeSpacingY;
			if(Render)
				ae::Assets.Fonts["hud_small"]->DrawText("Purchase to view", glm::ivec2(DrawPosition), ae::CENTER_BASELINE, ae::Assets.Colors["red"]);

			return DrawPosition.y + ControlSpacingY;
		}

		DrawPosition.y += ControlSpacingY;

		// Get size
		glm::vec2 TextureSize = glm::vec2(AltTexture->Size) * (TooltipElement->Size.x / AltTexture->Size.x / ae::_Element::GetUIScale()) * 0.45f * ae::_Element::GetUIScale();
		if(!Render)
			return DrawPosition.y + TextureSize.y * 2 + ControlSpacingY;

		// Draw image
		DrawPosition.y += TextureSize.y;
		ae::_Bounds Bounds(DrawPosition - TextureSize, DrawPosition + TextureSize);
		ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos_uv"]);
		ae::Graphics.SetColor(glm::vec4(1.0f));
		ae::Graphics.DrawImage(Bounds, AltTexture, true);

		return 0.0f;
	}

	// Set up view state
	ScopeType PlayerScope = Player->Character->GetScope();
	int DrawLevel = Level;
	int DrawNextLevel = 0;
	int PlayerMaxSkillLevel = 0;
	int SkillLevel = 0;
	int EnchanterMaxLevel = 0;
	int Upgrades = Tooltip.InventorySlot.Upgrades;
	int SkillButtonDirection = 0;
	bool IsLocked = false;
	bool ShowLevel = false;
	bool ShowingNextUpgradeLevel = false;
	bool Blacksmith = (Tooltip.Window == _HUD::WINDOW_BLACKSMITH);
	bool ShowAltText = ae::Input.ModKeyDown(KMOD_ALT);
	bool ShowFractions = false;
	if(Tooltip.Window == _HUD::WINDOW_SKILLS) {

		// Get skill button direction
		if(Tooltip.Element) {
			if(Tooltip.Element->ID == "button_skills_plus")
				SkillButtonDirection = 1;
			else if(Tooltip.Element->ID == "button_skills_minus")
				SkillButtonDirection = -1;
		}

		if(ShowAltText)
			ShowFractions = true;
	}
	else
		ShowFractions = ae::Input.ModKeyDown(KMOD_ALT) || Blacksmith;

	if(IsSkill()) {

		// Get max skill level
		if(Tooltip.Window == _HUD::WINDOW_ENCHANTER) {
			auto MaxSkillLevelIterator = Player->Character->MaxSkillLevels.find(ID);
			if(MaxSkillLevelIterator != Player->Character->MaxSkillLevels.end()) {
				DrawLevel = std::min(MaxSkillLevelIterator->second + 1, GAME_MAX_SKILL_LEVEL);
				if(Player->Character->Enchanter)
					EnchanterMaxLevel = Player->Character->Enchanter->Level;
			}
		}
		else if(Tooltip.Window == _HUD::WINDOW_DISENCHANTER) {
			auto MaxSkillLevelIterator = Player->Character->MaxSkillLevels.find(ID);
			if(MaxSkillLevelIterator != Player->Character->MaxSkillLevels.end()) {
				DrawLevel = MaxSkillLevelIterator->second - 1;
				EnchanterMaxLevel = -1;
			}
		}
		else {

			// Get skill level
			auto SkillIterator = Player->Character->Skills.find(ID);
			if(SkillIterator != Player->Character->Skills.end()) {
				PlayerMaxSkillLevel = Player->Character->MaxSkillLevels[ID];
				SkillLevel = DrawLevel = SkillIterator->second;

				// Modify next level increment value when ctrl/shift is held
				int NextLevelIncrement = 1;
				if(SkillButtonDirection) {
					if(ae::Input.ModKeyDown(KMOD_CTRL))
						NextLevelIncrement = 50 * SkillButtonDirection;
					else if(ae::Input.ModKeyDown(KMOD_SHIFT))
						NextLevelIncrement = 5 * SkillButtonDirection;
					else
						NextLevelIncrement = 1 * SkillButtonDirection;
				}

				// Cap the level to the amount of skill points the player can spend
				if(NextLevelIncrement > 0 && SkillButtonDirection > 0)
					NextLevelIncrement = std::min(NextLevelIncrement, Player->Character->GetSkillPointsAvailable());

				// When skill level is zero, set next level to increment value
				if(SkillLevel == 0 && SkillButtonDirection >= 0) {
					DrawLevel = 1;
					NextLevelIncrement = std::max(1, NextLevelIncrement - 1);
				}

				// Handle bonus points
				if(SkillIterator->second > 0 || SkillButtonDirection) {
					DrawLevel += Player->Character->Attributes["AllSkills"].Int;
					DrawLevel = std::min(DrawLevel, PlayerMaxSkillLevel);
				}

				// Set next draw level
				DrawNextLevel = std::min(DrawLevel + NextLevelIncrement, PlayerMaxSkillLevel);

				// Hide next level info when at max level or below level one
				if((DrawLevel == PlayerMaxSkillLevel && SkillButtonDirection >= 0) || DrawNextLevel < 1)
					DrawNextLevel = 0;
			}
			else
				IsLocked = true;
		}

		// For skills set minimum of level 1
		DrawLevel = std::max(DrawLevel, 1);

		// Show vendor skills at level 1
		if(Tooltip.Window == _HUD::WINDOW_EQUIPMENT || Tooltip.Window == _HUD::WINDOW_INVENTORY || Tooltip.Window == _HUD::WINDOW_VENDOR || Tooltip.Window == _HUD::WINDOW_TRADER)
			DrawLevel = 1;

		// Determine whether to show level
		if(Tooltip.Window == _HUD::WINDOW_SKILLS || Tooltip.Window == _HUD::WINDOW_ENCHANTER || Tooltip.Window == _HUD::WINDOW_DISENCHANTER || Tooltip.Window == _HUD::WINDOW_SKILLBAR)
			ShowLevel = true;
	}

	// Draw target text
	if(TargetID != TargetType::NONE) {
		DrawPosition.y += SubtitleSpacingY;

		// Get target type and count
		const _TargetType &TargetType = Player->Stats->TargetTypes.at((uint32_t)TargetID);
		int TargetCount = GetTargetCount(Player->Scripting, Player, DrawLevel);

		// Build text
		std::stringstream Buffer;
		if(TargetID == TargetType::ENEMY_CORPSE_AOE) {
			Buffer << "Target Enemy Corpse";
		}
		else {
			Buffer << "Target ";
			if(TargetID == TargetType::SELF) {
				Buffer << TargetType.Name;
			}
			else {
				std::string TargetText = (TargetCount == 1 ? TargetType.Singular : TargetType.Plural);
				TargetText[0] = toupper(TargetText[0]);

				Buffer << TargetCount << " ";
				if(!TargetAlive)
					Buffer << "Dead ";

				Buffer << TargetText;
			}
		}

		// Draw text
		if(Render)
			ae::Assets.Fonts["hud_small"]->DrawText(Buffer.str(), glm::ivec2(DrawPosition), ae::CENTER_BASELINE, glm::vec4(1.0f));
	}

	// Draw cooldown
	if(!IsEquippable() && std::abs(Cooldown) > 0.0) {
		DrawPosition.y += SubtitleSpacingY;
		if(Render) {
			std::stringstream Buffer;
			double Reduction = Cooldown > 0.0 ? Player->Character->Attributes["Cooldowns"].Mult() : 1.0;
			double Duration = std::max(GAME_MIN_COOLDOWN, std::abs(Cooldown) * Reduction);
			if(!ae::Input.ModKeyDown(KMOD_ALT) && Duration > 60.0)
				Buffer << std::fixed << std::setprecision(1) << (int)(Duration / 60.0) << " minute cooldown";
			else
				Buffer << std::fixed << std::setprecision(1) << Duration << " second cooldown";

			ae::Assets.Fonts["hud_small"]->DrawText(Buffer.str(), glm::ivec2(DrawPosition), ae::CENTER_BASELINE, ae::Assets.Colors["red"]);
		}
	}

	if(!IsSkill()){

		// Draw upgrade level for items
		if(IsEquippable()) {
			DrawPosition.y += SubtitleSpacingY;
			if(Render) {

				// Show next upgrade stats if holding ctrl and item is not in the blacksmith slot
				if(Player->Character->Blacksmith && ae::Input.ModKeyDown(KMOD_CTRL) && CompareSlot.Index != NOSLOT && Tooltip.Window != _HUD::WINDOW_BLACKSMITH) {
					if(Upgrades + 1 <= MaxLevel)
						ShowingNextUpgradeLevel = true;

					Upgrades = std::min(Upgrades + 1, MaxLevel);
				}

				// Get level text
				std::stringstream Buffer;
				if(MaxLevel > 0)
					Buffer << "Level " << Upgrades << "/" << MaxLevel;

				// Draw text
				ae::Assets.Fonts["hud_small"]->DrawText(Buffer.str(), glm::ivec2(DrawPosition), ae::CENTER_BASELINE, ae::Assets.Colors["gray"]);
			}
		}
	}

	DrawPosition.y += LargeSpacingY;

	// Set width of description text
	int DescriptionWidth = Size.x - SidePadding * 2;

	// Draw proc info
	DrawDescription(Render, Player, Proc, DrawPosition, Blacksmith, DrawLevel, PlayerMaxSkillLevel, EnchanterMaxLevel, Upgrades, ShowLevel, ShowFractions, DescriptionWidth, SpacingY);

	// Draw set info
	DrawSetDescription(Render, Player, DrawPosition, Blacksmith, DescriptionWidth, SpacingY);

	// Draw item info
	DrawDescription(Render, Player, Script, DrawPosition, Blacksmith, DrawLevel, PlayerMaxSkillLevel, EnchanterMaxLevel, Upgrades, ShowLevel, ShowFractions, DescriptionWidth, SpacingY);

	// Draw next level description
	if(IsSkill() && Tooltip.Window == _HUD::WINDOW_SKILLS && DrawNextLevel)
		DrawDescription(Render, Player, Script, DrawPosition, false, DrawNextLevel, PlayerMaxSkillLevel, EnchanterMaxLevel, 0, true, ShowFractions, Size.x - SidePadding * 2, SpacingY);

	// Get item to compare
	_InventorySlot CompareInventory;
	if(Player->Inventory->IsValidSlot(CompareSlot))
		CompareInventory = Player->Inventory->GetSlot(CompareSlot);

	bool StatDrawn = false;

	// Damage
	int64_t DrawMinDamage = std::floor(GetAttribute("MinDamage", Upgrades));
	int64_t DrawMaxDamage = std::floor(GetAttribute("MaxDamage", Upgrades));
	if(DrawMinDamage != 0 || DrawMaxDamage != 0) {
		if(Render) {
			std::stringstream Buffer;
			if(ShowAltText)
				Buffer << ae::Round1((DrawMinDamage + DrawMaxDamage) * 0.5f) << " avg";
			else if(DrawMinDamage != DrawMaxDamage)
				Buffer << DrawMinDamage << " - " << DrawMaxDamage;
			else
				Buffer << DrawMinDamage;

			glm::vec4 Color(1.0f);
			if(CompareInventory.Item)
				Color = GetCompareColor(GetAverageDamage(Upgrades), CompareInventory.Item->GetAverageDamage(CompareInventory.Upgrades));

			ae::Assets.Fonts["hud_medium"]->DrawText("Damage", glm::ivec2(DrawPosition + -Spacing), ae::RIGHT_BASELINE);
			ae::Assets.Fonts["hud_medium"]->DrawText(Buffer.str(), glm::ivec2(DrawPosition + Spacing), ae::LEFT_BASELINE, Color);
		}
		DrawPosition.y += SpacingY;
		StatDrawn = true;
	}

	// Damage type
	if(!IsSkill() && DamageTypeID > 1) {
		if(Render) {
			std::stringstream Buffer;
			Buffer << Stats->DamageTypes.at(DamageTypeID).Name;
			ae::Assets.Fonts["hud_medium"]->DrawText("Damage Type", glm::ivec2(DrawPosition + -Spacing), ae::RIGHT_BASELINE);
			ae::Assets.Fonts["hud_medium"]->DrawText(Buffer.str(), glm::ivec2(DrawPosition + Spacing), ae::LEFT_BASELINE);
		}
		DrawPosition.y += SpacingY;
		StatDrawn = true;
	}

	// Display attributes
	for(const auto &AttributeName : Stats->AttributeRank) {
		const _Attribute &Attribute = Stats->Attributes.at(AttributeName);
		if(!Attribute.Tooltip)
			continue;

		if(Attributes.find(AttributeName) == Attributes.end())
			continue;

		// Get upgraded stat
		float UpgradedValue = GetAttribute(AttributeName, Upgrades);
		if(UpgradedValue == 0.0f)
			continue;

		// Draw text
		if(Render) {

			// Show fractions
			if(!ShowFractions)
				UpgradedValue = std::floor(UpgradedValue);

			// Get value string
			std::stringstream Buffer;
			Buffer << (UpgradedValue < 0 ? "" : "+") << UpgradedValue;
			if(Attribute.Type == StatValueType::PERCENT || Attribute.Type == StatValueType::PERCENT64 || Attribute.Type == StatValueType::PERCENT_DOUBLE)
				Buffer << "%";

			// Get compare color
			glm::vec4 Color(1.0f);
			if(CompareInventory.Item)
				Color = GetCompareColor(GetAttribute(AttributeName, Upgrades), CompareInventory.Item->GetAttribute(AttributeName, CompareInventory.Upgrades));

			// Draw label and stat
			ae::Assets.Fonts["hud_medium"]->DrawText(Attribute.Label, glm::ivec2(DrawPosition + -Spacing), ae::RIGHT_BASELINE);
			ae::Assets.Fonts["hud_medium"]->DrawText(Buffer.str(), glm::ivec2(DrawPosition + Spacing), ae::LEFT_BASELINE, Color);
		}
		DrawPosition.y += SpacingY;
		StatDrawn = true;
	}

	// Resistance
	if(ResistanceTypeID) {
		if(Render) {
			float DrawResistance = GetAttribute("Resist", Upgrades);
			if(!ShowFractions)
				DrawResistance = std::floor(DrawResistance);

			std::stringstream Buffer;
			Buffer << (DrawResistance < 0 ? "" : "+") << DrawResistance << "%";

			glm::vec4 Color(1.0f);
			if(CompareInventory.Item)
				Color = GetCompareColor(GetAttribute("Resist", Upgrades), CompareInventory.Item->GetAttribute("Resist", CompareInventory.Upgrades));

			ae::Assets.Fonts["hud_medium"]->DrawText(Player->Stats->DamageTypes.at(ResistanceTypeID).Name + " Resist", glm::ivec2(DrawPosition + -Spacing), ae::RIGHT_BASELINE);
			ae::Assets.Fonts["hud_medium"]->DrawText(Buffer.str(), glm::ivec2(DrawPosition + Spacing), ae::LEFT_BASELINE, Color);
		}
		DrawPosition.y += SpacingY;
		StatDrawn = true;
	}

	// Cooldown reduction
	float DrawCooldownReduction = GetCooldownReduction(Upgrades);
	if(IsEquippable() && DrawCooldownReduction != 0.0f) {
		if(Render) {
			if(!ShowFractions)
				DrawCooldownReduction = std::floor(DrawCooldownReduction);

			std::stringstream Buffer;
			Buffer << (DrawCooldownReduction < 0 ? "" : "+") << DrawCooldownReduction << "%";

			glm::vec4 Color(1.0f);
			if(CompareInventory.Item)
				Color = GetCompareColor(-GetCooldownReduction(Upgrades), -CompareInventory.Item->GetCooldownReduction(CompareInventory.Upgrades));

			ae::Assets.Fonts["hud_medium"]->DrawText("Cooldowns", glm::ivec2(DrawPosition + -Spacing), ae::RIGHT_BASELINE);
			ae::Assets.Fonts["hud_medium"]->DrawText(Buffer.str(), glm::ivec2(DrawPosition + Spacing), ae::LEFT_BASELINE, Color);
		}
		DrawPosition.y += SpacingY;
		StatDrawn = true;
	}

	if(StatDrawn)
		DrawPosition.y += SpacingY;

	std::vector<std::string> HelpTextList;

	// Vendors
	if(Player->Character->Vendor) {
		std::stringstream Buffer;
		Buffer.imbue(std::locale(Config.Locale));
		if(Tooltip.Window == _HUD::WINDOW_VENDOR) {
			if(CanBuy(Player->Scripting, Player)) {
				if(Render) {
					Buffer << "Buy " << Tooltip.InventorySlot.Count << "x for " << Tooltip.Cost << " gold";
					ae::Assets.Fonts["hud_medium"]->DrawText(Buffer.str(), glm::ivec2(DrawPosition), ae::CENTER_BASELINE, ae::Assets.Colors["gold"]);
				}
				DrawPosition.y += SpacingY;
				HelpTextList.push_back("Right-click to buy");
				HelpTextList.push_back("Ctrl+Right-click to bulk buy");
			}
		}
		else if((Tooltip.Window == _HUD::WINDOW_EQUIPMENT || Tooltip.Window == _HUD::WINDOW_INVENTORY) && !(IsCursed() && Tooltip.Window == _HUD::WINDOW_EQUIPMENT)) {
			if(Render) {
				Buffer << "Sell for " << Tooltip.Cost << " gold";
				ae::Assets.Fonts["hud_medium"]->DrawText(Buffer.str(), glm::ivec2(DrawPosition), ae::CENTER_BASELINE, ae::Assets.Colors["gold"]);
			}
			DrawPosition.y += SpacingY;
			if(Config.RightClickSell)
				HelpTextList.push_back("Right-click to sell");
			else if(CanQuickSell())
				HelpTextList.push_back("Shift+Right-click to sell");
		}
	}
	// Blacksmiths
	else if(Player->Character->Blacksmith) {
		if(Player->Character->Blacksmith && Player->Character->Blacksmith->CanUpgrade(this, Tooltip.InventorySlot.Count, Upgrades - ShowingNextUpgradeLevel) && (Tooltip.Window == _HUD::WINDOW_EQUIPMENT || Tooltip.Window == _HUD::WINDOW_INVENTORY)) {
			if(Render) {
				glm::vec4 Color = ae::Assets.Colors["gold"];
				if(Tooltip.Cost > Player->Character->Attributes["Gold"].Int)
					Color = ae::Assets.Colors["red"];

				std::stringstream Buffer;
				Buffer.imbue(std::locale(Config.Locale));
				Buffer << "Upgrade for " << Tooltip.Cost << " gold";
				ae::Assets.Fonts["hud_medium"]->DrawText(Buffer.str(), glm::ivec2(DrawPosition), ae::CENTER_BASELINE, Color);
			}
			DrawPosition.y += SpacingY;
			HelpTextList.push_back("Ctrl+click to buy upgrade");
		}
	}
	// Enchanters
	else if(Tooltip.Window == _HUD::WINDOW_ENCHANTER) {
		if(Tooltip.Cost > 0) {
			if(Render) {
				std::stringstream Buffer;
				Buffer.imbue(std::locale(Config.Locale));
				Buffer << "Upgrade for " << Tooltip.Cost << " gold";
				ae::Assets.Fonts["hud_medium"]->DrawText(Buffer.str(), glm::ivec2(DrawPosition), ae::CENTER_BASELINE, ae::Assets.Colors["gold"]);
			}
			DrawPosition.y += SpacingY;
		}
	}
	// Disenchanters
	else if(Tooltip.Window == _HUD::WINDOW_DISENCHANTER) {
		if(Tooltip.Cost > 0) {
			if(Render) {
				std::stringstream Buffer;
				Buffer.imbue(std::locale(Config.Locale));
				Buffer << "Sell for " << Tooltip.Cost << " gold";
				ae::Assets.Fonts["hud_medium"]->DrawText(Buffer.str(), glm::ivec2(DrawPosition), ae::CENTER_BASELINE, ae::Assets.Colors["gold"]);
			}
			DrawPosition.y += SpacingY;
		}
	}

	// Draw help text
	std::string InfoText;
	glm::vec4 InfoColor = ae::Assets.Colors["gray"];
	switch(Type) {
		case ItemType::RELIC:
		case ItemType::HELMET:
		case ItemType::ARMOR:
		case ItemType::BOOTS:
		case ItemType::ONEHANDED_WEAPON:
		case ItemType::TWOHANDED_WEAPON:
		case ItemType::SHIELD:
		case ItemType::RING:
		case ItemType::AMULET:
		case ItemType::OFFHAND:

			if(Attributes.at("Endure").Int) {
				if(Render)
					ae::Assets.Fonts["hud_small"]->DrawText("Endures reincarnation when equipped", glm::ivec2(DrawPosition), ae::CENTER_BASELINE, ae::Assets.Colors["silver"]);
				DrawPosition.y += ControlSpacingY;
			}

			if(IsCursed()) {
				InfoText = "Cursed items cannot be unequipped";
				InfoColor = ae::Assets.Colors["red"];
			}
			else if((Tooltip.Slot.Type == BagType::INVENTORY || Tooltip.Slot.Type == BagType::TRADE || Tooltip.Window == _HUD::WINDOW_STASH) && !(Player->Character->Vendor && Config.RightClickSell))
				HelpTextList.push_back("Right-click to equip");
			else if(Tooltip.Slot.Type == BagType::EQUIPMENT)
				HelpTextList.push_back("Right-click to unequip");
		break;
		case ItemType::CONSUMABLE:
			if(Tooltip.Window == _HUD::WINDOW_INVENTORY && CheckScope(PlayerScope) && !(Player->Character->Vendor && Config.RightClickSell))
				HelpTextList.push_back("Right-click to use");
			else if(!HUD.SkillScreen->Element->Active && !HUD.InventoryScreen->Element->Active && Tooltip.Window == _HUD::WINDOW_SKILLBAR && CheckScope(PlayerScope))
				HelpTextList.push_back("Left-click to use");
		break;
		case ItemType::SKILL:
			if(Tooltip.Window == _HUD::WINDOW_SKILLBAR) {
				if(!HUD.SkillScreen->Element->Active && !HUD.InventoryScreen->Element->Active && TargetID != TargetType::NONE)
					HelpTextList.push_back("Left-click to use");

				if(!Player->Battle)
					HelpTextList.push_back("Right-click to remove");
			}
			else if(Tooltip.Window == _HUD::WINDOW_SKILLS) {
				if(Scope == ScopeType::NONE)
					HelpTextList.push_back("Passive skills must be equipped");
				if(SkillLevel > 0) {
					if(Player->Character->CanEquipSkill(this))
						HelpTextList.push_back("Right-click to equip");

					HelpTextList.push_back("Press skill hotkey to equip");
				}

				if(SkillButtonDirection > 0) {
					HelpTextList.push_back("Shift+click to increase by 5");
					HelpTextList.push_back("Ctrl+click to increase by 50");
				}
				else if(SkillButtonDirection < 0) {
					HelpTextList.push_back("Shift+click to decrease by 5");
					HelpTextList.push_back("Ctrl+click to decrease by 50");
				}
			}
			else if(Tooltip.Window == _HUD::WINDOW_ENCHANTER) {
				if(Tooltip.Cost > 0) {
					HelpTextList.push_back("Shift+click to buy 5x");
					HelpTextList.push_back("Ctrl+click to buy 50x");
				}
			}
			else if(Tooltip.Window == _HUD::WINDOW_DISENCHANTER) {
				if(Tooltip.Cost > 0)
					HelpTextList.push_back("Shift+click to sell 5x");
			}
			else {
				if(IsLocked) {
					if(Tooltip.Window == _HUD::WINDOW_INVENTORY && !(Player->Character->Vendor && Config.RightClickSell))
						HelpTextList.push_back("Right-click to learn");
				}
				else {
					InfoText = "Already learned";
					InfoColor = ae::Assets.Colors["red"];
				}
			}
		break;
		case ItemType::UNLOCKABLE: {
			bool Unlocked = Player->Character->HasUnlocked(this);
			if(!Unlocked && Tooltip.Window == _HUD::WINDOW_INVENTORY && !(Player->Character->Vendor && Config.RightClickSell))
				HelpTextList.push_back("Right-click to unlock");
			else if(Unlocked) {
				InfoText = "Already unlocked";
				InfoColor = ae::Assets.Colors["red"];
			}
		} break;
		case ItemType::KEY: {
			if(Player->Inventory->GetBag(BagType::KEYS).HasItemID(ID) != NOSLOT) {
				InfoText = "Already in keychain";
				InfoColor = ae::Assets.Colors["red"];
			}
			else if(Tooltip.Window == _HUD::WINDOW_INVENTORY && !(Player->Character->Vendor && Config.RightClickSell)) {
				HelpTextList.push_back("Right-click to add to keychain");
			}
		} break;
		default:
		break;
	}

	// Draw info text
	if(InfoText.length()) {
		if(Render)
			ae::Assets.Fonts["hud_small"]->DrawText(InfoText, glm::ivec2(DrawPosition), ae::CENTER_BASELINE, InfoColor);
		DrawPosition.y += ControlSpacingY;
	}

	// Tradable
	if(Tooltip.Window == _HUD::WINDOW_EQUIPMENT || Tooltip.Window == _HUD::WINDOW_INVENTORY || Tooltip.Window == _HUD::WINDOW_VENDOR || Tooltip.Window == _HUD::WINDOW_TRADER || Tooltip.Window == _HUD::WINDOW_STASH) {
		if(!Tradable) {
			if(Render)
				ae::Assets.Fonts["hud_small"]->DrawText("Cannot trade with other players", glm::ivec2(DrawPosition), ae::CENTER_BASELINE, ae::Assets.Colors["red"]);
			DrawPosition.y += ControlSpacingY;
		}
		else if(Player->Character->Level < Tradable) {
			if(Render)
				ae::Assets.Fonts["hud_small"]->DrawText("Can trade with players at level " + std::to_string(Tradable), glm::ivec2(DrawPosition), ae::CENTER_BASELINE, ae::Assets.Colors["red"]);
			DrawPosition.y += ControlSpacingY;
		}
	}

	// Move hint
	if(Player->Character->ViewingStash || (Player->Character->IsTrading() && Player->Inventory->IsTradable(this) && !(IsCursed() && Tooltip.Window == _HUD::WINDOW_EQUIPMENT)))
		HelpTextList.push_back("Shift+click to move");

	// Split hint
	if(Tooltip.InventorySlot.Count > 1 && Tooltip.Window != _HUD::WINDOW_VENDOR)
		HelpTextList.push_back("Ctrl+click to split");

	// Set hint
	if(SetID || Script.find("SetBonus") == 0)
		HelpTextList.push_back("Hold Alt for more info");

	// Draw ui hints
	for(const auto &Text : HelpTextList) {
		if(Render)
			ae::Assets.Fonts["hud_small"]->DrawText(Text, glm::ivec2(DrawPosition), ae::CENTER_BASELINE, ae::Assets.Colors["gray"]);
		DrawPosition.y += ControlSpacingY;
	}

	return DrawPosition.y;
}

// Draw item description
void _Item::DrawDescription(bool Render, _Object *Object, const std::string &Function, glm::vec2 &DrawPosition, bool Blacksmith, int DrawLevel, int PlayerMaxSkillLevel, int EnchanterMaxLevel, int Upgrades, bool ShowLevel, bool ShowFractions, float Width, float SpacingY) const {
	_Scripting *Scripting = Object->Scripting;

	// Check for scripting function
	std::string Info = "";
	if(Scripting->StartMethodCall(Function, "GetInfo")) {
		int SetLevel = 0;
		int MaxSetLevel = 0;
		if(SetID) {
			_SetData &SetData = Object->Character->Sets[SetID];
			SetLevel = std::min(SetData.Level + Blacksmith, MaxLevel);
			MaxSetLevel = SetData.MaxLevel;
		}

		// Get description from script
		Scripting->PushObject(Object);
		Scripting->PushItemParameters(ID, Chance, DrawLevel, Duration, Upgrades, SetLevel, MaxSetLevel, ShowFractions);
		Scripting->MethodCall(2, 1);
		Info = Scripting->GetString(1);
		Scripting->FinishMethodCall();

		// Draw level text
		if(ShowLevel) {
			std::string Text = "Level " + std::to_string(DrawLevel);
			glm::vec4 Color = ae::Assets.Colors["gray"];
			if(EnchanterMaxLevel == -1) {
				if(DrawLevel == 1 || DrawLevel < Object->Character->MinSkillLevels[ID]) {
					Text = "Skill at minimum level";
					Color = ae::Assets.Colors["red"];
				}
			}
			else if(EnchanterMaxLevel && (DrawLevel > EnchanterMaxLevel || DrawLevel > MaxLevel)) {
				Text = "I can't upgrade this";
				Color = ae::Assets.Colors["red"];
			}
			else if(DrawLevel == PlayerMaxSkillLevel) {
				Text = "Max " + Text;
				Color = ae::Assets.Colors["red"];
			}
			else if(!EnchanterMaxLevel && DrawLevel >= PlayerMaxSkillLevel) {
				if(DrawLevel > MaxLevel)
					return;

				Text = "Enchanter required for level " + std::to_string(DrawLevel);
				Color = ae::Assets.Colors["red"];
			}
			if(Render)
				ae::Assets.Fonts["hud_small"]->DrawText(Text, glm::ivec2(DrawPosition), ae::CENTER_BASELINE, Color);
			DrawPosition.y += SpacingY;
		}

		std::stringstream Buffer(Info);
		std::string Token;

		// Draw description
		float TextSpacingY = INVENTORY_TOOLTIP_TEXT_SPACING * ae::_Element::GetUIScale();
		while(std::getline(Buffer, Token, '\n')) {
			std::vector<std::string> Strings;
			ae::Assets.Fonts["hud_small"]->BreakupString(Token, Width, Strings, true);
			for(const auto &LineToken : Strings) {
				if(Render)
					ae::Assets.Fonts["hud_small"]->DrawTextFormatted(LineToken, glm::ivec2(DrawPosition), ae::CENTER_BASELINE);
				DrawPosition.y += TextSpacingY;
			}
		}

		DrawPosition.y += SpacingY;
	}
}

// Draw set description
void _Item::DrawSetDescription(bool Render, _Object *Object, glm::vec2 &DrawPosition, bool Blacksmith, float Width, float SpacingY) const {
	if(!SetID)
		return;

	// Don't draw full set description for added bonus items
	if(Script.find("SetBonus") == 0)
		return;

	_Scripting *Scripting = Object->Scripting;
	const _Set &Set = Object->Stats->Sets.at(SetID);

	// Check for scripting function
	std::string Info = "";
	if(Scripting->StartMethodCall(Set.Script, "GetSetInfo")) {
		_SetData &SetData = Object->Character->Sets[SetID];
		int Upgrades = std::min(SetData.Level + Blacksmith, MaxLevel);
		bool MoreInfo = ae::Input.ModKeyDown(KMOD_ALT);

		// Get description from script
		Scripting->PushObject(Object);
		Scripting->PushInt(Upgrades);
		Scripting->PushInt(SetData.MaxLevel);
		Scripting->PushBoolean(MoreInfo);
		Scripting->MethodCall(4, 1);
		Info = Scripting->GetString(1);
		Scripting->FinishMethodCall();

		std::stringstream Buffer(Info);
		std::string Token;
		float TextSpacingY = INVENTORY_TOOLTIP_TEXT_SPACING * ae::_Element::GetUIScale();

		// Draw header
		if(Render) {
			std::string LevelText;
			if(MoreInfo)
				LevelText = " Level " + std::to_string(Upgrades);
			else
				LevelText = " (" + std::to_string(SetData.EquippedCount) + "/" + std::to_string(std::max((int64_t)1, Set.Count + Object->Character->Attributes["SetLimit"].Int)) + ")";

			ae::Assets.Fonts["hud_small"]->DrawTextFormatted("[c light_green]" + Set.Name + " Set Bonus" + LevelText, glm::ivec2(DrawPosition), ae::CENTER_BASELINE);
		}
		DrawPosition.y += TextSpacingY;

		// Draw description
		while(std::getline(Buffer, Token, '\n')) {
			std::vector<std::string> Strings;
			ae::Assets.Fonts["hud_small"]->BreakupString(Token, Width, Strings, true);
			for(const auto &LineToken : Strings) {
				if(Render)
					ae::Assets.Fonts["hud_small"]->DrawTextFormatted(LineToken, glm::ivec2(DrawPosition), ae::CENTER_BASELINE);
				DrawPosition.y += TextSpacingY;
			}
		}

		DrawPosition.y += SpacingY;
	}
}

// Get target count based on target type
int _Item::GetTargetCount(_Scripting *Scripting, _Object *Object, int SkillLevelOverride) const {

	switch(TargetID) {
		case TargetType::ENEMY_ALL:
		case TargetType::ALLY_ALL:
			return BATTLE_MAX_OBJECTS_PER_SIDE;
		break;
		default: {

			// Check script function
			if(Scripting->StartMethodCall(Script, "GetTargetCount")) {

				// Get skill level
				int SkillLevel = SkillLevelOverride;
				if(!SkillLevel) {
					auto SkillIterator = Object->Character->Skills.find(ID);
					if(SkillIterator != Object->Character->Skills.end()) {
						SkillLevel = SkillIterator->second + (int)Object->Character->Attributes["AllSkills"].Int;
						if(Object->Character->MaxSkillLevels.find(ID) != Object->Character->MaxSkillLevels.end())
							SkillLevel = std::min(SkillLevel, Object->Character->MaxSkillLevels.at(ID));
					}
					SkillLevel = std::max(1, SkillLevel);
				}

				// Call function
				Scripting->PushObject(Object);
				Scripting->PushInt(SkillLevel);
				Scripting->MethodCall(2, 1);
				int TargetCount = Scripting->GetInt(1);
				Scripting->FinishMethodCall();

				return TargetCount;
			}

			return 1;
		}
		break;
	}

	return 0;
}

// Return a valid equipment slot for an item
void _Item::GetEquipmentSlot(_Slot &Slot) const {

	Slot.Type = BagType::EQUIPMENT;
	switch(Type) {
		case ItemType::HELMET:
			Slot.Index = EquipmentType::HEAD;
		break;
		case ItemType::ARMOR:
			Slot.Index = EquipmentType::BODY;
		break;
		case ItemType::BOOTS:
			Slot.Index = EquipmentType::LEGS;
		break;
		case ItemType::ONEHANDED_WEAPON:
		case ItemType::TWOHANDED_WEAPON:
			Slot.Index = EquipmentType::HAND1;
		break;
		case ItemType::SHIELD:
			Slot.Index = EquipmentType::HAND2;
		break;
		case ItemType::RING:
			Slot.Index = EquipmentType::RING1;
		break;
		case ItemType::AMULET:
			Slot.Index = EquipmentType::AMULET;
		break;
		case ItemType::OFFHAND:
			Slot.Index = EquipmentType::HAND2;
		break;
		case ItemType::RELIC:
			Slot.Index = EquipmentType::RELIC;
		break;
		default:
			Slot.Type = BagType::NONE;
		break;
	}
}

// Returns the item's price to/from a vendor
int64_t _Item::GetPrice(_Scripting *Scripting, _Object *Source, const _Vendor *Vendor, int QueryCount, bool Buy, int Upgrades) const {

	// Calculate
	double Percent = 1.0;
	if(Vendor) {
		if(Buy) {
			Percent = Vendor->BuyPercent;
		}
		else {
			Percent = Vendor->SellPercent;
			if(Category == GAME_RITE_CATEGORY || Type == ItemType::RELIC)
				Percent = 0.0;
		}
	}

	// Check for GetCost function in script
	int64_t ItemCost = Cost;
	if(Scripting->StartMethodCall(Script, "GetCost")) {
		Scripting->PushObject(Source);
		Scripting->MethodCall(1, 1);
		ItemCost = Scripting->GetInt64(1);
		Scripting->FinishMethodCall();
	}

	// Adjust price based on character's vendor discount
	ItemCost = Source->Character->GetItemCost(ItemCost);

	// Get vendor's price
	int64_t Price = (int64_t)(ItemCost * Percent);

	// Add upgrade value
	if(Upgrades) {
		for(int i = 1; i <= Upgrades; i++)
			Price += GetUpgradeCost(Source, i) * Percent;
	}

	// Check for overflow and add quantity
	if((double)Price * QueryCount > PLAYER_MAX_GOLD)
		Price = PLAYER_MAX_GOLD;
	else
		Price *= (int64_t)QueryCount;

	// Cap
	if(Price < 0)
		Price = 0;
	else if(Price > PLAYER_MAX_GOLD)
		Price = PLAYER_MAX_GOLD;

	return Price;
}

// Get upgrade cost
int64_t _Item::GetUpgradeCost(_Object *Source, int Level) const {
	if(MaxLevel <= 0)
		return 0;

	int64_t ItemCost = Source->Character->GetItemCost(Cost);
	return std::max((int64_t)1, (int64_t)std::floor(GAME_UPGRADE_COST_MULTIPLIER * Level * ItemCost + GAME_UPGRADE_BASE_COST));
}

// Determine if a player can buy the item
bool _Item::CanBuy(_Scripting *Scripting, _Object *Player) const {
	bool CanBuy = true;
	if(Scripting->StartMethodCall(Script, "CanBuy")) {
		Scripting->PushObject(Player);
		Scripting->PushInt((int)UnlockID);
		Scripting->MethodCall(2, 1);
		CanBuy = Scripting->GetBoolean(1);
		Scripting->FinishMethodCall();
	}

	return CanBuy;
}

// Get enchant cost
int64_t _Item::GetEnchantCost(_Object *Source, int Level) {
	int64_t Index = Level - GAME_DEFAULT_MAX_SKILL_LEVEL;
	return Source->Character->GetItemCost(std::floor(std::pow(Index, GAME_ENCHANT_COST_POWER) + Index * GAME_ENCHANT_COST_RATE + GAME_ENCHANT_COST_BASE));
}

// Get count of drawable attributes
int _Item::GetAttributeCount(int Upgrades) const {
	int Count = 0;

	if(std::floor(GetAttribute("MinDamage", Upgrades)) != 0 || std::floor(GetAttribute("MaxDamage", Upgrades)) != 0)
		Count++;

	if(!IsSkill() && DamageTypeID > 1)
		Count++;

	for(const auto &AttributeName : Stats->AttributeRank) {
		if(Attributes.find(AttributeName) == Attributes.end())
			continue;

		const _Attribute &Attribute = Stats->Attributes.at(AttributeName);
		if(!Attribute.Tooltip)
			continue;

		float UpgradedValue = GetAttribute(AttributeName, Upgrades);
		if(UpgradedValue == 0.0f)
			continue;

		Count++;
	}

	if(IsEquippable() && GetCooldownReduction(Upgrades) != 0.0f)
		Count++;

	if(ResistanceTypeID)
		Count++;

	return Count;
}

// Return true if the item can be used
bool _Item::CanUse(_Scripting *Scripting, _ActionResult &ActionResult) const {
	_Object *Object = ActionResult.Source.Object;
	if(!Object)
		return false;

	// Check cooldown
	if(Object->Character->Cooldowns.find(ActionResult.ActionUsed.Item->ID) != Object->Character->Cooldowns.end())
		return false;

	// Unlocking skill for the first time
	if(IsSkill() && ActionResult.ActionUsed.InventorySlot != -1)
		return !Object->Character->HasLearned(this);

	// Check for item in key bag
	if(IsKey())
		return Object->Inventory->GetBag(BagType::KEYS).HasItemID(ID) == NOSLOT;

	// Unlocking item
	if(IsUnlockable())
		return !Object->Character->HasUnlocked(this);

	// Check for item count
	if(!ActionResult.ActionUsed.Item->IsSkill()) {
		size_t Index;
		if(!Object->Inventory->FindItem(ActionResult.ActionUsed.Item, Index, (size_t)ActionResult.ActionUsed.InventorySlot))
			return false;
	}

	// Check scope
	if(!CheckScope(ActionResult.Scope))
		return false;

	// Check script's function
	if(Scripting->StartMethodCall(Script, "CanUse")) {
		Scripting->PushInt(ActionResult.ActionUsed.Level);
		Scripting->PushObject(ActionResult.Source.Object);
		Scripting->MethodCall(2, 1);
		int Value = Scripting->GetBoolean(1);
		Scripting->FinishMethodCall();

		return Value;
	}

	return true;
}

// Check if an item can target an object
bool _Item::CanTarget(_Scripting *Scripting, _Object *Source, _Object *Target, bool ForceTargetAlive) const {
	bool CurrentTargetAlive = ForceTargetAlive ? true : TargetAlive;

	if(CurrentTargetAlive && !Target->Character->IsAlive())
		return false;

	if(!CurrentTargetAlive && Target->Character->IsAlive())
		return false;

	if(Source->Battle) {
		if(Source->Fighter->BattleSide == Target->Fighter->BattleSide && !CanTargetAlly())
			return false;

		if(Source->Fighter->BattleSide != Target->Fighter->BattleSide && !CanTargetEnemy())
			return false;
	}

	// Check script's function
	if(Scripting->StartMethodCall(Script, "CanTarget")) {
		Scripting->PushObject(Source);
		Scripting->PushObject(Target);
		Scripting->PushBoolean(!CurrentTargetAlive);
		Scripting->MethodCall(3, 1);
		int Value = Scripting->GetBoolean(1);
		Scripting->FinishMethodCall();

		return Value;
	}

	return true;
}

// Check if the item can be used in the given scope
bool _Item::CheckScope(ScopeType CheckScope) const {
	if(Scope == ScopeType::NONE || (Scope != ScopeType::ALL && Scope != CheckScope))
		return false;

	return true;
}

// Apply the cost
void _Item::ApplyCost(_Scripting *Scripting, _ActionResult &ActionResult) const {
	if(Scripting->StartMethodCall(Script, "ApplyCost")) {
		Scripting->PushObject(ActionResult.Source.Object);
		Scripting->PushInt(ActionResult.ActionUsed.Level);
		Scripting->PushActionResult(&ActionResult);
		Scripting->MethodCall(3, 1);
		Scripting->GetActionResult(1, ActionResult);
		Scripting->FinishMethodCall();
	}
}

// Use an item
void _Item::Use(_Scripting *Scripting, _ActionResult &ActionResult, int Priority) const {
	if(Scripting->StartMethodCall(Script, "Use")) {
		Scripting->PushInt(ActionResult.ActionUsed.Level);
		Scripting->PushInt(ActionResult.ActionUsed.Duration);
		Scripting->PushObject(ActionResult.Source.Object);
		Scripting->PushObject(ActionResult.Target.Object);
		Scripting->PushActionResult(&ActionResult);
		Scripting->PushInt(Priority);
		Scripting->MethodCall(6, 1);
		Scripting->GetActionResult(1, ActionResult);
		Scripting->FinishMethodCall();
	}
}

// Get passive stats
void _Item::GetStats(_Scripting *Scripting, _ActionResult &ActionResult, int SetLevel, int MaxSetLevel) const {
	if(Scripting->StartMethodCall(Script, "Stats")) {
		if(IsSkill())
			Scripting->PushInt(ActionResult.ActionUsed.Level);
		else
			Scripting->PushItemParameters(ID, Chance, Level, Duration, ActionResult.ActionUsed.Level, SetLevel, MaxSetLevel, 0);
		Scripting->PushObject(ActionResult.Source.Object);
		Scripting->PushStatChange(&ActionResult.Source);
		Scripting->MethodCall(3, 1);
		Scripting->GetStatChange(1, ActionResult.Source.Object->Stats, ActionResult.Source);
		Scripting->FinishMethodCall();
	}
}

// Play audio through scripting
void _Item::PlaySound(_Scripting *Scripting) const {
	if(Scripting->StartMethodCall(Script, "PlaySound")) {
		Scripting->MethodCall(0, 0);
		Scripting->FinishMethodCall();
	}
}

// Get upgraded attribute
double _Item::GetAttribute(const std::string &Name, int Upgrades) const {
	auto AttributeIterator = Attributes.find(Name);
	if(AttributeIterator == Attributes.end())
		return 0.0;

	return GetUpgradedValue<double>(Name, Upgrades, AttributeIterator->second.Int);
}

// Get average damage
double _Item::GetAverageDamage(int Upgrades) const {
	return (GetAttribute("MinDamage", Upgrades) + GetAttribute("MaxDamage", Upgrades)) * 0.5;
}

// Get cooldown reduction
double _Item::GetCooldownReduction(int Upgrades) const {
	return -GetUpgradedValue<double>("Cooldowns", Upgrades, -Cooldown);
}

// Get appropriate text color when comparing items
glm::vec4 _Item::GetCompareColor(float ItemValue, float EquippedValue) const {
	if(ItemValue > EquippedValue)
		return ae::Assets.Colors["green"];
	else if(ItemValue < EquippedValue)
		return ae::Assets.Colors["red"];

	return glm::vec4(1.0f);
}

// Return value of a stat after upgrades
template<typename T> T _Item::GetUpgradedValue(const std::string &AttributeName, int Upgrades, T Value) const {
	if(MaxLevel <= 0)
		return Value;

	double UpgradedValue = Stats->Attributes.at(AttributeName).UpgradeScale * GAME_UPGRADE_AMOUNT * Upgrades * std::abs(Value);
	if(Value < 0)
		return std::min(0.0, Value + (T)(GAME_NEGATIVE_UPGRADE_SCALE * UpgradedValue));
	else
		return Value + (T)(UpgradedValue);
}
