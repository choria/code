/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <objects/stat_change.h>
#include <vector>

// Forward Declarations
class _Buff;
class _Item;
class _Stats;
class _StatusEffect;
struct _ActionResult;

namespace ae {
	class _Buffer;
	class _Texture;
}

// Types of targets
enum class TargetType : uint32_t {
	NONE,
	SELF,
	ENEMY,
	ALLY,
	ANY,
	ENEMY_ALL,
	ALLY_ALL,
	ENEMY_CORPSE_AOE,
};

// Scope of action
enum class ScopeType : uint8_t {
	NONE,
	WORLD,
	BATTLE,
	ALL
};

struct _Summon {
	const _Buff *SummonBuff{nullptr};
	double Duration{0.0};
	int64_t Health{0};
	int64_t Mana{0};
	int64_t Armor{0};
	int64_t MinDamage{0};
	int64_t MaxDamage{0};
	int64_t BattleSpeed{0};
	int64_t HealPower{0};
	int64_t BuffLevel{0};
	int64_t ResistAll{0};
	int64_t MaxResistAll{0};
	uint32_t ID{0};
	uint32_t SpellID{0};
	int Limit{0};
	int SkillLevel{0};
	int TargetCount{0};
};

struct _SummonCaptain {
	_Object *Owner{nullptr};
	std::vector<std::pair<_Summon, _StatusEffect *>> Summons;
};

class _Action {

	public:

		_Action() { }
		_Action(const _Item *Item) : Item(Item) { }

		bool operator==(const _Action &Action) const { return Action.Item == Item; }
		bool operator!=(const _Action &Action) const { return !(Action.Item == Item); }

		void Serialize(ae::_Buffer &Data);
		void Unserialize(ae::_Buffer &Data, const _Stats *Stats);

		bool Resolve(ae::_Buffer &Data, _Object *Source, ScopeType Scope);
		void HandleSummons(_ActionResult &ActionResult);

		bool Unconfirmed() { return Item && !Confirmed; }
		void Clear();

		TargetType GetTargetType() const;

		const _Item *Item{nullptr};
		_Object *Target{nullptr};
		double Duration{0.0f};
		int Level{0};
		int Count{0};
		int TargetCountOverride{0};
		int InventorySlot{-1};
		int ActionBarSlot{-1};
		bool Confirmed{false};
		bool LearningSkill{false};
};

struct _ActionResult {
	static ae::_Font *GetFont(const std::string &Text);

	_StatChange Source;
	_StatChange Target;
	const _Buff *SummonBuff{nullptr};
	std::vector<_Summon> Summons;
	glm::vec2 LastPosition{0.0f};
	glm::vec2 Position{0.0f};
	glm::vec2 PortraitOffset{0.0f};
	glm::vec2 SlotOffset{0.0f};
	_Action ActionUsed;
	const ae::_Texture *Texture{nullptr};
	double Time{0.0};
	double Timeout{HUD_ACTIONRESULT_TIMEOUT};
	double Speed{HUD_ACTIONRESULT_SPEED};
	ScopeType Scope{ScopeType::ALL};
	int Count{0};
	bool Short{false};
};
