/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <objects/action.h>
#include <objects/itemtype.h>

// Forward Declarations
class _Scripting;
struct _Cursor;
struct _Slot;
struct _Vendor;

namespace ae {
	class _Texture;
}

// Classes
class _Item {

	public:

		static int64_t GetEnchantCost(_Object *Source, int Level);

		int GetAttributeCount(int Upgrades) const;
		void DrawTooltip(const glm::vec2 &Offset, _Object *Player, const _Cursor &Tooltip, const _Slot &CompareSlot) const;
		void DrawDescription(bool Render, _Object *Object, const std::string &Function, glm::vec2 &DrawPosition, bool Blacksmith, int DrawLevel, int PlayerMaxLevelSkill, int EnchanterMaxLevel, int Upgrades, bool ShowLevel, bool ShowFractions, float Width, float SpacingY) const;
		void DrawSetDescription(bool Render, _Object *Object, glm::vec2 &DrawPosition, bool Blacksmith, float Width, float SpacingY) const;

		bool IsSkill() const { return Type == ItemType::SKILL; }
		bool IsConsumable() const { return Type == ItemType::CONSUMABLE; }
		bool IsKey() const { return Type == ItemType::KEY; }
		bool IsUnlockable() const { return Type == ItemType::UNLOCKABLE; }
		bool IsEquippable() const { return Type == ItemType::OFFHAND || Type == ItemType::RELIC || (Type >= ItemType::HELMET && Type <= ItemType::AMULET); }
		bool IsCursed() const { return Attributes.at("Cursed").Int; }
		bool IsStashable() const { return Attributes.at("Delete").Int; }
		bool UnlockOnBuy() const { return Category == GAME_RITE_CATEGORY || Type == ItemType::UNLOCKABLE; }
		bool UseMouseTargeting() const { return TargetID == TargetType::SELF || TargetID == TargetType::ENEMY || TargetID == TargetType::ALLY || TargetID == TargetType::ANY || TargetID == TargetType::ENEMY_CORPSE_AOE; }
		bool CanTargetEnemy() const {  return TargetID == TargetType::ENEMY || TargetID == TargetType::ENEMY_ALL || TargetID == TargetType::ANY || TargetID == TargetType::ENEMY_CORPSE_AOE; }
		bool CanTargetAlly() const {  return TargetID == TargetType::SELF || TargetID == TargetType::ALLY || TargetID == TargetType::ALLY_ALL || TargetID == TargetType::ANY; }
		bool CanTargetAll() const { return TargetID == TargetType::ENEMY_ALL || TargetID == TargetType::ALLY_ALL; }
		bool CanQuickSell() const { return Type != ItemType::RELIC; }
		int GetTargetCount(_Scripting *Scripting, _Object *Object, int SkillLevelOverride=0) const;

		void GetEquipmentSlot(_Slot &Slot) const;

		int64_t GetPrice(_Scripting *Scripting, _Object *Source, const _Vendor *Vendor, int QueryCount, bool Buy, int Upgrades=0) const;
		int64_t GetUpgradeCost(_Object *Source, int Level) const;

		bool CanBuy(_Scripting *Scripting, _Object *Player) const;
		bool CanUse(_Scripting *Scripting, _ActionResult &ActionResult) const;
		bool CanTarget(_Scripting *Scripting, _Object *Source, _Object *Target, bool ForceTargetAlive=false) const;
		bool CheckScope(ScopeType CheckScope) const;
		void ApplyCost(_Scripting *Scripting, _ActionResult &ActionResult) const;
		void Use(_Scripting *Scripting, _ActionResult &ActionResult, int Priority=0) const;
		void GetStats(_Scripting *Scripting, _ActionResult &ActionResult, int SetLevel, int MaxSetLevel) const;
		void PlaySound(_Scripting *Scripting) const;

		double GetAttribute(const std::string &Name, int Upgrades) const;
		double GetAverageDamage(int Upgrades) const;
		double GetCooldownReduction(int Upgrades) const;
		template<typename T> T GetUpgradedValue(const std::string &AttributeName, int Upgrades, T Value) const;

		const _Stats *Stats;

		std::unordered_map<std::string, _Value> Attributes;
		std::string Name;
		std::string Script;
		std::string Proc;
		const ae::_Texture *Texture;
		const ae::_Texture *AltTexture;
		double Duration;
		double Cooldown;
		int64_t Cost;
		ItemType Type;
		TargetType TargetID;
		ScopeType Scope;
		uint32_t ID;
		uint32_t DamageTypeID;
		uint32_t SetID;
		uint32_t UnlockID;
		uint32_t ResistanceTypeID;
		int Category;
		int Rank;
		int Level;
		int MaxLevel;
		int SetAdd;
		int Chance;
		int Tradable;
		bool BulkBuy;
		bool TargetAlive;
		bool Wait;

	private:

		glm::vec4 GetCompareColor(float ItemValue, float EquippedValue) const;
		float DrawTooltip(bool Render, float Height, const glm::vec2 &Offset, _Object *Player, const _Cursor &Tooltip, const _Slot &CompareSlot) const;

};

// Determine sort order on skill screen
inline bool CompareItems(const _Item *First, const _Item *Second) {
	return First->Category < Second->Category || (First->Category == Second->Category && First->Rank < Second->Rank);
}
