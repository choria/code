/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <framework.h>
#include <ae/actions.h>
#include <ae/assets.h>
#include <ae/audio.h>
#include <ae/clientnetwork.h>
#include <ae/console.h>
#include <ae/font.h>
#include <ae/framelimit.h>
#include <ae/graphics.h>
#include <ae/random.h>
#include <ae/texture.h>
#include <ae/util.h>
#include <states/benchmark.h>
#include <states/bots.h>
#include <states/convert.h>
#include <states/dedicated.h>
#include <states/editor.h>
#include <states/play.h>
#include <states/test.h>
#include <config.h>
#include <menu.h>
#include <save.h>
#include <version.h>
#include <SDL.h>
#include <algorithm>

#ifdef _WIN32
	#include <winuser.h>
#endif

_Framework Framework;

// Processes parameters and initializes the game
void _Framework::Init(int ArgumentCount, char **Arguments) {
	Console = nullptr;
	FrameLimit = nullptr;
	TimeStep = DEFAULT_TIMESTEP;
	TimeStepAccumulator = 0.0;
	RequestedState = nullptr;
	FrameworkState = INIT;
	State = (ae::_State *)&PlayState;
	Done = false;
	IgnoreNextInputEvent = false;
	Load4kAssets = false;

	// Settings
	glm::ivec2 WindowPosition(SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
	bool LoadClientAssets = true;
	bool AudioEnabled = true;
	DedicatedState.SetNetworkPort(Config.NetworkPort);
	Menu.SetUsername(Config.SavedUsername);
	Menu.SetPassword(Config.SavedPassword);

	// Process arguments
	std::string Token;
	int TokensRemaining;
	for(int i = 1; i < ArgumentCount; i++) {
		Token = std::string(Arguments[i]);
		TokensRemaining = ArgumentCount - i - 1;
		if(Token == "-4k") {
			Load4kAssets = true;
		}
		else if(Token == "-benchmark") {
			State = &BenchmarkState;
			Config.Vsync = false;
			Config.MaxFPS = 0;
		}
		else if(Token == "-bot") {
			if(PlayState.DevMode)
				PlayState.CreateBot = true;
		}
		else if(Token == "-bots") {
			if(PlayState.DevMode) {
				State = &BotState;
				LoadClientAssets = false;
			}
		}
		else if(Token == "-connect" && TokensRemaining > 1) {
			PlayState.ConnectNow = true;
			Config.LastHost = Arguments[++i];
			Config.LastPort = Arguments[++i];
		}
		else if(Token == "-convert") {
			if(PlayState.DevMode)
				State = &ConvertState;
		}
		else if(Token == "-crash") {
			abort();
		}
		else if(Token == "-dev") {
			#ifndef NDEBUG
				PlayState.DevMode = true;
				DedicatedState.SetDevMode(true);
			#endif
		}
		else if(Token == "-editor") {
			#if defined ENABLE_EDITOR && ENABLE_EDITOR == 1
				State = &EditorState;
				if(TokensRemaining && Arguments[i+1][0] != '-')
					EditorState.SetFilePath(Arguments[++i]);
			#endif
		}
		else if(Token == "-hardcore") {
			PlayState.IsHardcore = true;
			DedicatedState.SetHardcore(true);
		}
		else if(Token == "-noaudio") {
			AudioEnabled = false;
		}
		else if(Token == "-nopvp") {
			PlayState.NoPVP = true;
			DedicatedState.SetNoPVP(true);
		}
		else if(Token == "-password" && TokensRemaining > 0) {
			Menu.SetPassword(Arguments[++i]);
			Menu.AutoLogin = true;
		}
		else if(Token == "-port" && TokensRemaining > 0) {
			DedicatedState.SetNetworkPort(ae::ToNumber<uint16_t>(Arguments[++i]));
		}
		else if(Token == "-server") {
			if(!Config.MaxFPS)
				Config.MaxFPS = DEFAULT_MAXFPS;

			State = &DedicatedState;
			LoadClientAssets = false;
		}
		else if(Token == "-test") {
			State = &TestState;
		}
		else if(Token == "-timescale" && TokensRemaining > 0) {
			Config.TimeScale = ae::ToNumber<double>(Arguments[++i]);
		}
		else if(Token == "-username" && TokensRemaining > 0) {
			Menu.SetUsername(Arguments[++i]);
		}
		else if(Token == "-wx" && TokensRemaining > 0) {
			WindowPosition.x = ae::ToNumber<int>(Arguments[++i]);
		}
		else if(Token == "-wy" && TokensRemaining > 0) {
			WindowPosition.y = ae::ToNumber<int>(Arguments[++i]);
		}
	}

	// Initialize network subsystem
	ae::_Network::InitializeSystem();

	// Open client log file
	Log.Open((Config.LogPath + "client.log").c_str(), false);
	Log.PrependDate = true;
	Log << "choria " << GAME_VERSION << "-" << GetBuildVersion() << std::endl;

	// Set random seed
	ae::RandomGenerator.seed(SDL_GetPerformanceCounter());

	// Create frame limiter
	FrameLimit = new ae::_FrameLimit(Config.MaxFPS);

	// Load client assets
	if(LoadClientAssets) {

		// Disable scaling
		#ifdef _WIN32
			SetProcessDPIAware();
		#endif

		// Initialize SDL
		if(SDL_Init(SDL_INIT_VIDEO) < 0)
			throw std::runtime_error("Failed to initialize SDL");

		// Initialize audio
		ae::Audio.Init(AudioEnabled && Config.AudioEnabled, true, Config.AudioDevice.c_str());
		ae::Audio.SetSoundVolume(Config.SoundVolume);
		ae::Audio.SetMusicVolume(Config.MusicVolume);

		// Get window settings
		ae::_WindowSettings WindowSettings;
		WindowSettings.WindowTitle = "choria";
		WindowSettings.IconPath = "ui/icon.webp";
		WindowSettings.Fullscreen = Config.Fullscreen;
		WindowSettings.Vsync = Config.Vsync;
		WindowSettings.Size = Config.WindowSize;
		WindowSettings.Position = WindowPosition;

		// Set up graphics
		ae::Graphics.CircleVertices = 64;
		ae::Graphics.Init(WindowSettings);
		ae::Graphics.SetDepthTest(false);
		ae::Graphics.SetDepthMask(false);

		// Log graphics mode
		Log << "SDL_GetCurrentVideoDriver=" << SDL_GetCurrentVideoDriver() << std::endl;
		Log << "SDL_GetDesktopDisplayMode=" << ae::Graphics.FullscreenSize.x << "x" << ae::Graphics.FullscreenSize.y << std::endl;

		// Initialize font system
		ae::_Font::Init(State == &EditorState ? 100000 : 1000);

		// Load game assets
		LoadAssets();
		ae::Graphics.SetStaticUniforms();

		// Setup console
		Console = new ae::_Console(ae::Assets.Programs["ortho_pos"], ae::Assets.Fonts["console"]);
		Console->LoadHistory(Config.ConfigPath + "history.txt");
		Console->CommandList.push_back("fakelag");
		Console->CommandList.push_back("maxfps");
		Console->CommandList.push_back("networth");
		Console->CommandList.push_back("players");
		Console->CommandList.push_back("priority");
		Console->CommandList.push_back("quit");
		Console->CommandList.push_back("volume");
		Console->CommandList.push_back("vsync");

		// Add dev mode commands
		if(PlayState.DevMode) {
			Console->CommandList.push_back("ai");
			Console->CommandList.push_back("battle");
			Console->CommandList.push_back("bloodmoon");
			Console->CommandList.push_back("bounty");
			Console->CommandList.push_back("clock");
			Console->CommandList.push_back("copy");
			Console->CommandList.push_back("equipment");
			Console->CommandList.push_back("event");
			Console->CommandList.push_back("evolves");
			Console->CommandList.push_back("experience");
			Console->CommandList.push_back("ghost");
			Console->CommandList.push_back("give");
			Console->CommandList.push_back("gold");
			Console->CommandList.push_back("health");
			Console->CommandList.push_back("level");
			Console->CommandList.push_back("mana");
			Console->CommandList.push_back("map");
			Console->CommandList.push_back("move");
			Console->CommandList.push_back("paste");
			Console->CommandList.push_back("rebirths");
			Console->CommandList.push_back("reset");
			Console->CommandList.push_back("save");
			Console->CommandList.push_back("search");
			Console->CommandList.push_back("skills");
			Console->CommandList.push_back("transforms");
		}

		// Sort commands
		std::sort(Console->CommandList.begin(), Console->CommandList.end());
	}

	Timer = SDL_GetPerformanceCounter();
}

// Shuts down the game
void _Framework::Close() {

	// Close the state
	State->Close();

	// Close subsystems
	ae::Audio.Stop();
	ae::Assets.Close();
	ae::_Font::Close();
	ae::Graphics.Close();
	ae::Audio.Close();
	Config.Close();
	delete Console;
	delete FrameLimit;

	ae::_Network::CloseSystem();

	if(SDL_WasInit(SDL_INIT_VIDEO))
		SDL_Quit();
}

// Requests a state change
void _Framework::ChangeState(ae::_State *RequestedState) {
	this->RequestedState = RequestedState;
	FrameworkState = CLOSE;
}

// Updates the current state
void _Framework::Update() {

	// Get frame time
	double FrameTime = (SDL_GetPerformanceCounter() - Timer) / (double)SDL_GetPerformanceFrequency();
	Timer = SDL_GetPerformanceCounter();

	// Get events from SDL
	SDL_PumpEvents();
	ae::Input.Update(FrameTime * Config.TimeScale);

	// Loop through events
	SDL_Event Event;
	while(SDL_PollEvent(&Event)) {
		if(!State || FrameworkState != UPDATE)
			continue;

		switch(Event.type) {
			case SDL_KEYDOWN:
			case SDL_KEYUP: {
				if(!GlobalKeyHandler(Event)) {
					ae::_KeyEvent KeyEvent("", Event.key.keysym.scancode, Event.type == SDL_KEYDOWN, Event.key.repeat);

					// Handle console input
					bool SendAction = true;
					if(Console->IsOpen())
						ae::Graphics.Element->HandleKey(KeyEvent);
					else
						SendAction = State->HandleKey(KeyEvent);

					// Pass keys to action handler
					if(SendAction)
						ae::Actions.InputEvent(State, ae::_Input::KEYBOARD, Event.key.keysym.scancode, Event.type == SDL_KEYDOWN, Event.key.repeat);
				}
			} break;
			case SDL_TEXTINPUT: {
				if(!IgnoreNextInputEvent) {
					ae::_KeyEvent KeyEvent(Event.text.text, 0, 1, 1);
					if(Console->IsOpen())
						ae::Graphics.Element->HandleKey(KeyEvent);
					else
						State->HandleKey(KeyEvent);
				}

				IgnoreNextInputEvent = false;
			} break;
			case SDL_MOUSEMOTION: {
				if(!Console->IsOpen())
					State->HandleMouseMove(glm::ivec2(Event.motion.xrel, Event.motion.yrel));
			} break;
			case SDL_MOUSEBUTTONDOWN:
			case SDL_MOUSEBUTTONUP: {
				if(!Console->IsOpen()) {
					ae::_MouseEvent MouseEvent(glm::ivec2(Event.motion.x, Event.motion.y), Event.button.button, Event.type == SDL_MOUSEBUTTONDOWN);
					State->HandleMouseButton(MouseEvent);
					ae::Actions.InputEvent(State, ae::_Input::MOUSE_BUTTON, Event.button.button, Event.type == SDL_MOUSEBUTTONDOWN, false);
				}
			} break;
			case SDL_MOUSEWHEEL: {
				if(Console->IsOpen())
					Console->Scroll(Event.wheel.y);
				else
					State->HandleMouseWheel(Event.wheel.y);
			} break;
			case SDL_WINDOWEVENT:
				if(Event.window.event)
					State->HandleWindow(Event.window.event);
			break;
			case SDL_QUIT:
				State->HandleQuit();
			break;
		}
	}

	switch(FrameworkState) {
		case INIT: {
			if(State) {
				State->Init();
				FrameworkState = UPDATE;
			}
			else
				Done = true;
		} break;
		case UPDATE: {
			TimeStepAccumulator += FrameTime * Config.TimeScale;
			while(TimeStepAccumulator >= TimeStep) {
				State->Update(TimeStep);
				if(Console) {
					Console->Update(TimeStep);
					if(!Console->Command.empty()) {
						bool Handled = State->HandleCommand(Console);
						if(!Handled)
							HandleCommand(Console);
						Console->Command = "";
					}
				}

				TimeStepAccumulator -= TimeStep;
			}

			double BlendFactor = TimeStepAccumulator / TimeStep * Config.TimeScale;
			State->Render(BlendFactor);
			if(Console)
				Console->Render(BlendFactor);
		} break;
		case CLOSE: {
			if(State)
				State->Close();

			State = RequestedState;
			FrameworkState = INIT;
		} break;
	}

	ae::Audio.Update(FrameTime * Config.TimeScale);
	ae::Graphics.Flip(FrameTime);

	if(FrameLimit)
		FrameLimit->Update();
}

// Handles global hotkeys
int _Framework::GlobalKeyHandler(const SDL_Event &Event) {

	if(Event.type == SDL_KEYDOWN) {

		// Handle alt-enter
		if((Event.key.keysym.mod & KMOD_ALT) && (Event.key.keysym.scancode == SDL_SCANCODE_RETURN || Event.key.keysym.scancode == SDL_SCANCODE_KP_ENTER)) {
			if(!Event.key.repeat)
				Menu.SetFullscreen(!Config.Fullscreen);

			return 1;
		}
		// Ctrl-s
		else if((Event.key.keysym.mod & KMOD_CTRL) && Event.key.keysym.scancode == SDL_SCANCODE_S) {
			if(!Event.key.repeat) {
				if(Config.SoundVolume > 0.0f)
					Config.SoundVolume = 0.0f;
				else
					Config.SoundVolume = 1.0f;

				Config.Save();
				ae::Audio.SetSoundVolume(Config.SoundVolume);
			}

			return 1;
		}
		// Ctrl-m
		else if((Event.key.keysym.mod & KMOD_CTRL) && Event.key.keysym.scancode == SDL_SCANCODE_M) {
			if(!Event.key.repeat) {
				if(Config.MusicVolume > 0.0f)
					Config.MusicVolume = 0.0f;
				else
					Config.MusicVolume = 1.0f;

				Config.Save();
				ae::Audio.SetMusicVolume(Config.MusicVolume);
			}

			return 1;
		}
	}

	return 0;
}

// Handle generic console command
void _Framework::HandleCommand(ae::_Console *Console) {

	// Get parameters
	std::vector<std::string> Parameters;
	ae::TokenizeString(Console->Parameters, Parameters);

	if(Console->Command == "copy") {
		if(Parameters.size() == 1 && PlayState.Network->IsDisconnected()) {
			uint32_t CharacterID = ae::ToNumber<uint32_t>(Console->Parameters);
			std::string JsonString;
			_Save *Save = new _Save();
			Save->GetData(CharacterID, JsonString);

			SDL_SetClipboardText(JsonString.c_str());
		}
		else {
			Console->AddMessage("usage: copy [character_id]");
		}
	}
	else if(Console->Command == "fakelag") {
		if(Parameters.size() == 1) {
			Config.FakeLag = ae::ToNumber<float>(Parameters[0]);
			if(PlayState.Network)
				PlayState.Network->SetFakeLag(Config.FakeLag);
		}
		else {
			Console->AddMessage("fakelag = " + std::to_string(Config.FakeLag));
			Console->AddMessage("usage: fakelag [value]");
		}
	}
	else if(Console->Command == "maxfps") {
		if(Parameters.size() == 1) {
			Config.MaxFPS = ae::ToNumber<int>(Parameters[0]);
			Framework.FrameLimit->SetFrameRate(Config.MaxFPS);
			Config.Save();
		}
		else {
			Console->AddMessage("maxfps = " + std::to_string(Config.MaxFPS));
			Console->AddMessage("usage: maxfps [value]");
		}
	}
	else if(Console->Command == "paste") {
		if(SDL_HasClipboardText() && Parameters.size() == 1 && PlayState.Network->IsDisconnected()) {
			uint32_t CharacterID = ae::ToNumber<uint32_t>(Console->Parameters);

			char *PastedText = SDL_GetClipboardText();
			if(PastedText) {
				_Save *Save = new _Save();
				Save->SetData(CharacterID, PastedText);
				delete Save;
			}

			SDL_free(PastedText);
		}
		else {
			Console->AddMessage("usage: paste [character_id]");
		}
	}
	else if(Console->Command == "volume") {
		if(Parameters.size() == 1) {
			Config.SoundVolume = Config.MusicVolume = std::clamp(ae::ToNumber<float>(Console->Parameters), 0.0f, 1.0f);
			ae::Audio.SetSoundVolume(Config.SoundVolume);
			ae::Audio.SetMusicVolume(Config.MusicVolume);
			Config.Save();
		}
		else
			Console->AddMessage("usage: volume [value]");
	}
	else if(Console->Command == "vsync") {
		if(Parameters.size() == 1) {
			Config.Vsync = ae::ToNumber<bool>(Console->Parameters);
			ae::Graphics.SetVsync(Config.Vsync);
			Config.Save();
		}
		else {
			Console->AddMessage("vsync = " + std::to_string(ae::Graphics.GetVsync()));
			Console->AddMessage("usage: vsync [value]");
		}
	}
	else {
		Console->AddMessage("Command \"" + Console->Command + "\" not found");
	}
}

// Load assets
void _Framework::LoadAssets() {

	// Get texture pack suffix
	std::string PackSuffix = "";
	if(Load4kAssets || ae::Graphics.FullscreenSize.y >= 2160)
		PackSuffix = "_4k";

	// Load textures
	ae::_TextureSettings TextureSettings;
	TextureSettings.WrapMode = ae::_Texture::REPEAT;
	TextureSettings.Mipmaps = false;
	ae::Assets.LoadTexturePack("textures/hud_repeat", TextureSettings);

	TextureSettings.WrapMode = ae::_Texture::CLAMP_TO_EDGE;
	TextureSettings.Mipmaps = false;
	ae::Assets.LoadTexturePack("textures/atlas", TextureSettings);
	ae::Assets.LoadTexturePack("textures/lights", TextureSettings);

	TextureSettings.Nearest = true;
	ae::Assets.LoadTexturePack("textures/maps", TextureSettings);
	TextureSettings.Nearest = false;

	TextureSettings.WrapMode = ae::_Texture::CLAMP_TO_EDGE;
	TextureSettings.Mipmaps = true;
	ae::Assets.LoadTexturePack("textures/buffs" + PackSuffix, TextureSettings);
	ae::Assets.LoadTexturePack("textures/builds" + PackSuffix, TextureSettings);
	ae::Assets.LoadTexturePack("textures/buttonbar" + PackSuffix, TextureSettings);
	ae::Assets.LoadTexturePack("textures/hud" + PackSuffix, TextureSettings);
	ae::Assets.LoadTexturePack("textures/items" + PackSuffix, TextureSettings);
	ae::Assets.LoadTexturePack("textures/menu" + PackSuffix, TextureSettings);
	ae::Assets.LoadTexturePack("textures/minigames" + PackSuffix, TextureSettings);
	ae::Assets.LoadTexturePack("textures/models" + PackSuffix, TextureSettings);
	ae::Assets.LoadTexturePack("textures/monsters" + PackSuffix, TextureSettings);
	ae::Assets.LoadTexturePack("textures/objects" + PackSuffix, TextureSettings);
	ae::Assets.LoadTexturePack("textures/portraits" + PackSuffix, TextureSettings);
	ae::Assets.LoadTexturePack("textures/skills" + PackSuffix, TextureSettings);
	ae::Assets.LoadTexturePack("textures/status" + PackSuffix, TextureSettings);

	// Load tables
	ae::Assets.LoadLayers("tables/layers.tsv");
	ae::Assets.LoadPrograms("tables/programs.tsv");
	ae::Assets.LoadColors("ui/colors.tsv");
	ae::Assets.LoadStyles("ui/styles.tsv");
	ae::Assets.LoadSoundPack("data/sounds");
	ae::Assets.LoadMusic("music/");

	// Load fonts
	ae::Assets.LoadFonts("ui/fonts.tsv", true);
	ae::Assets.LoadFonts("ui/fonts.tsv");

	// Load UI
	ae::Assets.LoadUI("ui/ui.xml");
	//ae::Assets.SaveUI("ui/ui_new.xml");
}
