/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <states/convert.h>
#include <objects/map.h>
#include <framework.h>
#include <stats.h>

_ConvertState ConvertState;

void _ConvertState::Init() {

	_Stats *Stats = new _Stats();

	std::cout << "Converting maps" << std::endl;
	for(auto &MapStat : Stats->Maps) {
		_Map *Map = new _Map();
		if(MapStat.second.File.length()) {
			Map->Stats = Stats;
			Map->Load(&MapStat.second);
			Map->Save(MapStat.second.File);
		}
		delete Map;
	}

	delete Stats;
	Framework.Done = true;
}
