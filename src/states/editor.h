/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <ae/state.h>
#include <glm/vec2.hpp>

// Forward Declarations
class _Map;
class _Stats;
struct _Tile;

namespace ae {
	class _Camera;
	class _Element;
	class _Framebuffer;
	class _Texture;
}

enum MapRenderType {
	MAP_RENDER_BOUNDARY          = (1 << 1),
	MAP_RENDER_TEXTURE           = (1 << 2),
	MAP_RENDER_WALL              = (1 << 3),
	MAP_RENDER_PVP               = (1 << 4),
	MAP_RENDER_ZONE              = (1 << 5),
	MAP_RENDER_EVENTTYPE         = (1 << 6),
	MAP_RENDER_EVENTDATA         = (1 << 7),
	MAP_RENDER_EDITOR_AMBIENT    = (1 << 8),
	MAP_RENDER_NOBACKGROUND      = (1 << 9),
	MAP_RENDER_EDGE_BOUNDARY     = (1 << 10),
	MAP_RENDER_MAP_VIEW          = (1 << 11),
	MAP_RENDER_NOFOREGROUND      = (1 << 12),
};

enum BrushModeType {
	EDITOR_BRUSH_MODE_TILE,
	EDITOR_BRUSH_MODE_OBJECT,
};

// Classes
class _EditorState : public ae::_State {

	public:

		void Init() override;
		void Close() override;

		// Events
		bool HandleAction(int InputType, size_t Action, int Value, bool Repeat) override;
		bool HandleKey(const ae::_KeyEvent &KeyEvent) override;
		void HandleMouseButton(const ae::_MouseEvent &MouseEvent) override;
		void HandleMouseWheel(int Direction) override;
		void HandleWindow(uint8_t Event) override;
		void HandleQuit() override;

		// Update
		void Update(double FrameTime) override;
		void Render(double BlendFactor) override;

		void SetFilePath(const std::string &FilePath) { this->FilePath = FilePath; }
		std::string GetCleanMapName(const std::string &Path);

	private:

		void AllocateCopy();
		void ClearTextures();
		void CloseCopy();
		void CloseMap();
		void CreateMap();
		void ResizeMap();
		void SaveMap();
		void LoadMap();
		void Go();

		void ToggleTextures();
		void ToggleNewMap();
		void ToggleResize();
		void ToggleSaveMap();
		void ToggleLoadMap(const std::string &TempPath = "");

		void InitTextures();
		void InitNewMap();
		void InitResize();
		void InitSaveMap();
		void InitLoadMap(const std::string &TempPath = "");
		bool CloseWindows();

		// Brushes
		void ApplyBrush(const glm::vec2 &Position);
		void DrawBrushInfo();
		void AdjustValue(uint32_t &Value, int Direction);

		// Copy/Paste
		void CopyTiles();
		void PasteTiles();
		void GetTileDrawBounds(glm::ivec2 &Start, glm::ivec2 &End);

		// General
		const _Stats *Stats;

		// Graphics
		ae::_Camera *Camera{nullptr};
		ae::_Framebuffer *Framebuffer{nullptr};
		glm::vec2 WorldCursor;

		// Map
		_Map *Map{nullptr};
		_Tile **CopyBuffer{nullptr};
		uint32_t MapID;
		std::string FilePath;
		double Clock;
		bool UseClockAmbientLight;
		bool ShowBackgroundMap;
		bool ShowForeground;
		bool MapView;

		// Copy paste
		bool CopiedTiles;
		bool DrawCopyBounds;
		glm::ivec2 CopyStart;
		glm::ivec2 CopyEnd;
		glm::ivec2 PasteFlip{0, 0};

		// Brush
		int BrushMode;
		int Layer;
		float BrushRadius;
		_Tile *Brush{nullptr};

		// Objects
		int ObjectType;
		uint32_t ObjectData;

		// Filter
		int Filter;

		// UI
		ae::_Element *ButtonBarElement{nullptr};
		ae::_Element *TexturesElement{nullptr};
		ae::_Element *NewMapElement{nullptr};
		ae::_Element *ResizeMapElement{nullptr};
		ae::_Element *SaveMapElement{nullptr};
		ae::_Element *LoadMapElement{nullptr};
		ae::_Element *NewMapFilenameTextBox{nullptr};
		ae::_Element *NewMapWidthTextBox{nullptr};
		ae::_Element *NewMapHeightTextBox{nullptr};
		ae::_Element *ResizeMinXTextBox{nullptr};
		ae::_Element *ResizeMinYTextBox{nullptr};
		ae::_Element *ResizeMaxXTextBox{nullptr};
		ae::_Element *ResizeMaxYTextBox{nullptr};
		ae::_Element *SaveMapTextBox{nullptr};
		ae::_Element *LoadMapTextBox{nullptr};
};

extern _EditorState EditorState;
