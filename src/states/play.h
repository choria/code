/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

#include <ae/buffer.h>
#include <ae/state.h>
#include <ae/type.h>
#include <constants.h>
#include <glm/vec3.hpp>

// Forward Declarations
class _Battle;
class _Map;
class _Object;
class _Scripting;
class _Server;
class _StatChange;
class _Stats;

namespace ae {
	template<class T> class _Manager;
	class _AudioSource;
	class _Camera;
	class _ClientNetwork;
	class _Framebuffer;
}

// Play state
class _PlayState : public ae::_State {

	public:

		// Setup
		void Init() override;
		void Close() override;
		void StopGame();

		// Network
		void Connect(bool IsLocal);
		void StopLocalServer();
		void SendStatus(uint8_t Status);
		void SendActionUse(uint8_t Slot);
		void SendJoinRequest();

		// Input
		bool HandleAction(int InputType, size_t Action, int Value, bool Repeat) override;
		bool HandleKey(const ae::_KeyEvent &KeyEvent) override;
		void HandleMouseButton(const ae::_MouseEvent &MouseEvent) override;
		void HandleMouseMove(const glm::ivec2 &Position) override;
		bool HandleCommand(ae::_Console *Console) override;
		void HandleWindow(uint8_t Event) override;
		void HandleQuit() override;

		// Update
		void Update(double FrameTime) override;
		void Render(double BlendFactor) override;

		// State
		bool ShowExitWarning();

		// Sound
		void PlayCoinSound();
		void PlayDeathSound();
		void StopTeleportSound();

		// Menu map
		glm::vec3 GetRandomMapPosition();

		// Parameters
		bool DevMode{false};
		bool IsHardcore{false};
		bool NoPVP{false};
		bool FromEditor{false};
		bool ConnectNow{false};
		bool CreateBot{false};
		bool WasInBattle{false};

		// Game
		const _Stats *Stats{nullptr};
		double Time{0.0};
		int EventPhase{0};
		bool BloodMoonActive{false};

		// Graphics
		ae::_Framebuffer *Framebuffer{nullptr};
		double LightBlendTime{0.0};

		// Scripting
		_Scripting *Scripting{nullptr};

		// Objects
		ae::_Manager<_Object> *ObjectManager{nullptr};
		_Object *Player{nullptr};
		_Map *Map{nullptr};
		_Battle *Battle{nullptr};
		ae::NetworkIDType ClientNetworkID{0};

		// Camera
		ae::_Camera *Camera{nullptr};
		ae::_Camera *MenuCamera{nullptr};

		// Audio
		const ae::_AudioSource *TeleportSound{nullptr};
		bool CoinSoundPlayed{false};

		// Network
		ae::_ClientNetwork *Network{nullptr};
		_Server *Server{nullptr};
		std::string HostAddress{"127.0.0.1"};
		uint16_t ConnectPort{DEFAULT_NETWORKPORT};
		bool DoneOnDisconnect{false};

		// Menu
		_Map *MenuMap{nullptr};
		glm::vec3 MenuCameraTargetPosition{0.0f};

	protected:

		void StartLocalServer();

		void HandleConnect();
		void HandleDisconnect();
		void HandlePacket(ae::_Buffer &Data);

		void HandleObjectStats(ae::_Buffer &Data);
		void HandleClock(ae::_Buffer &Data);
		void HandleChangeMaps(ae::_Buffer &Data);
		void HandleObjectList(ae::_Buffer &Data);
		void HandleObjectCreate(ae::_Buffer &Data);
		void HandleObjectDelete(ae::_Buffer &Data);
		void HandleObjectUpdates(ae::_Buffer &Data);
		void HandlePlayerPosition(ae::_Buffer &Data);
		void HandleSpawnPoint(ae::_Buffer &Data);
		void HandleTeleportStart(ae::_Buffer &Data);
		void HandleEventStart(ae::_Buffer &Data);
		void HandleEventMessage(ae::_Buffer &Data);
		void HandleInventory(ae::_Buffer &Data);
		void HandleInventoryAdd(ae::_Buffer &Data);
		void HandleInventorySwap(ae::_Buffer &Data);
		void HandleInventoryUpdate(ae::_Buffer &Data);
		void HandleInventoryGold(ae::_Buffer &Data);
		void HandlePartyInfo(ae::_Buffer &Data);
		void HandlePartyName(ae::_Buffer &Data);
		void HandlePartyClear(ae::_Buffer &Data);
		void HandleChatMessage(ae::_Buffer &Data);
		void HandleTradeRequest(ae::_Buffer &Data);
		void HandleTradeCancel(ae::_Buffer &Data);
		void HandleTradeInventory(ae::_Buffer &Data);
		void HandleTradeGold(ae::_Buffer &Data);
		void HandleTradeAccept(ae::_Buffer &Data);
		void HandleTradeExchange(ae::_Buffer &Data);
		void HandleBattleStart(ae::_Buffer &Data);
		void HandleBattleAction(ae::_Buffer &Data);
		void HandleBattleJoin(ae::_Buffer &Data);
		void HandleBattleLeave(ae::_Buffer &Data);
		void HandleBattleEnd(ae::_Buffer &Data);
		void HandleBossCooldownMessage(ae::_Buffer &Data);
		void HandleBossCooldowns(ae::_Buffer &Data);
		void HandleClearWait(ae::_Buffer &Data);
		void HandleActionClear(ae::_Buffer &Data);
		void HandleActionResults(ae::_Buffer &Data);
		void HandleStatChange(ae::_Buffer &Data, _StatChange &StatChange, bool FromAction, const char *ItemUsed=nullptr);
		void HandleSkillMaxLevelAdjust(ae::_Buffer &Data);
		void HandleHUD(ae::_Buffer &Data);
		void HandleMinigameSeed(ae::_Buffer &Data);
		void HandleBuffClear(ae::_Buffer &Data);
		void HandleBuffUpdate(ae::_Buffer &Data);
		void HandleStatusEffects(ae::_Buffer &Data);
		void HandleWorldEvent(ae::_Buffer &Data);

		_Object *CreateObject(ae::_Buffer &Data, ae::NetworkIDType NetworkID);

		void AssignPlayer(_Object *Object);
		void DeleteBattle();
		void DeleteMap();
		void HandleLevelChange(int OldLevel);
		bool UpdateInput();

		void SetViewProjection(ae::_Camera *CameraUsed);

		// Network
		ae::_Buffer PongPacket{1024};
};

extern _PlayState PlayState;
