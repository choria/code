/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <states/dedicated.h>
#include <ae/database.h>
#include <ae/manager.h>
#include <ae/peer.h>
#include <ae/servernetwork.h>
#include <ae/util.h>
#include <objects/battle.h>
#include <objects/components/character.h>
#include <objects/map.h>
#include <objects/object.h>
#include <config.h>
#include <constants.h>
#include <framework.h>
#include <save.h>
#include <server.h>
#include <enet/enet.h>
#include <json/reader.h>
#include <picosha2/picosha2.h>
#include <iomanip>

_DedicatedState DedicatedState;

// Command loop
void RunCommandThread(_Server *Server) {
	std::cout << "Type help to list commands" << std::endl;

	bool Done = false;
	while(!Done) {
		std::string Input;
		std::getline(std::cin, Input);
		Server->Log << "[SERVER_COMMAND] " << Input << std::endl;
		try {
			std::vector<std::string> Parameters;
			Parameters.reserve(10);
			ae::TokenizeString(Input, Parameters);

			if(Input == "a" || Input == "accounts") {
				DedicatedState.ShowAccounts();
			}
			else if(Input.substr(0, 3) == "ban") {
				if(Parameters.size() == 3) {
					Server->Ban((uint32_t)std::stoi(Parameters[1]), "");
					std::cout << "unbanned" << std::endl;
				}
				else {
					if(Parameters.size() != 4)
						throw std::runtime_error("Bad parameters");

					uint32_t AccountID = (uint32_t)std::stoi(Parameters[1]);
					Server->Ban(AccountID, Parameters[2] + " " + Parameters[3]);

					ae::_Database *Database = Server->Save->Database;
					Database->PrepareQuery("SELECT DATETIME(banned, 'localtime') AS banned_until FROM account WHERE id = @id");
					Database->BindInt(1, AccountID);
					if(Database->FetchRow()) {
						const char *BannedUntil = Database->GetString("banned_until");
						if(BannedUntil)
							std::cout << "Account " << AccountID << " banned until " << Database->GetString("banned_until") << std::endl;
						else
							std::cout << "Bad parameters" << std::endl;
					}
					Database->CloseQuery();
				}
			}
			else if(Input == "b" || Input == "battles") {
				DedicatedState.ShowBattles();
			}
			else if(Input.substr(0, 2) == "c " || Input.substr(0, 10) == "character ") {
				if(Parameters.size() != 2)
					throw std::runtime_error("Bad parameters");

				uint32_t CharacterID = (uint32_t)std::stoi(Parameters[1]);
				DedicatedState.ShowCharacter(CharacterID);
			}
			else if(Input.substr(0, 3) == "cs " || Input.substr(0, 11) == "characters ") {
				if(Parameters.size() != 2)
					throw std::runtime_error("Bad parameters");

				uint32_t AccountID = (uint32_t)std::stoi(Parameters[1]);
				DedicatedState.ShowCharacters(AccountID);
			}
			else if(Input.substr(0, 5) == "event") {
				if(Input.size() > 6)
					Server->Save->BloodMoonTime = std::stod(Parameters[1]);

				std::cout << "Event time = " << Server->Save->BloodMoonTime << std::endl;
			}
			else if(Input.substr(0, 3) == "log" && Input.size() > 4) {
				ae::NetworkIDType PlayerID = std::stoi(Input.substr(4, std::string::npos));
				bool Mode = Server->StartLog(PlayerID);
				std::cout << "Logging has been " << (Mode ? "enabled" : "disabled") << std::endl;
			}
			else if(Input == "help") {
				DedicatedState.ShowCommands();
			}
			else if(Input == "parties") {
				DedicatedState.ShowParties();
			}
			else if(Input == "p" || Input == "players") {
				DedicatedState.ShowPlayers();
			}
			else if(Input.substr(0, 8) == "password") {
				if(Parameters.size() < 3)
					throw std::runtime_error("Bad parameters");

				std::string Find = Parameters[0] + " " + Parameters[1] + " ";
				std::string Password = Input.replace(0, Find.size(), "");
				DedicatedState.SetPassword((uint32_t)std::stoi(Parameters[1]), Password);
				std::cout << "Password set to \"" << Password << "\"" << std::endl;
			}
			else if(Input.substr(0, 4) == "mute") {
				if(Parameters.size() != 3)
					throw std::runtime_error("Bad parameters");

				Server->Mute((uint32_t)std::stoi(Parameters[1]), std::stoi(Parameters[2]));
			}
			else if(Input.substr(0, 3) == "say" && Input.size() > 4) {
				Server->BroadcastMessage(nullptr, Input.substr(4, std::string::npos), "purple");
			}
			else if(Input.substr(0, 4) == "slap" && Input.size() > 5) {
				ae::NetworkIDType PlayerID = std::stoi(Input.substr(5, std::string::npos));
				Server->Slap(PlayerID);
			}
			else if(Input.substr(0, 4) == "stop" || std::cin.eof() == 1) {
				int Seconds = 0;
				if(Input.size() > 5)
					Seconds = std::stoi(Input.substr(5, std::string::npos));
				Server->StopServer(Seconds);
				Done = true;
			}
			else
				throw std::runtime_error("Command not recognized");
		}
		catch(std::exception &Error) {
			std::cout << "Error: " << Error.what() << std::endl;
		}
	}

	Server->StopServer();
}

// Init
void _DedicatedState::Init() {

	// Setup server
	try {
		Server = new _Server(true, NetworkPort);
		Server->Hardcore = Hardcore;
		Server->NoPVP = NoPVP;
		Server->DevMode = DevMode;
		Server->Log.ToStdOut = false;

		if(Hardcore)
			std::cout << "Hardcore only is on" << std::endl;
		if(NoPVP)
			std::cout << "PVP is disabled" << std::endl;

		Thread = new std::thread(RunCommandThread, Server);
	}
	catch(std::exception &Error) {
		std::cerr << Error.what() << std::endl;
		Framework.Done = true;
	}
}

// Close
void _DedicatedState::Close() {
	if(Thread) {
		Thread->join();
		delete Thread;
	}

	delete Server;
}

// Update
void _DedicatedState::Update(double FrameTime) {
	if(Config.ShowStalls && Framework.GetTimeStepAccumulator() > DEBUG_STALL_THRESHOLD)
		Server->Log << "[STALL] TimeStepAccumulator=" << Framework.GetTimeStepAccumulator() << std::endl;

	// Update server
	Server->Update(FrameTime);

	if(Server->Done) {
		Framework.Done = true;
	}
}

// Set account password
void _DedicatedState::SetPassword(uint32_t AccountID, const std::string &Password) {
	std::string Salt = Server->Save->GenerateSalt();
	std::string Hash = picosha2::hash256_hex_string(Password + Salt);

	ae::_Database *Database = Server->Save->Database;
	Database->PrepareQuery("UPDATE account SET password = @password, salt = @salt WHERE id = @id");
	Database->BindString(1, Hash);
	Database->BindString(2, Salt);
	Database->BindInt(3, AccountID);
	Database->FetchRow();
	Database->CloseQuery();
}

// List available commands
void _DedicatedState::ShowCommands() {
	std::cout << std::endl;
	std::cout << "accounts                               show accounts" << std::endl;
	std::cout << "ban          <account_id> <time>       ban player (e.g. ban 1 5 days)" << std::endl;
	std::cout << "battles                                show current battles" << std::endl;
	std::cout << "character    <character_id>            show character info" << std::endl;
	std::cout << "characters   <account_id>              show characters for account" << std::endl;
	std::cout << "event        [time]                    show or update event time" << std::endl;
	std::cout << "log          <network_id>              toggle logging player data" << std::endl;
	std::cout << "mute         <account_id> <value>      mute player (e.g. mute 1 1)" << std::endl;
	std::cout << "password     <account_id> <password>   set account password" << std::endl;
	std::cout << "parties                                show parties" << std::endl;
	std::cout << "players                                show players" << std::endl;
	std::cout << "stop         [seconds]                 stop server" << std::endl;
	std::cout << "slap         <network_id>              slap player" << std::endl;
	std::cout << "say          <message>                 broadcast message" << std::endl;
}

// Show accounts
void _DedicatedState::ShowAccounts() {

	// Header
	std::cout << "\n"
		<< std::right << std::setw(7) << "id"
		<< std::left << std::setw(ACCOUNT_MAX_USERNAME_SIZE + 2) << " username"
		<< std::left << std::setw(8) << " muted"
		<< std::left << std::setw(20) << " banned_until"
		<< std::endl;

	// Run query
	ae::_Database *Database = Server->Save->Database;
	Database->PrepareQuery("SELECT *, DATETIME(banned, 'localtime') AS banned_until FROM account");
	while(Database->FetchRow()) {
		uint32_t ID = Database->GetInt<uint32_t>("id");
		std::string Username = Database->GetString("username");
		const char *BannedUntil = Database->GetString("banned_until");
		std::string BannedText = BannedUntil ? BannedUntil : "";
		bool Muted = Database->GetInt<int>("muted");
		std::cout
			<< std::right << std::setw(7) << ID << " "
			<< std::left << std::setw(ACCOUNT_MAX_USERNAME_SIZE + 2) << Username
			<< std::left << std::setw(8) << Muted
			<< std::left << std::setw(20) << BannedText
			<< std::endl;
	}

	Database->CloseQuery();
}

// Show character information
void _DedicatedState::ShowCharacter(uint32_t CharacterID) {

	// Run query
	ae::_Database *Database = Server->Save->Database;
	Database->PrepareQuery("SELECT * FROM character WHERE id = @character_id");
	Database->BindInt(1, CharacterID);
	if(Database->FetchRow()) {
		std::string Username = Database->GetString("name");
		const char *Data = Database->GetString("data");
		if(Data) {

			// Parse JSON
			Json::CharReaderBuilder Reader;
			std::istringstream Stream(Data);
			Json::Value JsonValue;
			std::string Errors;
			if(!Json::parseFromStream((Json::CharReader::Factory const &)Reader, Stream, &JsonValue, &Errors))
				throw std::runtime_error(Errors);

			// Format JSON
			std::cout << JsonValue.toStyledString() << std::endl;
		}
	}

	Database->CloseQuery();
}

// Show characters for an account
void _DedicatedState::ShowCharacters(uint32_t AccountID) {

	// Header
	std::cout << "\n"
		<< std::right << std::setw(7) << "id"
		<< std::left << " name"
		<< std::endl;

	// Run query
	ae::_Database *Database = Server->Save->Database;
	Database->PrepareQuery("SELECT * FROM character WHERE account_id = @account_id");
	Database->BindInt(1, AccountID);
	while(Database->FetchRow()) {
		uint32_t ID = Database->GetInt<uint32_t>("id");
		std::string Username = Database->GetString("name");
		std::cout
			<< std::right << std::setw(7) << ID << " "
			<< std::left << Username
			<< std::endl;
	}

	Database->CloseQuery();
}

// Show all players
void _DedicatedState::ShowPlayers() {
	auto &Peers = Server->Network->GetPeers();

	std::cout << "peer count=" << Peers.size() << std::endl;
	size_t i = 0;
	for(auto &Peer : Peers) {
		char Buffer[16];
		ENetAddress *Address = &Peer->ENetPeer->address;
		enet_address_get_host_ip(Address, Buffer, 16);

		std::cout << std::setw(5) << i << ": ip=" << Buffer << ", account_id=" << Peer->AccountID;
		if(Peer->Object) {
			uint32_t MapID = 0;
			if(Peer->Object->Map)
				MapID = Peer->Object->Map->NetworkID;

			std::cout
				<< ", network_id=" << Peer->Object->NetworkID
				<< ", map_id=" << MapID
				<< ", character_id=" << Peer->Object->Character->ID
				<< ", name=" << Peer->Object->Name
				<< ", muted=" << Peer->Object->Muted
				<< ", hardcore=" << (int)Peer->Object->Character->Hardcore;
		}

		std::cout << std::endl;
		i++;
	}

	std::cout << std::endl;
}

// Show parties
void _DedicatedState::ShowParties() {

	for(const auto &Party : Server->Parties) {
		std::cout << Party.first << std::endl;

		size_t i = 0;
		for(const auto &Group : Party.second) {
			std::cout << std::setw(5) << i << ": name=" << Group.second->Name << ", character_id=" << Group.first << std::endl;
			i++;
		}
	}

	std::cout << std::endl;
}

// Show all battles
void _DedicatedState::ShowBattles() {
	auto &Battles = Server->BattleManager->Objects;

	std::cout << "battle count=" << Battles.size() << std::endl;
	size_t i = 0;
	for(auto &Battle : Battles) {
		std::cout << i << ": id=" << Battle->NetworkID << std::endl;
		for(auto &Object : Battle->Objects) {
			std::cout << "\tnetwork_id=" << Object->NetworkID << "\thealth=" << ae::Round1(Object->Character->GetHealthPercent()) << "\tname=" << Object->Name << std::endl;
		}

		i++;
		std::cout << std::endl;
	}
}
