/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <ae/state.h>
#include <constants.h>
#include <mutex>
#include <thread>
#include <vector>

// Forward Declarations
class _Bot;
class _Stats;

// Bot manager class
class _BotsState : public ae::_State {

	public:

		// Setup
		void Init() override;
		void Close() override;

		// Update
		void Update(double FrameTime) override;

		// Commands
		void ShowCommands();
		void Add();
		void AddMultiple(int Count);
		void DisconnectAll();
		void HandleQuit() override;

		std::string HostAddress{"127.0.0.1"};
		std::string BotNamePrefix;
		uint16_t Port{DEFAULT_NETWORKPORT};

	protected:

		std::thread *Thread{nullptr};
		std::mutex Mutex;

		const _Stats *Stats{nullptr};
		std::vector<_Bot *> Bots;

		int NextBotNumber{0};
		bool Done{false};
};

extern _BotsState BotState;
