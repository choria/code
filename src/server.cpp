/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <server.h>
#include <ae/database.h>
#include <ae/manager.h>
#include <ae/peer.h>
#include <ae/random.h>
#include <ae/servernetwork.h>
#include <ae/util.h>
#include <objects/battle.h>
#include <objects/buff.h>
#include <objects/components/character.h>
#include <objects/components/controller.h>
#include <objects/components/fighter.h>
#include <objects/components/inventory.h>
#include <objects/components/monster.h>
#include <objects/item.h>
#include <objects/map.h>
#include <objects/minigame.h>
#include <objects/object.h>
#include <objects/status_effect.h>
#include <config.h>
#include <packet.h>
#include <save.h>
#include <scripting.h>
#include <stats.h>
#include <version.h>
#include <enet/enet.h>
#include <glm/common.hpp>
#include <SDL_timer.h>
#include <regex>

// Function for sorting objects in battle
inline bool CompareBattleObjects(const _Object *First, const _Object *Second) {
	if(First->Character->Priority == Second->Character->Priority)
		return First->NetworkID < Second->NetworkID;

	return First->Character->Priority < Second->Character->Priority;
}

// Function to run the server thread
static void RunThread(void *Arguments) {

	// Get server object
	_Server *Server = (_Server *)Arguments;

	// Init timer
	Uint64 Timer = SDL_GetPerformanceCounter();
	double TimeStepAccumulator = 0.0;
	double TimeStep = DEFAULT_TIMESTEP;
	while(!Server->Done) {
		double FrameTime = (SDL_GetPerformanceCounter() - Timer) / (double)SDL_GetPerformanceFrequency();
		Timer = SDL_GetPerformanceCounter();

		// Run server
		TimeStepAccumulator += FrameTime * Config.TimeScale;
		while(TimeStepAccumulator >= TimeStep) {
			if(Config.ShowStalls && TimeStepAccumulator > DEBUG_STALL_THRESHOLD)
				Server->Log << "[STALL] TimeStepAccumulator=" << TimeStepAccumulator << std::endl;

			Server->Update(TimeStep);
			TimeStepAccumulator -= TimeStep;
		}

		// Sleep thread
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
}

// Constructor
_Server::_Server(bool Dedicated, uint16_t NetworkPort) :
	Dedicated(Dedicated),
	Network(new ae::_ServerNetwork(Config.MaxClients, NetworkPort)) {

	if(!Network->HasConnection())
		throw std::runtime_error("Unable to start server on port " + std::to_string(NetworkPort));

	Network->CreatePingSocket(DEFAULT_NETWORKPINGPORT);
	Network->SetFakeLag(Config.FakeLag);
	Network->SetUpdatePeriod(Config.NetworkRate);

	ObjectManager = new ae::_Manager<_Object>();
	MapManager = new ae::_Manager<_Map>();
	BattleManager = new ae::_Manager<_Battle>();
	Stats = new _Stats(true);
	Save = new _Save();

	// Load extra attributes
	Scripting = new _Scripting();
	Scripting->Setup(Stats, SCRIPTS_GAME);
	Scripting->InjectServer(this);
	Scripting->LoadUnlocks(Stats->ItemMap, "Item_RitePassage", "Keys", KeyUnlocks);
	Scripting->LoadUnlocks(Stats->ItemMap, "Item_RiteTraversal", "Items", RelicUnlocks);

	Log.Open((Config.LogPath + "server.log").c_str());
	Log << "[SERVER_START] Listening on port " << NetworkPort << std::endl;

	// Load slap words
	std::ifstream SlapRegexFile(Config.ConfigPath + "slapregex.txt");
	SlapRegexes.clear();
	if(SlapRegexFile.is_open()) {
		std::string Line;
		while(std::getline(SlapRegexFile, Line))
			SlapRegexes.push_back(Line);
	}
}

// Destructor
_Server::~_Server() {
	Done = true;
	JoinThread();

	SaveGame();

	delete MapManager;
	delete BattleManager;
	delete ObjectManager;
	delete Scripting;
	delete Save;
	delete Stats;
	delete Thread;

	Log << "[SERVER_STOP] Stopping server" << std::endl;
}

// Save server
void _Server::SaveGame() {
	try {
		Save->StartTransaction();
		Save->SaveSettings();
		for(auto &Object : ObjectManager->Objects)
			Save->SavePlayer(Object, Object->GetMapID(), &Log);
		Save->EndTransaction();
	}
	catch(std::exception &Error) {
		Log << "[ERROR] Database exception: " << Error.what() << std::endl;
	}
}

// Start the server thread
void _Server::StartThread() {
	Thread = new std::thread(RunThread, this);
}

// Wait for thread to join
void _Server::JoinThread() {
	if(Thread)
		Thread->join();

	delete Thread;
	Thread = nullptr;
}

// Stop the server
void _Server::StopServer(int Seconds) {
	if(Seconds > 0) {
		StartShutdownTimer = true;
		ShutdownTime = Seconds;
	}
	else {
		StartDisconnect = true;
	}
}

// Create a summon object
_Object *_Server::CreateSummon(int Index, bool JoiningBattle, _Object *Source, const _Summon &Summon) {

	// Create monster
	_Object *Object = ObjectManager->Create();
	Object->Stats = Stats;
	Object->CreateComponents();
	Object->Server = this;
	Object->Scripting = Scripting;
	Object->Monster->DatabaseID = Summon.ID;
	Object->Monster->Owner = Source;
	Object->Monster->SummonBuff = Summon.SummonBuff;
	Object->Monster->SpellID = Summon.SpellID;
	Object->Monster->Duration = Summon.Duration;
	Object->Monster->Difficulty = Source->Monster->Difficulty;
	Object->Character->InitAttributes();

	// Get difficulty multipliers
	double DifficultyMultiplier = Object->Monster->GetDifficultyMultiplier();
	double DamageMultiplier = Object->Monster->GetDamageMultiplier();

	// Get stats from db and script
	Stats->SetMonsterStats(Object);
	Object->Character->Attributes["Health"].Int = Object->Character->BaseAttributes["MaxHealth"].Int = Summon.Health * DifficultyMultiplier;
	Object->Character->Attributes["Mana"].Int = Object->Character->BaseAttributes["MaxMana"].Int = Summon.Mana * DifficultyMultiplier;
	Object->Character->BaseAttributes["MinDamage"].Int = Summon.MinDamage * DamageMultiplier;
	Object->Character->BaseAttributes["MaxDamage"].Int = Summon.MaxDamage * DamageMultiplier;
	Object->Character->BaseAttributes["Armor"].Int = Summon.Armor;
	Object->Character->BaseAttributes["BattleSpeed"].Int = Summon.BattleSpeed;
	Object->Character->BaseAttributes["HealPower"].Int = Summon.HealPower;
	Object->Character->BaseAttributes["TargetCount"].Int = Summon.TargetCount;
	Object->Character->BaseAttributes["FireResist"].Int += Summon.ResistAll;
	Object->Character->BaseAttributes["ColdResist"].Int += Summon.ResistAll;
	Object->Character->BaseAttributes["LightningResist"].Int += Summon.ResistAll;
	Object->Character->BaseAttributes["PoisonResist"].Int += Summon.ResistAll;
	Object->Character->BaseAttributes["BleedResist"].Int += Summon.ResistAll;
	Object->Character->BaseAttributes["StunResist"].Int += Summon.ResistAll;
	Object->Character->BaseAttributes["MaxFireResist"].Int = Summon.MaxResistAll;
	Object->Character->BaseAttributes["MaxColdResist"].Int = Summon.MaxResistAll;
	Object->Character->BaseAttributes["MaxLightningResist"].Int = Summon.MaxResistAll;
	Object->Character->BaseAttributes["MaxPoisonResist"].Int = Summon.MaxResistAll;
	Object->Character->BaseAttributes["MaxBleedResist"].Int = Summon.MaxResistAll;
	Object->Character->BaseAttributes["MaxStunResist"].Int = Summon.MaxResistAll;
	Object->Character->BaseAttributes["BuffLevel"].Int += Summon.BuffLevel * DamageMultiplier;

	// Override skill levels from summon spell
	for(auto &Skill : Object->Character->Skills)
		Skill.second = Summon.SkillLevel;

	// Apply owner's status effects when joining a battle
	if(JoiningBattle) {
		for(auto &StatusEffect : Source->Character->StatusEffects) {
			if(StatusEffect->Deleted)
				continue;

			// Check inherit flag
			if(!StatusEffect->Buff->PassOn)
				continue;

			// Make sure status effect came from owner
			if(StatusEffect->Source != Source)
				continue;

			// Don't apply more than the original target count of status effect
			if(Index >= StatusEffect->TargetCount - 1)
				continue;

			// Create new effect for summon
			_StatusEffect *SummonEffect = new _StatusEffect();
			SummonEffect->Source = Object;
			SummonEffect->Stacks = StatusEffect->Stacks;
			SummonEffect->Buff = StatusEffect->Buff;
			SummonEffect->Time = StatusEffect->Time;
			SummonEffect->Duration = StatusEffect->Duration;
			SummonEffect->MaxDuration = StatusEffect->MaxDuration;
			SummonEffect->Level = StatusEffect->Level;
			SummonEffect->Priority = StatusEffect->Priority;
			SummonEffect->Infinite = StatusEffect->Infinite;

			Object->Character->StatusEffects.push_back(SummonEffect);
		}
	}

	// Update stats
	Object->Character->CalculateStats();

	return Object;
}

// Update
void _Server::Update(double FrameTime) {
	//if(std::abs(std::fmod(Time, 1.0)) >= 0.99)
	//	std::cout << "Server: O=" << ObjectManager->Objects.size() << " B=" << BattleManager->Objects.size() << std::endl;

	// Handle pings
	ae::_NetworkAddress PingAddress;
	while(Network->CheckPings(PingPacket, PingAddress)) {
		PingType Type = PingPacket.Read<PingType>();

		// Handle ping types
		switch(Type) {
			case PingType::SERVER_INFO: {
				ae::_Buffer PongPacket;
				PongPacket.Write<PingType>(PingType::SERVER_INFO_RESPONSE);
				PongPacket.Write<uint16_t>(Network->GetListenPort());
				PongPacket.Write<uint16_t>(Network->GetPeers().size());
				PongPacket.Write<uint16_t>(Network->GetMaxPeers());
				PongPacket.WriteBit(Hardcore);
				Network->SendPingPacket(PongPacket, ae::_NetworkAddress(PingAddress.Host, PingAddress.Port));
			} break;
			default:
			break;
		}

		// Reset packet
		PingPacket.StartRead();
	}

	// Update network
	Network->Update(FrameTime);

	// Get events
	ae::_NetworkEvent NetworkEvent;
	while(Network->GetNetworkEvent(NetworkEvent)) {

		switch(NetworkEvent.Type) {
			case ae::_NetworkEvent::CONNECT:
				HandleConnect(NetworkEvent);
			break;
			case ae::_NetworkEvent::DISCONNECT:
				HandleDisconnect(NetworkEvent);
			break;
			case ae::_NetworkEvent::PACKET:
				HandlePacket(*NetworkEvent.Data, NetworkEvent.Peer);
				delete NetworkEvent.Data;
			break;
		}
	}

	// Update objects
	ObjectManager->Update(FrameTime);

	// Spawn battles
	for(auto &BattleEvent : BattleEvents)
		StartBattle(BattleEvent);

	BattleEvents.clear();

	// Update maps
	MapManager->Update(FrameTime);

	// Update battles
	BattleManager->Update(FrameTime);

	// Check if updates should be sent
	if(Network->NeedsUpdate()) {
		Network->ResetUpdateTimer();
		if(Network->GetPeers().size() > 0) {

			// Send object updates
			for(auto &Map : MapManager->Objects) {
				Map->SendObjectUpdates();
			}
		}
	}

	// Handle rebirths
	for(auto &RebirthEvent : RebirthEvents)
		StartRebirth(RebirthEvent);

	RebirthEvents.clear();

	// Wait for peers to disconnect
	if(StartShutdownTimer) {
		ShutdownTime -= FrameTime;
		if(std::abs(std::fmod(ShutdownTime, 5.0)) >= 4.99)
			BroadcastMessage(nullptr, "The server will be shutting down in " + std::to_string((int)(ShutdownTime + 0.5)) + " seconds", "red");

		if(ShutdownTime <= 0 || !Network->GetPeers().size()) {
			StartDisconnect = true;
			StartShutdownTimer = false;
		}
	}
	else if(StartDisconnect) {
		Network->DisconnectAll(ae::_Network::DISCONNECT_SERVER);
		StartDisconnect = false;
		StartShutdown = true;
		if(PenalizeSinglePlayer)
			ApplyLeavePenalty(SinglePlayerNetworkID);
	}
	else if(StartShutdown && Network->GetPeers().size() == 0) {
		Done = true;
	}

	TimeSteps++;
	Time += FrameTime;

	// Update scripting environment
	Scripting->InjectGlobals(Time, Save->BloodMoonActive);

	// Update clock if server has active players
	if(ActiveCount) {
		double LastClock = Save->Clock;
		Save->Clock += FrameTime * MAP_CLOCK_SPEED;
		if(Save->Clock >= MAP_DAY_LENGTH)
			Save->Clock -= MAP_DAY_LENGTH;

		// Check global events
		if(LastClock < GAME_BLOODMOON_TIMECHECK && Save->Clock >= GAME_BLOODMOON_TIMECHECK) {
			Log << "[EVENT] Blood Moon check ( bloodmoon_time=" << Save->BloodMoonTime << " )" << std::endl;

			if(Save->BloodMoonTime <= 0.0) {
				Save->BloodMoonActive = true;
				Save->GenerateBloodMoonTime();
				SendEventStatus();
				BroadcastMessage(nullptr, "The Blood Moon beckons", "purple");

				Log << "[EVENT] Blood Moon begins ( bloodmoon_time=" << Save->BloodMoonTime << " )" << std::endl;
			}
			else {
				if(Save->BloodMoonActive) {
					Save->BloodMoonActive = false;
					SendEventStatus();
					BroadcastMessage(nullptr, "The Blood Moon wanes", "purple");

					Log << "[EVENT] Blood Moon ends ( bloodmoon_time=" << Save->BloodMoonTime << " )" << std::endl;
				}
			}

			// Update event phase
			for(auto &Peer : Network->GetPeers())
				SendHUD(Peer);
		}

		// Update blood moon time
		Save->BloodMoonTime -= FrameTime * MAP_CLOCK_SPEED;
		if(Save->BloodMoonTime <= 0.0)
			Save->BloodMoonTime = 0.0;
	}

	// Update party timer
	PartyTime += FrameTime;
	if(PartyTime >= HUD_PARTY_UPDATE_PERIOD) {
		PartyTime = 0.0;

		// Update parties
		UpdateParties();
	}

	// Update autosave
	SaveTime += FrameTime;
	if(Config.AutoSavePeriod > 0 && SaveTime >= Config.AutoSavePeriod) {
		SaveTime = 0;
		SaveGame();
	}

	// Update bot timer
	if(BotTime >= 0)
		BotTime += FrameTime;

	// Spawn bot
	if(AddBot && BotTime > 0.25) {
		BotTime = -1;
		CreateBot();
	}
}

// Handle client connect
void _Server::HandleConnect(ae::_NetworkEvent &Event) {

	// Set timeout
	enet_peer_timeout(Event.Peer->ENetPeer, 0, NETWORK_TIMEOUT_MINIMUM, NETWORK_TIMEOUT_MAXIMUM);

	// Get ip
	char Buffer[16];
	ENetAddress *Address = &Event.Peer->ENetPeer->address;
	enet_address_get_host_ip(Address, Buffer, 16);
	Log << "[CONNECT] Connect from " << Buffer << ":" << Address->port << std::endl;

	// Send game version
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::VERSION);
	Packet.Write<char>(PLATFORM);
	Packet.WriteString(GAME_VERSION);
	if(Config.ClientVersion.size())
		Packet.WriteString(Config.ClientVersion.c_str());
	else
		Packet.WriteString(GetBuildVersion());
	Network->SendPacket(Packet, Event.Peer);
}

// Handle client disconnect
void _Server::HandleDisconnect(ae::_NetworkEvent &Event) {

	// Get ip address
	char Buffer[16];
	ENetAddress *Address = &Event.Peer->ENetPeer->address;
	enet_address_get_host_ip(Address, Buffer, 16);

	// Get disconnect type and log message
	Log << "[DISCONNECT] ";
	switch(Event.Peer->DisconnectFlag) {
		case ae::_Network::DISCONNECT_TIMEOUT:
			Log << "Client Timeout";
		break;
		case ae::_Network::DISCONNECT_CLIENT:
			Log << "Client Disconnect";
		break;
		case ae::_Network::DISCONNECT_SERVER:
			Log << "Server Disconnect";
		break;
	}
	Log << " from " << Buffer << ":" << Address->port << std::endl;

	// Clean up objects
	ae::_Buffer Data;
	HandleExit(Data, Event.Peer);

	// Delete peer from network
	Network->DeletePeer(Event.Peer);
}

// Handle packet data
void _Server::HandlePacket(ae::_Buffer &Data, ae::_Peer *Peer) {
	PacketType Type = Data.Read<PacketType>();

	switch(Type) {
		case PacketType::ACCOUNT_LOGININFO:
			HandleLoginInfo(Data, Peer);
		break;
		case PacketType::CHARACTERS_REQUEST:
			HandleCharacterListRequest(Data, Peer);
		break;
		case PacketType::CHARACTERS_PLAY:
			HandleCharacterPlay(Data, Peer);
		break;
		case PacketType::CREATECHARACTER_INFO:
			HandleCharacterCreate(Data, Peer);
		break;
		case PacketType::CHARACTERS_DELETE:
			HandleCharacterDelete(Data, Peer);
		break;
		case PacketType::WORLD_INPUTCOMMAND:
			HandleInputCommand(Data, Peer);
		break;
		case PacketType::WORLD_RESTART:
			HandleRestart(Data, Peer);
		break;
		case PacketType::WORLD_JOIN:
			HandleJoin(Data, Peer);
		break;
		case PacketType::WORLD_EXIT:
			HandleExit(Data, Peer);
		break;
		case PacketType::WORLD_POSITION:
			HandleSyncRequest(Data, Peer);
		break;
		case PacketType::WORLD_UPDATEID:
			HandleUpdateID(Data, Peer);
		break;
		case PacketType::ACTION_USE:
			HandleActionUse(Data, Peer);
		break;
		case PacketType::CHAT_MESSAGE:
			HandleChatMessage(Data, Peer);
		break;
		case PacketType::BLACKSMITH_UPGRADE:
			HandleBlacksmithUpgrade(Data, Peer);
		break;
		case PacketType::BLACKSMITH_UPGRADESET:
			HandleBlacksmithUpgradeSet(Data, Peer);
		break;
		case PacketType::MINIGAME_PAY:
			HandleMinigamePay(Data, Peer);
		break;
		case PacketType::MINIGAME_GETPRIZE:
			HandleMinigameGetPrize(Data, Peer);
		break;
		case PacketType::INVENTORY_MOVE:
			HandleInventoryMove(Data, Peer);
		break;
		case PacketType::INVENTORY_PRIVILEGE:
			HandleInventoryPrivilege(Data, Peer);
		break;
		case PacketType::INVENTORY_TRANSFER:
			HandleInventoryTransfer(Data, Peer);
		break;
		case PacketType::INVENTORY_USE:
			HandleInventoryUse(Data, Peer);
		break;
		case PacketType::INVENTORY_SPLIT:
			HandleInventorySplit(Data, Peer);
		break;
		case PacketType::INVENTORY_DELETE:
			HandleInventoryDelete(Data, Peer);
		break;
		case PacketType::VENDOR_EXCHANGE:
			HandleVendorExchange(Data, Peer);
		break;
		case PacketType::TRADER_ACCEPT:
			HandleTraderAccept(Data, Peer);
		break;
		case PacketType::ACTIONBAR_CHANGED:
			HandleActionBarChanged(Data, Peer);
		break;
		case PacketType::SKILLS_SKILLADJUST:
			HandleSkillAdjust(Data, Peer);
		break;
		case PacketType::ENCHANTER_BUY:
			HandleEnchanterBuy(Data, Peer);
		break;
		case PacketType::DISENCHANTER_SELL:
			HandleDisenchanterSell(Data, Peer);
		break;
		case PacketType::TRADE_REQUEST:
			HandleTradeRequest(Data, Peer);
		break;
		case PacketType::TRADE_CANCEL:
			HandleTradeCancel(Data, Peer);
		break;
		case PacketType::TRADE_GOLD:
			HandleTradeGold(Data, Peer);
		break;
		case PacketType::TRADE_ACCEPT:
			HandleTradeAccept(Data, Peer);
		break;
		case PacketType::PARTY_NAME:
			HandlePartyName(Data, Peer);
		break;
		case PacketType::PLAYER_STATUS:
			HandlePlayerStatus(Data, Peer);
		break;
		case PacketType::PLAYER_CLEARBUFF:
			HandleClearBuff(Data, Peer);
		break;
		case PacketType::PLAYER_SWAPBUFF:
			HandleSwapBuff(Data, Peer);
		break;
		case PacketType::COMMAND:
			HandleCommand(Data, Peer);
		break;
		default:
		break;
	}
}

// Send map to player
void _Server::SendMap(ae::_Peer *Peer, ae::NetworkIDType MapID) {
	if(!ValidatePeer(Peer))
		return;

	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::WORLD_CHANGEMAPS);
	Packet.Write<uint32_t>(MapID);
	Packet.Write<double>(Save->Clock);
	Packet.Write<int>(GetEventPhase());
	Packet.WriteBit(Peer->Object->Character->IsAlive());
	Network->SendPacket(Packet, Peer);
}

// Send an item to the player
void _Server::SendItem(ae::_Peer *Peer, const _Item *Item, int Count) {
	if(!ValidatePeer(Peer))
		return;

	_Object *Player = Peer->Object;
	if(!Player || !Item)
		return;

	// Add item
	Count = std::min(Count, INVENTORY_MAX_STACK);
	int Added = Player->Inventory->AddItem(Item, 0, Count, true);
	SendInventoryFullMessage(Peer, Item, Added);
	if(!Added)
		return;

	// Send item
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::INVENTORY_ADD);
	Packet.WriteBit(0);
	Packet.Write<uint16_t>((uint16_t)Count);
	Packet.Write<uint32_t>(Item->ID);
	Network->SendPacket(Packet, Peer);

	// Update states
	Player->Character->CalculateStats();
	SendHUD(Player->Peer);
}

// Login information
void _Server::HandleLoginInfo(ae::_Buffer &Data, ae::_Peer *Peer) {

	// Read packet
	bool CreateAccount = Data.ReadBit();
	std::string Username(Data.ReadString());
	std::string Password(Data.ReadString());
	uint64_t Secret = Data.Read<uint64_t>();

	Username.resize(std::min(ACCOUNT_MAX_USERNAME_SIZE, Username.length()));
	Password.resize(std::min(ACCOUNT_MAX_PASSWORD_SIZE, Password.length()));

	// Validate single-player
	if(!Password.length() && Secret != Save->Secret)
		return;

	// Create account or login
	if(CreateAccount) {

		// Check config
		if(!Config.CreateAccount) {
			ae::_Buffer Packet;
			Packet.Write<PacketType>(PacketType::ACCOUNT_NOCREATE);
			Network->SendPacket(Packet, Peer);
			return;
		}

		// Check for existing account
		if(Save->CheckUsername(Username)) {
			ae::_Buffer Packet;
			Packet.Write<PacketType>(PacketType::ACCOUNT_EXISTS);
			Network->SendPacket(Packet, Peer);
			return;
		}
		else
			Save->CreateAccount(Username, Password);
	}

	// Get account id
	std::string BannedText;
	bool Found = Save->GetAccountInfo(Username, Password, Peer->AccountID, BannedText);

	// Make sure account exists
	ae::_Buffer Packet;
	if(Found) {

		// Check for account already being used
		bool AccountInUse = CheckAccountUse(Peer);
		if(AccountInUse) {
			Peer->AccountID = 0;
			Packet.Write<PacketType>(PacketType::ACCOUNT_INUSE);
		}
		else if(!BannedText.empty()) {
			Packet.Write<PacketType>(PacketType::ACCOUNT_BANNED);
			Packet.WriteString(BannedText.c_str());
		}
		else
			Packet.Write<PacketType>(PacketType::ACCOUNT_SUCCESS);
	}
	else
		Packet.Write<PacketType>(PacketType::ACCOUNT_NOTFOUND);

	Network->SendPacket(Packet, Peer);
}

// Sends a player his/her character list
void _Server::HandleCharacterListRequest(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!Peer->AccountID)
		return;

	SendCharacterList(Peer);
}

// Handles the character create request
void _Server::HandleCharacterCreate(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!Peer->AccountID)
		return;

	// Get character information
	bool IsHardcore = Data.ReadBit();
	std::string Name(Data.ReadString());
	uint32_t PortraitID = Data.Read<uint32_t>();
	uint32_t BuildID = Data.Read<uint32_t>();
	uint32_t Slot = Data.Read<uint8_t>();
	if(Name.size() > PLAYER_NAME_SIZE)
		return;

	// Check number of characters in account
	if(Save->GetCharacterCount(Peer->AccountID) >= ACCOUNT_MAX_CHARACTER_SLOTS)
		return;

	// Found an existing name
	if(Save->GetCharacterIDByName(Name) != 0) {
		ae::_Buffer NewPacket;
		NewPacket.Write<PacketType>(PacketType::CREATECHARACTER_INUSE);
		Network->SendPacket(NewPacket, Peer);
		return;
	}

	// Create the character
	try {
		Save->CreateCharacter(Stats, Scripting, Peer->AccountID, Slot, IsHardcore, Name, PortraitID, BuildID);
		Save->SetLastSlot(Peer->AccountID, Slot);
	}
	catch(std::exception &Error) {
		Log << "[ERROR] Database exception: " << Error.what() << std::endl;
	}

	// Notify the client
	ae::_Buffer NewPacket;
	NewPacket.Write<PacketType>(PacketType::CREATECHARACTER_SUCCESS);
	Network->SendPacket(NewPacket, Peer);
}

// Handle a character delete request
void _Server::HandleCharacterDelete(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!Peer->AccountID)
		return;

	// Get delete slot
	uint32_t Slot = Data.Read<uint8_t>();
	uint32_t CharacterID = 0;

	// Get character id
	CharacterID = Save->GetCharacterIDBySlot(Peer->AccountID, Slot);
	if(!CharacterID)
		return;

	// Delete player object
	for(auto &Object : ObjectManager->Objects)
		if(Object->Character && Object->Character->ID == CharacterID)
			Object->Deleted = true;

	// Remove player from monster's owner
	for(auto &Object : ObjectManager->Objects) {
		if(Object->Monster && Object->Monster->Owner && Object->Monster->Owner->Deleted)
			Object->Monster->Owner = nullptr;
	}

	// Delete character
	Save->DeleteCharacter(CharacterID);

	// Update the player
	SendCharacterList(Peer);
}

// Loads the player, updates the world, notifies clients
void _Server::HandleCharacterPlay(ae::_Buffer &Data, ae::_Peer *Peer) {
	_Object *Player = Peer->Object;

	// Read packet
	uint32_t Slot = Data.Read<uint8_t>();

	// Check for valid character id
	bool Muted = false;
	uint32_t CharacterID = Save->GetCharacterID(Peer->AccountID, Slot, Muted);
	if(!CharacterID)
		return;

	// Get player object
	bool Found = false;
	if(!Player) {

		// Find existing player in object list
		for(auto &Object : ObjectManager->Objects) {
			if(Object->Character->ID == CharacterID) {
				Player = Object;
				Player->Peer = Peer;
				Player->Offline = false;
				Found = true;
				break;
			}
		}

		// Create new player object
		if(!Player) {
			Player = CreatePlayer(Peer, CharacterID);
			Player->Muted = Muted;
		}

		// Set object in peer
		Peer->Object = Player;
	}

	// Set last played for account
	Save->SetLastSlot(Peer->AccountID, Slot);

	// Send spawn data to player
	if(Found) {

		// Send map
		SendMap(Peer, Player->Map->NetworkID);

		// Send objects
		Player->Map->SendObjectList(Peer);

		// Send player data
		SendPlayerInfo(Peer);

		// Send battle
		if(Player->Battle) {
			ae::_Buffer Packet;
			Packet.Write<PacketType>(PacketType::BATTLE_START);
			Player->Battle->Serialize(Packet);
			Network->SendPacket(Packet, Peer);
		}

		// Update events
		UpdateEventCount();
	}
	else
		SpawnPlayer(Player, Player->Character->LoadMapID, _Map::EVENT_NONE);

	// Update parties
	AddParty(Player->Character->PartyName, Player->Character->ID, Player);

	// Send event status
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::WORLD_EVENT);
	Packet.WriteBit(Save->BloodMoonActive);
	Network->SendPacket(Packet, Peer);

	// Notify player of world events
	if(Save->BloodMoonActive)
		SendMessage(Peer, "The Blood Moon beckons", "purple");

	// Broadcast message
	std::string Message = Player->Name + " has joined the server";
	BroadcastMessage(Peer, Message, "gray");

	// Update active player count
	ActiveCount++;

	// Log
	Log << "[JOIN] Player " << Message << " ( character_id=" << Player->Character->ID << " )" << std::endl;
}

// Handles move commands from a client
void _Server::HandleInputCommand(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	_Object *Player = Peer->Object;
	if(!Player->Character->IsAlive() || Player->Battle)
		return;

	// Reset idle time
	Player->Character->IdleTime = 0.0;

	// Read input
	int Input = Data.Read<char>();

	// Save input
	Player->Controller->InputStates.push_back(Input);
}

// Handle restart command from client
void _Server::HandleRestart(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Reset idle time
	_Object *Player = Peer->Object;
	Player->Character->IdleTime = 0.0;

	// Check battle
	if(Player->Battle) {

		// Suicide
		if(Player->Character->IsAlive()) {
			_StatChange StatChange;
			StatChange.Object = Player;
			StatChange.Values["Health"].Int = -1e18;
			Player->UpdateStats(StatChange);

			// Update client
			ae::_Buffer Packet;
			Packet.Write<PacketType>(PacketType::STAT_CHANGE);
			StatChange.Serialize(Packet);
			Player->SendPacket(Packet);
		}

		// Leave battle
		ApplyLeavePenalty(Player->NetworkID);
		Player->Battle->RemoveObject(Player);
		SendPlayerInfo(Peer);
	}
	else {

		// Ignore bad packet
		if(Player->Character->IsAlive()) {
			return;
		}
		// Respawn if not hardcore
		else if(!Player->Character->Hardcore) {

			// Give one of each summon on respawn
			for(auto &StatusEffect : Player->Character->StatusEffects) {
				if(StatusEffect->Buff->Summon)
					StatusEffect->Level = 1;
			}
			SendStatusEffects(Player);

			Player->Character->Attributes["Health"].Int = Player->Character->Attributes["MaxHealth"].Int / 2;
			Player->Character->Attributes["Mana"].Int = Player->Character->Attributes["MaxMana"].Int / 2;
			SpawnPlayer(Player, Player->Character->SpawnMapID, _Map::EVENT_SPAWN);
		}
	}
}

// Handle a chat message
void _Server::HandleChatMessage(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Reset idle time
	_Object *Player = Peer->Object;
	Player->Character->IdleTime = 0.0;

	// Get message
	std::string Message = Data.ReadString();
	if(Message.length() == 0)
		return;

	if(Player->Muted)
		return;

	// Resize large messages
	if(Message.length() > HUD_CHAT_SIZE)
		Message.resize(HUD_CHAT_SIZE);

	// Send message to other players
	BroadcastMessage(nullptr, Player->Name + ": " + Message, "white");

	// Log
	Log << "[CHAT] " << Player->Name + ": " + Message << std::endl;

	// Check for slap words
	for(const auto &SlapRegex : SlapRegexes) {
		std::regex Regex(SlapRegex, std::regex_constants::icase);
		if(std::regex_search(Message, Regex))
			Slap(Player->NetworkID);
	}
}

// Send position to player
void _Server::SendPlayerPosition(ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	_Object *Player = Peer->Object;

	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::WORLD_POSITION);
	Packet.Write<glm::ivec2>(Player->Position);
	Network->SendPacket(Packet, Player->Peer);
}

// Send player stats to peer
void _Server::SendPlayerInfo(ae::_Peer *Peer, bool ClearRecentItems) {
	if(!ValidatePeer(Peer))
		return;

	_Object *Player = Peer->Object;

	// Build packet
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::OBJECT_STATS);
	Packet.WriteBit(ClearRecentItems);
	Player->SerializeStats(Packet);

	Network->SendPacket(Packet, Peer);
}

// Send character list
void _Server::SendCharacterList(ae::_Peer *Peer) {

	// Create packet
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::CHARACTERS_LIST);
	Packet.Write<uint8_t>(Hardcore);
	Packet.Write<uint8_t>((uint8_t)Save->GetCharacterCount(Peer->AccountID));
	Packet.Write<uint8_t>((uint8_t)Save->GetLastSlot(Peer->AccountID));

	// Get objects for characters inactive on server
	std::unordered_map<uint32_t, _Object *> CharacterObjects;
	for(auto &Object : ObjectManager->Objects) {
		if(Object->Character->AccountID == Peer->AccountID)
			CharacterObjects[Object->Character->ID] = Object;
	}

	// Generate a list of characters
	Save->Database->PrepareQuery("SELECT * FROM character WHERE account_id = @account_id");
	Save->Database->BindInt(1, Peer->AccountID);
	while(Save->Database->FetchRow()) {
		_Object Player;
		Player.Stats = Stats;
		Player.CreateComponents();
		Player.UnserializeSaveData(Save->Database->GetString("data"));
		Packet.Write<uint8_t>(Save->Database->GetInt<uint8_t>("slot"));
		Packet.Write<uint8_t>(Player.Character->Hardcore);
		Packet.WriteString(Save->Database->GetString("name"));
		Packet.Write<uint8_t>(Player.Character->PortraitID);

		// Get more recent data if it exists
		uint32_t CharacterID = Save->Database->GetInt<uint32_t>("id");
		_Object *Object = CharacterObjects[CharacterID];
		if(Object) {
			Packet.WriteBit(Object->Battle);
			Packet.Write<int64_t>(Object->Character->Attributes["Health"].Int);
			Packet.Write<int64_t>(Object->Character->Attributes["Experience"].Int);
			Packet.Write<uint32_t>(Object->Character->Attributes["Rebirths"].Int);
			Packet.Write<uint32_t>(Object->Character->Attributes["Evolves"].Int);
			Packet.Write<uint32_t>(Object->Character->Attributes["Transforms"].Int);
		}
		else {
			Packet.WriteBit(false);
			Packet.Write<int64_t>(Player.Character->Attributes["Health"].Int);
			Packet.Write<int64_t>(Player.Character->Attributes["Experience"].Int);
			Packet.Write<uint32_t>(Player.Character->Attributes["Rebirths"].Int);
			Packet.Write<uint32_t>(Player.Character->Attributes["Evolves"].Int);
			Packet.Write<uint32_t>(Player.Character->Attributes["Transforms"].Int);
		}
	}
	Save->Database->CloseQuery();

	// Send list
	Network->SendPacket(Packet, Peer);
}

// Spawns a player at a particular spawn point
void _Server::SpawnPlayer(_Object *Player, ae::NetworkIDType MapID, uint32_t EventType) {
	if(!Stats)
		return;

	if(!ValidatePeer(Player->Peer) || !Player->Character->ID)
		return;

	// Use spawn point for new characters
	if(MapID == 0) {
		MapID = Player->Character->SpawnMapID;
		if(MapID == 0)
			MapID = 1;
		EventType = _Map::EVENT_SPAWN;
	}
	// Verify map id
	else if(Stats->Maps.find(MapID) == Stats->Maps.end() || Stats->Maps.at(MapID).File == "maps/")
		return;

	// Get map
	_Map *Map = MapManager->GetObject(MapID);

	// Load map
	if(!Map) {
		Map = MapManager->CreateWithID(MapID);
		Map->Clock = Save->Clock;
		Map->Server = this;
		Map->Stats = Stats;
		Map->Load(&Stats->Maps.at(MapID));
	}

	// Get old map
	_Map *OldMap = Player->Map;

	// Place player in new map
	Player->Character->ResetUIState();
	Player->Controller->InputStates.clear();
	if(Map != OldMap) {
		if(OldMap)
			OldMap->RemoveObject(Player);

		Player->Map = Map;

		// Check for spawning from events
		if(EventType != _Map::EVENT_NONE) {

			// Find spawn point in map
			uint32_t SpawnPoint = Player->Character->SpawnPoint;
			if(EventType == _Map::EVENT_MAPENTRANCE)
				SpawnPoint = OldMap->NetworkID;

			// Default to mapchange event if entrance not found
			if(!Map->FindEvent(_Event(EventType, SpawnPoint), Player->Position))
				Map->FindEvent(_Event(_Map::EVENT_MAPCHANGE, SpawnPoint), Player->Position);
		}

		// Validate position
		Player->Position = glm::clamp(Player->Position, glm::ivec2(0, 0), Map->Size - glm::ivec2(1, 1));

		// Add player to map
		Map->AddObject(Player);

		// Send data to peer
		if(Player->Peer->ENetPeer) {

			// Send new map id
			SendMap(Player->Peer, MapID);

			// Send player object list
			Map->SendObjectList(Player->Peer);

			// Send full player data to peer
			SendPlayerInfo(Player->Peer, false);
		}
		else
			Player->Character->Path.clear();
	}
	else {
		Map->FindEvent(_Event(EventType, Player->Character->SpawnPoint), Player->Position);
		SendPlayerPosition(Player->Peer);
		SendHUD(Player->Peer);
	}

	// Update events
	Player->Map->CheckEvents(Player, 0);
	UpdateEventCount();
}

// Queue a player for rebirth
void _Server::QueueRebirth(_Object *Object, int Mode, const std::string &Name, int64_t Value) {
	_RebirthEvent RebirthEvent;
	RebirthEvent.Object = Object;
	RebirthEvent.Name = Name;
	RebirthEvent.Mode = Mode;
	RebirthEvent.Value = Value;

	RebirthEvents.push_back(RebirthEvent);
}

// Queue a battle for an object
void _Server::QueueBattle(_Object *Object, const std::string &Name, uint32_t Zone, int Boss, bool PVP, bool Manual, float BountyEarned, float BountyClaimed, double Duration) {
	if(NoPVP)
		SendMessage(Object->Peer, "PVP is disabled", "red");

	_BattleEvent BattleEvent;
	BattleEvent.Name = Name;
	BattleEvent.Object = Object;
	BattleEvent.Zone = Zone;
	BattleEvent.Boss = Boss;
	BattleEvent.Manual = Manual;
	BattleEvent.PVP = PVP;
	BattleEvent.BountyEarned = BountyEarned;
	BattleEvent.BountyClaimed = BountyClaimed;
	BattleEvent.Side = BATTLE_PVP_VICTIM_SIDE;
	BattleEvent.Duration = Duration;
	if(BountyEarned > 0)
		BattleEvent.Side = BATTLE_PVP_ATTACKER_SIDE;

	BattleEvents.push_back(BattleEvent);
}

// Start teleporting a player
void _Server::StartTeleport(_Object *Object, double Time) {
	if(Object->Battle || !Object->Character->IsAlive())
		return;

	Object->Character->ResetUIState();
	Object->Character->TeleportTime = Time;
	SendPlayerPosition(Object->Peer);

	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::WORLD_TELEPORTSTART);
	Packet.Write<double>(Time);
	Network->SendPacket(Packet, Object->Peer);
}

// Create player object and load stats from save
_Object *_Server::CreatePlayer(ae::_Peer *Peer, uint32_t CharacterID) {

	// Create object
	_Object *Player = ObjectManager->Create();
	Player->Stats = Stats;
	Player->CreateComponents();
	Player->Scripting = Scripting;
	Player->Server = this;
	Player->Character->ID = CharacterID;
	Player->Character->AccountID = Peer->AccountID;
	Player->Peer = Peer;
	Player->Character->InitAttributes();
	Peer->Object = Player;

	Save->LoadPlayer(Player);
	Player->SetLogging(Player->Logging);

	return Player;
}

// Create server side bot
_Object *_Server::CreateBot() {

	// Check for account being used
	ae::_Peer TestPeer(nullptr);
	TestPeer.AccountID = ACCOUNT_BOTS_ID;
	if(CheckAccountUse(&TestPeer))
		return nullptr;

	// Choose slot
	uint32_t Slot = 0;

	// Check for valid character id
	bool Muted = false;
	uint32_t CharacterID = Save->GetCharacterID(ACCOUNT_BOTS_ID, Slot, Muted);
	if(!CharacterID) {
		std::string Name = "bot_test";
		CharacterID = Save->CreateCharacter(Stats, Scripting, ACCOUNT_BOTS_ID, Slot, Hardcore, Name, 1, 1);
	}

	// Create fake peer
	ae::_Peer *Peer = new ae::_Peer(nullptr);
	Peer->AccountID = ACCOUNT_BOTS_ID;

	// Create object
	_Object *Bot = ObjectManager->Create();
	Bot->Peer = Peer;
	Bot->Peer->Object = Bot;
	Bot->Stats = Stats;
	Bot->CreateComponents();
	Bot->Character->Bot = true;
	Bot->Character->AccountID = Peer->AccountID;
	Bot->Character->ID = CharacterID;
	Bot->Scripting = Scripting;
	Bot->Server = this;
	Bot->Character->ID = CharacterID;
	Bot->Character->InitAttributes();
	Save->LoadPlayer(Bot);
	Bot->Character->PartyName = "bot";

	// Simulate packet
	ae::_Buffer Packet;
	Packet.WriteBit(false);
	Packet.Write<uint8_t>(0);
	Packet.StartRead();
	HandleCharacterPlay(Packet, Bot->Peer);

	return Bot;
}

// Validate a peer's attributes
bool _Server::ValidatePeer(ae::_Peer *Peer) {
	if(!Peer)
		return false;

	if(!Peer->AccountID)
		return false;

	if(!Peer->Object)
		return false;

	return true;
}

// Check to see if an account is in use
bool _Server::CheckAccountUse(ae::_Peer *Peer) {
	for(auto &CheckPeer : Network->GetPeers()) {
		if(CheckPeer != Peer && CheckPeer->AccountID == Peer->AccountID)
			return true;
	}

	return false;
}

// Handles a player's inventory move
void _Server::HandleInventoryMove(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Reset idle time
	_Object *Player = Peer->Object;
	Player->Character->IdleTime = 0.0;

	// Get slots
	_Slot OldSlot;
	_Slot NewSlot;
	OldSlot.Unserialize(Data);
	NewSlot.Unserialize(Data);

	// Move items
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::INVENTORY_SWAP);
	if(Player->Inventory->MoveInventory(Packet, NewSlot, OldSlot)) {
		Network->SendPacket(Packet, Peer);
		Player->Character->CalculateStats();
	}
	else
		SendClearWait(Player);

	// Check for trading players
	if(OldSlot.Type == BagType::TRADE || NewSlot.Type == BagType::TRADE)
		SendTradePlayerInventory(Player);
}

// Handle privilege item move
void _Server::HandleInventoryPrivilege(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Get player
	_Object *Player = Peer->Object;
	_Character *Character = Player->Character;
	if(!Character->CanTrade() || !Character->CanOpenTrade())
		return;

	// Reset idle time
	Player->Character->IdleTime = 0.0;

	// Search for items last used
	std::vector<_Slot> SlotsUpdated;
	Player->MovePrivilegeItems(SlotsUpdated);

	// Update stats
	Player->Character->CalculateStats();

	// Send inventory update
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::INVENTORY_UPDATE);
	Packet.Write<uint8_t>(SlotsUpdated.size());
	for(const auto &Slot : SlotsUpdated)
		Player->Inventory->SerializeSlot(Packet, Slot);

	Network->SendPacket(Packet, Peer);
}

// Handle transfer between inventory bags
void _Server::HandleInventoryTransfer(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Reset idle time
	_Object *Player = Peer->Object;
	Player->Character->IdleTime = 0.0;

	// Read data
	_Slot SourceSlot;
	BagType TargetBagType;
	SourceSlot.Unserialize(Data);
	TargetBagType = (BagType)Data.Read<uint8_t>();

	// Transfer items
	std::vector<_Slot> SlotsUpdated;
	if(Player->Inventory->Transfer(SourceSlot, TargetBagType, SlotsUpdated)) {

		// Send inventory update
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::INVENTORY_UPDATE);
		Packet.Write<uint8_t>(SlotsUpdated.size());
		for(const auto &Slot : SlotsUpdated)
			Player->Inventory->SerializeSlot(Packet, Slot);

		Network->SendPacket(Packet, Peer);

		// Check for trading players
		if(SourceSlot.Type == BagType::TRADE || TargetBagType == BagType::TRADE)
			SendTradePlayerInventory(Player);

		// Update stats
		Player->Character->CalculateStats();
	}
}

// Handle a player's inventory use request
void _Server::HandleInventoryUse(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	_Object *Player = Peer->Object;

	// Get alternate flag
	bool Alternate = Data.ReadBit();

	// Get item slot index
	_Slot Slot;
	Slot.Unserialize(Data);
	if(!Player->Inventory->IsValidSlot(Slot))
		return;

	// Get item
	_InventorySlot &InventorySlot = Player->Inventory->GetSlot(Slot);
	if(!InventorySlot.Item)
		return;

	// Check for equipment
	if(InventorySlot.Item->IsEquippable()) {

		// Unequip
		_Slot TargetSlot;
		if(Slot.Type == BagType::EQUIPMENT) {
			TargetSlot = Player->Inventory->FindSlotForItemInBag(BagType::INVENTORY, InventorySlot.Item, InventorySlot.Upgrades, 1);
		}
		// Equip item
		else {
			InventorySlot.Item->GetEquipmentSlot(TargetSlot);

			// Check for empty second ring slot
			if(Alternate || (TargetSlot.Index == EquipmentType::RING1 && Player->Inventory->GetSlot(TargetSlot).Item && !Player->Inventory->GetBag(BagType::EQUIPMENT).Slots[EquipmentType::RING2].Item))
				TargetSlot.Index = EquipmentType::RING2;

			// Check for empty main hand when equipping off-hand
			if(InventorySlot.Item->Type == ItemType::OFFHAND && (Alternate || !Player->Inventory->GetBag(BagType::EQUIPMENT).Slots[EquipmentType::HAND1].Item))
				TargetSlot.Index = EquipmentType::HAND1;
		}

		// Attempt to move
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::INVENTORY_SWAP);
		if(Player->Inventory->MoveInventory(Packet, TargetSlot, Slot)) {
			Network->SendPacket(Packet, Peer);
			Player->Character->CalculateStats();
		}
		else {
			SendClearWait(Player);
		}

	}
	// Handle consumables
	else {

		// Check for existing action
		auto &Action = Player->Character->Action;
		if(!Action.Item) {
			Action.Item = InventorySlot.Item;
			Action.Level = InventorySlot.Item->Level;
			Action.Duration = InventorySlot.Item->Duration;
			Action.InventorySlot = (int)Slot.Index;
			if(InventorySlot.Item->IsSkill() && !Player->Character->HasLearned(InventorySlot.Item))
				Action.LearningSkill = true;
			else if(InventorySlot.Item->TargetAlive)
				Action.Target = Player;
			else if(Player->Map)
				Action.Target = Player->Map->FindDeadPlayer(Player, 1.0f);
		}
	}
}

// Handle a player's inventory split stack request
void _Server::HandleInventorySplit(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	_Object *Player = Peer->Object;

	_Slot Slot;
	Slot.Unserialize(Data);
	uint8_t Count = Data.Read<uint8_t>();

	// Split items
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::INVENTORY_UPDATE);
	if(Player->Inventory->SplitStack(Packet, Slot, Count))
		Network->SendPacket(Packet, Peer);

	// Check for trading players
	if(Slot.Type == BagType::TRADE)
		SendTradePlayerInventory(Player);
}

// Handle deleting an item
void _Server::HandleInventoryDelete(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Get player object
	_Object *Player = Peer->Object;

	// Get item slot
	_Slot Slot;
	Slot.Unserialize(Data);
	if(!Player->Inventory->IsValidSlot(Slot))
		return;

	// Delete item
	Player->Inventory->GetBag(Slot.Type).Slots[Slot.Index].Reset();

	// Update client
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::INVENTORY_UPDATE);
	Packet.Write<uint8_t>(1);
	Player->Inventory->SerializeSlot(Packet, Slot);
	Network->SendPacket(Packet, Peer);

	// Check for trading players
	if(Slot.Type == BagType::TRADE)
		SendTradePlayerInventory(Player);
}

// Handles a vendor exchange message
void _Server::HandleVendorExchange(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	_Object *Player = Peer->Object;

	// Release client
	SendClearWait(Player);

	// Get vendor
	const _Vendor *Vendor = Player->Character->Vendor;
	if(!Vendor)
		return;

	// Reset idle time
	Player->Character->IdleTime = 0.0;

	// Get info
	bool Buy = Data.ReadBit();
	int64_t Amount = (int64_t)Data.Read<uint16_t>();
	_Slot Slot;
	Slot.Unserialize(Data);

	// Buy item
	if(Buy) {
		if(Slot.Index >= Vendor->Items.size())
			return;

		// Get optional inventory slot
		_Slot TargetSlot;
		TargetSlot.Unserialize(Data);

		// Get item info
		const _Item *Item = Vendor->Items[Slot.Index];
		if(!Item->BulkBuy)
			Amount = 1;

		// Get cost of one item
		int64_t SinglePrice = Item->GetPrice(Scripting, Player, Vendor, 1, Buy);

		// Get purchaseable amount
		Amount = std::min(Amount, Player->Character->Attributes["Gold"].Int / SinglePrice);

		// Not enough gold
		if(Amount <= 0)
			return;

		// Get total price of purchase
		int64_t TotalPrice = Amount * SinglePrice;

		// Check if player can buy the item
		if(!Item->CanBuy(Scripting, Player))
			return;

		// Find open slot for new item
		if(!Player->Inventory->IsValidSlot(TargetSlot))
			TargetSlot = Player->Inventory->FindSlotForItem(Item, 0, (int)Amount, false);

		// No room
		if(!Player->Inventory->IsValidSlot(TargetSlot)) {
			SendInventoryFullMessage(Player->Peer, nullptr, 0);
			return;
		}

		// Attempt to add item
		if(!Player->Inventory->AddItem(Item, 0, (int)Amount, false, TargetSlot))
			return;

		// Update gold
		Player->Character->UpdateGold(-TotalPrice);
		{
			ae::_Buffer Packet;
			Packet.Write<PacketType>(PacketType::INVENTORY_GOLD);
			Packet.Write<int64_t>(Player->Character->Attributes["Gold"].Int);
			Network->SendPacket(Packet, Peer);
		}

		// Immediately use unlocks
		if(Item->UnlockOnBuy()) {
			Player->Character->Action.Item = Item;
			Player->Character->Action.Target = Player;
			Player->Character->Action.Level = Item->Level;
			Player->Character->Action.Duration = Item->Duration;
			Player->Character->Action.InventorySlot = (int)TargetSlot.Index;
			ae::_Buffer Packet;
			if(Player->Character->Action.Resolve(Packet, Player, ScopeType::WORLD))
				Network->SendPacket(Packet, Peer);
		}
		else {

			// Update items
			ae::_Buffer Packet;
			Packet.Write<PacketType>(PacketType::INVENTORY_UPDATE);
			Packet.Write<uint8_t>(1);
			Player->Inventory->SerializeSlot(Packet, TargetSlot);
			Network->SendPacket(Packet, Peer);
		}

		Player->Character->CalculateStats();

		// Log
		Log << "[PURCHASE] Player "
			<< Player->Name << " buys " << (int)Amount << "x " << Item->Name
			<< " ( character_id=" << Player->Character->ID
			<< " item_id=" << Item->ID
			<< " gold=" << Player->Character->Attributes["Gold"].Int
			<< " )" << std::endl;
	}
	// Sell item
	else {
		if(!Player->Inventory->IsValidSlot(Slot))
			return;

		// Get item info
		const _InventorySlot &InventorySlot = Player->Inventory->GetSlot(Slot);
		if(InventorySlot.Item) {

			// Get price of stack
			Amount = std::min((int)Amount, InventorySlot.Count);
			int64_t Price = InventorySlot.Item->GetPrice(Scripting, Player, Vendor, (int)Amount, Buy, InventorySlot.Upgrades);

			// Update gold
			Player->Character->UpdateGold(Price);
			if(Peer) {
				ae::_Buffer Packet;
				Packet.Write<PacketType>(PacketType::INVENTORY_GOLD);
				Packet.Write<int64_t>(Player->Character->Attributes["Gold"].Int);
				Network->SendPacket(Packet, Peer);
			}

			// Log
			Log << "[SELL] Player " << Player->Name
				<< " sells " << Amount << "x " << InventorySlot.Item->Name
				<< " ( character_id=" << Player->Character->ID
				<< " item_id=" << InventorySlot.Item->ID
				<< " gold=" << Player->Character->Attributes["Gold"].Int
				<< " )" << std::endl;

			// Update items
			Player->Inventory->UpdateItemCount(Slot, (int)(-Amount));
			if(Peer) {
				ae::_Buffer Packet;
				Packet.Write<PacketType>(PacketType::INVENTORY_UPDATE);
				Packet.Write<uint8_t>(1);
				Player->Inventory->SerializeSlot(Packet, Slot);
				Network->SendPacket(Packet, Peer);
			}

			// Update stats
			if(Slot.Type == BagType::EQUIPMENT)
				Player->Character->CalculateStats();
		}
	}
}

// Handles a trader accept
void _Server::HandleTraderAccept(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	_Object *Player = Peer->Object;
	_Inventory *Inventory = Player->Inventory;
	_Character *Character = Player->Character;
	const _Trader *Trader = Character->Trader;
	if(!Trader)
		return;

	// Reset idle time
	Character->IdleTime = 0.0;

	// Get trader information
	std::vector<_Slot> RequiredItemSlots(Trader->Items.size());
	_Slot RewardSlot;
	if(!Inventory->GetRequiredItemSlots(Trader, RequiredItemSlots, RewardSlot))
		return;

	// Give buff
	std::string RewardName;
	uint32_t RewardID;
	int RewardCount;
	if(Trader->RewardItem == nullptr) {
		_StatChange StatChange;
		StatChange.Object = Player;
		StatChange.Values["Buff"].Pointer = (void *)Stats->BuffMap.at("Good Fortune");
		StatChange.Values["BuffLevel"].Int = 25;
		StatChange.Values["BuffDuration"].Double = 60.0;
		Player->UpdateStats(StatChange);

		// Build packet
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::STAT_CHANGE);
		StatChange.Serialize(Packet);
		Network->SendPacket(Packet, Player->Peer);

		RewardName = "Beggar buff";
		RewardID = 0;
		RewardCount = 0;
	}
	else {
		RewardName = Trader->RewardItem->Name;
		RewardID = Trader->RewardItem->ID;
		RewardCount = Trader->Count;
	}

	// Update inventory slots for required items
	{

		// Trade in required items
		for(uint32_t i = 0; i < Trader->Items.size(); i++)
			Inventory->UpdateItemCount(RequiredItemSlots[i], -Trader->Items[i].Count);

		// Update inventory
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::INVENTORY_UPDATE);
		Packet.Write<uint8_t>(RequiredItemSlots.size());
		for(const auto &Slot : RequiredItemSlots)
			Inventory->SerializeSlot(Packet, Slot);
		Network->SendPacket(Packet, Peer);
	}

	// Send item
	{

		// Add reward
		Inventory->AddItem(Trader->RewardItem, Trader->Upgrades, Trader->Count, false);
		Character->CalculateStats();

		// Notify client
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::INVENTORY_ADD);
		Packet.WriteBit(1);
		Packet.Write<uint16_t>((uint16_t)RewardCount);
		Packet.Write<uint32_t>(RewardID);
		Network->SendPacket(Packet, Peer);
	}

	// Log
	Log << "[TRADER] Player " << Player->Name
		<< " trades for " << RewardCount << "x " << RewardName
		<< " ( character_id=" << Character->ID
		<< " item_id=" << RewardID
		<< " )" << std::endl;
}

// Handles a skill adjust
void _Server::HandleSkillAdjust(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Reset idle time
	_Object *Player = Peer->Object;
	Player->Character->IdleTime = 0.0;

	// Process packet
	uint32_t SkillID = Data.Read<uint32_t>();
	int Amount = Data.Read<int>();
	bool SoftMax = Data.ReadBit();
	bool SoftMin = Data.ReadBit();

	// Check respec condition
	if(Amount < 0 && !Player->CanRespec())
		return;

	// Update values
	Player->Character->AdjustSkillLevel(SkillID, Amount, SoftMax, SoftMin);
	Player->Character->CalculateStats();
}

// Handle purchase from enchanter
void _Server::HandleEnchanterBuy(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Check player
	_Object *Player = Peer->Object;
	if(!Player->Character->Enchanter)
		return;

	// Reset idle time
	Player->Character->IdleTime = 0.0;

	// Read packet
	uint32_t SkillID = Data.Read<uint32_t>();
	uint8_t Amount = Data.Read<uint8_t>();

	// Get skill
	if(Stats->Items.find(SkillID) == Stats->Items.end())
	   return;

	const _Item *Skill = Stats->Items.at(SkillID);

	// Check for skill unlocked
	if(Player->Character->Skills.find(SkillID) == Player->Character->Skills.end())
		return;

	// Determine total cost and upgrades available
	int64_t TotalCost = 0;
	int MaxSkillLevel = Player->Character->MaxSkillLevels[SkillID];
	for(int i = 0; i < Amount; i++) {

		// Check max skill level
		if(MaxSkillLevel >= Skill->MaxLevel)
			break;

		// Check enchanter level
		if(MaxSkillLevel >= Player->Character->Enchanter->Level)
			break;

		// Get upgrade price
		int64_t Price = _Item::GetEnchantCost(Player, MaxSkillLevel);

		// Check player gold
		if(TotalCost + Price > Player->Character->Attributes["Gold"].Int)
			break;

		// Increment level
		MaxSkillLevel++;
		TotalCost += Price;
	}

	// No gold or max levels available
	if(MaxSkillLevel == Player->Character->MaxSkillLevels[SkillID])
	   return;

	// Update skill
	{
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::SKILLS_MAXLEVELADJUST);
		Packet.Write<uint32_t>(SkillID);
		Packet.Write<int>(MaxSkillLevel);
		Network->SendPacket(Packet, Peer);
	}

	// Update gold
	{
		_StatChange StatChange;
		StatChange.Object = Player;
		StatChange.Values["Gold"].Int = -TotalCost;
		Player->UpdateStats(StatChange);

		// Build packet
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::STAT_CHANGE);
		StatChange.Serialize(Packet);
		Network->SendPacket(Packet, Player->Peer);
	}

	// Update values
	Player->Character->MaxSkillLevels[SkillID] = MaxSkillLevel;
	Player->Character->CalculateStats();
}

// Handle disenchanter sell
void _Server::HandleDisenchanterSell(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Check player
	_Object *Player = Peer->Object;
	if(!Player->Character->Disenchanter)
		return;

	// Reset idle time
	Player->Character->IdleTime = 0.0;

	// Read packet
	uint32_t SkillID = Data.Read<uint32_t>();
	uint8_t Amount = Data.Read<uint8_t>();

	// Get skill
	if(Stats->Items.find(SkillID) == Stats->Items.end())
	   return;

	// Check for skill unlocked
	if(Player->Character->Skills.find(SkillID) == Player->Character->Skills.end())
		return;

	// Determine total cost and levels available
	int64_t TotalCost = 0;
	int MaxSkillLevel = Player->Character->MaxSkillLevels[SkillID];
	int MinSkillLevel = Player->Character->MinSkillLevels[SkillID];
	for(int i = 0; i < Amount; i++) {

		// Check max skill level
		MaxSkillLevel--;
		if(MaxSkillLevel < MinSkillLevel) {
			MaxSkillLevel = MinSkillLevel;
			break;
		}

		// Get sell price
		int64_t Price = _Item::GetEnchantCost(Player, MaxSkillLevel) * Player->Character->Disenchanter->Percent;

		// Increase total
		TotalCost += Price;
	}

	// No max levels available
	if(TotalCost == 0)
	   return;

	// Update skill
	{
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::SKILLS_MAXLEVELADJUST);
		Packet.Write<uint32_t>(SkillID);
		Packet.Write<int>(MaxSkillLevel);
		Network->SendPacket(Packet, Peer);
	}

	// Update gold
	{
		_StatChange StatChange;
		StatChange.Object = Player;
		StatChange.Values["Gold"].Int = TotalCost;
		Player->UpdateStats(StatChange);

		// Build packet
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::STAT_CHANGE);
		StatChange.Serialize(Packet);
		Network->SendPacket(Packet, Player->Peer);
	}

	// Update values
	Player->Character->MaxSkillLevels[SkillID] = MaxSkillLevel;
	Player->Character->ValidateSkillLevels();
	Player->Character->CalculateStats();
}

// Handle a trade request
void _Server::HandleTradeRequest(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Reset idle time
	_Object *Player = Peer->Object;
	Player->Character->IdleTime = 0.0;

	// Get map object
	_Map *Map = Player->Map;
	if(!Map)
		return;

	// Check for level requirement
	if(!Player->Character->CanTrade())
		return;

	// Set status
	Player->Character->WaitingForTrade = true;

	// Find the nearest player to trade with
	_Object *TradePlayer = Map->FindTradePlayer(Player, 2.0f * 2.0f);
	if(TradePlayer == nullptr) {

		// Set up trade post
		Player->Character->TradeGold = 0;
		Player->Character->TradeAccepted = false;
		Player->Character->TradePlayer = nullptr;
	}
	else {

		// Set up trade screen for both players
		SendTradeInformation(Player, TradePlayer);
		SendTradeInformation(TradePlayer, Player);

		Player->Character->TradePlayer = TradePlayer;
		Player->Character->TradeAccepted = false;
		TradePlayer->Character->TradePlayer = Player;
		TradePlayer->Character->TradeAccepted = false;
		TradePlayer->Character->WaitingForTrade = true;
	}
}

// Handles a trade cancel
void _Server::HandleTradeCancel(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	_Object *Player = Peer->Object;

	// Notify trading player
	_Object *TradePlayer = Player->Character->TradePlayer;
	if(TradePlayer) {
		TradePlayer->Character->TradePlayer = nullptr;
		TradePlayer->Character->TradeAccepted = false;
		Player->Character->IdleTime = 0.0;

		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::TRADE_CANCEL);
		Network->SendPacket(Packet, TradePlayer->Peer);
	}

	// Set state back to normal
	Player->Character->TradePlayer = nullptr;
	Player->Character->WaitingForTrade = false;
}

// Handle a trade gold update
void _Server::HandleTradeGold(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Reset idle time
	_Object *Player = Peer->Object;
	Player->Character->IdleTime = 0.0;

	// Set gold amount
	int64_t Gold = Data.Read<int64_t>();
	if(Gold < 0)
		Gold = 0;
	else if(Gold > Player->Character->Attributes["Gold"].Int)
		Gold = std::max((int64_t)0, Player->Character->Attributes["Gold"].Int);
	Player->Character->TradeGold = Gold;
	Player->Character->TradeAccepted = false;

	// Notify player
	_Object *TradePlayer = Player->Character->TradePlayer;
	if(TradePlayer) {
		TradePlayer->Character->TradeAccepted = false;

		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::TRADE_GOLD);
		Packet.Write<int64_t>(Gold);
		Network->SendPacket(Packet, TradePlayer->Peer);
	}
}

// Handles a trade accept from a player
void _Server::HandleTradeAccept(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Reset idle time
	_Object *Player = Peer->Object;
	Player->Character->IdleTime = 0.0;

	// Get trading player
	_Object *TradePlayer = Player->Character->TradePlayer;
	if(!TradePlayer)
		return;

	// Set the player's state
	bool Accepted = !!Data.Read<char>();
	Player->Character->TradeAccepted = Accepted;

	// Check if both players agree
	if(Accepted && TradePlayer->Character->TradeAccepted) {

		// Exchange items
		_InventorySlot TempItems[INVENTORY_MAX_TRADE_ITEMS];
		_Slot Slot;
		Slot.Type = BagType::TRADE;
		std::ostringstream PlayerItemList;
		std::ostringstream TradePlayerItemList;
		for(size_t i = 0; i < INVENTORY_MAX_TRADE_ITEMS; i++) {
			Slot.Index = i;

			// Build item list
			_InventorySlot &PlayerSlot = Player->Inventory->GetSlot(Slot);
			_InventorySlot &TradePlayerSlot = TradePlayer->Inventory->GetSlot(Slot);
			if(PlayerSlot.Item) {
				if(PlayerItemList.str().size())
					PlayerItemList << ",";
				PlayerItemList << PlayerSlot.Count << "x " << PlayerSlot.Item->Name;
			}
			if(TradePlayerSlot.Item) {
				if(TradePlayerItemList.str().size())
					TradePlayerItemList << ",";
				TradePlayerItemList << TradePlayerSlot.Count << "x " << TradePlayerSlot.Item->Name;
			}

			// Swap
			TempItems[i] = PlayerSlot;
			Player->Inventory->GetSlot(Slot) = TradePlayerSlot;
			TradePlayer->Inventory->GetSlot(Slot) = TempItems[i];
		}

		// Exchange gold
		int64_t PlayerGold = TradePlayer->Character->TradeGold - Player->Character->TradeGold;
		int64_t TradePlayerGold = Player->Character->TradeGold - TradePlayer->Character->TradeGold;
		Player->Character->UpdateGold(PlayerGold);
		TradePlayer->Character->UpdateGold(TradePlayerGold);

		// Move items to inventory and reset
		Player->Character->WaitingForTrade = false;
		Player->Character->TradePlayer = nullptr;
		Player->Character->TradeGold = 0;
		Player->Inventory->MoveTradeToInventory();
		TradePlayer->Character->WaitingForTrade = false;
		TradePlayer->Character->TradePlayer = nullptr;
		TradePlayer->Character->TradeGold = 0;
		TradePlayer->Inventory->MoveTradeToInventory();

		// Update stats
		Player->Character->Attributes["Trades"].Int++;
		TradePlayer->Character->Attributes["Trades"].Int++;

		// Send packet to players
		{
			ae::_Buffer Packet;
			Packet.Write<PacketType>(PacketType::TRADE_EXCHANGE);
			Packet.Write<int64_t>(Player->Character->Attributes["Gold"].Int);
			Player->Inventory->Serialize(Packet);
			Network->SendPacket(Packet, Player->Peer);
		}
		{
			ae::_Buffer Packet;
			Packet.Write<PacketType>(PacketType::TRADE_EXCHANGE);
			Packet.Write<int64_t>(TradePlayer->Character->Attributes["Gold"].Int);
			TradePlayer->Inventory->Serialize(Packet);
			Network->SendPacket(Packet, TradePlayer->Peer);
		}

		// Log
		Log << "[TRADE] " << Player->Name << " trades with " << TradePlayer->Name
			<< " ("
			<< " character_id=" << Player->Character->ID
			<< " character_id=" << TradePlayer->Character->ID
			<< " gold=" << PlayerGold
			<< " gold=" << TradePlayerGold
			<< " items=" << PlayerItemList.str()
			<< " items=" << TradePlayerItemList.str()
			<< " )" << std::endl;
	}
	else {

		// Notify trading player
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::TRADE_ACCEPT);
		Packet.Write<char>(Accepted);
		Network->SendPacket(Packet, TradePlayer->Peer);
	}
}

// Handle party name from client
void _Server::HandlePartyName(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Validate player
	_Object *Player = Peer->Object;
	if(!Player->Character->CanOpenParty())
		return;

	// Reset idle time
	Player->Character->IdleTime = 0.0;

	// Get new party name
	std::string NewPartyName = Data.ReadString();

	// Change party name
	if(Player->Character->PartyName != NewPartyName) {
		RemoveParty(Player->Character->PartyName, Player->Character->ID, Player);
		Player->Character->PartyName = NewPartyName;
		AddParty(Player->Character->PartyName, Player->Character->ID, Player);

		// Broadcast party to all objects in map
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::PARTY_NAME);
		Packet.Write<ae::NetworkIDType>(Player->NetworkID);
		Packet.WriteString(Player->Character->PartyName.c_str());
		if(Player->Map)
			Player->Map->BroadcastPacket(Packet);
	}
}

// Handles player status change
void _Server::HandlePlayerStatus(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	_Object *Player = Peer->Object;

	// Check battle
	if(Player->Battle)
		return;

	// Read packet
	uint8_t Status = Data.Read<uint8_t>();
	switch(Status) {
		case _Character::STATUS_NONE:
			Player->Character->ResetUIState();
		break;
		case _Character::STATUS_MENU:
			Player->Character->MenuOpen = true;
			Player->Character->IdleTime = 0.0;
		break;
		case _Character::STATUS_INVENTORY:
			Player->Character->InventoryOpen = true;
			Player->Character->IdleTime = 0.0;
		break;
		case _Character::STATUS_SKILLS:
			Player->Character->SkillsOpen = true;
			Player->Character->IdleTime = 0.0;
		break;
		default:
		break;
	}
}

// Handle a player dismissing a buff
void _Server::HandleClearBuff(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Get data
	uint32_t BuffID = Data.Read<uint32_t>();
	_Object *Player = Peer->Object;
	if(Player->Battle)
		return;

	// Reset idle time
	Player->Character->IdleTime = 0.0;

	// Update buff
	bool Deleted = false;
	bool Updated = false;
	int64_t Level = 0;
	for(auto &StatusEffect : Player->Character->StatusEffects) {
		if(!StatusEffect->Buff->Dismiss)
			continue;

		if(StatusEffect->Buff->ID != BuffID)
			continue;

		// Handle dismissing summons
		if(StatusEffect->Buff->Dismiss == 2) {
			StatusEffect->Level--;
			if(StatusEffect->Level <= 0) {
				StatusEffect->Deleted = true;
				Deleted = true;
			}
			else
				Updated = true;

			Level = StatusEffect->Level;
		}
		// Handle other types
		else {
			StatusEffect->Deleted = true;
			Deleted = true;
		}
	}

	// Notify clients
	if(Updated)
		UpdateBuff(Player, BuffID, Level);
	if(Deleted)
		ClearBuff(Player, BuffID);
}

// Swap a buff type
void _Server::HandleSwapBuff(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Get data
	uint32_t BuffID = Data.Read<uint32_t>();
	_Object *Player = Peer->Object;
	if(Player->Battle)
		return;

	// Reset idle time
	Player->Character->IdleTime = 0.0;

	// Find source and target effects
	_StatusEffect *SourceEffect = nullptr;
	_StatusEffect *TargetEffect = nullptr;
	for(auto &StatusEffect : Player->Character->StatusEffects) {
		if(!StatusEffect->Infinite)
			continue;

		if(StatusEffect->Buff->ID == BuffID)
			SourceEffect = StatusEffect;

		if(StatusEffect->Buff->SwapBuffID == BuffID)
			TargetEffect = StatusEffect;
	}

	// No swappable effect found
	if(!SourceEffect)
		return;

	// Reduce level of source effect
	SourceEffect->Level--;
	if(SourceEffect->Level <= 0) {
		SourceEffect->Deleted = true;
		ClearBuff(Player, BuffID);
	}
	else
		UpdateBuff(Player, BuffID, SourceEffect->Level);

	// Increase level of target effect
	if(TargetEffect) {
		TargetEffect->Level++;
		UpdateBuff(Player, TargetEffect->Buff->ID, TargetEffect->Level);
	}
	else {

		// Create new effect
		_StatusEffect *StatusEffect = new _StatusEffect();
		StatusEffect->Buff = Stats->Buffs.at(SourceEffect->Buff->SwapBuffID);
		StatusEffect->Level = 1;
		StatusEffect->Source = Player;
		StatusEffect->Infinite = true;
		if(!Player->Character->AddStatusEffect(StatusEffect))
			delete StatusEffect;

		Player->Character->CalculateStats();

		// Update player
		SendStatusEffects(Player);
	}
}

// Upgrade an item
void _Server::HandleBlacksmithUpgrade(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	_Object *Player = Peer->Object;
	if(!Player->Character->Blacksmith)
		return;

	// Reset idle time
	Player->Character->IdleTime = 0.0;

	// Get amount
	uint8_t Amount = Data.Read<uint8_t>();

	// Get slot
	_Slot Slot;
	Slot.Unserialize(Data);
	if(!Player->Inventory->IsValidSlot(Slot))
		return;

	// Get item
	_InventorySlot &InventorySlot = Player->Inventory->GetSlot(Slot);

	// Determine total cost and upgrades available
	int64_t TotalCost = 0;
	int Upgrades = InventorySlot.Upgrades;
	for(int i = 0; i < Amount; i++) {

		// Check item's max level
		if(Upgrades >= InventorySlot.Item->MaxLevel)
			break;

		// Check if blacksmith can upgrade
		if(!Player->Character->Blacksmith->CanUpgrade(InventorySlot.Item, InventorySlot.Count, Upgrades))
			break;

		// Get upgrade price
		int64_t UpgradePrice = InventorySlot.Item->GetUpgradeCost(Player, Upgrades+1);

		// Check player gold
		if(TotalCost + UpgradePrice > Player->Character->Attributes["Gold"].Int)
			break;

		// Upgrade item
		Upgrades++;
		TotalCost += UpgradePrice;
	}

	// No gold or upgrades available
	if(Upgrades == InventorySlot.Upgrades)
	   return;

	// Set new upgrade level
	InventorySlot.Upgrades = Upgrades;

	// Update gold
	{
		_StatChange StatChange;
		StatChange.Object = Player;
		StatChange.Values["Gold"].Int = -TotalCost;
		Player->UpdateStats(StatChange);

		// Build packet
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::STAT_CHANGE);
		StatChange.Serialize(Packet);
		Network->SendPacket(Packet, Player->Peer);
	}

	// Update items
	{
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::INVENTORY_UPDATE);
		Packet.Write<uint8_t>(1);
		Player->Inventory->SerializeSlot(Packet, Slot);
		Network->SendPacket(Packet, Peer);
	}

	// Log
	Log << "[UPGRADE] Player " << Player->Name << " upgrades "
		<< InventorySlot.Item->Name << " to level " << InventorySlot.Upgrades
		<< " ( character_id=" << Player->Character->ID
		<< " item_id=" << InventorySlot.Item->ID
		<< " gold=" << Player->Character->Attributes["Gold"].Int
		<< " )" << std::endl;

	Player->Character->CalculateStats();
}

// Handle set upgrade
void _Server::HandleBlacksmithUpgradeSet(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Reset idle time
	_Object *Player = Peer->Object;
	Player->Character->IdleTime = 0.0;

	// Get amount
	uint8_t Amount = Data.Read<uint8_t>();

	// Get blacksmith
	const _Blacksmith *Blacksmith = Player->Character->Blacksmith;
	if(!Blacksmith)
		return;

	// Upgrade items
	int64_t TotalCost = 0;
	_Bag &Bag = Player->Inventory->GetBag(BagType::EQUIPMENT);
	std::unordered_map<EquipmentType, bool> SlotsUpdated;
	for(int i = 0; i < Amount; i++) {
		bool Upgraded = false;
		for(const auto &SlotType : _Inventory::UpgradeSetSlotTypes) {

			// Get inventory slot
			_InventorySlot &InventorySlot = Bag.Slots[SlotType];
			const _Item *Item = InventorySlot.Item;
			if(!Item || !Item->SetID)
				continue;

			// Check for complete set
			_SetData &SetData = Player->Character->Sets.at(Item->SetID);
			if(!SetData.Complete)
				continue;

			// Check blacksmith
			if(!Blacksmith->CanUpgrade(InventorySlot.Item, InventorySlot.Count, InventorySlot.Upgrades))
				continue;

			// Only upgrade lowest items in set
			if(InventorySlot.Upgrades != SetData.Level || InventorySlot.Upgrades >= SetData.MaxLevel)
				continue;

			// Get cost
			int64_t UpgradeCost = InventorySlot.Item->GetUpgradeCost(Player, InventorySlot.Upgrades + 1);

			// Upgrade
			if(UpgradeCost <= Player->Character->Attributes["Gold"].Int) {
				InventorySlot.Upgrades++;
				TotalCost += UpgradeCost;
				Player->Character->UpdateGold(-UpgradeCost);
				SlotsUpdated[SlotType] = true;
				Upgraded = true;

				// Log
				Log << "[UPGRADE] Player " << Player->Name << " upgrades "
					<< InventorySlot.Item->Name << " to level " << InventorySlot.Upgrades
					<< " ( character_id=" << Player->Character->ID
					<< " item_id=" << InventorySlot.Item->ID
					<< " gold=" << Player->Character->Attributes["Gold"].Int
					<< " )" << std::endl;
			}
		}

		// Update set level
		if(Upgraded)
			Player->Character->CalculateStats();
	}

	// Nothing updated
	if(SlotsUpdated.empty())
		return;

	// Update gold
	{
		_StatChange StatChange;
		StatChange.Object = Player;
		StatChange.Values["Gold"].Int = -TotalCost;

		// Build packet
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::STAT_CHANGE);
		StatChange.Serialize(Packet);
		Network->SendPacket(Packet, Player->Peer);
	}

	// Send inventory update
	{
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::INVENTORY_UPDATE);
		Packet.Write<uint8_t>(SlotsUpdated.size());
		for(const auto &SlotType : SlotsUpdated) {
			_Slot Slot(BagType::EQUIPMENT, SlotType.first);
			Player->Inventory->SerializeSlot(Packet, Slot);
		}
		Network->SendPacket(Packet, Peer);
	}
}

// Handle paying to play a minigame
void _Server::HandleMinigamePay(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Validate
	_Object *Player = Peer->Object;
	const _MinigameType *Minigame = Player->Character->Minigame;
	if(!Minigame || Player->Inventory->CountItem(Minigame->RequiredItem) < Minigame->Cost)
		return;

	// Trade in required items
	Player->Inventory->SpendItems(Minigame->RequiredItem, Minigame->Cost);

	// Send new inventory
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::INVENTORY);
	Player->Inventory->Serialize(Packet);
	Network->SendPacket(Packet, Peer);

	// Update stats
	Player->Character->Attributes["GamesPlayed"].Int++;
	Player->Character->PaidSeed = Player->Character->Seed;
	Player->Character->Seed = ae::GetRandomInt((uint32_t)1, std::numeric_limits<uint32_t>::max());
	Player->Character->IdleTime = 0.0;
}

// Give player minigame reward
void _Server::HandleMinigameGetPrize(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Validate
	_Object *Player = Peer->Object;
	if(!Player->Character->Minigame)
		return;

	// Get input
	float DropX = Data.Read<float>();

	// Simulate game
	_Minigame Minigame(Player->Character->Minigame);
	Minigame.IsServer = true;
	Minigame.StartGame(Player->Character->PaidSeed);
	Minigame.Drop(DropX);

	// Update game
	double Time = 0;
	while(Time < 30) {
		Minigame.Update(DEFAULT_TIMESTEP);
		if(Minigame.State == _Minigame::StateType::DONE)
			break;

		Time += DEFAULT_TIMESTEP;
	}

	// Update client seed
	Player->SendSeed();

	// Check bucket
	if(Minigame.Bucket >= Minigame.Prizes.size())
		return;

	// Check item
	const _MinigameItem *MinigameItem = Minigame.Prizes[Minigame.Bucket];
	if(!MinigameItem || !MinigameItem->Item)
		return;

	// Give reward
	const _Item *Item = MinigameItem->Item;
	if(Item->UnlockOnBuy()) {

		// Attempt to add item
		int Amount = std::min(MinigameItem->Count, INVENTORY_MAX_STACK);
		_Slot TargetSlot = Player->Inventory->FindSlotForItem(Item, 0, Amount, false);
		int Added = Player->Inventory->AddItem(Item, 0, Amount, false, TargetSlot);
		SendInventoryFullMessage(Peer, Item, Added);
		if(!Added)
			return;

		// Use item
		Player->Character->Action.Item = Item;
		Player->Character->Action.Target = Player;
		Player->Character->Action.Level = Item->Level;
		Player->Character->Action.Duration = Item->Duration;
		Player->Character->Action.InventorySlot = (int)TargetSlot.Index;
		ae::_Buffer Packet;
		if(Player->Character->Action.Resolve(Packet, Player, ScopeType::WORLD))
			Network->SendPacket(Packet, Peer);

		Player->Character->CalculateStats();
	}
	else
		SendItem(Peer, Stats->Items.at(Item->ID), MinigameItem->Count);
}

// Handle join battle request by player
void _Server::HandleJoin(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	_Object *Player = Peer->Object;
	if(!Player->Character->AcceptingMoveInput())
		return;

	// Reset idle time
	Player->Character->IdleTime = 0.0;

	// Find a nearby battle instance
	bool HitPrivateParty = false;
	bool HitFullBattle = false;
	bool HitLevelRestriction = false;
	bool HitBossBattle = false;
	_Battle *Battle = Player->Map->GetCloseBattle(Player, HitPrivateParty, HitFullBattle, HitLevelRestriction, HitBossBattle);
	if(!Battle) {
		if(HitBossBattle)
			SendMessage(Peer, "Can't join boss battles", "red");
		else if(HitPrivateParty)
			SendMessage(Peer, "Can't join private parties", "red");
		else if(HitFullBattle)
			SendMessage(Peer, "Can't join full battles", "red");
		else if(HitLevelRestriction)
			SendMessage(Peer, "Can't join level restricted battles", "red");

		return;
	}

	// Add player to battle
	Player->Fighter->Corpse = 1;
	Battle->AddObject(Player, 0, true);
	AddBattleSummons(Battle, 0, Player, true);
	Battle->BroadcastStatusEffects(Player);

	// Send battle to new player
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::BATTLE_START);
	Battle->Serialize(Packet);
	Network->SendPacket(Packet, Peer);
}

// Handle client exit command
void _Server::HandleExit(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!Peer)
		return;

	// Get object
	_Object *Player = Peer->Object;
	if(!Player)
		return;

	// Clear state
	Player->Character->ResetUIState();

	// Update parties
	RemoveParty(Player->Character->PartyName, Player->Character->ID, Player);

	// Broadcast message
	if(Player->Map) {
		BroadcastMessage(Peer, Player->Name + " has left the server", "gray");
		Player->Character->LoadMapID = Player->GetMapID();
	}

	// Leave trading screen
	_Object *TradePlayer = Player->Character->TradePlayer;
	if(TradePlayer) {
		TradePlayer->Character->TradePlayer = nullptr;

		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::TRADE_CANCEL);
		Network->SendPacket(Packet, TradePlayer->Peer);
	}

	// Save player
	try {
		Save->StartTransaction();
		Save->SavePlayer(Player, Player->Character->LoadMapID, &Log);
		Save->EndTransaction();
	}
	catch(std::exception &Error) {
		Log << "[ERROR] Database exception: " << Error.what() << std::endl;
	}

	// Remove peer
	Player->Character->WaitingForTrade = false;
	Player->Offline = true;
	Player->Peer = nullptr;
	Peer->Object = nullptr;

	// Update events
	UpdateEventCount();

	// Update active player count
	ActiveCount--;

	// Log message
	Log << "[LEAVE] Player " << Player->Name << " has left the server"
		<< " ( character_id=" << Player->Character->ID
		<< " battle=" << (bool)Player->Battle
		<< " )" << std::endl;
}

// Handle console commands
void _Server::HandleCommand(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Get player
	_Object *Player = Peer->Object;
	if(!Player->Map)
		return;

	// Reset idle time
	Player->Character->IdleTime = 0.0;

	// Handle player commands
	std::string Command = Data.ReadString();
	if(Command == "players") {
		std::stringstream Buffer;
		int Count = 0;
		for(auto &Object : ObjectManager->Objects) {
			if(!Object->Peer)
				continue;

			if(Count > 0)
				Buffer << ", ";
			Buffer << Object->Name;
			Count++;
		}

		std::string Plural = Count == 1 ? "" : "s";
		std::string Message = std::to_string(Count) + " player" + Plural + ": " + Buffer.str();
		SendMessage(Peer, Message, "white");
	}
	else if(Command == "priority") {
		Player->Character->Priority = Data.Read<int>();
	}

	if(!DevMode)
		return;

	// Process command
	if(Command == "ai") {
		AI = !AI;
	}
	else if(Command == "battle") {
		Player->Character->BossCooldowns.clear();

		// Get zone
		uint32_t ZoneID = Data.Read<uint32_t>();

		// Check zone
		auto Iterator = Stats->Zones.find(ZoneID);
		if(Iterator != Stats->Zones.end()) {
			const _Zone &Zone = Iterator->second;
			QueueBattle(Player, Zone.Name, Zone.ID, Zone.Boss, false, false, 0.0f, 0.0f, Zone.Duration);
		}

		// Update stats
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::OBJECT_STATS);
		Packet.WriteBit(false);
		Player->SerializeStats(Packet);
		Network->SendPacket(Packet, Peer);
	}
	else if(Command == "bloodmoon") {
		Save->BloodMoonTime = 0.0;
	}
	else if(Command == "bounty") {
		bool Adjust = Data.ReadBit();
		int64_t Change = Data.Read<int64_t>();
		Player->Character->Attributes["Bounty"].Int = std::max((int64_t)0, Adjust ? Player->Character->Attributes["Bounty"].Int + Change : Change);
		SendHUD(Peer);
	}
	else if(Command == "clock") {
		double Clock = Data.Read<int>();
		if(Clock < 0)
			Clock = 0;
		else if(Clock >= MAP_DAY_LENGTH)
			Clock = MAP_DAY_LENGTH;

		SetClock(Clock);
	}
	else if(Command == "equipment") {
		int Level = Data.Read<int>();

		_Bag &EquipmentBag = Player->Inventory->GetBag(BagType::EQUIPMENT);
		std::vector<EquipmentType> UpgradeTypes = { EquipmentType::HEAD,  EquipmentType::BODY, EquipmentType::LEGS, EquipmentType::HAND1, EquipmentType::HAND2 };
		for(const auto &Type : UpgradeTypes) {
			_InventorySlot &InventorySlot = EquipmentBag.Slots.at(Type);
			if(InventorySlot.Item)
				InventorySlot.Upgrades = std::clamp(Level, 0, InventorySlot.Item->MaxLevel);
		}
		Player->Character->CalculateStats();
		SendPlayerInfo(Peer);
	}
	else if(Command == "event") {
		_Event Event;
		Event.Type = Data.Read<uint32_t>();
		Event.Data = Data.Read<uint32_t>();

		Player->Map->StartEvent(Player, Event, false);
	}
	else if(Command == "experience") {
		bool Adjust = Data.ReadBit();
		int64_t Change = Data.Read<int64_t>();
		Player->Character->Attributes["Experience"].Int = std::max((int64_t)0, Adjust ? Player->Character->Attributes["Experience"].Int + Change : Change);
		Player->Character->CalculateStats();
		SendHUD(Peer);
	}
	else if(Command == "evolves") {
		Player->Character->Attributes["Evolves"].Int = std::max(0, Data.Read<int>());
		Player->Character->CalculateStats();
		SendPlayerInfo(Peer);
	}
	else if(Command == "transforms") {
		Player->Character->Attributes["Transforms"].Int = std::max(0, Data.Read<int>());
		Player->Character->CalculateStats();
		SendPlayerInfo(Peer);
	}
	else if(Command == "ghost") {
		Player->Character->GhostMode = !Player->Character->GhostMode;
	}
	else if(Command == "give") {
		uint32_t ItemID = Data.Read<uint32_t>();
		int Count = Data.Read<int>();

		// Find item
		auto Iterator = Stats->Items.find(ItemID);
		if(Iterator == Stats->Items.end())
			return;

		// Check texture
		const _Item *Item = Iterator->second;
		if(!Item->Texture)
			return;

		SendItem(Peer, Item, Count);
	}
	else if(Command == "gold") {
		bool Adjust = Data.ReadBit();
		int64_t Change = Data.Read<int64_t>();
		Change = std::clamp(Change, -PLAYER_MAX_GOLD, PLAYER_MAX_GOLD);
		Player->Character->Attributes["Gold"].Int = Adjust ? Player->Character->Attributes["Gold"].Int + Change : Change;
		Player->Character->Attributes["GoldLost"].Int = 0;
		Player->Character->UpdateGold(0);
		SendHUD(Peer);
	}
	else if(Command == "health") {
		bool Adjust = Data.ReadBit();
		int64_t Change = Data.Read<int64_t>();
		Player->Character->Attributes["Health"].Int = std::max((int64_t)0, Adjust ? Player->Character->Attributes["Health"].Int + Change : Change);
		Player->Character->CalculateStats();
		SendHUD(Peer);
	}
	else if(Command == "level") {
		int Level = Data.Read<int>();
		Level = std::max(Level, 1);
		Level = std::min(Level, Stats->GetMaxLevel());
		Player->Character->Attributes["Experience"].Int = Stats->GetLevel(Level)->Experience;
		Player->Character->CalculateStats();
		SendHUD(Peer);
	}
	else if(Command == "mana") {
		bool Adjust = Data.ReadBit();
		int64_t Change = Data.Read<int64_t>();
		Player->Character->Attributes["Mana"].Int = std::max((int64_t)0, Adjust ? Player->Character->Attributes["Mana"].Int + Change : Change);
		Player->Character->CalculateStats();
		SendHUD(Peer);
	}
	else if(Command == "map") {
		ae::NetworkIDType MapID = Data.Read<ae::NetworkIDType>();
		SpawnPlayer(Player, MapID, _Map::EVENT_MAPENTRANCE);
	}
	else if(Command == "move") {
		uint8_t X = Data.Read<uint8_t>();
		uint8_t Y = Data.Read<uint8_t>();

		Player->Position = Player->Map->GetValidCoord(glm::ivec2(X, Y));
		SendPlayerPosition(Player->Peer);
	}
	else if (Command == "reset") {
		std::string Parameter = Data.ReadString();

		if(Parameter == "cooldowns") {
			Player->Character->Cooldowns.clear();
		}
		else if(Parameter == "eternal") {
			Player->Character->ResetEternal(true);
		}
		else if(Parameter == "keys") {
			Player->Inventory->GetBag(BagType::KEYS).Slots.clear();
		}
		else if(Parameter == "kills") {
			Player->Character->BossCooldowns.clear();
			Player->Character->BossKills.clear();
		}
		else if(Parameter == "rites") {
			Player->Character->ResetRites();
		}
		else if(Parameter == "unlocks") {
			Player->Character->ClearUnlocks();
		}
	}
	else if(Command == "rebirths") {
		Player->Character->Attributes["Rebirths"].Int = std::max(0, Data.Read<int>());
		Player->Character->CalculateStats();
		SendPlayerInfo(Peer);
	}
	else if(Command == "save") {
		SaveTime = Config.AutoSavePeriod;
	}
	else if(Command == "skills") {
		int Level = Data.Read<int>();
		for(auto &Skill : Player->Character->Skills)
			Skill.second = std::clamp(Level, 0, GAME_MAX_SKILL_LEVEL);
		Player->Character->ResetUIState();
		Player->Character->CalculateStats();
		SendPlayerInfo(Peer);
	}
}

// Handle last update id from player
void _Server::HandleUpdateID(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	Peer->Object->UpdateID = Data.Read<uint8_t>();
}

// Handle request for position sync
void _Server::HandleSyncRequest(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Send position
	SendPlayerPosition(Peer);

	// Log
	_Object *Player = Peer->Object;
	Log
		<< "[SYNC] Player " << Player->Name
		<< " ( character_id=" << Player->Character->ID
		<< " )" << std::endl;
}

// Handle action use by player
void _Server::HandleActionUse(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Check player
	_Object *Player = Peer->Object;
	if(!Player->Character->IsAlive())
		return;

	// Reset idle time
	Player->Character->IdleTime = 0.0;

	// Set action used
	bool WasSet = Player->SetActionUsing(Data, ObjectManager);
	if(!WasSet)
		return;

	// Check for battle
	if(Player->Battle) {

		// Notify other players of action
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::BATTLE_ACTION);
		Packet.Write<ae::NetworkIDType>(Player->NetworkID);
		if(Player->Character->Action.Item)
			Packet.Write<uint32_t>(Player->Character->Action.Item->ID);
		else
			Packet.Write<uint32_t>(0);

		Player->Battle->BroadcastPacket(Packet);
	}
}

// Handle an action bar change
void _Server::HandleActionBarChanged(ae::_Buffer &Data, ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	// Reset idle time
	_Object *Player = Peer->Object;
	Player->Character->IdleTime = 0.0;

	// Read skills
	for(size_t i = 0; i < Player->Character->ActionBar.size(); i++) {
		Player->Character->ActionBar[i].Unserialize(Data, Stats);
		Player->Character->ActionBar[i].ActionBarSlot = (int)i;
	}

	Player->Character->CalculateStats();
}

// Updates the player's HUD
void _Server::SendHUD(ae::_Peer *Peer) {
	if(!ValidatePeer(Peer))
		return;

	_Object *Player = Peer->Object;

	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::WORLD_HUD);
	Packet.Write<int64_t>(Player->Character->Attributes["Health"].Int);
	Packet.Write<int64_t>(Player->Character->Attributes["Mana"].Int);
	Packet.Write<int64_t>(Player->Character->Attributes["MaxHealth"].Int);
	Packet.Write<int64_t>(Player->Character->Attributes["MaxMana"].Int);
	Packet.Write<int64_t>(Player->Character->Attributes["Experience"].Int);
	Packet.Write<int64_t>(Player->Character->Attributes["Gold"].Int);
	Packet.Write<int64_t>(Player->Character->Attributes["Bounty"].Int);
	Packet.Write<double>(Save->Clock);
	Packet.Write<int>(GetEventPhase());

	Network->SendPacket(Packet, Peer);
}

// Set server clock
void _Server::SetClock(double Clock) {
	if(!Save)
		return;

	Save->Clock = Clock;

	// Build packet
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::WORLD_CLOCK);
	Packet.Write<float>(Clock);
	Packet.Write<int>(GetEventPhase());

	// Broadcast packet
	for(auto &Peer : Network->GetPeers())
		Network->SendPacket(Packet, Peer);
}

// Update players with event status
void _Server::SendEventStatus() {
	if(!Save || !Network)
		return;

	// Build packet
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::WORLD_EVENT);
	Packet.WriteBit(Save->BloodMoonActive);

	// Broadcast packet
	for(auto &Peer : Network->GetPeers())
		Network->SendPacket(Packet, Peer);
}

// Delete buff on client
void _Server::ClearBuff(_Object *Player, uint32_t BuffID) {

	// Create packet
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::PLAYER_CLEARBUFF);
	Packet.Write<ae::NetworkIDType>(Player->NetworkID);
	Packet.Write<uint32_t>(BuffID);

	// Notify players in battle
	if(Player->Battle)
		Player->Battle->BroadcastPacket(Packet);
	else
		Network->SendPacket(Packet, Player->Peer);
}

// Update buff on client
void _Server::UpdateBuff(_Object *Player, uint32_t BuffID, int64_t Level) {

	// Create packet
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::PLAYER_UPDATEBUFF);
	Packet.Write<ae::NetworkIDType>(Player->NetworkID);
	Packet.Write<uint32_t>(BuffID);
	Packet.Write<int64_t>(Level);

	// Notify players in battle
	if(Player->Battle)
		Player->Battle->BroadcastPacket(Packet);
	else
		Network->SendPacket(Packet, Player->Peer);
}

// Slap a misbehaving player
void _Server::Slap(ae::NetworkIDType PlayerID) {
	_Object *Player = ObjectManager->GetObject(PlayerID);
	if(!Player)
		return;

	// Update slap counter
	auto &Attributes = Player->Character->Attributes;
	Attributes["Slaps"].Int++;
	double SlapPenalty = -std::min(Attributes["Slaps"].Int * GAME_SLAP_GOLD, 1.0);

	// Penalty
	_StatChange StatChange;
	StatChange.Object = Player;
	StatChange.Values["Gold"].Int = std::min((int64_t)-1, (int64_t)(SlapPenalty * std::abs(Attributes["Gold"].Int)));
	StatChange.Values["Health"].Int = -Attributes["MaxHealth"].Int / 2;
	Player->UpdateStats(StatChange);

	// Build packet
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::STAT_CHANGE);
	StatChange.Serialize(Packet);
	Network->SendPacket(Packet, Player->Peer);

	// Shame them
	BroadcastMessage(nullptr, Player->Name + " has been slapped for misbehaving!", "yellow");

	// Log message
	Log
		<< "[SLAP] Player " << Player->Name << " for " << StatChange.Values["Gold"].Int << " gold!"
		<< " ( character_id=" << Player->Character->ID
		<< " gold=" << Player->Character->Attributes["Gold"].Int
		<< " )" << std::endl;
}

// Mute an account
void _Server::Mute(uint32_t AccountID, bool Value) {
	Save->SetMute(AccountID, Value);

	for(auto &Object : ObjectManager->Objects) {
		if(Object->Character->AccountID == AccountID)
			Object->Muted = Value;
	}
}

// Ban account
void _Server::Ban(uint32_t AccountID, const std::string &TimeFromNow) {
	Save->SetBanTime(AccountID, TimeFromNow);

	for(auto &Object : ObjectManager->Objects) {
		if(Object->Character->AccountID == AccountID)
			Network->DisconnectPeer(Object->Peer, ae::_Network::DISCONNECT_SERVER);
	}
}

// Log character data
bool _Server::StartLog(ae::NetworkIDType PlayerID) {
	_Object *Player = ObjectManager->GetObject(PlayerID);
	if(!Player)
		throw std::runtime_error("Player ID not found!");

	Player->SetLogging(!Player->Logging);

	return Player->Logging;
}

// Notify player of full inventory
void _Server::SendInventoryFullMessage(ae::_Peer *Peer, const _Item *Item, int Type) {

	std::string Message = "Inventory is full";
	switch(Type) {
		case 0:
			if(Item)
				Message += ": " + Item->Name + " lost!";
		break;
		case 2:
			if(Item)
				Message = Item->Name + " added to stash";
		break;
		default:
			return;
		break;
	}

	SendMessage(Peer, Message, "yellow");
}

// Send a player their status effects
void _Server::SendStatusEffects(_Object *Player) {
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::PLAYER_STATUSEFFECTS);
	Packet.Write<ae::NetworkIDType>(Player->NetworkID);
	Player->SerializeStatusEffects(Packet);
	Network->SendPacket(Packet, Player->Peer);
}

// Handle object deletion
void _Server::RemoveObject(_Object *Object) {

	// Remove from battle events
	for(auto &BattleEvent : BattleEvents) {
		if(BattleEvent.Object == Object)
			BattleEvent.Object = nullptr;
	}
}

// Send out party data
void _Server::UpdateParties() {
	if(Parties.empty())
		return;

	std::vector<ae::_Peer *> Peers;
	Peers.reserve(8);
	for(const auto &Party : Parties) {
		if(Party.second.size() <= 1)
			continue;

		Peers.clear();

		// Cap party size
		int PartySize = std::min((int)Party.second.size(), HUD_PARTY_MAX);

		// Create packet
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::PARTY_INFO);
		Packet.Write<uint8_t>((uint8_t)PartySize);

		// Build packet data
		int i = 0;
		for(const auto &Member : Party.second) {
			_Object *Player = Member.second;
			if(!Player->Character)
				continue;

			// Format gold
			std::stringstream Buffer;
			ae::FormatSI<int64_t>(Buffer, Player->Character->Attributes["Gold"].Int, ae::RoundDown1);

			// Write packet
			Packet.Write<ae::NetworkIDType>(Player->NetworkID);
			Packet.WriteString(Player->Name.c_str());
			Packet.WriteString(Buffer.str().c_str());
			Packet.Write<uint16_t>((uint16_t)Player->Character->Level);
			Packet.Write<uint32_t>((uint32_t)Player->Character->Attributes["Rebirths"].Int);
			Packet.Write<uint32_t>((uint32_t)Player->Character->Attributes["Evolves"].Int);
			Packet.Write<uint32_t>((uint32_t)Player->Character->Attributes["Transforms"].Int);
			Packet.Write<uint8_t>(Player->Character->GetHealthPercent() * 255);
			Packet.Write<uint8_t>(Player->Character->GetManaPercent() * 255);
			Packet.Write<uint8_t>((uint8_t)Player->Character->PortraitID);
			Packet.Write<uint8_t>((uint8_t)Player->GetMapID());
			Peers.push_back(Player->Peer);
			if(i++ >= PartySize)
				break;
		}

		// Broadcast packet to party members
		for(auto &Peer : Peers)
			Network->SendPacket(Packet, Peer, ae::_Network::SEND_UNSEQUENCED);
	}
}

// Update player counts when standing on trigger events
void _Server::UpdateEventCount() {
	for(auto &Peer : Network->GetPeers()) {
		if(!ValidatePeer(Peer))
			continue;

		_Object *Player = Peer->Object;

		if(!Player->Character)
			continue;

		if(Player->Deleted)
			continue;

		if(!Player->Map)
			continue;

		if(!Player->Character->OnTrigger)
			continue;

		// Get list of potential battle partners
		std::vector<_Object *> Players;
		Player->Map->GetPotentialBattlePlayers(Player, 0.0f, BATTLE_MAX_OBJECTS_PER_SIDE - 1, true, Players);

		// Send message
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::EVENT_MESSAGE);
		Packet.Write<uint8_t>(Players.size() + 1);
		Network->SendPacket(Packet, Peer);
	}
}

// Add player to party map
void _Server::AddParty(const std::string &PartyName, uint32_t CharacterID, _Object *Object) {
	if(PartyName.empty())
		return;

	Parties[PartyName][CharacterID] = Object;

	// Update event counts
	UpdateEventCount();
}

// Remove player from party map
void _Server::RemoveParty(const std::string &PartyName, uint32_t CharacterID, _Object *Object) {
	if(PartyName.empty())
		return;

	// Find party name
	auto PartiesIterator = Parties.find(PartyName);
	if(PartiesIterator == Parties.end())
		return;

	// Find character id
	auto &Party = Parties.at(PartyName);
	auto Iterator = Party.find(CharacterID);
	if(Iterator == Party.end())
		return;

	// Erase character id
	Party.erase(Iterator);

	// Clear party hud for last player in party
	if(Party.size() == 1) {
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::PARTY_CLEAR);
		Network->SendPacket(Packet, Party.begin()->second->Peer);
	}
	// Erase party if empty
	else if(Party.empty())
		Parties.erase(PartiesIterator);

	// Update event counts
	UpdateEventCount();
}

// Send spawn point to player
void _Server::SendSpawnPoint(_Object *Player) {
	if(!Player || !ValidatePeer(Player->Peer))
		return;

	// Build packet
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::PLAYER_SPAWNPOINT);
	Packet.Write<ae::NetworkIDType>(Player->Character->SpawnMapID);
	Packet.Write<uint32_t>(Player->Character->SpawnPoint);

	// Send
	Network->SendPacket(Packet, Player->Peer);
}

// Send a message to the player
void _Server::SendMessage(ae::_Peer *Peer, const std::string &Message, const std::string &ColorName) {
	if(!ValidatePeer(Peer))
		return;

	// Build message
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::CHAT_MESSAGE);
	Packet.WriteString(ColorName.c_str());
	Packet.WriteString(Message.c_str());

	// Send
	Network->SendPacket(Packet, Peer);
}

// Broadcast message to all peers
void _Server::BroadcastMessage(ae::_Peer *IgnorePeer, const std::string &Message, const std::string &ColorName) {
	for(auto &Peer : Network->GetPeers()) {
		if(Peer == IgnorePeer)
			continue;

		SendMessage(Peer, Message, ColorName);
	}
}

// Sends information to another player about items they're trading
void _Server::SendTradeInformation(_Object *Sender, _Object *Receiver) {
	_Bag &Bag = Sender->Inventory->GetBag(BagType::TRADE);

	// Send items to trader player
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::TRADE_REQUEST);
	Packet.Write<ae::NetworkIDType>(Sender->NetworkID);
	Packet.Write<int64_t>(Sender->Character->TradeGold);
	for(size_t i = 0; i < Bag.Slots.size(); i++)
		Sender->Inventory->SerializeSlot(Packet, _Slot(BagType::TRADE, i));

	Network->SendPacket(Packet, Receiver->Peer);
}

// Add summons to the battle from summon buffs
void _Server::AddBattleSummons(_Battle *Battle, int Side, _Object *JoinPlayer, bool Join) {

	// Get list of objects on a side
	std::vector<_Object *> ObjectList;
	ObjectList.reserve(BATTLE_MAX_OBJECTS_PER_SIDE);
	Battle->GetObjectList(Side, ObjectList);

	// Iterate over all players in battle, collecting summons for each player
	std::vector<_SummonCaptain> SummonCaptains;
	for(auto &SummonOwner : ObjectList) {
		if(JoinPlayer && SummonOwner != JoinPlayer)
			continue;

		// Collect all summons
		_SummonCaptain SummonCaptain;
		SummonCaptain.Summons.reserve(BATTLE_MAX_OBJECTS_PER_SIDE);
		SummonCaptain.Owner = SummonOwner;
		SummonOwner->Character->GetSummonsFromBuffs(SummonCaptain.Summons);
		std::shuffle(SummonCaptain.Summons.begin(), SummonCaptain.Summons.end(), ae::RandomGenerator);
		SummonCaptains.push_back(SummonCaptain);
	}

	// Shuffle who goes first
	std::shuffle(SummonCaptains.begin(), SummonCaptains.end(), ae::RandomGenerator);

	// Keep track of summons added per owner
	std::unordered_map<_Object *, int> SummonsAdded;

	// Get summons from summon buffs
	int SlotsLeft = BATTLE_MAX_OBJECTS_PER_SIDE - (int)ObjectList.size();
	while(SlotsLeft > 0) {

		// Add summons round-robin
		int Added = 0;
		for(size_t i = 0; i < SummonCaptains.size(); i++) {
			_SummonCaptain &Captain = SummonCaptains[i];

			// Check for any summons left in captain's pool
			if(!Captain.Summons.size())
				continue;

			// Create object
			_Object *Object = CreateSummon(SummonsAdded[Captain.Owner], true, Captain.Owner, Captain.Summons.back().first);

			// If joining battle, broadcast create
			if(Join) {
				ae::_Buffer Packet;
				Packet.Write<PacketType>(PacketType::WORLD_CREATEOBJECT);
				Object->SerializeCreate(Packet);
				Battle->BroadcastPacket(Packet);
			}

			// Add object to battle
			Battle->AddObject(Object, Captain.Owner->Fighter->BattleSide, Join);

			// Remove summon from pool and decrement owner's status effect level
			_StatusEffect *StatusEffect = Captain.Summons.back().second;
			StatusEffect->Level--;
			if(StatusEffect->Level <= 0)
				StatusEffect->Deleted = true;
			else
				StatusEffect->Duration = StatusEffect->MaxDuration;

			Captain.Summons.pop_back();
			SummonsAdded[Captain.Owner]++;
			Added++;
			SlotsLeft--;
			if(SlotsLeft <= 0)
				break;
		}

		// No summons left to add
		if(!Added)
			break;
	}
}

// Send the list of trade items to a trading player
void _Server::SendTradePlayerInventory(_Object *Player) {
	_Object *TradePlayer = Player->Character->TradePlayer;
	if(!TradePlayer)
		return;

	// Reset agreement
	Player->Character->TradeAccepted = false;
	TradePlayer->Character->TradeAccepted = false;

	// Send inventory
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::TRADE_INVENTORY);
	Player->Inventory->GetBag(BagType::TRADE).Serialize(Packet);
	Network->SendPacket(Packet, TradePlayer->Peer);
}

// Send the clear WaitForServer packet
void _Server::SendClearWait(_Object *Player) {
	if(!Player || !Player->Peer)
		return;

	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::PLAYER_CLEARWAIT);
	Network->SendPacket(Packet, Player->Peer);
}

// Start a battle event
void _Server::StartBattle(_BattleEvent &BattleEvent) {

	// Object was deleted
	if(!BattleEvent.Object)
		return;

	// Return if object is already in battle or in zone 0
	if(BattleEvent.Object->Battle || (!BattleEvent.PVP && !BattleEvent.Zone))
		return;

	// Handle PVP
	if(BattleEvent.PVP) {
		if(NoPVP)
			return;

		// Get list of players on current tile
		std::vector<_Object *> Players;
		Players.reserve(BATTLE_MAX_OBJECTS_PER_SIDE);
		BattleEvent.Object->Map->GetPVPPlayers(BattleEvent.Object, Players, BattleEvent.BountyEarned > 0);
		if(!Players.size())
			return;

		// Create a new battle instance
		_Battle *Battle = BattleManager->Create();
		Battle->Manager = ObjectManager;
		Battle->Stats = Stats;
		Battle->Server = this;
		Battle->Scripting = Scripting;
		Battle->Name = BattleEvent.Name;
		Battle->PVP = BattleEvent.PVP;
		Battle->BountyEarned = BattleEvent.BountyEarned;
		Battle->BountyClaimed = BattleEvent.BountyClaimed;
		Battle->Duration = BattleEvent.Duration;
		Scripting->CreateBattle(Battle);

		// Add players to battle
		Battle->AddObject(BattleEvent.Object, BattleEvent.Side);
		for(auto &TargetPlayer : Players) {
			Battle->AddObject(TargetPlayer, !BattleEvent.Side);
		}

		// Add summons
		AddBattleSummons(Battle, 0);
		AddBattleSummons(Battle, 1);

		// Set corpses
		for(auto &Object : Battle->Objects)
			Object->Fighter->Corpse = 1;

		// Send battle to players
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::BATTLE_START);
		Battle->Serialize(Packet);
		Battle->BroadcastPacket(Packet);
	}
	else {

		// Check for cooldown
		if(BattleEvent.Object->Character->IsZoneOnCooldown(BattleEvent.Zone))
			return;

		// Get difficulty
		int64_t Difficulty = 100;
		double Multiplier = 1.0;
		if(Scripting->StartMethodCall("Game", "GetDifficulty")) {
			Scripting->PushReal(Save->Clock);
			Scripting->PushBoolean(Save->BloodMoonActive);
			Scripting->MethodCall(2, 2);
			Difficulty += Scripting->GetInt(1);
			Multiplier = Scripting->GetReal(2);
			Scripting->FinishMethodCall();
		}

		// Get a list of players
		std::vector<_Object *> Players;
		Players.reserve(BATTLE_MAX_OBJECTS_PER_SIDE);
		BattleEvent.Object->Map->GetPotentialBattlePlayers(BattleEvent.Object, BATTLE_COOP_DISTANCE, BATTLE_MAX_OBJECTS_PER_SIDE-1, BattleEvent.Manual, Players);
		int AdditionalCount = 0;
		if(BattleEvent.Boss == 0)
			AdditionalCount = (int)Players.size();

		// Add host player
		Players.push_back(BattleEvent.Object);

		// Get monster count modifier and difficulty multiplier
		int MonsterCountModifier = 100;
		for(const auto &Player : Players) {
			MonsterCountModifier += Player->Character->Attributes["MonsterCount"].Int - 100;
			if(!Player->Character->IsZoneOnCooldown(BattleEvent.Zone))
				Multiplier += Player->Character->Attributes["DifficultyMultiplier"].Double - 1;
		}

		// Get monsters
		std::vector<_Zone> Monsters;
		bool Boss = false;
		double Cooldown = 0.0;
		Stats->GenerateMonsterListFromZone(AdditionalCount, Multiplier * (MonsterCountModifier * 0.01), BattleEvent.Zone, Monsters, Boss, Cooldown);

		// Fight if there are monsters
		if(!Monsters.size())
			return;

		// Create a new battle instance
		_Battle *Battle = BattleManager->Create();
		Battle->Manager = ObjectManager;
		Battle->Stats = Stats;
		Battle->Server = this;
		Battle->Boss = Boss;
		Battle->Cooldown = Cooldown;
		Battle->Name = BattleEvent.Name;
		Battle->Zone = BattleEvent.Zone;
		Battle->Scripting = Scripting;
		Battle->Multiplier = Multiplier;
		Battle->Duration = BattleEvent.Duration;
		Scripting->CreateBattle(Battle);

		// Sort by players
		std::sort(Players.begin(), Players.end(), CompareBattleObjects);

		// Get difficulty increase
		int DifficultyAdjust = BATTLE_DIFFICULTY_PER_PLAYER;
		if(Boss)
			DifficultyAdjust = BATTLE_DIFFICULTY_PER_PLAYER_BOSS;

		// Add players to battle
		Difficulty -= DifficultyAdjust;
		for(auto &PartyPlayer : Players) {
			Battle->AddObject(PartyPlayer, 0);

			// Increase difficulty for each player
			Difficulty += DifficultyAdjust;

			// Increase difficulty with each boss kill
			if(!PartyPlayer->Character->IsZoneOnCooldown(BattleEvent.Zone))
				Difficulty += PartyPlayer->Character->BossKills[BattleEvent.Zone] * BATTLE_BOSS_DIFFICULTY_PER_KILL;

			// Increase by each player's difficulty stat
			Difficulty += PartyPlayer->Character->Attributes["Difficulty"].Int - 100;
		}

		// Set total difficulty
		Battle->Difficulty = ((BattleEvent.Boss == -1) ? 100 : Difficulty) * Multiplier;

		// Add summons
		AddBattleSummons(Battle, 0);

		// Add monsters
		for(auto &Monster : Monsters) {
			_Object *Object = ObjectManager->Create();
			Object->Stats = Stats;
			Object->CreateComponents();
			Object->Server = this;
			Object->Scripting = Scripting;
			Object->Monster->DatabaseID = Monster.MonsterID;
			Object->Monster->Difficulty = (BattleEvent.Boss == -1) ? 100 : (Difficulty + Monster.Difficulty) * Multiplier;
			Object->Character->InitAttributes();
			Stats->SetMonsterStats(Object);
			Object->Character->CalculateStats();
			Battle->AddObject(Object, 1);
		}

		// Set corpses
		for(auto &Object : Battle->Objects)
			Object->Fighter->Corpse = 1;

		// Send battle to players
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::BATTLE_START);
		Battle->Serialize(Packet);
		Battle->BroadcastPacket(Packet);
	}
}

// Start a rebirth
void _Server::StartRebirth(_RebirthEvent &RebirthEvent) {
	_Object *Player = RebirthEvent.Object;
	_Character *Character = Player->Character;
	if(Player->Battle)
		return;

	// Send time stats
	std::stringstream Buffer;
	bool SendTimeMessage = false;
	if(RebirthEvent.Mode == 0 && Character->Attributes["Rebirths"].Int) {
		SendTimeMessage = true;
		Buffer << "Last rebirth time: ";
		ae::FormatTime(Buffer, Character->Attributes["RebirthTime"].Double);
	}
	else if(RebirthEvent.Mode == 1 && Character->Attributes["Evolves"].Int) {
		SendTimeMessage = true;
		Buffer << "Last evolve time: ";
		ae::FormatTime(Buffer, Character->Attributes["EvolveTime"].Double);
	}
	else if(RebirthEvent.Mode == 2 && Character->Attributes["Transforms"].Int) {
		SendTimeMessage = true;
		Buffer << "Last transform time: ";
		ae::FormatTime(Buffer, Character->Attributes["TransformTime"].Double);
	}
	if(SendTimeMessage)
		SendMessage(Player->Peer, Buffer.str(), "cyan");
	Buffer.str("");

	// Update
	switch(RebirthEvent.Mode) {
		case 0: {
			Character->Attributes["Rebirths"].Int += Character->Attributes["RebirthGrowth"].Int + 1;
		} break;
		case 1: {
			Character->Attributes["Rebirths"].Int = 0;
			Character->Attributes["Evolves"].Int += Character->Attributes["RebirthEvolution"].Int + 1;
		} break;
		case 2: {
			Character->Attributes["Rebirths"].Int = 0;
			Character->Attributes["Evolves"].Int = 0;
			Character->Attributes["Transforms"].Int++;
		} break;
	}

	// Cancel trades
	if(Character->TradePlayer) {
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::TRADE_CANCEL);
		Network->SendPacket(Packet, Character->TradePlayer->Peer);
	}

	// Move last privileged items into trade bag
	std::vector<_Slot> SlotsUpdated;
	Player->MovePrivilegeItems(SlotsUpdated, true);

	// Load build
	const auto &BuildIterator = Stats->Builds.find(Character->BuildID);
	if(BuildIterator == Stats->Builds.end())
		return;

	const _Object *Build = BuildIterator->second;

	// Log info
	Log
		<< "[REBIRTH] Player " << Player->Name
		<< " ( character_id=" << Character->ID
		<< " mode=" << RebirthEvent.Mode
		<< " type=" << RebirthEvent.Name
		<< " value=" << RebirthEvent.Value
		<< " exp=" << Character->Attributes["Experience"].Int
		<< " gold=" << Character->Attributes["Gold"].Int
		<< " rebirths=" << Character->Attributes["Rebirths"].Int
		<< " evolves=" << Character->Attributes["Evolves"].Int
		<< " transforms=" << Character->Attributes["Transforms"].Int
		<< " rebirthtime=" << Character->Attributes["RebirthTime"].Double
		<< " evolvetime=" << Character->Attributes["EvolveTime"].Double
		<< " transformtime=" << Character->Attributes["TransformTime"].Double
		<< " )" << std::endl;

	// Save old info
	std::vector<_Bag> OldBags = Player->Inventory->GetBags();
	std::vector<_Action> OldActionBar = Character->ActionBar;
	std::unordered_map<uint32_t, _Cooldown> OldCooldowns = Character->Cooldowns;
	std::unordered_map<uint32_t, int> OldSkills = Character->Skills;

	// Reduce rite levels on evolve
	switch(RebirthEvent.Mode) {
		case 1: {
			int64_t RiteLevel = Character->Attributes["Transforms"].Int * GAME_TRANSFORM_RITE_SCALE + Character->Attributes["Evolves"].Int;
			for(const auto &RiteName : Stats->RiteStatNames)
				Character->Attributes[RiteName].Int = std::min(Character->Attributes[RiteName].Int, RiteLevel);
		} break;
		case 2: {
			int64_t RiteLevel = (Character->Attributes["Transforms"].Int) * GAME_TRANSFORM_RITE_SCALE;
			for(const auto &RiteName : Stats->RiteStatNames)
				Character->Attributes[RiteName].Int = std::min(Character->Attributes[RiteName].Int, RiteLevel);
		} break;
	}

	// Reset character
	if(Character->Attributes["RebirthPrivilege"].Int) {
		_Inventory BlankInventory(nullptr);
		Player->Inventory->Bags = BlankInventory.GetBags();
	}
	else
		Player->Inventory->Bags = Build->Inventory->GetBags();
	Character->ActionBar = Build->Character->ActionBar;
	Character->Skills = Build->Character->Skills;
	Character->MinSkillLevels.clear();
	Character->MaxSkillLevels.clear();
	Character->Unlocks.clear();
	Character->Seed = ae::GetRandomInt((uint32_t)1, std::numeric_limits<uint32_t>::max());
	Character->Attributes["Gold"].Int = std::min((int64_t)(Character->Attributes["Experience"].Int * Character->Attributes["RebirthWealth"].Mult() * GAME_REBIRTH_WEALTH_MULTIPLIER), PLAYER_MAX_GOLD);
	Character->Attributes["Experience"].Int = Stats->GetLevel(std::min(Stats->GetMaxLevel(), (int)Character->Attributes["RebirthWisdom"].Int + 1))->Experience;
	Character->UpdateTimer = 0;
	Character->SkillPointsUnlocked = 0;
	Character->Vendor = nullptr;
	Character->Trader = nullptr;
	Character->Blacksmith = nullptr;
	Character->Enchanter = nullptr;
	Character->Disenchanter = nullptr;
	Character->Minigame = nullptr;
	Character->TradePlayer = nullptr;
	Character->TradeGold = 0;
	Character->WaitingForTrade = false;
	Character->TradeAccepted = false;
	Character->TeleportTime = 0.0;
	Character->MenuOpen = false;
	Character->InventoryOpen = false;
	Character->SkillsOpen = false;
	Character->BeltSize = ACTIONBAR_DEFAULT_BELTSIZE;
	Character->SkillBarSize = ACTIONBAR_DEFAULT_SKILLBARSIZE;
	Character->Cooldowns.clear();
	Character->BossCooldowns.clear();
	Character->BossKills.clear();

	// Reset time
	Character->Attributes["RebirthTime"].Double = 0.0;
	switch(RebirthEvent.Mode) {
		case 1:
			Character->Attributes["EvolveTime"].Double = 0.0;
		break;
		case 2:
			Character->Attributes["EvolveTime"].Double = 0.0;
			Character->Attributes["TransformTime"].Double = 0.0;
		break;
	}

	// Remove status effects
	int SummonsLeft = Character->Attributes["RebirthPrivilege"].Int;
	for(auto Iterator = Character->StatusEffects.begin(); Iterator != Character->StatusEffects.end(); ) {
		_StatusEffect *StatusEffect = *Iterator;

		// Keep summons
		if(SummonsLeft && StatusEffect->Buff->Summon) {
			StatusEffect->Level = std::clamp(SummonsLeft, 1, (int)StatusEffect->Level);
			SummonsLeft -= StatusEffect->Level;
			++Iterator;
		}
		else {
			delete StatusEffect;
			Iterator = Character->StatusEffects.erase(Iterator);
		}
	}

	// Give rebirth bonus
	if(RebirthEvent.Mode == 0) {
		Character->Attributes[RebirthEvent.Name].Int += RebirthEvent.Value * (Character->Attributes["RebirthGrowth"].Int + 1);
	}
	// Give evolve bonus
	else if(RebirthEvent.Mode == 1) {
		Character->ResetEternal(false);
		Character->Attributes[RebirthEvent.Name].Int += RebirthEvent.Value * (Character->Attributes["RebirthEvolution"].Int + 1);
	}
	// Give transform bonus
	else if(RebirthEvent.Mode == 2) {
		Character->ResetEternal(true);
		Character->Attributes[RebirthEvent.Name].Int += RebirthEvent.Value;
	}

	// Keep belt unlocks
	Character->BeltSize = std::min((int)Character->Attributes["RebirthGirth"].Int + ACTIONBAR_DEFAULT_BELTSIZE, ACTIONBAR_MAX_BELTSIZE);
	Character->UnlockBySearch("Belt Slot %", (int)Character->Attributes["RebirthGirth"].Int);

	// Keep skill bar unlocks
	Character->SkillBarSize = std::min((int)Character->Attributes["RebirthProficiency"].Int + ACTIONBAR_DEFAULT_SKILLBARSIZE, ACTIONBAR_MAX_SKILLBARSIZE);
	Character->UnlockBySearch("Skill Slot %", (int)Character->Attributes["RebirthProficiency"].Int);

	// Keep skill point unlocks
	Character->SkillPointsUnlocked = Character->UnlockBySearch("Skill Point %", (int)Character->Attributes["RebirthInsight"].Int);

	// Keep belt layout when privileged
	if(Character->Attributes["RebirthPrivilege"].Int) {
		for(size_t i = 0; i < (size_t)Character->BeltSize; i++)
			Character->ActionBar[ACTIONBAR_BELT_STARTS + i].Item = OldActionBar[ACTIONBAR_BELT_STARTS + i].Item;
	}

	// Keep items from trade bag
	Character->LastPrivilege.clear();
	int ItemCount = (int)Character->Attributes["RebirthPrivilege"].Int;
	Player->Inventory->AddItem(Stats->ItemMap.at("Teleport Scroll"), 0, ItemCount, true);
	for(const auto &Slot : OldBags[(size_t)BagType::TRADE].Slots) {
		if(ItemCount && Slot.Item) {
			int MaxStackSize = Slot.Item->IsEquippable() ? GAME_PRIVILEGE_EQUIPMENT_STACK : GAME_PRIVILEGE_ITEM_STACK;
			int MoveAmount = std::min(Slot.Count, MaxStackSize * (int)Character->Attributes["RebirthPrivilege"].Int);
			Player->Inventory->AddItem(Slot.Item, Slot.Upgrades, MoveAmount, false);
			Character->LastPrivilege.push_back(_PrivilegeItem(Slot.Item, MoveAmount));
			ItemCount--;
			if(ItemCount <= 0)
				break;
		}
	}

	// Keep enduring relic
	const _InventorySlot &OldRelicSlot = OldBags[(size_t)BagType::EQUIPMENT].Slots[EquipmentType::RELIC];
	if(OldRelicSlot.Item && OldRelicSlot.Item->Attributes.at("Endure").Int) {
		Player->Inventory->AddItem(OldRelicSlot.Item, OldRelicSlot.Upgrades, OldRelicSlot.Count, false, _Slot(BagType::EQUIPMENT, EquipmentType::RELIC));
	}
	else {

		// Search for a non-cursed enduring relic in inventory
		std::vector<BagType> BagTypes = { BagType::INVENTORY, BagType::STASH };
		for(const auto &BagType : BagTypes) {
			_Bag &Bag = OldBags[(size_t)BagType];
			for(size_t i = 0; i < Bag.Slots.size(); i++) {
				const _Item *Item = Bag.Slots[i].Item;
				if(Item && Item->Type == ItemType::RELIC && Item->Attributes.at("Endure").Int && !Item->IsCursed())
					Player->Inventory->AddItem(Item, Bag.Slots[i].Upgrades, 1, false, _Slot(BagType::EQUIPMENT, EquipmentType::RELIC));
			}
		}
	}

	// Unlock keys
	size_t KeyUnlockCount = std::clamp((size_t)Character->Attributes["RebirthPassage"].Int, (size_t)0, KeyUnlocks.size());
	for(size_t i = 0; i < KeyUnlockCount; i++)
		Player->Inventory->GetBag(BagType::KEYS).Slots.push_back(_InventorySlot(KeyUnlocks[i], 1));

	// Give starting relics
	size_t RelicCount = std::clamp((size_t)Character->Attributes["RebirthTraversal"].Int, (size_t)0, RelicUnlocks.size());
	for(size_t i = 0; i < RelicCount; i++)
		Player->Inventory->AddItem(RelicUnlocks[i], 0, 1, false);

	// Equip last equipped relic
	_Slot RelicSlot(BagType::EQUIPMENT, EquipmentType::RELIC);
	if(OldRelicSlot.Item && Player->Inventory->GetSlot(RelicSlot).Item != OldRelicSlot.Item) {
		size_t Slot;
		Player->Inventory->FindItem(OldRelicSlot.Item, Slot, 0);
		ae::_Buffer Dummy;
		Player->Inventory->MoveInventory(Dummy, RelicSlot, _Slot(BagType::INVENTORY, Slot));
	}

	// Give item on evolve/transform
	if(RebirthEvent.Mode == 1)
		Player->Inventory->AddItem(Stats->ItemMap.at("Ultimate Battle Potion"), 0, Character->Attributes["Evolves"].Int, false);
	else if(RebirthEvent.Mode == 2)
		Player->Inventory->AddItem(Stats->ItemMap.at("Bottle of Tears"), 0, 1, false);

	// Unlock highest learned skills
	int SkillCount = (int)Character->Attributes["RebirthKnowledge"].Int;
	if(SkillCount) {
		std::vector<_HighestSkill> HighestSkills;
		HighestSkills.reserve(OldSkills.size());
		for(const auto &Skill : OldSkills) {

			// Don't add skills from build
			if(Build->Character->Skills.find(Skill.first) == Build->Character->Skills.end())
				HighestSkills.push_back(_HighestSkill(Skill.first, Skill.second));
		}
		std::sort(HighestSkills.begin(), HighestSkills.end());

		// Unlock
		for(const auto &Skill : HighestSkills) {
			Character->Skills[Skill.ID] = 0;

			SkillCount--;
			if(SkillCount <= 0)
				break;
		}
	}

	// Set min/max level for skills
	for(const auto &Skill : Character->Skills) {
		const _Item *SkillItem = Stats->Items.at(Skill.first);
		Character->MinSkillLevels[Skill.first] = Character->MaxSkillLevels[Skill.first] = std::min(SkillItem->MaxLevel, GAME_DEFAULT_MAX_SKILL_LEVEL + (int)Character->Attributes["RebirthEnchantment"].Int);
	}

	// Handle knowledge starting setup
	if(Character->Attributes["RebirthKnowledge"].Int) {

		// Remove all skill points
		for(auto &Skill : Character->Skills)
			Skill.second = 0;

		Character->CalculateStats();

		// Keep skill bar layout and points assigned
		for(size_t i = 0; i < (size_t)Character->SkillBarSize; i++) {
			Character->ActionBar[i].Item = nullptr;

			const _Item *OldSkill = OldActionBar[i].Item;
			if(!OldSkill)
				continue;

			if(!Character->GetSkillPointsAvailable())
				continue;

			if(Character->Skills.find(OldSkill->ID) == Character->Skills.end())
				continue;

			// Get skills back to their original level
			Character->ActionBar[i].Item = OldSkill;
			Character->AdjustSkillLevel(OldSkill->ID, OldSkills[OldSkill->ID], true, false);
			Character->CalculateStats();
		}

		// Get unequipped skills back to their original level
		for(auto &Skill : Character->Skills) {
			Character->AdjustSkillLevel(Skill.first, OldSkills[Skill.first] - Skill.second, true, false);
			Character->CalculateStats();
			if(!Character->GetSkillPointsAvailable())
				break;
		}
	}

	// Keep old cooldowns
	Character->Cooldowns = OldCooldowns;

	// Calculate all stats
	Character->CalculateStats();

	// Spawn player
	Character->Attributes["Health"].Int = Character->Attributes["MaxHealth"].Int;
	Character->Attributes["Mana"].Int = Character->Attributes["MaxMana"].Int;
	Character->GenerateNextBattle();
	Character->LoadMapID = 0;
	Character->SpawnMapID = 1;
	Character->SpawnPoint = 0;
	SpawnPlayer(Player, Character->LoadMapID, _Map::EVENT_NONE);
	SendPlayerInfo(Player->Peer);
}

// Apply penalty to player leaving in single-player
void _Server::ApplyLeavePenalty(ae::NetworkIDType NetworkID) {
	_Object *Player = ObjectManager->GetObject(NetworkID);
	if(!Player || !Player->Battle)
		return;

	// Remove stolen gold
	if(Player->Fighter->GoldStolen)
		Player->Character->Attributes["Gold"].Int -= Player->Fighter->GoldStolen;

	// Apply penalty
	Player->ApplyDeathPenalty(true, PLAYER_DEATH_GOLD_PENALTY, 0);
	Player->Character->Attributes["Health"].Int = 0;
	Player->Character->Attributes["Mana"].Int = Player->Character->Attributes["MaxMana"].Int / 2;
	Player->Character->LoadMapID = 0;
	Player->Character->DeleteStatusEffects();

	// Add monsters from battle to player's summon buffs
	for(const auto &BattleObject : Player->Battle->Objects) {
		if(BattleObject->Monster->Owner != Player)
			continue;

		_StatChange Summons;
		Summons.Object = Player;
		Summons.Values["Buff"].Pointer = (void *)BattleObject->Monster->SummonBuff;
		Summons.Values["BuffLevel"].Int = 1;
		Summons.Values["BuffDuration"].Double = -1.0;
		Player->UpdateStats(Summons, Player);
	}

	// Give one of each summon on death
	for(auto &StatusEffect : Player->Character->StatusEffects) {
		if(StatusEffect->Buff->Summon)
			StatusEffect->Level = 1;
	}
}

// Get event phase
int _Server::GetEventPhase() {
	int DaysUntilEvent = 0;
	if(Scripting->StartMethodCall("Game", "DaysUntilEvent")) {
		Scripting->PushReal(Save->Clock);
		Scripting->PushReal(Save->BloodMoonTime);
		Scripting->MethodCall(2, 1);
		DaysUntilEvent = std::min(GAME_BLOODMOON_PHASE_MAX, Scripting->GetInt(1));
		Scripting->FinishMethodCall();
	}

	return GAME_BLOODMOON_PHASE_MAX - DaysUntilEvent;
}
