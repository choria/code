/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <ae/circular_buffer.h>
#include <objects/stat_change.h>
#include <objects/components/inventory.h>

// Forward Declarations
class _Action;
class _BlacksmithScreen;
class _CharacterScreen;
class _DisenchanterScreen;
class _EnchanterScreen;
class _InventoryScreen;
class _Map;
class _Minigame;
class _Scripting;
class _SkillScreen;
class _StashScreen;
class _StatusEffect;
class _TraderScreen;
class _TradeScreen;
class _VendorScreen;

namespace ae {
	class _Element;
	class _Texture;
	struct _MouseEvent;
}

// Structures
struct _Cursor {

	_Cursor() { Reset(); }

	void Reset() {
		InventorySlot.Reset();
		Element = nullptr;
		Cost = 0;
		StatusEffect = nullptr;
		Slot.Type = BagType::NONE;
		Slot.Index = NOSLOT;
		Window = -1;
	}

	bool IsEqual(size_t Slot, int Window) { return this->Slot.Index == Slot && this->Window == Window; }

	_InventorySlot InventorySlot;
	ae::_Element *Element;
	const _StatusEffect *StatusEffect;
	_Slot Slot;
	int64_t Cost;
	int Window;
};

struct _Message {

	_Message() { }
	_Message(const std::string &Message, const glm::vec4 &Color, double Time) : Message(Message), Color(Color), Time(Time) { }

	std::string Message;
	glm::vec4 Color{1.0f};
	double Time{0.0};
};

struct _RecentItem {
	const _Item *Item{nullptr};
	double Time{0.0};
	int Count{0};
	int Total{0};
};

struct _RecentGold {

	_RecentGold() { }
	_RecentGold(int64_t Value, double Time) : Value(Value), Time(Time) { }

	int64_t Value{0};
	double Time{0.0};
};

// Classes
class _HUD {

	friend class _CharacterScreen;
	friend class _InventoryScreen;
	friend class _VendorScreen;
	friend class _TradeScreen;
	friend class _TraderScreen;
	friend class _BlacksmithScreen;
	friend class _EnchanterScreen;
	friend class _SkillScreen;
	friend class _StashScreen;
	friend class _DisenchanterScreen;
	friend class _Screen;
	friend class _Character;

	public:

		enum WindowType {
			WINDOW_BATTLE,
			WINDOW_BUTTONBAR,
			WINDOW_HUD_EFFECTS,
			WINDOW_SKILLBAR,
			WINDOW_EQUIPMENT,
			WINDOW_INVENTORY,
			WINDOW_VENDOR,
			WINDOW_TRADER,
			WINDOW_BLACKSMITH,
			WINDOW_TRADETHEIRS,
			WINDOW_TRADEYOURS,
			WINDOW_SKILLS,
			WINDOW_MINIGAME,
			WINDOW_INVENTORY_TABS,
			WINDOW_KEYS,
			WINDOW_ENCHANTER,
			WINDOW_UNLOCKS,
			WINDOW_REBIRTHS,
			WINDOW_STASH,
			WINDOW_RELICS,
			WINDOW_DISENCHANTER,
		};

		void Init(_Scripting *Scripting);
		void Close();

		// Static functions
		static void GetPlayerPrefix(std::stringstream &Buffer, uint32_t Rebirths, uint32_t Evolves, uint32_t Transforms, bool SI=true);
		static const char *GetBossCooldownMessage(int MessageID);

		// Updates
		void Reset(bool ClearRecentItems=true);
		void ResetChat();
		void HandleEnter();
		void HandleMouseButton(const ae::_MouseEvent &MouseEvent);
		void Update(double FrameTime);
		void UpdateRestartButton();
		void Refresh();
		void RefreshRelicWheel();

		// Render
		void Render(_Map *Map, double BlendFactor, double Time);
		void DrawRecentItems();
		void DrawParty();
		void DrawRelicWheel();

		// Events
		void SetMessage(const std::string &Text, double Duration);
		void ShowMapName(const std::string &Text, double Duration);
		void StartTeleport();
		void StopTeleport();

		// Objects
		void SetPlayer(_Object *Player);
		void UpdateActionBarSize();
		void AddStatChange(_StatChange &StatChange, bool FromAction);
		void ClearStatChanges(bool BattleOnly);
		void UpdateLabels();

		// Button bar
		void ToggleParty(bool IgnoreNextChar=true);
		void ToggleInGameMenu(bool Force);
		void UpdateButtonBarLabels();

		// Windows
		void SetBarState(bool State);
		void InitConfirm(const std::string &WarningMessage, const ae::_Texture *Icon, int Amount);
		void InitMinigame();
		void InitParty();
		void ToggleRelics();
		bool CloseWindows(bool SendStatus, bool HardClose=true, bool SendNotify=true, bool DelayClose=false);

		// Chat
		void ToggleChat();
		bool IsChatting();
		void AddChatMessage(const _Message &Chat);
		void UpdateSentHistory(int Direction);
		void CloseChat();

		// Trade
		bool IsTypingGold();

		// Party
		void HidePartyMembers();
		void UpdatePartyMember(size_t Index, bool SameMap, const std::string &Name, const std::string &MapName, const std::string &Gold, const ae::_Texture *Portrait, float HealthPercent, float ManaPercent, float Rotation, bool SamePosition);
		void SendPartyName();
		bool IsTypingParty();

		// Input
		bool AllowMovement();
		bool HandleSkillHotkey(int SkillIndex);
		int GetSkillModifierAmount();
		bool EnableMouseCombat{false};
		bool ShowRelics{false};
		bool RelicWheelEnabled{false};

		// Stats
		ae::_CircularBuffer<_RecentGold> GoldGained;
		bool ShowDebug{false};

		// Scripting
		_Scripting *Scripting;

		// Screens
		_CharacterScreen *CharacterScreen;
		_InventoryScreen *InventoryScreen;
		_VendorScreen *VendorScreen;
		_TradeScreen *TradeScreen;
		_TraderScreen *TraderScreen;
		_BlacksmithScreen *BlacksmithScreen;
		_EnchanterScreen *EnchanterScreen;
		_DisenchanterScreen *DisenchanterScreen;
		_SkillScreen *SkillScreen;
		_StashScreen *StashScreen;

		// Minigames
		_Minigame *Minigame{nullptr};

		// UI
		std::vector<_StatChangeUI> StatChanges;
		std::vector<_RecentItem> RecentItems;
		std::string UseText;
		double MapNameTimer{0.0};

	private:

		bool CloseConfirm();
		bool CloseMinigame();
		bool CloseParty();
		bool CloseTeleport();

		void DrawConfirm();
		void DrawChat(double Time, bool IgnoreTimeout);
		void DrawStatusEffects();
		void DrawTeleport();
		void DrawMinigame(double BlendFactor);
		void DrawActionBar();
		void DrawPartyInput();
		void DrawMessage();
		void DrawMapName();
		void DrawItemPrice(const _Item *Item, int Count, const glm::vec2 &DrawPosition, bool Buy, int Level=0);
		void DrawCursorItem();
		void DrawCooldown(const ae::_Element *Button, const _Item *Item);

		void SetActionBar(size_t Slot, size_t OldSlot, const _Action &Action);
		void SplitStack(const _Slot &Slot, uint8_t Count);
		void Transfer(const _Slot &Slot);
		BagType GetBagFromWindow(int Window);
		int64_t GetGPM();

		// UI
		ae::_Element *PartyMembersElement;
		ae::_Element *DarkOverlayElement;
		ae::_Element *ConfirmElement;
		ae::_Element *DiedElement;
		ae::_Element *StatusEffectsElement;
		ae::_Element *ActionBarElement;
		ae::_Element *ButtonBarElement;
		ae::_Element *FullscreenElement;
		ae::_Element *MinigameElement;
		ae::_Element *PartyElement;
		ae::_Element *TeleportElement;
		ae::_Element *ChatElement;
		ae::_Element *HealthElement;
		ae::_Element *ManaElement;
		ae::_Element *ExperienceElement;
		ae::_Element *RecentItemsElement;
		ae::_Element *MessageElement;
		ae::_Element *MapNameElement;
		ae::_Element *PartyTextBox;
		ae::_Element *GoldElement;
		ae::_Element *MessageLabel;
		ae::_Element *RelicElement;
		ae::_Element *RelicsElement;
		ae::_Element *AmuletElement;
		ae::_Element *RespawnInstructions;
		_Cursor Cursor;
		_Cursor Tooltip;
		_Message Message;
		double BuyCooldown{0.0};

		// Objects
		_Object *Player{nullptr};
		double LowestRecentItemTime{0.0};

		// Chat
		std::vector<_Message> ChatHistory;
		std::vector<std::string> SentHistory;
		std::vector<std::string>::iterator SentHistoryIterator;
		ae::_Element *ChatTextBox;

		// Inventory
		_Slot DeleteSlot;

};

extern _HUD HUD;
