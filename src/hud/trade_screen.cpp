/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <hud/trade_screen.h>
#include <ae/assets.h>
#include <ae/clientnetwork.h>
#include <ae/ui.h>
#include <ae/util.h>
#include <hud/hud.h>
#include <hud/inventory_screen.h>
#include <objects/components/character.h>
#include <objects/components/controller.h>
#include <objects/object.h>
#include <states/play.h>
#include <config.h>
#include <packet.h>
#include <stats.h>
#include <regex>

// Constructor
_TradeScreen::_TradeScreen(ae::_Element *CurrentElement) : _Screen(CurrentElement) {
	PrivilegeButton = ae::Assets.Elements["button_inventory_move"];
	AcceptButton = ae::Assets.Elements["button_trade_accept_yours"];
	PrivilegeButton->SetActive(false);
}

// Initialize
void _TradeScreen::Init() {
	if(!HUD.Player || HUD.Player->Character->WaitingForTrade)
		return;

	HUD.Player->Character->WaitingForTrade = true;
	HUD.InventoryScreen->InitInventoryTab(0);
	Element->SetActive(true);

	// Send request to server
	SendTradeRequest();

	// Reset UI
	AcceptButton->SetEnabled(false);
	ResetAcceptButton();

	// Reset their trade UI
	ResetTradeTheirsWindow(true);

	// Set up gold buttons
	int64_t PlayerGold = HUD.Player->Character->Attributes["Gold"].Int;
	ae::_Element *GoldButtons = ae::Assets.Elements["element_trade_gold_buttons"];
	for(const auto &GoldButton : GoldButtons->Children) {
		if(!GoldButton->Index)
			continue;

		GoldButton->SetActive(PlayerGold >= pow(1e3, GoldButton->Index));
	}

	// Adjust offset for single button
	GoldButtons->Children.front()->BaseOffset.y = PlayerGold < 1e3 ? -7 : 0;
	GoldButtons->CalculateBounds();
}

// Close screen
bool _TradeScreen::Close(bool SendNotify, bool Delay) {
	bool WasOpen = Element->Active;

	// Reset UI
	ResetAcceptButton();

	// Close inventory
	HUD.InventoryScreen->Close(SendNotify, Delay);
	Element->SetActive(false);
	if(HUD.IsTypingGold())
		ae::FocusedElement = nullptr;

	// Notify server
	if(SendNotify)
		SendTradeCancel();

	if(HUD.Player) {
		HUD.Player->Character->WaitingForTrade = false;
		HUD.Player->Character->TradePlayer = nullptr;
	}

	return WasOpen;
}

// Toggle screen
void _TradeScreen::Toggle() {
	if(!HUD.Player || HUD.Player->Controller->WaitForServer || !HUD.Player->Character->CanOpenTrade())
		return;

	// Restrict trading for new characters
	if(!HUD.Player->Character->CanTrade()) {
		HUD.SetMessage("Trading unlocks at level " + std::to_string(GAME_TRADING_LEVEL), HUD_MESSAGE_TIMEOUT);
		return;
	}

	if(!Element->Active) {
		HUD.CloseWindows(true);
		Init();
	}
	else {
		HUD.CloseWindows(true);
	}
}

// Render
void _TradeScreen::Render(double BlendFactor) {
	if(!Element->Active || !HUD.Player)
		return;

	Element->Render();

	// Show move items button
	PrivilegeButton->SetActive(HUD.Player->Character->ShowMoveItems);

	// Draw items
	DrawTradeItems(HUD.Player, "button_trade_yourbag_", _HUD::WINDOW_TRADEYOURS);
	DrawTradeItems(HUD.Player->Character->TradePlayer, "button_trade_theirbag_", _HUD::WINDOW_TRADETHEIRS);
}

// Draws trading items
void _TradeScreen::DrawTradeItems(_Object *Player, const std::string &ElementPrefix, int Window) {
	if(!Player)
		return;

	// Draw offered items
	int BagIndex = 0;
	_Bag &Bag = Player->Inventory->GetBag(BagType::TRADE);
	for(size_t i = 0; i < Bag.Slots.size(); i++) {

		// Get inventory slot
		_InventorySlot *Slot = &Bag.Slots[i];
		if(Slot->Item) {

			// Get bag button
			std::stringstream Buffer;
			Buffer << ElementPrefix << BagIndex;
			ae::_Element *Button = ae::Assets.Elements[Buffer.str()];

			// Draw
			RenderInventoryItem(Button, Slot);
		}

		BagIndex++;
	}
}

// Update accept button label text
void _TradeScreen::UpdateAcceptButton() {
	ae::_Element *LabelTradeStatusYours = ae::Assets.Elements["label_trade_status_yours"];
	if(AcceptButton->Checked) {
		LabelTradeStatusYours->Text = "Accepted";
		LabelTradeStatusYours->Color = ae::Assets.Colors["green"];
	}
	else {
		LabelTradeStatusYours->Text = "Accept";
		LabelTradeStatusYours->Color = glm::vec4(1.0f);
	}
}

// Resets the trade agreement
void _TradeScreen::ResetAcceptButton() {
	AcceptButton->Checked = false;
	UpdateAcceptButton();
	UpdateTradeStatus(false);
}

// Resets upper trade window status
void _TradeScreen::ResetTradeTheirsWindow(bool ResetYourGold) {
	if(!HUD.Player)
		return;

	ae::Assets.Elements["element_trade_theirs"]->SetActive(false);
	ae::Assets.Elements["label_trade_status"]->SetActive(true);
	ae::Assets.Elements["textbox_trade_gold_theirs"]->Enabled = false;
	ae::Assets.Elements["textbox_trade_gold_theirs"]->SetText("0");
	if(ResetYourGold) {
		OldGoldText = "0";
		ae::Assets.Elements["textbox_trade_gold_yours"]->SetText("0");
	}
	ae::Assets.Elements["label_trade_name_yours"]->Text = HUD.Player->Name;
	ae::Assets.Elements["image_trade_portrait_yours"]->Texture = HUD.Player->Character->Portrait;

	const _Level *Level = HUD.Player->Stats->GetLevel(HUD.Player->Character->Level);
	ae::_Element *RestrictLabel = ae::Assets.Elements["label_trade_status_restrict"];
	RestrictLabel->SetActive(true);
	RestrictLabel->Text = "Other player must be level " + std::to_string(Level->MinInteract) + " to " + std::to_string(Level->MaxInteract);

	AcceptButton->SetEnabled(false);
}

// Trade with another player
void _TradeScreen::SendTradeRequest() {
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::TRADE_REQUEST);
	PlayState.Network->SendPacket(Packet);
}

// Cancel a trade
void _TradeScreen::SendTradeCancel() {
	if(!HUD.Player)
		return;

	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::TRADE_CANCEL);
	PlayState.Network->SendPacket(Packet);

	HUD.Player->Character->TradePlayer = nullptr;
}

// Update their status label
void _TradeScreen::UpdateTradeStatus(bool Accepted) {
	ae::_Element *LabelTradeStatusTheirs = ae::Assets.Elements["label_trade_status_theirs"];
	if(Accepted) {
		LabelTradeStatusTheirs->Text = "Accepted";
		LabelTradeStatusTheirs->Color = ae::Assets.Colors["green"];
	}
	else {
		LabelTradeStatusTheirs->Text = "Unaccepted";
		LabelTradeStatusTheirs->Color = ae::Assets.Colors["red"];
	}
}

// Make sure the trade gold box is valid and send gold to player
void _TradeScreen::ValidateTradeGold() {
	if(!HUD.Player || !Element->Active)
		return;

	ae::_Element *GoldTextBox = ae::Assets.Elements["textbox_trade_gold_yours"];
	ae::_Element *GoldShortElement = ae::Assets.Elements["label_trade_gold_yours_short"];

	// Remove non-numeric characters
	std::regex Regex("[^0-9]");
	GoldTextBox->Text = std::regex_replace(GoldTextBox->Text, Regex, "");

	// Get gold amount
	int64_t Gold = ae::ToNumber<int64_t>(GoldTextBox->Text);
	if(Gold < 0)
		Gold = 0;
	else if(Gold > HUD.Player->Character->Attributes["Gold"].Int)
		Gold = std::max((int64_t)0, HUD.Player->Character->Attributes["Gold"].Int);

	// Update all button label
	ae::Assets.Elements["label_trade_gold_all"]->Text = Gold > 0 ? "None" : "All";

	// Update text box
	std::stringstream Buffer;
	Buffer.imbue(std::locale(Config.Locale));
	Buffer << Gold;
	GoldTextBox->Font = Gold >= 1e15 ? ae::Assets.Fonts["hud_char"] : ae::Assets.Fonts["hud_small"];
	GoldTextBox->SetText(Buffer.str());
	Buffer.str("");

	// Update short gold label
	if(Gold >= 1e4) {
		ae::FormatSI<int64_t>(Buffer, Gold, ae::RoundDown2);
		GoldShortElement->Text = Buffer.str();
	}
	else
		GoldShortElement->Text = "";

	// Send new amount
	if(OldGoldText != GoldTextBox->Text) {

		// Save new amount
		OldGoldText = GoldTextBox->Text;

		// Notify server
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::TRADE_GOLD);
		Packet.Write<int64_t>(Gold);
		PlayState.Network->SendPacket(Packet);

		// Reset agreement
		ResetAcceptButton();
	}
}
