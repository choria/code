/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <hud/disenchanter_screen.h>
#include <ae/assets.h>
#include <ae/database.h>
#include <ae/font.h>
#include <hud/hud.h>
#include <objects/components/character.h>
#include <objects/components/controller.h>
#include <objects/item.h>
#include <objects/object.h>
#include <states/play.h>
#include <stats.h>
#include <algorithm>

// Initialize
void _DisenchanterScreen::Init() {
	if(!HUD.Player)
		return;

	// Clear old children
	ClearSkills();

	glm::vec2 Start(14, 36);
	glm::vec2 Offset(Start);
	glm::vec2 LevelOffset(0, -6);
	glm::vec2 Spacing(14, 70);
	glm::vec2 PlusOffset(0, 64 + 6);
	glm::vec2 LabelOffset(0, 2);
	glm::vec2 ButtonSize(64, 30);

	// Get all player skills
	std::vector<const _Item *> SortedSkills;
	for(auto &SkillID : HUD.Player->Character->Skills) {
		const _Item *Skill = PlayState.Stats->Items.at(SkillID.first);
		if(!Skill)
			continue;

		SortedSkills.push_back(Skill);
	}

	// Sort skills
	std::sort(SortedSkills.begin(), SortedSkills.end(), CompareItems);

	// Iterate over skills
	for(auto &Skill : SortedSkills) {

		// Add skill icon
		ae::_Element *Button = new ae::_Element();
		Button->ID = "button_max_skill_levels_skill";
		Button->Parent = Element;
		Button->BaseOffset = Offset;
		Button->BaseSize = UI_SLOT_SIZE;
		Button->Alignment = ae::LEFT_TOP;
		Button->Texture = Skill->Texture;
		Button->Index = (int)Skill->ID;
		Button->Clickable = true;
		Element->Children.push_back(Button);

		// Add level label
		ae::_Element *LevelLabel = new ae::_Element();
		LevelLabel->ID = "label_max_skill_levels_level";
		LevelLabel->Parent = Button;
		LevelLabel->BaseOffset = LevelOffset;
		LevelLabel->Alignment = ae::CENTER_BASELINE;
		LevelLabel->Font = ae::Assets.Fonts["hud_small"];
		LevelLabel->Index = (int)Skill->ID;
		Element->Children.push_back(LevelLabel);

		// Add sell button
		ae::_Element *SellButton = new ae::_Element();
		SellButton->ID = "button_max_skill_levels_sell";
		SellButton->Parent = Element;
		SellButton->BaseSize = ButtonSize;
		SellButton->BaseOffset = Offset + PlusOffset;
		SellButton->Alignment = ae::LEFT_TOP;
		SellButton->Style = ae::Assets.Styles["style_menu_button"];
		SellButton->HoverStyle = ae::Assets.Styles["style_menu_button_hover"];
		SellButton->DisabledStyle = ae::Assets.Styles["style_menu_button_disabled"];
		SellButton->Index = (int)Skill->ID;
		SellButton->Clickable = true;
		Element->Children.push_back(SellButton);

		// Add sell label
		ae::_Element *SellLabel = new ae::_Element();
		SellLabel->Parent = SellButton;
		SellLabel->Text = "Sell";
		SellLabel->BaseOffset = LabelOffset;
		SellLabel->Alignment = ae::CENTER_MIDDLE;
		SellLabel->Font = ae::Assets.Fonts["hud_tiny"];
		SellLabel->Color = ae::Assets.Colors["gold"];
		SellButton->Children.push_back(SellLabel);

		// Update position
		Offset.x += UI_SLOT_SIZE.x + Spacing.x;
		if(Offset.x > Element->BaseSize.x - UI_SLOT_SIZE.x) {
			Offset.y += UI_SLOT_SIZE.y + Spacing.y;
			Offset.x = Start.x;
		}
	}
	PlayState.Stats->Database->CloseQuery();

	Element->CalculateBounds();
	Element->SetActive(true);

	RefreshSellButtons();
	HUD.Cursor.Reset();

	PlayState.SendStatus(_Character::STATUS_SKILLS);
}

// Close screen
bool _DisenchanterScreen::Close(bool SendNotify, bool Delay) {
	bool WasOpen = Element->Active;

	Element->SetActive(false);
	HUD.Cursor.Reset();

	if(HUD.Player)
		HUD.Player->Character->Disenchanter = nullptr;

	return WasOpen;
}

// Toggle display
void _DisenchanterScreen::Toggle() {
	if(!HUD.Player || HUD.Player->Controller->WaitForServer || !HUD.Player->Character->CanOpenInventory())
		return;

	if(!Element->Active) {
		HUD.CloseWindows(true);
		Init();
	}
	else {
		HUD.CloseWindows(true);
	}
}

// Render
void _DisenchanterScreen::Render(double BlendFactor) {
	if(!Element->Active || !HUD.Player)
		return;

	Element->Render();

	const _Disenchanter *Disenchanter = HUD.Player->Character->Disenchanter;

	// Show help text
	std::string Text = Disenchanter->Name + " decreases max skill levels";
	glm::vec2 DrawPosition = glm::vec2((Element->Bounds.End.x + Element->Bounds.Start.x) / 2, Element->Bounds.End.y - 38 * ae::_Element::GetUIScale());
	ae::Assets.Fonts["hud_medium"]->DrawText(Text, DrawPosition, ae::CENTER_BASELINE, ae::Assets.Colors["gray"]);
}

// Delete memory used by skill page
void _DisenchanterScreen::ClearSkills() {

	// Delete children
	for(auto &Child : Element->Children)
		delete Child;

	Element->Children.clear();
}

// Enable or disable sell button
void _DisenchanterScreen::RefreshSellButtons() {
	if(!HUD.Player)
		return;

	const _Disenchanter *Disenchanter = HUD.Player->Character->Disenchanter;
	if(!Disenchanter)
		return;

	// Loop through buttons
	for(auto &ChildElement : Element->Children) {
		uint32_t SkillID = (uint32_t)ChildElement->Index;
		int MinLevel = HUD.Player->Character->MinSkillLevels[SkillID];
		if(ChildElement->ID == "label_max_skill_levels_level") {
			int DrawLevel = HUD.Player->Character->MaxSkillLevels[SkillID];
			glm::vec4 LevelColor = (DrawLevel <= MinLevel) ? ae::Assets.Colors["red"] : glm::vec4(1.0f);

			ChildElement->Text = std::to_string(DrawLevel);
			ChildElement->Color = LevelColor;
		}
		else if(ChildElement->ID == "button_max_skill_levels_sell") {
			int CurrentLevel = HUD.Player->Character->MaxSkillLevels[SkillID];

			// Set button state
			if(CurrentLevel <= MinLevel) {
				ChildElement->SetEnabled(false);
				ChildElement->Children.front()->Color = ae::Assets.Colors["gray"];
			}
			else {
				ChildElement->SetEnabled(true);
				ChildElement->Children.front()->Color = ae::Assets.Colors["gold"];
			}
		}
	}
}
