/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <hud/character_screen.h>
#include <ae/assets.h>
#include <ae/font.h>
#include <ae/graphics.h>
#include <ae/input.h>
#include <ae/ui.h>
#include <hud/hud.h>
#include <objects/item.h>
#include <SDL_keycode.h>

// Constructor
_Screen::_Screen(ae::_Element *Element) : Element(Element) {
	Element->SetActive(false);
}

// Toggle screen
void _Screen::Toggle() {
	Element->SetActive(!Element->Active);
}

// Render an inventory item for a slot
void _Screen::RenderInventoryItem(const ae::_Element *Button, const _InventorySlot *Slot) {

	// Get position of slot
	glm::vec2 DrawPosition = (Button->Bounds.Start + Button->Bounds.End) / 2.0f;

	// Draw item
	ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos_uv"]);
	ae::Graphics.DrawScaledImage(DrawPosition, Slot->Item->Texture, UI_SLOT_SIZE);

	// Draw cooldown overlay
	HUD.DrawCooldown(Button, Slot->Item);

	// Draw upgrade count if holding alt
	if(!Slot->Item->IsSkill() && Slot->Item->MaxLevel && ae::Input.ModKeyDown(KMOD_ALT)) {
		glm::vec4 Color = glm::vec4(1.0f, 1.0f, 1.0f, 0.5f);
		ae::Assets.Fonts["hud_tiny"]->DrawText(std::to_string(Slot->Upgrades), glm::ivec2(DrawPosition + glm::vec2(-30, 28) * ae::_Element::GetUIScale()), ae::LEFT_BASELINE, Color);
	}

	// Draw count
	if(Slot->Count > 1)
		ae::Assets.Fonts["hud_tiny"]->DrawText(std::to_string(Slot->Count), DrawPosition + glm::vec2(28, 28) * ae::_Element::GetUIScale(), ae::RIGHT_BASELINE);
}
