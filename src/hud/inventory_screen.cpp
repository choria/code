/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <hud/inventory_screen.h>
#include <ae/assets.h>
#include <ae/font.h>
#include <ae/graphics.h>
#include <ae/ui.h>
#include <ae/util.h>
#include <hud/character_screen.h>
#include <hud/hud.h>
#include <hud/trade_screen.h>
#include <objects/components/character.h>
#include <objects/components/controller.h>
#include <objects/item.h>
#include <objects/object.h>
#include <states/play.h>
#include <config.h>
#include <stats.h>
#include <SDL_keycode.h>
#include <algorithm>
#include <sstream>

// Constructor
_InventoryScreen::_InventoryScreen(ae::_Element *CurrentElement) : _Screen(CurrentElement) {
	TabsElement = ae::Assets.Elements["element_inventory_tabs"];
	BagsElement = ae::Assets.Elements["element_bags"];
	KeysElement = ae::Assets.Elements["element_keys"];
	UnlocksElement = ae::Assets.Elements["element_unlocks"];
	RebirthsElement = ae::Assets.Elements["element_rebirths"];

	Element->SetActive(false);
}

// Close screen
bool _InventoryScreen::Close(bool SendNotify, bool Delay) {
	bool WasOpen = Element->Active;
	HUD.Cursor.Reset();

	Element->SetActive(false);
	HUD.CharacterScreen->Close(SendNotify, Delay);

	return WasOpen;
}

// Toggle display
void _InventoryScreen::Toggle() {
	if(!HUD.Player || HUD.Player->Controller->WaitForServer || !HUD.Player->Character->CanOpenInventory())
		return;

	if(HUD.Minigame) {
		HUD.CharacterScreen->Toggle();
		return;
	}

	if(!Element->Active) {
		HUD.CloseWindows(true);

		Element->SetActive(true);
		InitInventoryTab(0);
		HUD.CharacterScreen->Init();
		PlayState.SendStatus(_Character::STATUS_INVENTORY);
	}
	else
		HUD.CloseWindows(true);
}

// Render
void _InventoryScreen::Render(double BlendFactor) {
	if(!Element->Active || !HUD.Player)
		return;

	// Update gold
	std::stringstream Buffer;
	Buffer.imbue(std::locale(Config.Locale));
	Buffer << HUD.Player->Character->Attributes["Gold"].Int;
	ae::Assets.Elements["label_inventory_gold"]->Text = Buffer.str();

	// Show inventory tabs
	ShowTabs();

	// Render elements
	Element->Render();
	if(BagsElement->Active) {
		BagsElement->Render();
		DrawBag(BagType::EQUIPMENT);
		DrawBag(BagType::INVENTORY);
	}
	else if(KeysElement->Active) {
		KeysElement->Render();
		DrawKeys();
	}
	else if(UnlocksElement->Active) {
		UnlocksElement->Render();
		DrawUnlocks();
	}
	else if(RebirthsElement->Active) {
		RebirthsElement->Render();
		DrawRebirths();
	}
}

// Initialize an inventory tab
void _InventoryScreen::InitInventoryTab(int Index) {
	if(!HUD.Player)
		return;

	for(auto &Child : TabsElement->Children)
		Child->Checked = Child->Index == Index ? true : false;

	Element->SetActive(true);
	BagsElement->SetActive(false);
	KeysElement->SetActive(false);
	UnlocksElement->SetActive(false);
	RebirthsElement->SetActive(false);
	if(Index == 0) {
		BagsElement->SetActive(true);
	}
	else if(Index == 1) {
		KeysElement->SetActive(true);
	}
	else if(Index == 2) {
		UnlocksElement->SetActive(true);
	}
	else if(Index == 3) {
		RebirthsElement->SetActive(true);
	}

	// Show move items button
	HUD.TradeScreen->PrivilegeButton->SetActive(HUD.TradeScreen->Element->Active && HUD.Player->Character->ShowMoveItems);
}

// Draw an inventory bag
void _InventoryScreen::DrawBag(BagType Type) {
	if(!HUD.Player)
		return;

	_Bag &Bag = HUD.Player->Inventory->GetBag(Type);
	for(size_t i = 0; i < Bag.Slots.size(); i++) {

		// Get inventory slot
		_InventorySlot *Slot = &Bag.Slots[i];
		if(!Slot->Item)
			continue;

		// Get bag button
		std::stringstream Buffer;
		Buffer << "button_" << Bag.Name << "_bag_" << i;
		ae::_Element *Button = ae::Assets.Elements[Buffer.str()];
		Buffer.str("");

		// Get position of slot
		glm::vec2 DrawPosition = (Button->Bounds.Start + Button->Bounds.End) / 2.0f;

		// Draw item
		ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos_uv"]);
		ae::Graphics.DrawScaledImage(DrawPosition, Slot->Item->Texture, UI_SLOT_SIZE);

		// Draw cooldown overlay
		HUD.DrawCooldown(Button, Slot->Item);

		// Draw two handed weapon twice in equipment bag
		if(Type == BagType::EQUIPMENT && i == EquipmentType::HAND1 && Slot->Item->Type == ItemType::TWOHANDED_WEAPON) {
			Buffer << "button_" << Bag.Name << "_bag_" << EquipmentType::HAND2;
			ae::_Element *Button = ae::Assets.Elements[Buffer.str()];
			ae::Graphics.DrawScaledImage((Button->Bounds.Start + Button->Bounds.End) / 2.0f, Slot->Item->Texture, UI_SLOT_SIZE, ae::Assets.Colors["itemfade"]);
		}

		// Draw price if using vendor
		HUD.DrawItemPrice(Slot->Item, Slot->Count, DrawPosition, HUD.Player->Character->Blacksmith, Slot->Upgrades);

		// Draw upgrade count
		if(!Slot->Item->IsSkill() && Slot->Item->MaxLevel) {
			glm::vec4 Color = glm::vec4(1.0f, 1.0f, 1.0f, 0.5f);
			if(HUD.Player->Character->Blacksmith) {
				if(Slot->Upgrades >= Slot->Item->MaxLevel || Slot->Upgrades >= HUD.Player->Character->Blacksmith->Level)
					Color = ae::Assets.Colors["red"];
				else
					Color = ae::Assets.Colors["green"];
			}

			ae::Assets.Fonts["hud_tiny"]->DrawText(std::to_string(Slot->Upgrades), glm::ivec2(DrawPosition + glm::vec2(-30, 28) * ae::_Element::GetUIScale()), ae::LEFT_BASELINE, Color);
		}

		// Draw count
		if(Slot->Count > 1)
			ae::Assets.Fonts["hud_tiny"]->DrawText(std::to_string(Slot->Count), glm::ivec2(DrawPosition + glm::vec2(28, 28) * ae::_Element::GetUIScale()), ae::RIGHT_BASELINE);
	}
}

// Draw keychain
void _InventoryScreen::DrawKeys() {
	if(!HUD.Player)
		return;

	glm::vec2 StartOffset = glm::vec2(14, 26) * ae::_Element::GetUIScale();
	glm::vec2 Spacing = glm::vec2(168, 20) * ae::_Element::GetUIScale();
	int Column = 0;
	int Row = 0;

	_Bag &Bag = HUD.Player->Inventory->GetBag(BagType::KEYS);

	// No keys
	if(!Bag.Slots.size()) {
		glm::vec2 DrawPosition = KeysElement->Bounds.Start + StartOffset;
		ae::Assets.Fonts["hud_tiny"]->DrawText("No keys", glm::ivec2(DrawPosition), ae::LEFT_BASELINE, ae::Assets.Colors["gray"]);

		return;
	}

	// Draw key names
	for(size_t i = 0; i < Bag.Slots.size(); i++) {

		// Get inventory slot
		_InventorySlot *Slot = &Bag.Slots[i];
		if(Slot->Item) {

			// Get position
			glm::vec2 DrawPosition = KeysElement->Bounds.Start + StartOffset + glm::vec2(Spacing.x * Column, Spacing.y * Row);
			ae::Assets.Fonts["hud_tiny"]->DrawText(Slot->Item->Name, glm::ivec2(DrawPosition));

			// Update position
			Row++;
			if(Row >= 21) {
				Column++;
				Row = 0;
			}
		}
	}
}

// Draw unlocks
void _InventoryScreen::DrawUnlocks() {
	if(!HUD.Player)
		return;

	glm::vec2 StartOffset = glm::vec2(14, 26) * ae::_Element::GetUIScale();
	glm::vec2 Spacing = glm::vec2(168, 20) * ae::_Element::GetUIScale();
	int Column = 0;
	int Row = 0;

	const auto &Unlocks = HUD.Player->Character->Unlocks;

	// Check for unlocks
	if(!Unlocks.size()) {
		glm::vec2 DrawPosition = UnlocksElement->Bounds.Start + StartOffset;
		ae::Assets.Fonts["hud_tiny"]->DrawText("No unlocks", glm::ivec2(DrawPosition), ae::LEFT_BASELINE, ae::Assets.Colors["gray"]);

		return;
	}

	// Sort by id
	std::vector<uint32_t> UnlockIDs;
	UnlockIDs.reserve(Unlocks.size());
	for(const auto &Unlock : Unlocks) {
		UnlockIDs.push_back(Unlock.first);
	}
	std::sort(UnlockIDs.begin(), UnlockIDs.end());

	// Draw names
	for(const auto &UnlockID : UnlockIDs) {

		// Get position
		glm::vec2 DrawPosition = UnlocksElement->Bounds.Start + StartOffset + glm::vec2(Spacing.x * Column, Spacing.y * Row);
		ae::Assets.Fonts["hud_tiny"]->DrawText(HUD.Player->Stats->Unlocks.at(UnlockID), glm::ivec2(DrawPosition));

		// Update position
		Row++;
		if(Row >= 21) {
			Column++;
			Row = 0;
		}
	}
}

// Draw rebirth stats
void _InventoryScreen::DrawRebirths() {
	if(!HUD.Player)
		return;

	glm::vec2 StartOffset = glm::vec2(200, 26) * ae::_Element::GetUIScale();
	glm::vec2 Spacing = glm::vec2(230, 20) * ae::_Element::GetUIScale();
	glm::vec2 CenterSpacing = glm::vec2(10, 0) * ae::_Element::GetUIScale();
	int Column = 0;
	int Row = 0;

	// Draw stats
	std::stringstream Buffer;
	Buffer.imbue(std::locale(Config.Locale));
	for(const auto &Attribute : HUD.Player->Stats->RebirthTabAttributes) {
		int64_t Value = HUD.Player->Character->Attributes.at(Attribute).Int;
		if(Value == 0)
			continue;

		// Update column
		if(Column == 0 && Attribute.substr(0, 1) == "E") {
			Column++;
			Row = 0;
		}

		// Format value
		if(ae::Input.ModKeyDown(KMOD_ALT))
			Buffer << Value;
		else
			ae::FormatSI(Buffer, Value);

		// Get position
		glm::vec2 DrawPosition = UnlocksElement->Bounds.Start + StartOffset + glm::vec2(Spacing.x * Column, Spacing.y * Row);

		// Draw text
		ae::Assets.Fonts["hud_tiny"]->DrawText(HUD.Player->Stats->Attributes.at(Attribute).Label, glm::ivec2(DrawPosition - CenterSpacing), ae::RIGHT_BASELINE);
		ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), glm::ivec2(DrawPosition + CenterSpacing));
		Buffer.str("");
		Row++;
	}
}

// Show/hide rebirth tab
void _InventoryScreen::ShowTabs() {
	if(!HUD.Player)
		return;

	// Determine if a rebirth stat is present
	bool ShowRebirthTab = false;
	for(const auto &Attribute : HUD.Player->Stats->RebirthTabAttributes) {
		if(HUD.Player->Character->Attributes.at(Attribute).Int != 0) {
			ShowRebirthTab = true;
			break;
		}
	}

	// Toggle tab
	ae::_Element *RebirthsTabElement = ae::Assets.Elements["button_inventory_tabs_rebirth"];
	RebirthsTabElement->SetEnabled(ShowRebirthTab);
}
