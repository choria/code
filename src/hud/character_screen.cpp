/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <hud/character_screen.h>
#include <ae/assets.h>
#include <ae/font.h>
#include <ae/input.h>
#include <ae/ui.h>
#include <ae/util.h>
#include <hud/hud.h>
#include <objects/components/character.h>
#include <objects/object.h>
#include <config.h>
#include <stats.h>
#include <SDL_keycode.h>
#include <iomanip>

// Init
void _CharacterScreen::Init() {
	Element->SetActive(true);
	CloseTimer = 0.0;
}

// Close
bool _CharacterScreen::Close(bool SendNotify, bool Delay) {
	bool WasOpen = Element->Active;

	if(Delay) {
		if(CloseTimer == 0.0)
			CloseTimer = HUD_CHARACTER_SCREEN_CLOSE_TIME;
	}
	else
		Element->SetActive(false);

	return WasOpen;
}

// Update
void _CharacterScreen::Update(double FrameTime) {

	// Update close timer
	if(CloseTimer > 0.0) {
		CloseTimer -= FrameTime;
		if(CloseTimer <= 0.0) {
			CloseTimer = 0.0;
			Element->SetActive(false);
		}
	}
}

// Render
void _CharacterScreen::Render(double BlendFactor) {
	if(!Element->Active || !HUD.Player)
		return;

	// Render background
	Element->Render();

	// Get font
	ae::_Font *Font = ae::Assets.Fonts["hud_char"];

	// Set up UI
	int SpacingY = Font->MaxAbove + Font->MaxBelow;
	glm::vec2 Spacing((int)(SpacingY * 0.5f), 0);
	glm::vec2 DrawPosition = Element->Bounds.Start;
	DrawPosition.x += (int)(Element->Size.x * 0.5f);
	DrawPosition.y += (int)(SpacingY * 1.5f);

	// Set up buffer
	std::stringstream Buffer;
	Buffer.imbue(std::locale(Config.Locale));
	Buffer << std::fixed << std::setprecision(2);

	// Handle weapon damage as special case
	if(ae::Input.ModKeyDown(KMOD_ALT)) {
		ae::FormatSI<double>(Buffer, (HUD.Player->Character->Attributes["MinDamage"].Int + HUD.Player->Character->Attributes["MaxDamage"].Int) * 0.5f);
	}
	else {
		ae::FormatSI<int64_t>(Buffer, HUD.Player->Character->Attributes["MinDamage"].Int);
		Buffer << " - ";
		ae::FormatSI<int64_t>(Buffer, HUD.Player->Character->Attributes["MaxDamage"].Int);
	}
	Font->DrawText("Weapon Damage", DrawPosition + -Spacing, ae::RIGHT_BASELINE);
	Font->DrawText(Buffer.str(), DrawPosition + Spacing);
	Buffer.str("");
	DrawPosition.y += SpacingY;

	// Display attributes
	int LastCategory = 1;
	for(const auto &AttributeName : HUD.Player->Stats->AttributeRank) {
		const _Attribute &Attribute = HUD.Player->Stats->Attributes.at(AttributeName);
		if(!Attribute.Show)
			continue;

		// Get category
		int Category = std::abs(Attribute.Show);
		bool AlwaysShow = Attribute.Show < 0;

		_Value &AttributeValue = HUD.Player->Character->Attributes[AttributeName];
		if(Attribute.UpdateType == StatUpdateType::MULTIPLICATIVE && AttributeValue.Int == 0)
			continue;

		if(!AlwaysShow && AttributeValue.Int == Attribute.Default.Int)
			continue;

		// Separator
		if(LastCategory != Category)
			DrawPosition.y += (int)(SpacingY * 0.7f);

		// Prefix
		Buffer << Attribute.Prefix;

		// Build buffer
		switch(Attribute.Type) {
			case StatValueType::INTEGER:
			case StatValueType::INTEGER64:
				if(Attribute.SI && !ae::Input.ModKeyDown(KMOD_ALT))
					ae::FormatSI<int64_t>(Buffer, AttributeValue.Int,  ae::RoundDown2);
				else
					Buffer << AttributeValue.Int;
			break;
			case StatValueType::DOUBLE:
				Buffer << AttributeValue.Double;
			break;
			case StatValueType::PERCENT:
				Buffer << AttributeValue.Int << "%";
			break;
			case StatValueType::PERCENT64:
				if(Attribute.SI && !ae::Input.ModKeyDown(KMOD_ALT))
					ae::FormatSI<int64_t>(Buffer, AttributeValue.Int, ae::RoundDown2);
				else
					Buffer << AttributeValue.Int;
				Buffer << "%";
			break;
			case StatValueType::PERCENT_DOUBLE: {
				if(ae::Input.ModKeyDown(KMOD_ALT))
					Buffer << std::fixed << std::setprecision(10);
				Buffer << AttributeValue.Double << "%";
				Buffer << std::fixed << std::setprecision(2);
			} break;
			case StatValueType::TIME:
				if(ae::Input.ModKeyDown(KMOD_ALT))
					Buffer << AttributeValue.Double << "s";
				else
					ae::FormatTimeHMS(Buffer, (int64_t)AttributeValue.Double);
			break;
			default:
			break;
		}

		if(Attribute.ShowMax) {
			_Value &AttributeMax = HUD.Player->Character->Attributes["Max" + AttributeName];
			Buffer << " (" << AttributeMax.Int << "%)";
		}

		// Draw values
		Font->DrawText(Attribute.Label, DrawPosition + -Spacing, ae::RIGHT_BASELINE);
		Font->DrawText(Buffer.str(), DrawPosition + Spacing);
		Buffer.str("");
		DrawPosition.y += SpacingY;

		LastCategory = Category;
	}
}
