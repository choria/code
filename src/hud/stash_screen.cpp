/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <hud/stash_screen.h>
#include <ae/assets.h>
#include <ae/ui.h>
#include <hud/hud.h>
#include <hud/inventory_screen.h>
#include <objects/components/character.h>
#include <objects/object.h>
#include <states/play.h>
#include <sstream>

// Initialize
void _StashScreen::Init() {
	if(!HUD.Player)
		return;

	HUD.Cursor.Reset();
	HUD.InventoryScreen->InitInventoryTab(0);
	Element->SetActive(true);
	PlayState.SendStatus(_Character::STATUS_INVENTORY);
}

// Close screen
bool _StashScreen::Close(bool SendNotify, bool Delay) {
	bool WasOpen = Element->Active;
	HUD.InventoryScreen->Close(SendNotify, Delay);
	if(HUD.Player)
		HUD.Player->Character->ViewingStash = 0;

	Element->SetActive(false);

	return WasOpen;
}

// Render
void _StashScreen::Render(double BlendFactor) {
	if(!HUD.Player || !HUD.Player->Character->ViewingStash) {
		Element->Active = false;
		return;
	}

	// Render main element
	Element->Render();

	// Draw stash items
	int BagIndex = 0;
	_Bag &Bag = HUD.Player->Inventory->GetBag(BagType::STASH);
	for(size_t i = 0; i < Bag.Slots.size(); i++) {

		// Get inventory slot
		_InventorySlot *Slot = &Bag.Slots[i];
		if(Slot->Item) {

			// Get bag button
			std::stringstream Buffer;
			Buffer << "button_stash_bag_" << BagIndex;
			ae::_Element *Button = ae::Assets.Elements[Buffer.str()];

			// Draw
			RenderInventoryItem(Button, Slot);
		}

		BagIndex++;
	}
}
