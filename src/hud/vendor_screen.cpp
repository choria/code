/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <hud/vendor_screen.h>
#include <ae/assets.h>
#include <ae/clientnetwork.h>
#include <ae/graphics.h>
#include <ae/ui.h>
#include <hud/hud.h>
#include <hud/inventory_screen.h>
#include <objects/components/character.h>
#include <objects/components/controller.h>
#include <objects/item.h>
#include <objects/object.h>
#include <states/play.h>
#include <packet.h>
#include <stats.h>
#include <sstream>

// Initialize
void _VendorScreen::Init() {
	if(!HUD.Player)
		return;

	HUD.Cursor.Reset();
	HUD.InventoryScreen->InitInventoryTab(0);
	Element->SetActive(true);

	// Set name
	const _Vendor *Vendor = HUD.Player->Character->Vendor;
	ae::Assets.Elements["label_vendor_name"]->Text = Vendor->Label;

	// Set dialogue
	ae::_Element *DialogueElement = ae::Assets.Elements["element_vendor_dialogue"];
	if(!Vendor->Dialogue.empty()) {
		ae::_Element *DialogueLabel = ae::Assets.Elements["label_vendor_dialogue"];
		DialogueLabel->Text = Vendor->Dialogue;
		DialogueLabel->SetWrap(DialogueLabel->Size.x);
		DialogueElement->SetActive(true);
	}
	else
		DialogueElement->SetActive(false);
}

// Close screen
bool _VendorScreen::Close(bool SendNotify, bool Delay) {
	bool WasOpen = Element->Active;
	HUD.InventoryScreen->Close(SendNotify, Delay);
	if(HUD.Player)
		HUD.Player->Character->Vendor = nullptr;

	Element->SetActive(false);

	return WasOpen;
}

// Render
void _VendorScreen::Render(double BlendFactor) {
	if(!HUD.Player || !HUD.Player->Character->Vendor) {
		Element->Active = false;
		return;
	}

	// Render main element
	Element->Render();

	// Draw vendor items
	for(size_t i = 0; i < HUD.Player->Character->Vendor->Items.size(); i++) {
		const _Item *Item = HUD.Player->Character->Vendor->Items[i];
		if(!Item)
			continue;

		// Don't draw item while dragging
		if(HUD.Cursor.IsEqual(i, _HUD::WINDOW_VENDOR))
			continue;

		// Get bag button
		std::stringstream Buffer;
		Buffer << "button_vendor_bag_" << i;
		ae::_Element *Button = ae::Assets.Elements[Buffer.str()];

		// Get position of slot
		glm::vec2 DrawPosition = (Button->Bounds.Start + Button->Bounds.End) / 2.0f;

		// Draw item
		ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos_uv"]);
		if(Item->Texture)
			ae::Graphics.DrawScaledImage(DrawPosition, Item->Texture, UI_SLOT_SIZE);

		// Draw price
		if(Item->CanBuy(HUD.Scripting, HUD.Player))
			HUD.DrawItemPrice(Item, 1, DrawPosition, true);
	}
}

// Buys an item
void _VendorScreen::BuyItem(_Cursor *Cursor, _Slot TargetSlot) {
	if(!HUD.Player)
		return;

	HUD.Player->Controller->WaitForServer = true;

	_Slot VendorSlot;
	VendorSlot.Index = Cursor->Slot.Index;

	// Notify server
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::VENDOR_EXCHANGE);
	Packet.WriteBit(1);
	Packet.Write<uint16_t>((uint16_t)Cursor->InventorySlot.Count);
	VendorSlot.Serialize(Packet);
	TargetSlot.Serialize(Packet);
	PlayState.Network->SendPacket(Packet);
}

// Sell an item
void _VendorScreen::SellItem(_Cursor *Cursor, int Amount) {
	if(!HUD.Player)
		return;

	const _Item *Item = Cursor->InventorySlot.Item;
	if(!Item || !HUD.Player->Character->Vendor)
		return;

	// Can't sell cursed equipped items
	if(Item->IsCursed() && Cursor->Slot.Type == BagType::EQUIPMENT)
		return;

	HUD.Player->Controller->WaitForServer = true;

	// Notify server
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::VENDOR_EXCHANGE);
	Packet.WriteBit(0);
	Packet.Write<uint16_t>((uint16_t)Amount);
	Cursor->Slot.Serialize(Packet);
	PlayState.Network->SendPacket(Packet);
}
