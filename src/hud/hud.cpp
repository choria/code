/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <hud/hud.h>
#include <ae/actions.h>
#include <ae/assets.h>
#include <ae/audio.h>
#include <ae/clientnetwork.h>
#include <ae/font.h>
#include <ae/graphics.h>
#include <ae/ui.h>
#include <ae/util.h>
#include <hud/blacksmith_screen.h>
#include <hud/character_screen.h>
#include <hud/disenchanter_screen.h>
#include <hud/enchanter_screen.h>
#include <hud/inventory_screen.h>
#include <hud/skill_screen.h>
#include <hud/stash_screen.h>
#include <hud/trader_screen.h>
#include <hud/trade_screen.h>
#include <hud/vendor_screen.h>
#include <objects/components/character.h>
#include <objects/components/controller.h>
#include <objects/components/fighter.h>
#include <objects/components/inventory.h>
#include <objects/battle.h>
#include <objects/buff.h>
#include <objects/item.h>
#include <objects/map.h>
#include <objects/minigame.h>
#include <objects/object.h>
#include <objects/status_effect.h>
#include <states/play.h>
#include <actiontype.h>
#include <config.h>
#include <framework.h>
#include <menu.h>
#include <packet.h>
#include <scripting.h>
#include <stats.h>
#include <glm/ext/scalar_constants.hpp>
#include <SDL_keycode.h>
#include <SDL_mouse.h>
#include <iomanip>
#include <regex>

_HUD HUD;

// Initialize
void _HUD::Init(_Scripting *Scripting) {
	this->Scripting = Scripting;
	Tooltip.Reset();
	Cursor.Reset();
	GoldGained.Init(100);

	ChatTextBox = ae::Assets.Elements["textbox_chat"];

	UpdateButtonBarLabels();
	ae::Assets.Elements["label_hud_pvp"]->Text = "";

	PartyMembersElement = ae::Assets.Elements["element_hud_party"];
	DarkOverlayElement = ae::Assets.Elements["element_dark_overlay"];
	ConfirmElement = ae::Assets.Elements["element_confirm"];
	DiedElement = ae::Assets.Elements["element_died"];
	StatusEffectsElement = ae::Assets.Elements["element_hud_statuseffects"];
	ActionBarElement = ae::Assets.Elements["element_actionbar"];
	ButtonBarElement = ae::Assets.Elements["element_buttonbar"];
	MinigameElement = ae::Assets.Elements["element_minigame"];
	PartyElement = ae::Assets.Elements["element_party"];
	TeleportElement = ae::Assets.Elements["element_teleport"];
	ChatElement = ae::Assets.Elements["element_chat"];
	HealthElement = ae::Assets.Elements["element_hud_health"];
	ManaElement = ae::Assets.Elements["element_hud_mana"];
	ExperienceElement = ae::Assets.Elements["element_hud_experience"];
	RecentItemsElement = ae::Assets.Elements["element_hud_recentitems"];
	RelicsElement = ae::Assets.Elements["element_hud_relics"];
	PartyTextBox = ae::Assets.Elements["textbox_party"];
	GoldElement = ae::Assets.Elements["label_hud_gold"];
	MessageElement = ae::Assets.Elements["element_hud_message"];
	MapNameElement = ae::Assets.Elements["element_hud_mapname"];
	MessageLabel = ae::Assets.Elements["label_hud_message"];
	RespawnInstructions = ae::Assets.Elements["label_died_respawn"];
	FullscreenElement = ae::Assets.Elements["button_fullscreen"];
	RelicElement = ae::Assets.Elements["image_hud_relic"];
	AmuletElement = ae::Assets.Elements["image_hud_amulet"];

	RelicsElement->SetActive(false);
	DarkOverlayElement->SetActive(false);
	ConfirmElement->SetActive(false);
	DiedElement->SetActive(false);
	StatusEffectsElement->SetActive(true);
	ActionBarElement->SetActive(true);
	ButtonBarElement->SetActive(true);
	MinigameElement->SetActive(false);
	PartyElement->SetActive(false);
	TeleportElement->SetActive(false);
	ChatElement->SetActive(false);
	HealthElement->SetActive(true);
	ManaElement->SetActive(true);
	ExperienceElement->SetActive(true);
	GoldElement->SetActive(true);
	MessageElement->SetActive(false);
	MapNameElement->SetActive(false);
	RecentItemsElement->SetActive(false);
	FullscreenElement->SetActive(true);
	PartyMembersElement->SetActive(true);
	HidePartyMembers();

	ae::Assets.Elements["label_hud_pvp"]->SetActive(true);
	ae::Assets.Elements["element_hud"]->SetActive(true);

	CharacterScreen = new _CharacterScreen(ae::Assets.Elements["element_character"]);
	InventoryScreen = new _InventoryScreen(ae::Assets.Elements["element_inventory"]);
	VendorScreen = new _VendorScreen(ae::Assets.Elements["element_vendor"]);
	TradeScreen = new _TradeScreen(ae::Assets.Elements["element_trade"]);
	TraderScreen = new _TraderScreen(ae::Assets.Elements["element_trader"]);
	BlacksmithScreen = new _BlacksmithScreen(ae::Assets.Elements["element_blacksmith"]);
	EnchanterScreen = new _EnchanterScreen(ae::Assets.Elements["element_enchanter"]);
	DisenchanterScreen = new _DisenchanterScreen(ae::Assets.Elements["element_disenchanter"]);
	SkillScreen = new _SkillScreen(ae::Assets.Elements["element_skills"]);
	StashScreen = new _StashScreen(ae::Assets.Elements["element_stash"]);
}

// Shutdown
void _HUD::Close() {
	Reset();
	GoldGained.Close();

	delete CharacterScreen;
	delete InventoryScreen;
	delete VendorScreen;
	delete TradeScreen;
	delete TraderScreen;
	delete BlacksmithScreen;
	delete EnchanterScreen;
	delete DisenchanterScreen;
	delete SkillScreen;
	delete StashScreen;
}

// Build prefix for character name
void _HUD::GetPlayerPrefix(std::stringstream &Buffer, uint32_t Rebirths, uint32_t Evolves, uint32_t Transforms, bool SI) {
	if(Transforms)
		Buffer << "[c blood]" + std::to_string(Transforms) + "[c white] ";
	if(Evolves)
		Buffer << "[c silver]" + std::to_string(Evolves) + "[c white] ";
	if(Rebirths) {
		Buffer << "[c gold]";
		if(SI)
			ae::FormatSI(Buffer, (double)Rebirths, ae::RoundDown1);
		else
			Buffer << Rebirths;
		Buffer << "[c white] ";
	}
}

// Get a boss cooldown message given time left
const char *_HUD::GetBossCooldownMessage(int MessageID) {
	switch(MessageID) {
		case 0: return "The soul rests for eternity";
		case 1: return "The soul is upon you";
		case 2: return "The soul radiates";
		case 3: return "The soul shines";
		case 4: return "The soul glows";
		case 5: return "The soul shimmers";
		case 6: return "The soul is faint";
		case 7: return "The soul has vanished... for now";
		case 8: return "The soul has been... banished?";
		case 9: return "The soul has been banished";
	}

	return "";
}

// Reset state
void _HUD::Reset(bool ClearRecentItems) {
	delete Minigame;
	Minigame = nullptr;

	CloseWindows(false);
	SkillScreen->ClearSkills();
	EnchanterScreen->ClearSkills();
	DisenchanterScreen->ClearSkills();
	if(ClearRecentItems)
		RecentItems.clear();
}

// Reset chat history and messages
void _HUD::ResetChat() {
	ChatHistory.clear();
	SentHistory.clear();
	SetMessage("", 0);
}

// Handle the enter key
void _HUD::HandleEnter() {

	if(IsTypingGold()) {
		ae::FocusedElement = nullptr;
		TradeScreen->ValidateTradeGold();
	}
	else if(IsTypingParty()) {
		ae::FocusedElement = nullptr;
		SendPartyName();
	}
	else {
		ToggleChat();
	}
}

// Mouse events
void _HUD::HandleMouseButton(const ae::_MouseEvent &MouseEvent) {
	if(!Player)
		return;

	// Press
	if(MouseEvent.Pressed) {

		// Update minigame
		if(Player->Character->Minigame && Minigame) {
			if(Minigame->State == _Minigame::StateType::CANDROP && Player->Inventory->CountItem(Player->Character->Minigame->RequiredItem) >= Player->Character->Minigame->Cost) {
				Minigame->HandleMouseButton(MouseEvent);

				// Pay
				if(Minigame->State == _Minigame::StateType::DROPPED) {
					ae::_Buffer Packet;
					Packet.Write<PacketType>(PacketType::MINIGAME_PAY);
					PlayState.Network->SendPacket(Packet);

					Player->Character->Attributes["GamesPlayed"].Int++;
					PlayState.PlayCoinSound();
				}
			}
		}

		// Handle clicking status effect
		if(Tooltip.StatusEffect && Tooltip.StatusEffect->Buff->Dismiss && !Player->Battle) {

			// Swap buff with other type
			if(MouseEvent.Button == SDL_BUTTON_LEFT) {
				if(Tooltip.StatusEffect->Buff->SwapBuffID) {
					ae::_Buffer Packet;
					Packet.Write<PacketType>(PacketType::PLAYER_SWAPBUFF);
					Packet.Write<uint32_t>(Tooltip.StatusEffect->Buff->ID);
					PlayState.Network->SendPacket(Packet);
				}
			}
			// Dismiss status effect
			else if(MouseEvent.Button == SDL_BUTTON_RIGHT) {
				ae::_Buffer Packet;
				Packet.Write<PacketType>(PacketType::PLAYER_CLEARBUFF);
				Packet.Write<uint32_t>(Tooltip.StatusEffect->Buff->ID);
				PlayState.Network->SendPacket(Packet);
			}
		}

		if(Tooltip.InventorySlot.Item) {
			switch(Tooltip.Window) {
				case WINDOW_TRADEYOURS:
				case WINDOW_EQUIPMENT:
				case WINDOW_INVENTORY:
				case WINDOW_STASH:
					if(MouseEvent.Button == SDL_BUTTON_LEFT) {

						// Upgrade
						if(ae::Input.ModKeyDown(KMOD_CTRL)) {
							if(Player->Character->Blacksmith && Player->Character->Blacksmith->CanUpgrade(Tooltip.InventorySlot.Item, Tooltip.InventorySlot.Count, Tooltip.InventorySlot.Upgrades)) {
								ae::_Buffer Packet;
								Packet.Write<PacketType>(PacketType::BLACKSMITH_UPGRADE);
								Packet.Write<uint8_t>(ae::Input.ModKeyDown(KMOD_SHIFT) ? 5 : 1);
								Tooltip.Slot.Serialize(Packet);
								PlayState.Network->SendPacket(Packet);
							}
							else
								SplitStack(Tooltip.Slot, 1 + (INVENTORY_SPLIT_MODIFIER - 1) * ae::Input.ModKeyDown(KMOD_SHIFT));
						}
						// Transfer between trade/inventory
						else if(ae::Input.ModKeyDown(KMOD_SHIFT))
							Transfer(Tooltip.Slot);
						// Pickup item
						else
							Cursor = Tooltip;
					}
					// Use or sell an item
					else if(MouseEvent.Button == SDL_BUTTON_RIGHT) {

						// Quick sell item
						if((ae::Input.ModKeyDown(KMOD_SHIFT) || (Config.RightClickSell && Player->Character->Vendor && !ae::Input.ModKeyDown(KMOD_CTRL)))) {
							if(Tooltip.InventorySlot.Item->CanQuickSell()) {
								int Amount = 1;
								if(Tooltip.InventorySlot.Item->BulkBuy && ae::Input.ModKeyDown(KMOD_CTRL))
									Amount += (INVENTORY_SPLIT_MODIFIER - 1);

								VendorScreen->SellItem(&Tooltip, Amount);
							}
						}
						// Use item
						else if(!Tooltip.InventorySlot.Item->IsCursed()) {
							ae::_Buffer Packet;
							Packet.Write<PacketType>(PacketType::INVENTORY_USE);
							Packet.WriteBit(ae::Input.ModKeyDown(KMOD_CTRL));
							Tooltip.Slot.Serialize(Packet);
							PlayState.Network->SendPacket(Packet);
							auto &Item = Tooltip.InventorySlot.Item;
							if(Item->IsEquippable() || Item->Wait)
								Player->Controller->WaitForServer = true;
						}
					}
				break;
				case WINDOW_VENDOR:
					if(MouseEvent.Button == SDL_BUTTON_LEFT) {
						Cursor = Tooltip;
					}
					else if(MouseEvent.Button == SDL_BUTTON_RIGHT) {
						if(Tooltip.Window == WINDOW_VENDOR)
							VendorScreen->BuyItem(&Tooltip);
					}
				break;
				case WINDOW_SKILLBAR:
					if(MouseEvent.Button == SDL_BUTTON_LEFT) {

						// Start dragging skill
						if(SkillScreen->Element->Active || InventoryScreen->Element->Active)
							Cursor = Tooltip;
					}
					else if(MouseEvent.Button == SDL_BUTTON_RIGHT) {

						// Unequip skill
						if(Tooltip.Slot.Index < ACTIONBAR_BELT_STARTS) {
							_Action Action;
							SetActionBar(Tooltip.Slot.Index, Player->Character->ActionBar.size(), Action);
						}
					}
				break;
				case WINDOW_SKILLS:
					if(ae::Graphics.Element->HitElement && ae::Graphics.Element->HitElement->ID == "button_skills_skill") {
						if(MouseEvent.Button == SDL_BUTTON_LEFT) {
							if(Player->Character->Skills[Tooltip.InventorySlot.Item->ID] > 0)
								Cursor = Tooltip;
						}
						else if(MouseEvent.Button == SDL_BUTTON_RIGHT) {
							SkillScreen->EquipSkill(Tooltip.InventorySlot.Item->ID);
						}
					}
				break;
				case WINDOW_BLACKSMITH:
					if(Tooltip.Element && Tooltip.Element->ID == "button_blacksmith_bag") {
						BlacksmithScreen->UpgradeSlot.Reset();
					}
				break;
			}
		}
		else if(Tooltip.Window == WINDOW_RELICS) {
			if(MouseEvent.Button == SDL_BUTTON_LEFT || MouseEvent.Button == SDL_BUTTON_RIGHT) {
				_Slot Slot(BagType::INVENTORY, (size_t)Tooltip.Element->UserData);
				ae::_Buffer Packet;
				Packet.Write<PacketType>(PacketType::INVENTORY_USE);
				Packet.WriteBit(false);
				Slot.Serialize(Packet);
				PlayState.Network->SendPacket(Packet);
				Player->Controller->WaitForServer = true;
				if(MouseEvent.Button == SDL_BUTTON_LEFT)
					CloseWindows(true);
			}
		}
	}
	// Handle gold buttons
	else if(TradeScreen->Element->GetClickedElement() && TradeScreen->Element->GetClickedElement()->ID.find("button_trade_gold_") == 0) {
		int Index = TradeScreen->Element->GetClickedElement()->Index;
		ae::_Element *GoldTextBox = ae::Assets.Elements["textbox_trade_gold_yours"];

		// Get existing gold amount
		std::regex Regex("[^0-9]");
		GoldTextBox->Text = std::regex_replace(GoldTextBox->Text, Regex, "");
		int64_t ExistingGold = ae::ToNumber<int64_t>(GoldTextBox->Text);

		// Handle all/none button
		if(Index == 0) {
			if(ExistingGold > 0)
				GoldTextBox->Text = "0";
			else
				GoldTextBox->Text = std::to_string(Player->Character->Attributes["Gold"].Int);
		}
		else {
			int64_t Adjust = pow(1e3, Index);
			if(MouseEvent.Button == SDL_BUTTON_RIGHT)
				Adjust = -Adjust;

			if(ae::Input.ModKeyDown(KMOD_SHIFT))
				Adjust *= 5;
			if(ae::Input.ModKeyDown(KMOD_CTRL))
				Adjust *= 50;

			ExistingGold = std::max(ExistingGold + Adjust, (int64_t)0);
			GoldTextBox->Text = std::to_string(ExistingGold);
		}

		TradeScreen->ValidateTradeGold();
	}
	// Release left mouse button
	else if(MouseEvent.Button == SDL_BUTTON_LEFT) {

		// Check confirm screen
		if(ConfirmElement->GetClickedElement()) {
			if(ConfirmElement->GetClickedElement()->ID == "button_confirm_ok") {

				// Delete item
				if(Player->Inventory->IsValidSlot(DeleteSlot)) {
					ae::_Buffer Packet;
					Packet.Write<PacketType>(PacketType::INVENTORY_DELETE);
					DeleteSlot.Serialize(Packet);
					PlayState.Network->SendPacket(Packet);

					if(DeleteSlot == BlacksmithScreen->UpgradeSlot)
						BlacksmithScreen->UpgradeSlot.Reset();

					if(DeleteSlot.Type == BagType::TRADE)
						TradeScreen->ResetAcceptButton();

					if(DeleteSlot.Type == BagType::EQUIPMENT)
						Player->Controller->WaitForServer = true;
				}

				CloseConfirm();
			}
			else if(ConfirmElement->GetClickedElement()->ID == "button_confirm_cancel") {
				CloseConfirm();
			}
		}
		else if(DarkOverlayElement->GetClickedElement()) {
			return;
		}
		// Check button bar
		else if(ButtonBarElement->GetClickedElement()) {
			if(ButtonBarElement->GetClickedElement()->ID == "button_buttonbar_join") {
				PlayState.SendJoinRequest();
			}
			else if(ButtonBarElement->GetClickedElement()->ID == "button_buttonbar_inventory") {
				InventoryScreen->Toggle();
			}
			else if(ButtonBarElement->GetClickedElement()->ID == "button_buttonbar_trade") {
				TradeScreen->Toggle();
			}
			else if(ButtonBarElement->GetClickedElement()->ID == "button_buttonbar_party") {
				ToggleParty(false);
			}
			else if(ButtonBarElement->GetClickedElement()->ID == "button_buttonbar_relics") {
				ToggleRelics();
			}
			else if(ButtonBarElement->GetClickedElement()->ID == "button_buttonbar_skills") {
				SkillScreen->Toggle();
			}
			else if(ButtonBarElement->GetClickedElement()->ID == "button_buttonbar_menu") {
				ToggleInGameMenu(true);
			}
		}
		else if(FullscreenElement->GetClickedElement() && FullscreenElement->GetClickedElement()->ID == "button_fullscreen") {
			Menu.SetFullscreen(!Config.Fullscreen);
		}
		// Check inventory tabs
		else if(InventoryScreen->TabsElement->GetClickedElement()) {
			InventoryScreen->InitInventoryTab(InventoryScreen->TabsElement->GetClickedElement()->Index);
		}
		// Move last privilege items to stash
		else if(InventoryScreen->BagsElement->GetClickedElement() && InventoryScreen->BagsElement->GetClickedElement()->ID == "button_inventory_move") {
			ae::_Buffer Packet;
			Packet.Write<PacketType>(PacketType::INVENTORY_PRIVILEGE);
			PlayState.Network->SendPacket(Packet);
			Player->Controller->WaitForServer = true;
		}
		// Check skill level up/down
		else if(SkillScreen->Element->GetClickedElement()) {
			if(SkillScreen->Element->GetClickedElement()->ID == "button_skills_plus")
				SkillScreen->AdjustSkillLevel((uint32_t)SkillScreen->Element->GetClickedElement()->Index, GetSkillModifierAmount());
			else if(SkillScreen->Element->GetClickedElement()->ID == "button_skills_minus")
				SkillScreen->AdjustSkillLevel((uint32_t)SkillScreen->Element->GetClickedElement()->Index, -GetSkillModifierAmount());
		}
		// Check enchanter buy
		else if(EnchanterScreen->Element->GetClickedElement()) {
			if(EnchanterScreen->Element->GetClickedElement()->ID == "button_max_skill_levels_buy") {
				uint32_t SkillID = (uint32_t)EnchanterScreen->Element->GetClickedElement()->Index;
				ae::_Buffer Packet;
				Packet.Write<PacketType>(PacketType::ENCHANTER_BUY);
				Packet.Write<uint32_t>(SkillID);
				Packet.Write<uint8_t>(GetSkillModifierAmount());
				PlayState.Network->SendPacket(Packet);
			}
		}
		// Check disenchanter sell
		else if(DisenchanterScreen->Element->GetClickedElement()) {
			if(DisenchanterScreen->Element->GetClickedElement()->ID == "button_max_skill_levels_sell") {
				uint32_t SkillID = (uint32_t)DisenchanterScreen->Element->GetClickedElement()->Index;
				ae::_Buffer Packet;
				Packet.Write<PacketType>(PacketType::DISENCHANTER_SELL);
				Packet.Write<uint32_t>(SkillID);
				Packet.Write<uint8_t>(GetSkillModifierAmount());
				PlayState.Network->SendPacket(Packet);
			}
		}
		// Accept trader button
		else if(TraderScreen->Element->GetClickedElement() == ae::Assets.Elements["button_trader_accept"]) {
			TraderScreen->TradeItem();
		}
		// Cancel trader button
		else if(TraderScreen->Element->GetClickedElement() == ae::Assets.Elements["button_trader_cancel"]) {
			CloseWindows(true);
		}
		// Upgrade item
		else if(BlacksmithScreen->Element->GetClickedElement() == ae::Assets.Elements["button_blacksmith_upgrade"]) {
			if(Player->Inventory->IsValidSlot(BlacksmithScreen->UpgradeSlot)) {
				ae::_Buffer Packet;
				Packet.Write<PacketType>(PacketType::BLACKSMITH_UPGRADE);
				Packet.Write<uint8_t>(GetSkillModifierAmount());
				BlacksmithScreen->UpgradeSlot.Serialize(Packet);
				PlayState.Network->SendPacket(Packet);
			}
		}
		// Upgrade set
		else if(BlacksmithScreen->Element->GetClickedElement() == ae::Assets.Elements["button_blacksmith_upgrade_set"]) {
			if(Player->Character->Blacksmith && Player->Inventory->CanUpgradeSet(Player->Character->Blacksmith->Level)) {
				ae::_Buffer Packet;
				Packet.Write<PacketType>(PacketType::BLACKSMITH_UPGRADESET);
				Packet.Write<uint8_t>(GetSkillModifierAmount());
				PlayState.Network->SendPacket(Packet);
			}
		}
		// Released an item
		else if(Cursor.InventorySlot.Item) {

			// Check source window
			switch(Cursor.Window) {
				case WINDOW_TRADEYOURS:
				case WINDOW_EQUIPMENT:
				case WINDOW_INVENTORY:
				case WINDOW_STASH:

					// Check destination window
					switch(Tooltip.Window) {

						// Send inventory move packet
						case WINDOW_EQUIPMENT:
						case WINDOW_INVENTORY:
						case WINDOW_TRADEYOURS:
						case WINDOW_STASH:
							if(Player->Inventory->IsValidSlot(Tooltip.Slot) && Player->Inventory->IsValidSlot(Cursor.Slot) && Cursor.Slot != Tooltip.Slot) {
								ae::_Buffer Packet;
								Packet.Write<PacketType>(PacketType::INVENTORY_MOVE);
								Cursor.Slot.Serialize(Packet);
								Tooltip.Slot.Serialize(Packet);
								PlayState.Network->SendPacket(Packet);
								if(Cursor.Slot.Type == BagType::EQUIPMENT || Tooltip.Slot.Type == BagType::EQUIPMENT)
									Player->Controller->WaitForServer = true;

								// Remove upgrade item from upgrade window
								if(Cursor.Slot == BlacksmithScreen->UpgradeSlot || Tooltip.Slot == BlacksmithScreen->UpgradeSlot)
									BlacksmithScreen->UpgradeSlot.Type = BagType::NONE;
							}
						break;
						// Sell an item
						case WINDOW_VENDOR:
							VendorScreen->SellItem(&Cursor, Cursor.InventorySlot.Count);
						break;
						// Upgrade an item
						case WINDOW_BLACKSMITH:

							// Replace item if dragging onto upgrade slot only
							if(Cursor.InventorySlot.Item->IsEquippable() && Tooltip.Slot.Index != 1)
								BlacksmithScreen->UpgradeSlot = Cursor.Slot;
						break;
						// Move item to actionbar
						case WINDOW_SKILLBAR:
							if((Cursor.Window == WINDOW_EQUIPMENT || Cursor.Window == WINDOW_INVENTORY) && !Cursor.InventorySlot.Item->IsSkill())
								SetActionBar(Tooltip.Slot.Index, Player->Character->ActionBar.size(), Cursor.InventorySlot.Item);
							else if(Cursor.Window == WINDOW_SKILLBAR)
								SetActionBar(Tooltip.Slot.Index, Cursor.Slot.Index, Cursor.InventorySlot.Item);
						break;
						// Delete item
						case -1: {
							if(!ae::Graphics.Element->HitElement && Cursor.InventorySlot.Item) {

								// Can't deleted cursed equipped items
								if(Cursor.InventorySlot.Item->IsCursed() && Cursor.Slot.Type == BagType::EQUIPMENT)
									break;

								// Check delete stat
								if(!Cursor.InventorySlot.Item->Attributes.at("Delete").Int) {
									SetMessage("Item cannot be deleted", HUD_MESSAGE_SHORT_TIMEOUT);
									break;
								}

								InitConfirm("Delete [c green]" + Cursor.InventorySlot.Item->Name + "[c white]?", Cursor.InventorySlot.Item->Texture, Cursor.InventorySlot.Count);
								DeleteSlot = Cursor.Slot;
							}
						} break;
					}
				break;
				// Buy an item
				case WINDOW_VENDOR:
					if(Tooltip.Window == WINDOW_EQUIPMENT || Tooltip.Window == WINDOW_INVENTORY) {
						BagType BagType = GetBagFromWindow(Tooltip.Window);
						VendorScreen->BuyItem(&Cursor, _Slot(BagType, Tooltip.Slot.Index));
					}
				break;
				// Drag item from actionbar
				case WINDOW_SKILLBAR:
					switch(Tooltip.Window) {
						case WINDOW_EQUIPMENT:
						case WINDOW_INVENTORY:
						case WINDOW_STASH:

							// Swap actionbar with inventory
							if(Tooltip.InventorySlot.Item && !Tooltip.InventorySlot.Item->IsSkill())
								SetActionBar(Cursor.Slot.Index, Player->Character->ActionBar.size(), Tooltip.InventorySlot.Item);
							else
								SetActionBar(Cursor.Slot.Index, Player->Character->ActionBar.size(), nullptr);
						break;
						case WINDOW_SKILLBAR:
							SetActionBar(Tooltip.Slot.Index, Cursor.Slot.Index, Cursor.InventorySlot.Item);
						break;
						default:

							// Remove action
							if(Tooltip.Slot.Index >= Player->Character->ActionBar.size() || Tooltip.Window == -1) {
								_Action Action;
								SetActionBar(Cursor.Slot.Index, Player->Character->ActionBar.size(), Action);
							}
						break;
					}
				break;
				case WINDOW_SKILLS:
					if(Tooltip.Window == WINDOW_SKILLBAR) {
						SetActionBar(Tooltip.Slot.Index, Player->Character->ActionBar.size(), Cursor.InventorySlot.Item);
					}
				break;
			}
		}
		// Use action
		else if(ActionBarElement->GetClickedElement()) {
			uint8_t Slot = (uint8_t)ActionBarElement->GetClickedElement()->Index;
			if(Player->Battle) {
				if(Slot >= ACTIONBAR_BELT_STARTS)
					Player->Battle->ClientHandleInput(Action::GAME_ITEM1 + Slot - ACTIONBAR_BELT_STARTS);
				else
					Player->Battle->ClientHandleInput(Action::GAME_SKILL1 + Slot);
			}
			else
				PlayState.SendActionUse(Slot);
		}
		// Handle mouse click during combat
		else if(EnableMouseCombat && Player->Battle && !Player->Character->Action.Confirmed) {
			Player->Battle->ClientSetAction((uint8_t)Player->Character->Action.ActionBarSlot);
		}

		if(Player->Character->WaitingForTrade) {
			if(MouseEvent.Button == SDL_BUTTON_LEFT) {
				if(!Cursor.InventorySlot.Item) {

					// Check for accept button
					if(TradeScreen->Element->GetClickedElement() == TradeScreen->AcceptButton) {
						TradeScreen->AcceptButton->Checked = !TradeScreen->AcceptButton->Checked;
						TradeScreen->UpdateAcceptButton();

						ae::_Buffer Packet;
						Packet.Write<PacketType>(PacketType::TRADE_ACCEPT);
						Packet.Write<char>(TradeScreen->AcceptButton->Checked);
						PlayState.Network->SendPacket(Packet);
					}
				}
			}
		}

		Cursor.Reset();
	}
}

// Updates the HUD
void _HUD::Update(double FrameTime) {
	if(!Player)
		return;

	// Get hovered element
	Tooltip.Reset();
	ae::_Element *HitElement = ae::Graphics.Element->HitElement;

	// Update screens
	CharacterScreen->Update(FrameTime);

	// Handle hovered elements
	if(!DarkOverlayElement->Active && HitElement) {
		Tooltip.Slot.Index = (size_t)HitElement->Index;
		Tooltip.Element = HitElement;

		// Get window id, stored in parent's userdata field
		if(HitElement->Parent && Tooltip.Slot.Index != NOSLOT) {
			Tooltip.Window = HitElement->Parent->Index;
			Tooltip.Slot.Type = GetBagFromWindow(Tooltip.Window);
		}

		switch(Tooltip.Window) {
			case WINDOW_EQUIPMENT:
			case WINDOW_INVENTORY:
			case WINDOW_TRADEYOURS:
			case WINDOW_STASH: {
				if(Player->Inventory->IsValidSlot(Tooltip.Slot)) {
					Tooltip.InventorySlot = Player->Inventory->GetSlot(Tooltip.Slot);
					if(Tooltip.InventorySlot.Item) {
						if(Player->Character->Vendor) {
							Tooltip.Cost = Tooltip.InventorySlot.Item->GetPrice(Scripting, Player, Player->Character->Vendor, Tooltip.InventorySlot.Count, false, Tooltip.InventorySlot.Upgrades);
						}
						else if(Player->Character->Blacksmith && Player->Character->Blacksmith->CanUpgrade(Tooltip.InventorySlot.Item, Tooltip.InventorySlot.Count, Tooltip.InventorySlot.Upgrades)) {
							Tooltip.InventorySlot = Player->Inventory->GetSlot(Tooltip.Slot);

							// Get total upgrade cost
							int Amount = ae::Input.ModKeyDown(KMOD_SHIFT) ? 5 : 1;
							for(int i = 0; i < Amount; i++) {
								int NextUpgrade = Tooltip.InventorySlot.Upgrades + i + 1;
								if(NextUpgrade > Tooltip.InventorySlot.Item->MaxLevel)
									break;

								Tooltip.Cost += Tooltip.InventorySlot.Item->GetUpgradeCost(Player, NextUpgrade);
								if(!Player->Character->Blacksmith->CanUpgrade(Tooltip.InventorySlot.Item, Tooltip.InventorySlot.Count, NextUpgrade))
									break;
							}
						}
					}
				}
			} break;
			case WINDOW_TRADETHEIRS: {
				if(Player->Character->TradePlayer && Player->Character->TradePlayer->Inventory->IsValidSlot(Tooltip.Slot)) {
					Tooltip.InventorySlot = Player->Character->TradePlayer->Inventory->GetSlot(Tooltip.Slot);
				}
			} break;
			case WINDOW_VENDOR: {
				if(Player->Character->Vendor && Tooltip.Slot.Index < Player->Character->Vendor->Items.size()) {
					Tooltip.InventorySlot.Item = Player->Character->Vendor->Items[Tooltip.Slot.Index];
					if(ae::Input.ModKeyDown(KMOD_SHIFT) && Tooltip.InventorySlot.Item->BulkBuy)
						Tooltip.InventorySlot.Count = INVENTORY_INCREMENT_MODIFIER;
					else
						Tooltip.InventorySlot.Count = 1;

					if(Tooltip.InventorySlot.Item) {
						Tooltip.Cost = Tooltip.InventorySlot.Item->GetPrice(Scripting, Player, Player->Character->Vendor, Tooltip.InventorySlot.Count, true);
						if(ae::Input.ModKeyDown(KMOD_CTRL) && ae::Input.MouseDown(SDL_BUTTON_RIGHT) && BuyCooldown <= 0.0) {
							BuyCooldown = VENDOR_BUY_COOLDOWN;
							VendorScreen->BuyItem(&Tooltip);
						}
					}
				}
			} break;
			case WINDOW_TRADER: {
				if(Player->Character->Trader) {
					if(Tooltip.Slot.Index < Player->Character->Trader->Items.size())
						Tooltip.InventorySlot.Item = Player->Character->Trader->Items[Tooltip.Slot.Index].Item;
					else if(Tooltip.Slot.Index == TRADER_MAXITEMS)
						Tooltip.InventorySlot.Item = Player->Character->Trader->RewardItem;

					if(Tooltip.Element->ID == "button_trader_accept" && ae::Input.ModKeyDown(KMOD_CTRL) && ae::Input.MouseDown(SDL_BUTTON_LEFT) && BuyCooldown <= 0.0) {
						BuyCooldown = TRADER_COOLDOWN;
						TraderScreen->TradeItem();
					}
				}
			} break;
			case WINDOW_BLACKSMITH: {
				if(Player->Character->Blacksmith && Player->Inventory->IsValidSlot(BlacksmithScreen->UpgradeSlot)) {
					Tooltip.InventorySlot = Player->Inventory->GetSlot(BlacksmithScreen->UpgradeSlot);
					if(Tooltip.InventorySlot.Item && Tooltip.InventorySlot.Upgrades < Tooltip.InventorySlot.Item->MaxLevel) {
						Tooltip.InventorySlot.Upgrades++;
					}
				}
			} break;
			case WINDOW_SKILLS: {
				Tooltip.InventorySlot.Item = PlayState.Stats->Items.at((uint32_t)Tooltip.Slot.Index);
			} break;
			case WINDOW_ENCHANTER: {
				uint32_t SkillID = (uint32_t)Tooltip.Slot.Index;
				const _Item *Skill = Tooltip.InventorySlot.Item = PlayState.Stats->Items.at(SkillID);

				// Get total upgrade cost
				int MaxSkillLevel = Player->Character->MaxSkillLevels[SkillID];
				int Amount = GetSkillModifierAmount();
				for(int i = 0; i < Amount; i++) {
					int SkillLevel = MaxSkillLevel + i;
					if(SkillLevel >= Skill->MaxLevel || SkillLevel >= Player->Character->Enchanter->Level)
						break;

					Tooltip.Cost += _Item::GetEnchantCost(Player, SkillLevel);
				}
			} break;
			case WINDOW_DISENCHANTER: {
				uint32_t SkillID = (uint32_t)Tooltip.Slot.Index;
				Tooltip.InventorySlot.Item = PlayState.Stats->Items.at(SkillID);

				// Get total sell amount
				int MinSkillLevel = Player->Character->MinSkillLevels[SkillID];
				int MaxSkillLevel = Player->Character->MaxSkillLevels[SkillID] - 1;
				int Amount = ae::Input.ModKeyDown(KMOD_SHIFT) ? 5 : 1;
				for(int i = 0; i < Amount; i++) {
					int SkillLevel = MaxSkillLevel - i;
					if(SkillLevel < MinSkillLevel)
						break;

					Tooltip.Cost += _Item::GetEnchantCost(Player, SkillLevel) * Player->Character->Disenchanter->Percent;
				}
			} break;
			case WINDOW_SKILLBAR: {
				if(Tooltip.Slot.Index < Player->Character->ActionBar.size())
					Tooltip.InventorySlot.Item = Player->Character->ActionBar[Tooltip.Slot.Index].Item;
			} break;
			case WINDOW_BATTLE: {
				_Object *MouseObject = (_Object *)HitElement->UserData;
				if(!MouseObject)
					break;

				auto &Action = Player->Character->Action;
				if(!Action.Item)
					break;

				auto &Battle = Player->Battle;
				if(!Battle)
					break;

				// Set target based on mouse cursor
				if(EnableMouseCombat && Action.Item->UseMouseTargeting() && Action.Item->CanTarget(Scripting, Player, MouseObject))
					Action.Target = Battle->SetTargets(Player, Action, MouseObject, Battle->ClientTargets);
			} break;
			case WINDOW_HUD_EFFECTS: {
				Tooltip.StatusEffect = (_StatusEffect *)HitElement->UserData;
			} break;
		}
	}

	// Show certain equipped items on HUD
	const auto &Slots = Player->Inventory->GetBag(BagType::EQUIPMENT).Slots;
	const _Item *RelicItem = Slots[EquipmentType::RELIC].Item;
	const _Item *AmuletItem = Slots[EquipmentType::AMULET].Item;
	RelicElement->Texture = RelicItem ? RelicItem->Texture : nullptr;
	AmuletElement->Texture = AmuletItem ? AmuletItem->Texture : nullptr;

	// Update skill point number
	int SkillPoints = Player->Character->GetSkillPointsAvailable();
	if(SkillPoints) {
		std::stringstream Buffer;
		ae::FormatSI<int64_t>(Buffer, SkillPoints, ae::RoundDown1);
		ae::Assets.Elements["label_buttonbar_skillpoints"]->Text = Buffer.str();
	}
	else
		ae::Assets.Elements["label_buttonbar_skillpoints"]->Text = "";

	// Update trade UI
	if(Player->Character->WaitingForTrade) {
		ae::Assets.Elements["element_trade_theirs"]->SetActive(false);
		if(Player->Character->TradePlayer) {
			ae::Assets.Elements["element_trade_theirs"]->SetActive(true);
			ae::Assets.Elements["label_trade_status"]->SetActive(false);
			ae::Assets.Elements["label_trade_status_restrict"]->SetActive(false);

			int64_t TheirGold = Player->Character->TradePlayer->Character->TradeGold;

			// Update their trade gold display
			ae::_Element *TheirGoldElement = ae::Assets.Elements["textbox_trade_gold_theirs"];
			std::stringstream Buffer;
			Buffer.imbue(std::locale(Config.Locale));
			Buffer << TheirGold;
			TheirGoldElement->Font = TheirGold >= 1e15 ? ae::Assets.Fonts["hud_char"] : ae::Assets.Fonts["hud_small"];
			TheirGoldElement->Text = Buffer.str();
			Buffer.str("");

			// Update short gold label
			ae::_Element *TheirGoldShortElement = ae::Assets.Elements["label_trade_gold_theirs_short"];
			if(TheirGold >= 1e4) {
				ae::FormatSI<int64_t>(Buffer, TheirGold, ae::RoundDown2);
				TheirGoldShortElement->Text = Buffer.str();
			}
			else
				TheirGoldShortElement->Text = "";

			// Update name and portrait
			ae::Assets.Elements["label_trade_name_theirs"]->Text = Player->Character->TradePlayer->Name;
			ae::Assets.Elements["image_trade_portrait_theirs"]->Texture = Player->Character->TradePlayer->Character->Portrait;
		}
	}

	// Handle typing in chat
	if(IsChatting()) {

		// Resize text box
		ae::_TextBounds TextBounds;
		ChatTextBox->Font->GetStringDimensions(ChatTextBox->Text, TextBounds);
		float BaseTextSize = std::max(HUD_CHAT_TEXTBOX_WIDTH, TextBounds.Width / ae::_Element::GetUIScale());
		ChatElement->BaseSize.x = BaseTextSize + 20;
		ChatElement->CalculateBounds();

		// Close chat if it loses focus
		if(ChatTextBox != ae::FocusedElement)
			CloseChat();
	}

	// Update stat changes
	for(auto Iterator = StatChanges.begin(); Iterator != StatChanges.end(); ) {
		_StatChangeUI &StatChangeUI = *Iterator;

		// Find start position
		StatChangeUI.LastPosition = StatChangeUI.Position;

		// Interpolate between start and end position
		StatChangeUI.Position = glm::mix(StatChangeUI.StartPosition, StatChangeUI.StartPosition + glm::vec2(0, HUD_STATCHANGE_DISTANCE * StatChangeUI.Direction), StatChangeUI.Time / StatChangeUI.Timeout);
		if(StatChangeUI.Time == 0.0)
			StatChangeUI.LastPosition = StatChangeUI.Position;

		// Update timer
		StatChangeUI.Time += FrameTime;
		if(StatChangeUI.Time >= StatChangeUI.Timeout) {
			Iterator = StatChanges.erase(Iterator);
		}
		else
			++Iterator;
	}

	// Update recent items
	LowestRecentItemTime = std::numeric_limits<double>::infinity();
	for(auto Iterator = RecentItems.begin(); Iterator != RecentItems.end(); ) {
		_RecentItem &RecentItem = *Iterator;

		// Update timer
		RecentItem.Time += FrameTime;
		if(RecentItem.Time < LowestRecentItemTime)
			LowestRecentItemTime = RecentItem.Time;

		if(RecentItem.Time >= HUD_RECENTITEM_TIMEOUT) {
			Iterator = RecentItems.erase(Iterator);
		}
		else
			++Iterator;
	}

	// Update minigame
	if(Minigame) {
		for(int i = 0; i < Player->Character->Attributes["MinigameSpeed"].Int; i++) {
			Minigame->Update(FrameTime);
			if(Minigame->State == _Minigame::StateType::DONE) {
				ae::_Buffer Packet;
				Packet.Write<PacketType>(PacketType::MINIGAME_GETPRIZE);
				Packet.Write<float>(Minigame->DropX);
				PlayState.Network->SendPacket(Packet);

				Minigame->State =_Minigame::StateType::NEEDSEED;

				// Play sound
				const _MinigameItem *MinigameItem = Minigame->Prizes[Minigame->Bucket];
				if(MinigameItem) {
					if(!MinigameItem->Item->UnlockOnBuy())
						ae::Audio.PlaySound(ae::Assets.Sounds["success1.ogg"]);
				}
				else
					ae::Audio.PlaySound(ae::Assets.Sounds["fail0.ogg"]);

				break;
			}
		}
	}

	// Update GPM
	while(GoldGained.Size() && PlayState.Time - GoldGained.Front().Time >= HUD_GPM_TIME)
		GoldGained.Pop();

	BuyCooldown -= FrameTime;
	if(BuyCooldown < 0.0)
		BuyCooldown = 0.0;

	if(Message.Time > 0.0) {
		Message.Time -= FrameTime;
		if(Message.Time <= 0.0) {
			MessageElement->SetActive(false);
			Message.Time = 0.0;
		}
	}

	if(MapNameTimer > 0.0) {
		MapNameTimer -= FrameTime;
		if(MapNameTimer <= 0.0) {
			MapNameElement->SetActive(false);
			MapNameTimer = 0.0;
		}
	}
}

// Set state of restart button for in-game menu
void _HUD::UpdateRestartButton() {
	ae::_Element *Button = ae::Assets.Elements["button_menu_ingame_restart"];
	ae::_Element *Label = ae::Assets.Elements["label_menu_ingame_restart"];
	Button->SetActive(false);

	// Check for player
	if(!Player)
		return;

	if(Player->Character->IsAlive()) {

		// Show suicide in battle
		if(Player->Battle) {
			Button->SetActive(true);
			Label->Text = "Suicide";
		}
	}
	else {

		// Leave battle early
		if(Player->Battle) {
			Button->SetActive(true);
			Label->Text = "Leave Battle";
		}
		// Respawn
		else if(!Player->Character->Hardcore) {
			Button->SetActive(true);
			Label->Text = "Respawn";
		}
	}
}

// Refresh various screens
void _HUD::Refresh() {
	SkillScreen->RefreshSkillButtons(!ae::Input.ModKeyDown(KMOD_ALT));
	EnchanterScreen->RefreshBuyButtons();
	DisenchanterScreen->RefreshSellButtons();
}

// Initialize relic element positions
void _HUD::RefreshRelicWheel() {
	if(!Player)
		return;

	// Disable all elements
	for(size_t i = 0; i < RelicsElement->Children.size(); i++)
		RelicsElement->Children[i]->SetActive(false);

	// Build map of relic items to inventory slots
	std::map<const _Item *, size_t> RelicMap;
	_Bag &Bag = Player->Inventory->GetBag(BagType::INVENTORY);
	_Slot EquippedSlot(BagType::EQUIPMENT, EquipmentType::RELIC);
	for(size_t i = 0; i < Bag.Slots.size(); i++) {
		_InventorySlot &Slot = Bag.Slots[i];
		if(!Slot.Item)
			continue;

		// Add to wheel once if it's not already equipped
		if(RelicMap.find(Slot.Item) == RelicMap.end() && Slot.Item != Player->Inventory->GetSlot(EquippedSlot).Item)
			RelicMap[Slot.Item] = i;
	}

	// Add to wheel
	size_t RelicIndex = 0;
	for(const auto &RelicItem : RelicMap) {
		if(RelicItem.first->Type == ItemType::RELIC && !RelicItem.first->IsCursed()) {
			ae::_Element *Element = ae::Assets.Elements["element_hud_relics_" + std::to_string(RelicIndex)];
			Element->Children[0]->Texture = RelicItem.first->Texture;
			Element->UserData = (void *)RelicItem.second;
			Element->SetActive(true);

			// Add hotkey
			if(RelicIndex < ACTIONBAR_MAX_SKILLBARSIZE) {
				ae::_Element *LabelElement = ae::Assets.Elements["label_hud_relics_" + std::to_string(RelicIndex)];
				LabelElement->Text = ae::Actions.GetInputNameForAction(Action::GAME_SKILL1 + RelicIndex);
			}

			RelicIndex++;
		}
	}

	RelicWheelEnabled = RelicIndex;
	if(!RelicWheelEnabled) {
		ShowRelics = false;
		return;
	}

	// Set offsets
	float DegreeOffset = 360.0f / RelicIndex;
	float Degrees = -90.0f;
	for(size_t i = 0; i < RelicIndex; i++) {
		ae::_Element *Element = RelicsElement->Children[i];
		float Radians = Degrees * glm::pi<float>() / 180.0f;
		Element->BaseOffset.x = cosf(Radians) * HUD_RELIC_WHEEL_SIZE;
		Element->BaseOffset.y = sinf(Radians) * HUD_RELIC_WHEEL_SIZE;
		Element->CalculateBounds();

		Degrees += DegreeOffset;
	}
}

// Draws the HUD elements
void _HUD::Render(_Map *Map, double BlendFactor, double Time) {
	if(!Player)
		return;

	// Draw chat messages
	DrawChat(Time, IsChatting());

	// Show network stats
	if(ShowDebug) {
		glm::vec2 Offset = glm::vec2(ae::Graphics.ViewportSize.x - 150 * ae::_Element::GetUIScale(), 31 * ae::_Element::GetUIScale());
		float SpacingY = 22 * ae::_Element::GetUIScale();

		std::stringstream DebugBuffer;
		DebugBuffer << ae::Graphics.FramesPerSecond << " FPS";
		ae::Assets.Fonts["hud_tiny"]->DrawText(DebugBuffer.str(), glm::ivec2(Offset + glm::vec2(0, SpacingY * 0)));
		DebugBuffer.str("");

		DebugBuffer << std::fixed << std::setprecision(4) << PlayState.Network->GetSentSpeed() / 1024.0f << " KiB/s out";
		ae::Assets.Fonts["hud_tiny"]->DrawText(DebugBuffer.str(), glm::ivec2(Offset + glm::vec2(0, SpacingY * 1)));
		DebugBuffer.str("");

		DebugBuffer << std::fixed << std::setprecision(4) << PlayState.Network->GetReceiveSpeed() / 1024.0f << " KiB/s in";
		ae::Assets.Fonts["hud_tiny"]->DrawText(DebugBuffer.str(), glm::ivec2(Offset + glm::vec2(0, SpacingY * 2)));
		DebugBuffer.str("");

		DebugBuffer << PlayState.Network->GetPacketsLost() << "/" << PlayState.Network->GetPacketsSent() << "";
		ae::Assets.Fonts["hud_tiny"]->DrawText(DebugBuffer.str(), glm::ivec2(Offset + glm::vec2(0, SpacingY * 4)));
		DebugBuffer.str("");

		DebugBuffer << PlayState.Network->GetRTT() << "ms";
		ae::Assets.Fonts["hud_tiny"]->DrawText(DebugBuffer.str(), glm::ivec2(Offset + glm::vec2(0, SpacingY * 3)));
		DebugBuffer.str("");
	}

	// Draw button bar
	ButtonBarElement->Render();
	FullscreenElement->Render();

	// Draw hud elements
	ae::Assets.Elements["element_hud"]->Render();

	// Draw action bar
	DrawActionBar();

	// Update label text
	UpdateLabels();

	// Update clock
	std::stringstream Buffer;
	if(ae::Input.ModKeyDown(KMOD_ALT)) {
		std::time_t CurrentTime = std::time(nullptr);
		Buffer << std::put_time(std::localtime(&CurrentTime), "%X");
	}
	else
		Map->GetClockAsString(Buffer, Config.Clock24Hour);

	ae::Assets.Elements["label_hud_clock"]->Text = Buffer.str();
	Buffer.str("");

	// Get night status
	bool IsNight = false;
	if(Scripting->StartMethodCall("Game", "IsNight")) {
		Scripting->PushReal(Map->Clock);
		Scripting->MethodCall(1, 1);
		IsNight = Scripting->GetBoolean(1);
		Scripting->FinishMethodCall();
	}

	// Update clock icon
	const ae::_Texture *ClockIcon;
	if(PlayState.BloodMoonActive)
		ClockIcon = ae::Assets.Textures["textures/hud/clock_blood.webp"];
	else if(IsNight)
		ClockIcon = ae::Assets.Textures["textures/hud/clock_moon" + std::to_string(PlayState.EventPhase) + ".webp"];
	else
		ClockIcon = ae::Assets.Textures["textures/hud/clock_sun.webp"];
	ae::Assets.Elements["image_hud_clock"]->Texture = ClockIcon;

	// Update pvp zone
	if(Map->IsPVPZone(Player->Position))
		ae::Assets.Elements["label_hud_pvp"]->Text = "PVP Zone";
	else
		ae::Assets.Elements["label_hud_pvp"]->Text = "";

	// Set locale formatting
	std::stringstream ImbueBuffer;
	ImbueBuffer.imbue(std::locale(Config.Locale));

	// Draw experience bar
	if(ae::Input.ModKeyDown(KMOD_ALT))
		ImbueBuffer << Player->Character->Attributes["Experience"].Int << " XP";
	else
		ImbueBuffer << Player->Character->ExperienceNextLevel - Player->Character->ExperienceNeeded << " / " << Player->Character->ExperienceNextLevel << " XP";

	ae::Assets.Elements["label_hud_experience"]->Text = ImbueBuffer.str();
	ImbueBuffer.str("");
	ae::Assets.Elements["image_hud_experience_bar_full"]->SetWidth(ExperienceElement->Size.x * Player->Character->GetNextLevelPercent());
	ae::Assets.Elements["element_hud_experience_bar_empty"]->SetWidth(ExperienceElement->Size.x);
	ExperienceElement->Render();

	// Draw health bar
	ae::FormatSI<int64_t>(Buffer, Player->Character->Attributes["Health"].Int, ae::RoundDown1);
	Buffer << " / ";
	ae::FormatSI<int64_t>(Buffer, Player->Character->Attributes["MaxHealth"].Int, ae::RoundDown1);
	ae::Assets.Elements["label_hud_health"]->Text = Buffer.str();
	Buffer.str("");
	ae::Assets.Elements["image_hud_health_bar_full"]->SetWidth(HealthElement->Size.x * Player->Character->GetHealthPercent());
	ae::Assets.Elements["image_hud_health_bar_empty"]->SetWidth(HealthElement->Size.x);
	HealthElement->Render();

	// Draw mana bar
	ae::FormatSI<int64_t>(Buffer, Player->Character->Attributes["Mana"].Int, ae::RoundDown1);
	Buffer << " / ";
	ae::FormatSI<int64_t>(Buffer, Player->Character->Attributes["MaxMana"].Int, ae::RoundDown1);
	ae::Assets.Elements["label_hud_mana"]->Text = Buffer.str();
	Buffer.str("");
	ae::Assets.Elements["image_hud_mana_bar_full"]->SetWidth(ManaElement->Size.x * Player->Character->GetManaPercent());
	ae::Assets.Elements["image_hud_mana_bar_empty"]->SetWidth(ManaElement->Size.x);
	ManaElement->Render();

	DrawMapName();
	DrawMessage();
	DrawStatusEffects();
	DrawMinigame(BlendFactor);
	InventoryScreen->Render(BlendFactor);
	VendorScreen->Render(BlendFactor);
	TradeScreen->Render(BlendFactor);
	TraderScreen->Render(BlendFactor);
	CharacterScreen->Render(BlendFactor);
	BlacksmithScreen->Render(BlendFactor);
	EnchanterScreen->Render(BlendFactor);
	DisenchanterScreen->Render(BlendFactor);
	SkillScreen->Render(BlendFactor);
	StashScreen->Render(BlendFactor);
	ae::Assets.Elements["label_hud_pvp"]->Render();
	DrawPartyInput();
	DrawTeleport();
	DrawConfirm();

	// Draw stat changes
	for(auto &StatChange : StatChanges)
		StatChange.Render(BlendFactor);

	// Draw item information
	DrawCursorItem();
	const _Item *Item = Tooltip.InventorySlot.Item;
	if(Item) {

		// Compare items if not dragging
		_Slot CompareSlot;
		if(!Cursor.InventorySlot.Item) {

			// Compare items
			Item->GetEquipmentSlot(CompareSlot);
			if(Item->IsEquippable() && (Tooltip.Window == WINDOW_EQUIPMENT || Tooltip.Window == WINDOW_INVENTORY || Tooltip.Window == WINDOW_STASH || Tooltip.Window == WINDOW_VENDOR || Tooltip.Window == WINDOW_TRADEYOURS || Tooltip.Window == WINDOW_TRADETHEIRS || Tooltip.Window == WINDOW_BLACKSMITH)) {

				// Get equipment slot to compare
				switch(Tooltip.Window) {
					case WINDOW_BLACKSMITH:
						CompareSlot = BlacksmithScreen->UpgradeSlot;
					break;
					case WINDOW_EQUIPMENT:
						CompareSlot.Type = BagType::NONE;
					break;
					default:
					break;
				}

				// Check for valid slot
				if(Player->Inventory->IsValidSlot(CompareSlot)) {

					// Draw equipped item tooltip
					_Cursor EquippedTooltip;
					EquippedTooltip.InventorySlot = Player->Inventory->GetSlot(CompareSlot);
					if(EquippedTooltip.InventorySlot.Item)
						EquippedTooltip.InventorySlot.Item->DrawTooltip(glm::vec2(5, -1), Player, EquippedTooltip, _Slot());
				}
			}
		}

		// Draw item tooltip
		Item->DrawTooltip(ae::Input.GetMouse(), Player, Tooltip, CompareSlot);
	}

	// Draw status effect tooltip
	if(Tooltip.StatusEffect) {
		int DismissLevel = !Player->Battle ? Tooltip.StatusEffect->Buff->Dismiss : 0;
		Tooltip.StatusEffect->Buff->DrawTooltip(
			Scripting,
			Tooltip.StatusEffect,
			DismissLevel
		);
	}

	// Draw relic wheel
	DrawRelicWheel();

	// Dead outside of combat
	if(!Player->Character->IsAlive() && !Player->Battle) {
		if(!DiedElement->Active)
			CloseWindows(false);

		// Show respawn instructions
		Buffer << "Press " << ae::Actions.GetInputNameForAction(Action::MENU_BACK);
		if(Player->Character->Hardcore)
			Buffer << " to exit";
		else
			Buffer << " to respawn";

		RespawnInstructions->Text = Buffer.str();
		Buffer.str("");

		DiedElement->SetActive(true);
		DiedElement->Render();
	}
	else
		DiedElement->SetActive(false);
}

// Starts the chat box
void _HUD::ToggleChat() {
	if(IsTypingGold())
		return;

	if(IsChatting()) {
		if(ChatTextBox->Text != "") {

			// Send message to server
			ae::_Buffer Packet;
			Packet.Write<PacketType>(PacketType::CHAT_MESSAGE);
			Packet.WriteString(ChatTextBox->Text.c_str());
			PlayState.Network->SendPacket(Packet);

			// Add message to history if not a repeat
			if(SentHistory.size() == 0 || (SentHistory.size() > 0 && SentHistory.back() != ChatTextBox->Text))
				SentHistory.push_back(ChatTextBox->Text);
		}

		CloseChat();
	}
	else {
		ChatElement->BaseSize.x = HUD_CHAT_TEXTBOX_WIDTH;
		ChatElement->CalculateBounds();
		ChatElement->SetActive(true);
		ChatTextBox->ResetCursor();
		ae::FocusedElement = ChatTextBox;
	}
}

// Open/close party screen
void _HUD::ToggleParty(bool IgnoreNextChar) {
	if(Player->Controller->WaitForServer || !Player->Character->CanOpenParty())
		return;

	if(!PartyElement->Active) {
		CloseWindows(true);
		InitParty();
		Framework.IgnoreNextInputEvent = IgnoreNextChar;
	}
	else {
		CloseWindows(true);
	}
}

// Open/close menu
void _HUD::ToggleInGameMenu(bool Force) {

	// Cancel teleport
	if(CloseTeleport())
		return;

	// Waiting for update from server
	if(Player->Controller->WaitForServer)
		return;

	// Close windows if open
	if(CloseWindows(true))
		return;

	if(PlayState.DevMode && !Force)
		PlayState.Network->Disconnect(false, 1);
	else
		Menu.InitInGame();
}

// Update key/button names for button bar
void _HUD::UpdateButtonBarLabels() {
	ae::Assets.Elements["label_buttonbar_join"]->Text = ae::Actions.GetInputNameForAction(Action::GAME_JOIN).substr(0, HUD_KEYNAME_LENGTH);
	ae::Assets.Elements["label_buttonbar_inventory"]->Text = ae::Actions.GetInputNameForAction(Action::GAME_INVENTORY).substr(0, HUD_KEYNAME_LENGTH);
	ae::Assets.Elements["label_buttonbar_trade"]->Text = ae::Actions.GetInputNameForAction(Action::GAME_TRADE).substr(0, HUD_KEYNAME_LENGTH);
	ae::Assets.Elements["label_buttonbar_skills"]->Text = ae::Actions.GetInputNameForAction(Action::GAME_SKILLS).substr(0, HUD_KEYNAME_LENGTH);
	ae::Assets.Elements["label_buttonbar_party"]->Text = ae::Actions.GetInputNameForAction(Action::GAME_PARTY).substr(0, HUD_KEYNAME_LENGTH);
	ae::Assets.Elements["label_buttonbar_relics"]->Text = ae::Actions.GetInputNameForAction(Action::GAME_RELIC).substr(0, HUD_KEYNAME_LENGTH);
	ae::Assets.Elements["label_buttonbar_menu"]->Text = ae::Actions.GetInputNameForAction(Action::MENU_BACK).substr(0, HUD_KEYNAME_LENGTH);
}

// Set clickable state of action/button bar
void _HUD::SetBarState(bool State) {
	ButtonBarElement->SetClickable(State, 2);
	ActionBarElement->SetClickable(State, 2);
}

// Initialize the confirm screen
void _HUD::InitConfirm(const std::string &WarningMessage, const ae::_Texture *Icon, int Amount) {
	ae::Assets.Elements["label_confirm_warning"]->Text = WarningMessage;
	ae::Assets.Elements["image_confirm_icon"]->Texture = Icon;
	ae::Assets.Elements["image_confirm_icon_big"]->Texture = nullptr;
	ae::Assets.Elements["label_confirm_icon_quantity"]->Text = Amount > 1 ? std::to_string(Amount) : "";

	ConfirmElement->SetActive(true);
}

// Initialize minigame
void _HUD::InitMinigame() {
	if(!Player->Character->Minigame)
		return;

	Cursor.Reset();

	// Show UI
	MinigameElement->SetActive(true);
	ae::_Element *NameElement = ae::Assets.Elements["label_minigame_name"];
	ae::_Element *CostElement = ae::Assets.Elements["label_minigame_cost"];
	ae::_Element *ImageElement = ae::Assets.Elements["image_minigame_token"];
	NameElement->Text = Player->Character->Minigame->Name;
	CostElement->Text = std::to_string(Player->Character->Minigame->Cost) + " " + Player->Character->Minigame->RequiredItem->Name;
	if(Player->Character->Minigame->Cost != 1)
		CostElement->Text += "s";
	CostElement->Text += " to play";
	ImageElement->Texture = Player->Character->Minigame->RequiredItem->Texture;

	// Disable certain ui elements
	SetBarState(false);

	// Create minigame
	Minigame = new _Minigame(Player->Character->Minigame);
}

// Initialize the party screen
void _HUD::InitParty() {
	Cursor.Reset();

	PartyElement->SetActive(true);
	PartyTextBox->Text = Player->Character->PartyName;
	PartyTextBox->CursorPosition = PartyTextBox->Text.size();
	PartyTextBox->ResetCursor();
	ae::FocusedElement = PartyTextBox;
}

// Toggle relic wheel
void _HUD::ToggleRelics() {
	if(!HUD.Player->Character->CanOpenRelicWheel())
		return;

	CloseWindows(true);
	if(!RelicsElement->Active && RelicWheelEnabled)
		ShowRelics = true;
}

// Closes the chat window
void _HUD::CloseChat() {
	SentHistoryIterator = SentHistory.end();
	ChatElement->SetActive(false);
	ChatTextBox->Clear();
	if(ae::FocusedElement == ChatTextBox)
		ae::FocusedElement = nullptr;
}

// Close confirmation screen
bool _HUD::CloseConfirm() {
	bool WasOpen = ConfirmElement->Active;
	ConfirmElement->SetActive(false);
	DarkOverlayElement->SetActive(false);

	DeleteSlot.Reset();

	return WasOpen;
}

// Close party screen
bool _HUD::CloseParty() {
	bool WasOpen = PartyElement->Active;

	PartyElement->SetActive(false);
	Cursor.Reset();
	if(ae::FocusedElement == PartyTextBox)
		ae::FocusedElement = nullptr;

	return WasOpen;
}

// Cancel teleport
bool _HUD::CloseTeleport() {
	bool WasOpen = TeleportElement->Active;
	TeleportElement->SetActive(false);
	if(WasOpen) {
		PlayState.SendStatus(_Character::STATUS_NONE);
		Player->Controller->WaitForServer = false;
		Player->Character->TeleportTime = 0.0;
		PlayState.StopTeleportSound();
	}

	return WasOpen;
}

// Draw confirm action
void _HUD::DrawConfirm() {
	if(!ConfirmElement->Active)
		return;

	DarkOverlayElement->SetActive(true);
	DarkOverlayElement->Render();

	ConfirmElement->Render();
}

// Close minigame
bool _HUD::CloseMinigame() {
	bool WasOpen = MinigameElement->Active;

	// Re-enable ui elements
	SetBarState(true);

	// Cleanup
	MinigameElement->SetActive(false);
	delete Minigame;
	Minigame = nullptr;

	Cursor.Reset();

	if(Player)
		Player->Character->Minigame = nullptr;

	return WasOpen;
}

// Closes all windows
bool _HUD::CloseWindows(bool SendStatus, bool HardClose, bool SendNotify, bool DelayClose) {
	Cursor.Reset();

	bool WasOpen = false;
	WasOpen |= InventoryScreen->Close(true, DelayClose);
	WasOpen |= BlacksmithScreen->Close(true, DelayClose);
	WasOpen |= EnchanterScreen->Close(true, DelayClose);
	WasOpen |= DisenchanterScreen->Close(true, DelayClose);
	WasOpen |= SkillScreen->Close(true, DelayClose);
	WasOpen |= VendorScreen->Close(true, DelayClose);
	WasOpen |= TraderScreen->Close(true, DelayClose);
	WasOpen |= StashScreen->Close(true, DelayClose);
	WasOpen |= TradeScreen->Close(SendNotify, DelayClose);
	WasOpen |= CloseConfirm();
	WasOpen |= CloseParty();
	WasOpen |= CloseMinigame();

	if(HardClose) {
		WasOpen |= ShowRelics;
		ShowRelics = false;
	}

	if(WasOpen && SendStatus)
		PlayState.SendStatus(_Character::STATUS_NONE);

	return WasOpen;
}

// Draws chat messages
void _HUD::DrawChat(double Time, bool IgnoreTimeout) {

	// Draw window
	ChatElement->Render();

	// Set up UI position
	ae::_Font *Font = ae::Assets.Fonts["hud_small"];
	int SpacingY = -(Font->MaxAbove + Font->MaxBelow);
	glm::vec2 DrawPosition = glm::vec2(ChatElement->Bounds.Start.x, ChatElement->Bounds.End.y);
	DrawPosition.y += 2 * SpacingY;

	// Draw messages
	int Index = 0;
	for(auto Iterator = ChatHistory.rbegin(); Iterator != ChatHistory.rend(); ++Iterator) {
		_Message &ChatMessage = (*Iterator);

		double TimeLeft = ChatMessage.Time - Time + HUD_CHAT_TIMEOUT;
		if(Index >= HUD_CHAT_MESSAGES || (!IgnoreTimeout && TimeLeft <= 0))
			break;

		// Set color
		glm::vec4 Color = ChatMessage.Color;
		if(!IgnoreTimeout && TimeLeft <= HUD_CHAT_FADETIME)
			Color.a = (float)TimeLeft;

		// Draw text
		Font->DrawText(ChatMessage.Message, DrawPosition, ae::LEFT_BASELINE, Color);
		DrawPosition.y += SpacingY;

		Index++;
	}
}

// Draws player's status effects
void _HUD::DrawStatusEffects() {

	// Draw status effects
	glm::vec2 Offset(0, 0);
	for(auto &Effect : Player->Character->StatusEffects) {
		if(!Effect->HUDElement)
			continue;

		Effect->HUDElement->BaseOffset = Offset;
		Effect->HUDElement->CalculateBounds();
		Effect->Render(Effect->HUDElement, PlayState.Time, false);
		Offset.x += UI_BUFF_SIZE.x + 2;
	}
}

// Draw the teleport sequence
void _HUD::DrawTeleport() {
	if(!TeleportElement->Active)
		return;

	// Get spawn map id
	ae::NetworkIDType MapID = Player->Character->SpawnMapID;
	if(Player->Stats->Maps.find(MapID) == Player->Stats->Maps.end())
		return;

	// Update label
	ae::_Element *LabelElement = ae::Assets.Elements["label_teleport_timeleft"];
	std::stringstream Buffer;
	Buffer << "Teleporting to [c green]" << Player->Stats->Maps.at(MapID).Name << "[c white] in " << std::fixed << std::setprecision(1) << Player->Character->TeleportTime;
	LabelElement->Text = Buffer.str();

	// Get text size
	ae::_TextBounds TextBounds;
	LabelElement->Font->GetStringDimensions(Buffer.str(), TextBounds, true);

	// Render element
	TeleportElement->BaseSize.x = TextBounds.Width / ae::_Element::GetUIScale() + 40;
	TeleportElement->CalculateBounds();
	TeleportElement->Render();
}

// Draw minigame
void _HUD::DrawMinigame(double BlendFactor) {
	if(!Player->Character->Minigame || !Minigame) {
		MinigameElement->Active = false;
		return;
	}

	// Get token count
	int Tokens = Player->Inventory->CountItem(Player->Character->Minigame->RequiredItem);
	ae::Assets.Elements["label_minigame_tokens"]->Text = std::to_string(Tokens);

	// Update text color
	ae::_Element *CostElement = ae::Assets.Elements["label_minigame_cost"];
	if(Tokens < Player->Character->Minigame->Cost)
		CostElement->Color = ae::Assets.Colors["red"];
	else
		CostElement->Color = ae::Assets.Colors["gold"];

	// Update board size
	ae::_Element *BoardElement = ae::Assets.Elements["element_minigame_board"];
	Minigame->GetUIBoundary(BoardElement->Bounds);
	BoardElement->Size = BoardElement->Bounds.End - BoardElement->Bounds.Start;
	BoardElement->CalculateChildrenBounds(false);

	// Draw element
	MinigameElement->Render();

	// Draw game
	Minigame->Render(BlendFactor);
}

// Draw the skill and belt bar
void _HUD::DrawActionBar() {
	if(!Player || !ActionBarElement->Active)
		return;

	ActionBarElement->Render();

	// Draw skill bar
	for(int i = 0; i < ACTIONBAR_MAX_SIZE; i++) {
		if(i >= ACTIONBAR_MAX_SKILLBARSIZE && i < ACTIONBAR_BELT_STARTS)
			continue;

		// Get offsets
		int ActionButton = Action::GAME_SKILL1 + i;
		glm::vec2 HotkeyOffset = glm::vec2(-28, -15);
		if(i >= ACTIONBAR_BELT_STARTS) {
			HotkeyOffset = glm::vec2(-21, -10);
			ActionButton = Action::GAME_ITEM1 + i - ACTIONBAR_BELT_STARTS;
		}

		// Get button position
		std::stringstream Buffer;
		Buffer << "button_actionbar_" << i;
		ae::_Element *Button = ae::Assets.Elements[Buffer.str()];
		Buffer.str("");
		glm::vec2 DrawPosition = (Button->Bounds.Start + Button->Bounds.End) / 2.0f;

		// Draw item icon
		const _Item *Item = Player->Character->ActionBar[(size_t)i].Item;
		if(Item) {
			ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos_uv"]);
			ae::Graphics.DrawScaledImage(DrawPosition, Item->Texture, UI_SLOT_SIZE);

			// Draw cooldown overlay
			DrawCooldown(Button, Item);

			// Draw item count
			if(!Item->IsSkill())
				ae::Assets.Fonts["hud_tiny"]->DrawText(std::to_string(Player->Character->ActionBar[(size_t)i].Count), DrawPosition + glm::vec2(21, 20) * ae::_Element::GetUIScale(), ae::RIGHT_BASELINE);

			// Draw skill levels
			if(Item->IsSkill() && ae::Input.ModKeyDown(KMOD_ALT)) {
				int AllSkills = (int)Player->Character->Attributes["AllSkills"].Int;
				int SkillLevel = Player->Character->Skills[Item->ID];
				if(AllSkills) {
					int DrawLevel = SkillLevel + AllSkills;
					int MaxSkillLevel = Player->Character->MaxSkillLevels[Item->ID];

					// Get color
					std::string Color;
					if(DrawLevel > MaxSkillLevel)
						Color = "red";
					else if(DrawLevel == MaxSkillLevel)
						Color = "yellow";
					else
						Color = "light_blue";

					Buffer << "[c " << Color << "]" << std::min(DrawLevel, MaxSkillLevel) << " [c gray](" << SkillLevel << ")";
				}
				else
					Buffer << SkillLevel;

				ae::Assets.Fonts["hud_tiny"]->DrawTextFormatted(Buffer.str(), DrawPosition + glm::vec2(-28, 28) * ae::_Element::GetUIScale(), ae::LEFT_BASELINE);
			}
		}

		// Draw hotkey
		ae::Assets.Fonts["hud_tiny"]->DrawText(ae::Actions.GetInputNameForAction((size_t)ActionButton), DrawPosition + HotkeyOffset * ae::_Element::GetUIScale(), ae::LEFT_BASELINE);
	}
}

// Draw the party screen
void _HUD::DrawPartyInput() {
	if(!PartyElement->Active)
		return;

	PartyElement->Render();
}

// Draw hud message
void _HUD::DrawMessage() {
	if(!MessageElement->Active)
		return;

	// Get alpha
	float Fade = 1.0f;
	if(Message.Time < HUD_MESSAGE_FADETIME)
		Fade = (float)(Message.Time / HUD_MESSAGE_FADETIME);

	MessageElement->SetFade(Fade);
	MessageElement->Render();
}

// Draw map name
void _HUD::DrawMapName() {
	if(MapNameTimer <= 0.0)
		return;

	float Fade = 1.0f;
	if(MapNameTimer < 1.0)
		Fade = (float)(MapNameTimer / 1.0);

	MapNameElement->SetFade(Fade);
	MapNameElement->Render();
}

// Draw party members
void _HUD::DrawParty() {
	PartyMembersElement->Render();
}

// Draw relic wheel
void _HUD::DrawRelicWheel() {
	if(!ShowRelics) {
		RelicsElement->Active = false;
		return;
	}

	RelicsElement->Active = true;
	RelicsElement->Render();
}

// Draw recently acquired items
void _HUD::DrawRecentItems() {
	if(!RecentItems.size() || !Player->Character->IsAlive())
		return;

	struct _RecentItemDisplay {
		_RecentItem *RecentItem;
		glm::vec4 Color{1.0f};
		glm::vec2 Position;
		std::string Label;
	};

	// Array of items to display
	static std::vector<_RecentItemDisplay> RecentItemsDisplay;
	RecentItemsDisplay.clear();

	// Set fade
	const ae::_Font *Font = ae::Assets.Fonts["hud_small"];
	double TimeLeft = HUD_RECENTITEM_TIMEOUT - LowestRecentItemTime;
	RecentItemsElement->SetFade(1.0f);
	if(TimeLeft < HUD_RECENTITEM_FADETIME)
		RecentItemsElement->SetFade((float)(TimeLeft / HUD_RECENTITEM_FADETIME));

	// Build array of recent items to display
	float Spacing = UI_SLOT_SIZE.y + 5;
	glm::vec2 DrawPosition = RecentItemsElement->Bounds.End + glm::vec2(-32, -32) * ae::_Element::GetUIScale();
	int MaxWidth = 0;
	for(auto &RecentItem : RecentItems) {
		if(!RecentItem.Item)
			continue;

		double TimeLeft = HUD_RECENTITEM_TIMEOUT - RecentItem.Time;

		// Get color
		_RecentItemDisplay RecentDisplay;
		RecentDisplay.RecentItem = &RecentItem;
		if(TimeLeft < HUD_RECENTITEM_FADETIME)
			RecentDisplay.Color.a = (float)(TimeLeft / HUD_RECENTITEM_FADETIME);

		// Build label
		std::stringstream Buffer;
		Buffer << "+" << RecentItem.Count << " (" << RecentItem.Total << ")";
		RecentDisplay.Label = Buffer.str();
		RecentDisplay.Position = DrawPosition;

		// Get size of label
		ae::_TextBounds TextBounds;
		Font->GetStringDimensions(RecentDisplay.Label, TextBounds);

		// Keep track of longest label
		if(TextBounds.Width > MaxWidth)
			MaxWidth = TextBounds.Width;

		// Add to array
		RecentItemsDisplay.push_back(RecentDisplay);

		// Update position
		DrawPosition.y -= Spacing * ae::_Element::GetUIScale();

		// Don't draw off screen
		if(DrawPosition.y < (UI_SLOT_SIZE.y * ae::_Element::GetUIScale()))
			break;
	}

	// Set size
	RecentItemsElement->BaseSize.x = MaxWidth / ae::_Element::GetUIScale() + 32 + 40 + 20;
	RecentItemsElement->BaseSize.y = Spacing * RecentItemsDisplay.size();
	RecentItemsElement->CalculateBounds();

	// Draw background
	RecentItemsElement->SetActive(true);
	RecentItemsElement->Render();

	// Draw
	for(auto &Display : RecentItemsDisplay) {

		// Draw item
		ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos_uv"]);
		ae::Graphics.DrawScaledImage(Display.Position, Display.RecentItem->Item->Texture, UI_SLOT_SIZE, Display.Color);

		// Draw amount
		Font->DrawText(Display.Label, Display.Position + glm::vec2(-40, 7) * ae::_Element::GetUIScale(), ae::RIGHT_BASELINE, Display.Color);
	}

	// Disable
	RecentItemsElement->SetActive(false);
}

// Set hud message
void _HUD::SetMessage(const std::string &Text, double Duration) {
	if(!Text.length()) {
		MessageElement->SetActive(false);
		return;
	}

	// Append message to chat window
	_Message Chat;
	Chat.Color = ae::Assets.Colors["gray"];
	Chat.Message = Text;
	Chat.Time = PlayState.Time;
	if(ChatHistory.empty() || (ChatHistory.size() && Chat.Message != ChatHistory.back().Message))
		AddChatMessage(Chat);

	// Set message data
	Message.Time = Duration;
	MapNameTimer = 0.0;
	MessageLabel->Text = Text;

	// Set background size
	ae::_TextBounds Bounds;
	MessageLabel->Font->GetStringDimensions(Text, Bounds, true);
	MessageElement->BaseSize.x = Bounds.Width / ae::_Element::GetUIScale() + 80;
	MessageElement->SetActive(true);
	MessageElement->CalculateBounds();
}

// Show name of map
void _HUD::ShowMapName(const std::string &Text, double Duration) {
	if(!Text.length()) {
		MapNameElement->SetActive(false);
		return;
	}

	// Set message data
	MapNameTimer = Duration;
	Message.Time = 0.0;
	MapNameElement->Children[0]->Text = Text;

	// Set background size
	ae::_TextBounds Bounds;
	MapNameElement->Children[0]->Font->GetStringDimensions(Text, Bounds, true);
	MapNameElement->BaseSize.x = Bounds.Width / ae::_Element::GetUIScale() + 80;
	MapNameElement->SetActive(true);
	MapNameElement->CalculateBounds();
}

// Show the teleport window
void _HUD::StartTeleport() {
	TeleportElement->SetActive(true);
}

// Stop teleporting
void _HUD::StopTeleport() {
	TeleportElement->SetActive(false);
}

// Draws the item under the cursor
void _HUD::DrawCursorItem() {
	if(Cursor.InventorySlot.Item) {
		glm::vec2 DrawPosition = ae::Input.GetMouse();
		ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos_uv"]);
		ae::Graphics.DrawScaledImage(DrawPosition, Cursor.InventorySlot.Item->Texture, UI_SLOT_SIZE, ae::Assets.Colors["itemfade"]);
	}
}

// Draw cooldown overlay
void _HUD::DrawCooldown(const ae::_Element *Button, const _Item *Item) {

	// Check cooldown
	auto CooldownIterator = Player->Character->Cooldowns.find(Item->ID);
	if(CooldownIterator == Player->Character->Cooldowns.end())
		return;

	if(CooldownIterator->second.MaxDuration <= 0.0)
		return;

	// Draw cooldown
	double CooldownPercent = CooldownIterator->second.Duration / CooldownIterator->second.MaxDuration;

	// Set up graphics
	ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos"]);
	ae::Graphics.SetColor(glm::vec4(0, 0, 0, 0.7f));

	// Draw dark percentage bg
	float OverlayHeight = (1.0 - CooldownPercent) * (Button->Bounds.End.y - Button->Bounds.Start.y);
	ae::Graphics.DrawRectangle(glm::ivec2(Button->Bounds.Start + glm::vec2(0, OverlayHeight)), glm::ivec2(Button->Bounds.End), true);

	// Get duration
	std::stringstream Buffer;
	double Duration = CooldownIterator->second.Duration;
	if(Duration > 60.0)
		Buffer << std::fixed << std::setprecision(1) << (int)(Duration / 60.0) << "m";
	else
		Buffer << std::fixed << std::setprecision(1) << ae::Round1(Duration);
	ae::_TextBounds TextBounds;
	ae::Assets.Fonts["hud_small"]->GetStringDimensions(Buffer.str(), TextBounds);
	glm::vec2 TextSize(TextBounds.Width + 3, TextBounds.AboveBase + TextBounds.BelowBase + 3);

	// Draw timer and overlay
	glm::vec2 DrawPosition = (Button->Bounds.Start + Button->Bounds.End) / 2.0f;
	ae::Graphics.DrawRectangle(glm::ivec2(DrawPosition - TextSize * 0.5f), glm::ivec2(DrawPosition + TextSize * 0.5f), true);
	ae::Assets.Fonts["hud_small"]->DrawText(Buffer.str(), glm::ivec2(DrawPosition + glm::vec2(0, 7) * ae::_Element::GetUIScale()), ae::CENTER_BASELINE);
}

// Draws an item's price
void _HUD::DrawItemPrice(const _Item *Item, int Count, const glm::vec2 &DrawPosition, bool Buy, int Level) {
	if(!Player->Character->Vendor && !Player->Character->Blacksmith)
		return;

	// Get price
	int64_t Price = 0;
	ae::RoundFunction *RoundFunction = ae::RoundUp2;
	if(Player->Character->Vendor) {
		Price = Item->GetPrice(Scripting, Player, Player->Character->Vendor, Count, Buy, Level);
	}
	else if(Player->Character->Blacksmith) {
		if(!Player->Character->Blacksmith->CanUpgrade(Item, 1, Level))
			return;

		Price = Item->GetUpgradeCost(Player, Level + 1);
		RoundFunction = ae::Round2;
	}

	// Color
	glm::vec4 Color;
	if(Buy && Player->Character->Attributes["Gold"].Int < Price)
		Color = ae::Assets.Colors["red"];
	else
		Color = ae::Assets.Colors["light_yellow"];

	std::stringstream Buffer;
	ae::FormatSI<int64_t>(Buffer, Price, RoundFunction);
	ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), glm::ivec2(DrawPosition + glm::vec2(28, -15) * ae::_Element::GetUIScale()), ae::RIGHT_BASELINE, Color);
}

// Sets the player's action bar
void _HUD::SetActionBar(size_t Slot, size_t OldSlot, const _Action &Action) {
	if(Player->Battle)
		return;

	if(Player->Character->ActionBar[Slot] == Action)
		return;

	// Check for bringing new skill/item onto bar
	if(OldSlot >= Player->Character->ActionBar.size()) {

		// Check for valid item types
		if(Action.Item) {
			if(!(Action.Item->IsSkill() || Action.Item->IsConsumable()))
				return;

			if(Action.Item->IsSkill() && Slot >= ACTIONBAR_MAX_SKILLBARSIZE)
				return;

			if(Action.Item->IsConsumable() && Slot < ACTIONBAR_BELT_STARTS)
				return;
		}

		// Remove duplicate skills
		for(size_t i = 0; i < Player->Character->ActionBar.size(); i++) {
			if(Player->Character->ActionBar[i] == Action)
				Player->Character->ActionBar[i].Clear();
		}
	}
	// Rearrange action bar
	else {
		const _Item *OldItem = Player->Character->ActionBar[OldSlot].Item;
		if(!OldItem)
			return;

		if(OldItem->IsSkill() && Slot >= ACTIONBAR_MAX_SKILLBARSIZE)
			return;
		if(!OldItem->IsSkill() && Slot < ACTIONBAR_BELT_STARTS)
			return;

		Player->Character->ActionBar[OldSlot] = Player->Character->ActionBar[Slot];
	}

	// Update player
	Player->Character->ActionBar[Slot] = Action;
	Player->Character->CalculateStats();

	// Notify server
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::ACTIONBAR_CHANGED);
	for(size_t i = 0; i < Player->Character->ActionBar.size(); i++) {
		Player->Character->ActionBar[i].Serialize(Packet);
	}

	PlayState.Network->SendPacket(Packet);
}

// Send party name to server and close screen
void _HUD::SendPartyName() {
	if(!Player || !PartyElement->Active)
		return;

	// Reset ui
	HidePartyMembers();

	// Send info
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::PARTY_NAME);
	Packet.WriteString(PartyTextBox->Text.c_str());
	PlayState.Network->SendPacket(Packet);

	// Close screen
	CloseParty();
}

// Split a stack of items
void _HUD::SplitStack(const _Slot &Slot, uint8_t Count) {

	// Build packet
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::INVENTORY_SPLIT);
	Slot.Serialize(Packet);
	Packet.Write<uint8_t>(Count);

	PlayState.Network->SendPacket(Packet);
}

// Move a stack of items between bags
void _HUD::Transfer(const _Slot &SourceSlot) {

	// Get target bag
	bool Wait = false;
	BagType TargetBagType = BagType::NONE;
	switch(SourceSlot.Type) {
		case BagType::INVENTORY:
		case BagType::EQUIPMENT:
			if(SourceSlot.Type == BagType::EQUIPMENT && Player->Inventory->GetSlot(SourceSlot).Item && Player->Inventory->GetSlot(SourceSlot).Item->IsCursed())
				return;

			if(TradeScreen->Element->Active)
				TargetBagType = BagType::TRADE;
			else if(Player->Character->ViewingStash)
				TargetBagType = BagType::STASH;

			if(SourceSlot.Type == BagType::EQUIPMENT)
				Wait = true;
		break;
		case BagType::TRADE:
		case BagType::STASH:
			TargetBagType = BagType::INVENTORY;
		break;
		default:
		break;
	}

	if(TargetBagType == BagType::NONE)
		return;

	// Write packet
	ae::_Buffer Packet;
	Packet.Write<PacketType>(PacketType::INVENTORY_TRANSFER);
	SourceSlot.Serialize(Packet);
	Packet.Write<uint8_t>((uint8_t)TargetBagType);
	PlayState.Network->SendPacket(Packet);
	if(Wait)
		Player->Controller->WaitForServer = true;
}

// Convert window into a player bag
BagType _HUD::GetBagFromWindow(int Window) {

	switch(Window) {
		case WINDOW_EQUIPMENT:
			return BagType::EQUIPMENT;
		break;
		case WINDOW_INVENTORY:
			return BagType::INVENTORY;
		break;
		case WINDOW_TRADEYOURS:
		case WINDOW_TRADETHEIRS:
			return BagType::TRADE;
		break;
		case WINDOW_STASH:
			return BagType::STASH;
		break;
	}

	return BagType::NONE;
}

// Calculate GPM
int64_t _HUD::GetGPM() {

	int64_t Sum = 0;
	for(int i = 0; i < GoldGained.Size(); i++)
		Sum += GoldGained.Back(i).Value;

	return Sum;
}

// Return true if player is typing gold
bool _HUD::IsTypingGold() {
	return ae::FocusedElement == ae::Assets.Elements["textbox_trade_gold_yours"];
}

// Hide party member ui elements
void _HUD::HidePartyMembers() {
	for(auto &Element : PartyMembersElement->Children)
		Element->SetActive(false);
}

// Update party member stats
void _HUD::UpdatePartyMember(size_t Index, bool SameMap, const std::string &Name, const std::string &MapName, const std::string &Gold, const ae::_Texture *Portrait, float HealthPercent, float ManaPercent, float Rotation, bool SamePosition) {

	// Set portrait
	ae::_Element *PortraitElement = ae::Assets.Elements["image_hud_party_portrait" + std::to_string(Index)];
	PortraitElement->SetActive(true);
	PortraitElement->Texture = Portrait;

	// Set name
	PortraitElement->Children[0]->Text = Name + " (" + MapName + ")";

	// Update health
	ae::_Element *HealthElement = ae::Assets.Elements["image_hud_party_health_bar_full" + std::to_string(Index)];
	HealthElement->BaseSize.x = HealthPercent * 100;
	HealthElement->CalculateBounds();

	// Update mana
	ae::_Element *ManaElement = ae::Assets.Elements["image_hud_party_mana_bar_full" + std::to_string(Index)];
	ManaElement->BaseSize.x = ManaPercent * 100;
	ManaElement->CalculateBounds();

	// Set direction
	ae::_Element *ArrowElement = ae::Assets.Elements["image_hud_party_direction" + std::to_string(Index)];
	ArrowElement->Texture = SamePosition ? ae::Assets.Textures["textures/hud/circle.webp"] : ae::Assets.Textures["textures/hud/arrow.webp"];
	ArrowElement->Active = SameMap;
	ArrowElement->Rotation = Rotation;

	// Set gold
	ae::_Element *GoldElement = ae::Assets.Elements["label_hud_party_gold" + std::to_string(Index)];
	GoldElement->Text = Gold;
	GoldElement->Color = (Gold.size() && Gold[0] == '-') ? ae::Assets.Colors["red"] : ae::Assets.Colors["gold"];
}

// Return true if player is typing in the party screen
bool _HUD::IsTypingParty() {
	return PartyElement->Active;
}

// Return true if player is allowed to move
bool _HUD::AllowMovement() {
	return !IsChatting() && !TradeScreen->AcceptButton->Checked;
}

// Handle pressing a skill hotkey, return false if not handled
bool _HUD::HandleSkillHotkey(int SkillIndex) {

	// Equip skill while hovering over skill on skill screen
	if(Tooltip.Window == _HUD::WINDOW_SKILLS) {
		SkillScreen->EquipSkill(Tooltip.InventorySlot.Item->ID, SkillIndex);
		return true;
	}
	// Equip relic with hotkey
	else if(ShowRelics) {
		ae::_Element *Element = ae::Assets.Elements["element_hud_relics_" + std::to_string(SkillIndex)];
		if(!Element->Active)
			return false;

		// Send use packet
		_Slot Slot(BagType::INVENTORY, (size_t)Element->UserData);
		ae::_Buffer Packet;
		Packet.Write<PacketType>(PacketType::INVENTORY_USE);
		Packet.WriteBit(false);
		Slot.Serialize(Packet);
		PlayState.Network->SendPacket(Packet);
		Player->Controller->WaitForServer = true;
		CloseWindows(true);

		return true;
	}

	return false;
}

// Return skill modifier amount based on holding shift/ctrl
int _HUD::GetSkillModifierAmount() {
	return ae::Input.ModKeyDown(KMOD_CTRL) ? 50 : (ae::Input.ModKeyDown(KMOD_SHIFT) ? 5 : 1);
}

// Return true if the chatbox is open
bool _HUD::IsChatting() {
	return ChatTextBox->Active;
}

// Add chat message and break up if necessary
void _HUD::AddChatMessage(const _Message &Chat) {
	ae::_Font *Font = ae::Assets.Fonts["hud_small"];
	std::vector<std::string> Strings;
	Font->BreakupString(Chat.Message, ae::Graphics.CurrentSize.x - 100, Strings);
	for(const auto &String : Strings)
		ChatHistory.push_back(_Message(String, Chat.Color, Chat.Time));
}

// Set chat textbox string based on history
void _HUD::UpdateSentHistory(int Direction) {
	if(SentHistory.size() == 0)
		return;

	if(Direction < 0 && SentHistoryIterator != SentHistory.begin()) {
		SentHistoryIterator--;

		ChatTextBox->Text = *SentHistoryIterator;
		ChatTextBox->CursorPosition = ChatTextBox->Text.length();
	}
	else if(Direction > 0 && SentHistoryIterator != SentHistory.end()) {

		SentHistoryIterator++;
		if(SentHistoryIterator == SentHistory.end()) {
			ChatTextBox->Text = "";
			ChatTextBox->CursorPosition = 0;
			return;
		}

		ChatTextBox->Text = *SentHistoryIterator;
		ChatTextBox->CursorPosition = ChatTextBox->Text.length();
	}
}

// Set player for HUD
void _HUD::SetPlayer(_Object *Player) {
	this->Player = Player;

	Tooltip.Reset();
	Cursor.Reset();
}

// Resize action bar
void _HUD::UpdateActionBarSize() {

	// Enable unlocked slots
	for(int i = 0; i < ACTIONBAR_MAX_SKILLBARSIZE; i++)
		ae::Assets.Elements["button_actionbar_" + std::to_string(i)]->SetEnabled(i < Player->Character->SkillBarSize);
	for(int i = ACTIONBAR_BELT_STARTS; i < ACTIONBAR_MAX_SIZE; i++)
		ae::Assets.Elements["button_actionbar_" + std::to_string(i)]->SetEnabled(i < ACTIONBAR_BELT_STARTS + Player->Character->BeltSize);
}

// Add multiple statchange ui elements
void _HUD::AddStatChange(_StatChange &StatChange, bool FromAction) {
	if(StatChange.Values.size() == 0 || !StatChange.Object)
		return;

	if(Config.LogStatChanges)
		StatChange.Log();

	// Don't show certain stat changes from actions
	bool ShowStatChange = !StatChange.Object->Battle || (StatChange.Object->Battle && !FromAction);

	// Show health change
	if(StatChange.HasStat("Health") && ShowStatChange) {
		_StatChangeUI StatChangeUI;
		StatChangeUI.Change = StatChange.Values["Health"].Int;
		if(StatChange.Object->Battle) {
			glm::vec2 Offset(StatChangeUI.Change < 0 ? -24 : 48, -50);
			StatChangeUI.StartPosition = StatChange.Object->Fighter->StatPosition + Offset * ae::_Element::GetUIScale();
			StatChangeUI.Battle = true;
			StatChangeUI.Font = ae::Assets.Fonts["hud_small"];
		}
		else {
			glm::vec2 Offset(StatChangeUI.Change < 0 ? -48 : 48, 0);
			StatChangeUI.StartPosition = HealthElement->Bounds.Start + glm::vec2(HealthElement->Size.x / 2.0f, 0) + Offset * ae::_Element::GetUIScale();
			StatChangeUI.Font = ae::Assets.Fonts["hud_medium"];
		}

		StatChangeUI.SetText(ae::Assets.Colors["red"], ae::Assets.Colors["green"]);
		StatChanges.push_back(StatChangeUI);
	}

	// Show mana change
	if(StatChange.HasStat("Mana") && ShowStatChange) {
		_StatChangeUI StatChangeUI;
		StatChangeUI.Change = StatChange.Values["Mana"].Int;
		if(StatChange.Object->Battle) {
			glm::vec2 Offset(StatChangeUI.Change < 0 ? -24 : 48, -14);
			StatChangeUI.StartPosition = StatChange.Object->Fighter->StatPosition + Offset * ae::_Element::GetUIScale();
			StatChangeUI.Battle = true;
			StatChangeUI.Font = ae::Assets.Fonts["hud_small"];
		}
		else {
			glm::vec2 Offset(StatChangeUI.Change < 0 ? -48 : 48, 0);
			StatChangeUI.StartPosition = ManaElement->Bounds.Start + glm::vec2(ManaElement->Size.x / 2.0f, 0) + Offset * ae::_Element::GetUIScale();
			StatChangeUI.Font = ae::Assets.Fonts["hud_medium"];
		}

		StatChangeUI.SetText(ae::Assets.Colors["blue"], ae::Assets.Colors["light_blue"]);
		StatChanges.push_back(StatChangeUI);
	}

	// Show experience change
	if(StatChange.HasStat("Experience")) {
		_StatChangeUI StatChangeUI;
		StatChangeUI.StartPosition = HealthElement->Bounds.Start + glm::vec2(HealthElement->Size.x / 2.0f, -25 * ae::_Element::GetUIScale());
		StatChangeUI.Change = StatChange.Values["Experience"].Int;
		StatChangeUI.Direction = -1.0f;
		StatChangeUI.Timeout = HUD_STATCHANGE_TIMEOUT_LONG;
		StatChangeUI.Font = ae::Assets.Fonts["menu_buttons"];
		StatChangeUI.SetText(glm::vec4(1.0f), glm::vec4(1.0f));
		StatChangeUI.Text += " XP";
		StatChanges.push_back(StatChangeUI);
	}

	// Show gold change
	if(StatChange.HasStat("Gold") || StatChange.HasStat("GoldStolen")) {
		_StatChangeUI StatChangeUI;

		// Check for battle
		if(StatChange.Object->Battle) {
			StatChangeUI.StartPosition = StatChange.Object->Fighter->ResultPosition;
			StatChangeUI.Battle = true;
		}
		else {
			ae::_TextBounds TextBounds;
			GoldElement->Font->GetStringDimensions(GoldElement->Text, TextBounds);
			StatChangeUI.StartPosition = glm::vec2(GoldElement->Bounds.Start.x + TextBounds.Width / 2, GoldElement->Bounds.Start.y - 40 * ae::_Element::GetUIScale());
		}

		// Get amount
		if(StatChange.HasStat("Gold")) {
			StatChangeUI.Change = StatChange.Values["Gold"].Int;
			if(StatChange.Object == Player && StatChange.Values["Gold"].Int > 0)
				GoldGained.PushBack(_RecentGold(StatChange.Values["Gold"].Int, PlayState.Time));
		}
		else {
			StatChangeUI.Change = StatChange.Values["GoldStolen"].Int;
			if(StatChange.Object == Player)
				GoldGained.PushBack(_RecentGold(StatChange.Values["GoldStolen"].Int, PlayState.Time));
		}

		StatChangeUI.Direction = -1.0f;
		StatChangeUI.Timeout = HUD_STATCHANGE_TIMEOUT_LONG;
		StatChangeUI.Font = ae::Assets.Fonts["menu_buttons"];
		StatChangeUI.SetText(ae::Assets.Colors["gold"], ae::Assets.Colors["gold"]);
		StatChanges.push_back(StatChangeUI);

		// Play sound
		if(StatChangeUI.Change != 0.0)
			PlayState.PlayCoinSound();
	}

	// Update screens
	Refresh();
}

// Remove all battle stat changes
void _HUD::ClearStatChanges(bool BattleOnly) {
	for(auto &StatChange : StatChanges) {
		if(!BattleOnly || (BattleOnly && StatChange.Battle))
			StatChange.Time = StatChange.Timeout;
	}
}

// Update hud labels
void _HUD::UpdateLabels() {
	auto &Attributes = Player->Character->Attributes;

	// Update portrait
	ae::Assets.Elements["image_hud_portrait"]->Texture = Player->Character->Portrait;

	// Update name
	std::stringstream PrefixBuffer;
	PrefixBuffer.imbue(std::locale(Config.Locale));
	GetPlayerPrefix(PrefixBuffer, Attributes["Rebirths"].Int, Attributes["Evolves"].Int, Attributes["Transforms"].Int, false);
	PrefixBuffer << Player->Name;
	ae::Assets.Elements["label_hud_name"]->Text = PrefixBuffer.str();

	// Update level
	std::stringstream Buffer;
	Buffer << "Level " << Player->Character->Level;
	ae::Assets.Elements["label_hud_level"]->Text = Buffer.str();
	Buffer.str("");

	// Update party
	if(Player->Character->PartyName.size())
		ae::Assets.Elements["label_hud_party"]->Text = "Party: " + Player->Character->PartyName;
	else
		ae::Assets.Elements["label_hud_party"]->Text = "No Party";

	// Update hardcore status
	ae::Assets.Elements["label_hud_hardcore"]->SetActive(Player->Character->Hardcore);

	// Update gold
	if(ae::Input.ModKeyDown(KMOD_ALT)) {
		ae::FormatSI<int64_t>(Buffer, GetGPM(), ae::RoundDown2);
		Buffer << " GPM";
	}
	else
		ae::FormatSI<int64_t>(Buffer, Attributes["Gold"].Int, ae::RoundDown2);
	GoldElement->Text = Buffer.str();
	Buffer.str("");

	// Set color
	if(Attributes["Gold"].Int < 0)
		GoldElement->Color = ae::Assets.Colors["red"];
	else
		GoldElement->Color = ae::Assets.Colors["gold"];
}
