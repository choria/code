/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

#include <glm/vec2.hpp>
#include <glm/vec4.hpp>

//     Config
const  int          DEFAULT_CONFIG_VERSION             =  8;
const  int          DEFAULT_SAVE_VERSION               =  11;
const  glm::ivec2   DEFAULT_WINDOW_SIZE                =  glm::ivec2(1024,768);
const  bool         DEFAULT_FULLSCREEN                 =  false;
const  bool         DEFAULT_AUDIOENABLED               =  true;
const  bool         DEFAULT_VSYNC                      =  true;
const  double       DEFAULT_MAXFPS                     =  240.0;
const  size_t       DEFAULT_MAXCLIENTS                 =  999;
const  double       DEFAULT_NETWORKRATE                =  1.0/20.0;
const  uint16_t     DEFAULT_NETWORKPORT                =  31234;
const  uint16_t     DEFAULT_NETWORKPINGPORT            =  31235;
const  double       DEFAULT_TIMESTEP                   =  1/100.0;
const  double       DEFAULT_AUTOSAVE_PERIOD            =  60.0;
//     Network
const  uint32_t     NETWORK_TIMEOUT_MINIMUM            =  5000;
const  uint32_t     NETWORK_TIMEOUT_MAXIMUM            =  10000;
//     Debug
const  double       DEBUG_STALL_THRESHOLD              =  1.0;
//     Camera
const  float        CAMERA_DISTANCE                    =  8.4375f;
const  float        CAMERA_DIVISOR                     =  0.3f;
const  float        CAMERA_EDITOR_DIVISOR              =  0.05f;
const  float        CAMERA_FOVY                        =  90.0f;
const  float        CAMERA_NEAR                        =  0.1f;
const  float        CAMERA_FAR                         =  100.0f;
const  float        CAMERA_SNAPPING_THRESHOLD          =  0.01f;
//     Account
const  uint32_t     ACCOUNT_SINGLEPLAYER_ID            =  1;
const  uint32_t     ACCOUNT_BOTS_ID                    =  2;
const  size_t       ACCOUNT_MAX_USERNAME_SIZE          =  20;
const  size_t       ACCOUNT_MAX_PASSWORD_SIZE          =  20;
const  int          ACCOUNT_MAX_CHARACTER_SLOTS        =  10;
//     Map
const  int          MAP_VERSION                        =  1;
const  int          MAP_TILE_WIDTH                     =  128;
const  int          MAP_TILE_HEIGHT                    =  128;
const  double       MAP_CLOCK_START                    =  8.0*60.0;
const  double       MAP_DAY_LENGTH                     =  24.0*60.0;
const  double       MAP_CLOCK_SPEED                    =  1.0;
const  double       MAP_EDITOR_CLOCK_SPEED             =  200.0;
const  glm::vec4    MAP_AMBIENT_LIGHT                  =  glm::vec4(0.3,0.3,0.3,1);
const  char*const   MAP_DEFAULT_TILESET                =  "textures/atlas/main.webp";
//     UI
const  glm::vec2    UI_PORTRAIT_SIZE                   =  glm::vec2(100,100);
const  glm::vec2    UI_STATUS_ICON_SIZE                =  glm::vec2(48,48);
const  glm::vec2    UI_SLOT_SIZE                       =  glm::vec2(64,64);
const  glm::vec2    UI_BUFF_SIZE                       =  glm::vec2(48,48);
const  glm::vec2    UI_TILE_SIZE                       =  glm::vec2(64,64);
const  glm::vec2    UI_GRID_SIZE                       =  glm::vec2(64,64);
//     HUD
const  int          HUD_CHAT_MESSAGES                  =  13;
const  int          HUD_CHAT_TIMEOUT                   =  10;
const  double       HUD_CHAT_FADETIME                  =  1.0;
const  int          HUD_CHAT_SIZE                      =  100;
const  float        HUD_CHAT_TEXTBOX_WIDTH             =  492.0f;
const  int          HUD_KEYNAME_LENGTH                 =  3;
const  double       HUD_RECENTITEM_TIMEOUT             =  10.0;
const  double       HUD_RECENTITEM_FADETIME            =  2.0;
const  double       HUD_MESSAGE_TIMEOUT                =  10.0;
const  double       HUD_MESSAGE_SHORT_TIMEOUT          =  3.0;
const  double       HUD_MESSAGE_FADETIME               =  2.0;
const  double       HUD_MAPNAME_TIMEOUT                =  3.0;
const  double       HUD_ACTIONRESULT_TIMEOUT           =  2.5;
const  double       HUD_ACTIONRESULT_SPEED             =  5.0;
const  double       HUD_ACTIONRESULT_TIMEOUT_SHORT     =  1.0;
const  double       HUD_ACTIONRESULT_SPEED_SHORT       =  0.25;
const  double       HUD_ACTIONRESULT_FADETIME          =  1.0;
const  double       HUD_STATCHANGE_TIMEOUT             =  1.0;
const  double       HUD_STATCHANGE_TIMEOUT_LONG        =  3.0;
const  double       HUD_STATCHANGE_FADETIME            =  0.5;
const  float        HUD_STATCHANGE_DISTANCE            =  20.0f;
const  double       HUD_GPM_TIME                       =  60.0;
const  int          HUD_PARTY_MAX                      =  8;
const  double       HUD_PARTY_UPDATE_PERIOD            =  0.5;
const  double       HUD_CHARACTER_SCREEN_CLOSE_TIME    =  0.5;
const  float        HUD_RELIC_WHEEL_SIZE               =  200.0f;
//     Battle
const  int          BATTLE_MINSTEPS                    =  12;
const  int          BATTLE_MAXSTEPS                    =  30;
const  int          BATTLE_MAX_OBJECTS_PER_SIDE        =  8;
const  int          BATTLE_ROWS_PER_SIDE               =  4;
const  int          BATTLE_COLUMN_SPACING              =  324;
const  double       BATTLE_DEFAULTATTACKPERIOD         =  2.0;
const  double       BATTLE_STUNNED_BATTLESPEED         =  5.0;
const  double       BATTLE_WAITDEADTIME                =  0.75;
const  double       BATTLE_MAX_START_TURNTIMER         =  0.35;
const  float        BATTLE_HEALTHBAR_WIDTH             =  126;
const  float        BATTLE_HEALTHBAR_HEIGHT            =  30;
const  int          BATTLE_PVP_VICTIM_SIDE             =  0;
const  int          BATTLE_PVP_ATTACKER_SIDE           =  1;
const  float        BATTLE_PVP_DISTANCE                =  1.42f*1.42f;
const  float        BATTLE_JOIN_DISTANCE               =  1.42f*1.42f;
const  float        BATTLE_COOP_DISTANCE               =  7.0f*7.0f;
const  int          BATTLE_BOSS_DIFFICULTY_PER_KILL    =  50;
const  double       BATTLE_DIFFICULTY_POWER            =  0.05;
const  double       BATTLE_DIFFICULTY_DAMAGE           =  0.2;
const  int          BATTLE_DIFFICULTY_PER_PLAYER       =  20;
const  int          BATTLE_DIFFICULTY_PER_PLAYER_BOSS  =  50;
//     Player
const  double       PLAYER_TELEPORT_TIME               =  3.0;
const  double       PLAYER_MOVETIME                    =  0.15;
const  int          PLAYER_MIN_MOVESPEED               =  5;
const  double       PLAYER_ATTACKTIME                  =  1.0;
const  int          PLAYER_NAME_SIZE                   =  15;
const  float        PLAYER_INVIS_ALPHA                 =  0.27f;
const  float        PLAYER_OFFLINE_ALPHA               =  0.4f;
const  int64_t      PLAYER_MAX_GOLD                    =  1e18;
const  double       PLAYER_DEATH_GOLD_PENALTY          =  0.2;
const  double       PLAYER_IDLE_TIME                   =  300.0;
const  double       PLAYER_SYNC_TIME                   =  2.0;
//     Game
const  int64_t      GAME_MAX_NUMBER                    =  1e18;
const  double       GAME_MIN_COOLDOWN                  =  0.1;
const  int          GAME_MAX_RESISTANCE                =  99;
const  int          GAME_MIN_RESISTANCE                =  -1000;
const  int          GAME_MAX_EVASION                   =  99;
const  int          GAME_MIN_BATTLE_SPEED              =  5;
const  int          GAME_MAX_BATTLE_SPEED              =  2000;
const  int          GAME_MAX_GROWTH                    =  999;
const  int          GAME_MAX_EVOLUTION                 =  9;
const  int64_t      GAME_ENCHANT_COST_BASE             =  5;
const  int64_t      GAME_ENCHANT_COST_POWER            =  3;
const  int64_t      GAME_ENCHANT_COST_RATE             =  5;
const  double       GAME_UPGRADE_COST_MULTIPLIER       =  0.2;
const  int64_t      GAME_UPGRADE_BASE_COST             =  0;
const  double       GAME_UPGRADE_AMOUNT                =  0.2;
const  double       GAME_NEGATIVE_UPGRADE_SCALE        =  0.25;
const  int          GAME_TRADING_LEVEL                 =  3;
const  int          GAME_DEFAULT_MAX_SKILL_LEVEL       =  5;
const  int          GAME_MAX_SKILL_UNLOCKS             =  10;
const  int          GAME_MAX_SKILL_LEVEL               =  50;
const  int          GAME_MAX_SKILLS                    =  50;
const  int          GAME_MAX_VENDOR_DISCOUNT           =  99;
const  int          GAME_PRIVILEGE_ITEM_STACK          =  25;
const  int          GAME_PRIVILEGE_EQUIPMENT_STACK     =  1;
const  double       GAME_REBIRTH_WEALTH_MULTIPLIER     =  0.1;
const  double       GAME_REBIRTH_PROGRESS_START        =  5.0;
const  double       GAME_REBIRTH_PROGRESS_SCALE        =  0.1;
const  double       GAME_BOSS_MIN_COOLDOWN             =  10.0;
const  int          GAME_BOSS_STRONGER_THRESHOLD       =  1000000;
const  int          GAME_BOSS_BANISHED_THRESHOLD       =  5000000;
const  int          GAME_REBIRTH_DIFFICULTY            =  1;
const  int          GAME_EVOLVE_DIFFICULTY             =  10;
const  int          GAME_TRANSFORM_DIFFICULTY          =  100;
const  int          GAME_ETERNAL_KNOWLEDGE_DIVISOR     =  5;
const  int          GAME_ETERNAL_PROTECTION_DIVISOR    =  4;
const  int          GAME_ETERNAL_GUARD_DIVISOR         =  4;
const  double       GAME_ETERNAL_ALACRITY_SCALE        =  10.0;
const  double       GAME_ETERNAL_COMMAND_SCALE         =  10.0;
const  double       GAME_ETERNAL_WARD_SCALE            =  0.5;
const  double       GAME_ETERNAL_IMPATIENCE_SCALE      =  1;
const  double       GAME_ETERNAL_CHARISMA_SCALE        =  1.0;
const  double       GAME_ETERNAL_HELL_SCALE            =  1.0;
const  double       GAME_ETERNAL_MALICE_SCALE          =  1.0;
const  double       GAME_ETERNAL_DECEIT_SCALE          =  -1.0;
const  int          GAME_TRANSFORM_RITE_SCALE          =  2;
const  int          GAME_TRANSFORM_REBIRTH_TIERS       =  10;
const  int          GAME_RITE_CATEGORY                 =  100;
const  glm::vec4    GAME_BLOODMOON_LIGHTCOLOR          =  glm::vec4(1.0f,0.1f,0.1f,1.0f);
const  double       GAME_BLOODMOON_TIMECHECK           =  12*60.0;
const  double       GAME_BLOODMOON_START_RANGE         =  5*MAP_DAY_LENGTH;
const  double       GAME_BLOODMOON_END_RANGE           =  10*MAP_DAY_LENGTH;
const  double       GAME_BLOODMOON_BLEND_SPEED         =  0.1;
const  int          GAME_BLOODMOON_PHASE_MAX           =  3;
const  double       GAME_SLAP_GOLD                     =  0.1;
//     Levels
const  int          LEVELS_MAX                         =  9999;
const  int          LEVELS_HEALTH_BASE                 =  150;
const  int          LEVELS_HEALTH_RATE                 =  25;
const  int          LEVELS_MANA_BASE                   =  0;
const  int          LEVELS_MANA_RATE                   =  0;
const  int          LEVELS_SKILL_POINTS                =  3;
const  int          LEVELS_INTERACT_FLAT_RANGE         =  10;
const  double       LEVELS_INTERACT_PERCENT_RANGE      =  0.9;
//     Actionbar
const  int          ACTIONBAR_BELT_STARTS              =  10;
const  int          ACTIONBAR_DEFAULT_BELTSIZE         =  1;
const  int          ACTIONBAR_DEFAULT_SKILLBARSIZE     =  4;
const  int          ACTIONBAR_MAX_SKILLBARSIZE         =  8;
const  int          ACTIONBAR_MAX_BELTSIZE             =  4;
const  int          ACTIONBAR_MAX_SIZE                 =  ACTIONBAR_BELT_STARTS+ACTIONBAR_DEFAULT_SKILLBARSIZE;
//     Inventory
const  int          INVENTORY_TOOLTIP_OFFSET           =  50;
const  int          INVENTORY_TOOLTIP_PADDING          =  14;
const  float        INVENTORY_TOOLTIP_WIDTH            =  420;
const  float        INVENTORY_TOOLTIP_HEIGHT           =  280;
const  float        INVENTORY_TOOLTIP_MAP_HEIGHT       =  550;
const  float        INVENTORY_TOOLTIP_TEXT_SPACING     =  26;
const  int          INVENTORY_MAX_TRADE_ITEMS          =  10;
const  int          INVENTORY_MAX_STASH_ITEMS          =  35;
const  int          INVENTORY_SIZE                     =  42;
const  int          INVENTORY_MAX_STACK                =  65535;
const  int          INVENTORY_INCREMENT_MODIFIER       =  5;
const  int          INVENTORY_SPLIT_MODIFIER           =  5;
//     Trader
const  int          TRADER_MAXITEMS                    =  8;
const  double       TRADER_COOLDOWN                    =  0.05;
//     Vendor
const  double       VENDOR_BUY_COOLDOWN                =  0.05;
//     Menu
const  float        MENU_ACCEPTINPUT_FADE              =  0.7f;
const  float        MENU_PAUSE_FADE                    =  0.7f;
const  double       MENU_DOUBLECLICK_TIME              =  0.250;
const  float        MENU_MAP_SCROLL_SPEED              =  0.005f;
const  double       MENU_CONNECT_COOLDOWN              =  3.0;
//     Scripting
const  char*const   SCRIPTS_GAME                       =  "scripts/game.lua";
const  char*const   SCRIPTS_DATA                       =  "scripts/data.lua";
const  char*const   SCRIPTS_DIALOGUE                   =  "scripts/dialogue.lua";
