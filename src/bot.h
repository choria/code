/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <ae/type.h>
#include <memory>
#include <string>

// Forward Declarations
class _Battle;
class _Map;
class _Object;
class _Scripting;
class _StatChange;
class _Stats;

namespace ae {
	template<class T> class _Manager;
	class _Buffer;
	class _ClientNetwork;
}

// Bot class
class _Bot {

	public:

		_Bot(const std::string &Username, const std::string &Password, const std::string &HostAddress, uint16_t Port);
		~_Bot();

		// Update
		void Update(double FrameTime);

		// Network
		void HandlePacket(ae::_Buffer &Data);
		void AssignPlayer(_Object *Object);
		void HandleStatChange(ae::_Buffer &Data, _StatChange &StatChange);
		_Object *CreateObject(ae::_Buffer &Data, ae::NetworkIDType NetworkID);

		std::unique_ptr<ae::_ClientNetwork> Network;

		ae::_Manager<_Object> *ObjectManager{nullptr};

		_Scripting *Scripting{nullptr};
		_Map *Map{nullptr};
		_Stats *Stats{nullptr};
		_Battle *Battle{nullptr};
		_Object *Player{nullptr};

		std::string Script;
		std::string Username;
		std::string Password;

};
