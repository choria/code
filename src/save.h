/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <ae/type.h>
#include <string>

// Forward Declarations
class _Object;
class _Scripting;
class _Stats;

namespace ae {
	class _Database;
	class _LogFile;
}

namespace Json {
	class Value;
}

// Classes
class _Save {

	public:

		_Save();
		~_Save();

		ae::_Database *Database;

		// Misc
		void StartTransaction();
		void EndTransaction();
		void SaveSettings();
		void GetSettings();
		void GetClockSettings(Json::Value &Data);
		std::string GenerateSalt();
		void GenerateBloodMoonTime();

		// Accounts
		bool CheckUsername(const std::string &Username);
		void CreateAccount(const std::string &Username, const std::string &Password);
		bool GetAccountInfo(const std::string &Username, const std::string &Password, uint32_t &AccountID, std::string &BannedText);
		void SetBanTime(uint32_t AccountID, const std::string &TimeFromNow);
		void SetMute(uint32_t AccountID, bool Value);
		void SetLastSlot(uint32_t AccountID, uint32_t Slot);

		// Characters
		uint8_t GetLastSlot(uint32_t AccountID);
		uint32_t GetCharacterID(uint32_t AccountID, uint32_t Slot, bool &Muted);
		uint32_t GetCharacterCount(uint32_t AccountID);
		uint32_t GetCharacterIDByName(const std::string &Name);
		uint32_t GetCharacterIDBySlot(uint32_t AccountID, uint32_t Slot);
		void DeleteCharacter(uint32_t CharacterID);
		uint32_t CreateCharacter(const _Stats *Stats, _Scripting *Scripting, uint32_t AccountID, uint32_t Slot, bool Hardcore, const std::string &Name, uint32_t PortraitID, uint32_t BuildID);

		// Objects
		void SetData(uint32_t CharacterID, const char *JsonString);
		void GetData(uint32_t CharacterID, std::string &JsonString);
		void SavePlayer(const _Object *Player, ae::NetworkIDType MapID, ae::_LogFile *Log);
		void LoadPlayer(_Object *Player);

		// State
		uint64_t Secret{0};
		double Clock{0.0};
		double BloodMoonTime{0.0};
		bool BloodMoonActive{false};

	private:

		int GetSaveVersion();
		void CreateDefaultDatabase();

};
