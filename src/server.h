/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <ae/buffer.h>
#include <ae/log.h>
#include <ae/type.h>
#include <map>
#include <memory>
#include <thread>
#include <unordered_map>
#include <vector>

// Forward Declarations
class _Battle;
class _Item;
class _Map;
class _Object;
class _Save;
class _Scripting;
class _Stats;
struct _Summon;

namespace ae {
	template<class T> class _Manager;
	class _Peer;
	class _ServerNetwork;
	struct _NetworkEvent;
}

struct _BattleEvent {
	_Object *Object;
	std::string Name;
	double Duration;
	uint32_t Zone;
	float BountyEarned;
	float BountyClaimed;
	int PVP;
	int Side;
	int Boss;
	bool Manual;
};

struct _RebirthEvent {
	_Object *Object;
	std::string Name;
	int Mode;
	int64_t Value;
};

struct _HighestSkill {
	_HighestSkill(uint32_t ID, int Level) : ID(ID), Level(Level) { }
	bool operator<(const _HighestSkill &Skill) const { return Skill.Level < Level; }
	uint32_t ID;
	int Level;
};

// Server class
class _Server {

	public:

		_Server(bool Dedicated, uint16_t NetworkPort);
		~_Server();

		void SaveGame();
		void Update(double FrameTime);
		void StartThread();
		void JoinThread();
		void StopServer(int Seconds=0);

		_Object *CreateSummon(int Index, bool JoiningBattle, _Object *Source, const _Summon &Summon);
		void SpawnPlayer(_Object *Player, ae::NetworkIDType MapID, uint32_t EventType);
		void QueueRebirth(_Object *Object, int Mode, const std::string &Name, int64_t Value);
		void QueueBattle(_Object *Object, const std::string &Name, uint32_t Zone, int Boss, bool PVP, bool Manual, float BountyEarned, float BountyClaimed, double Duration);
		void StartTeleport(_Object *Object, double Time);
		void SendMessage(ae::_Peer *Peer, const std::string &Message, const std::string &ColorName);
		void BroadcastMessage(ae::_Peer *IgnorePeer, const std::string &Message, const std::string &ColorName);
		void SendHUD(ae::_Peer *Peer);
		void SendPlayerPosition(ae::_Peer *Peer);
		void SetClock(double Clock);
		void SendEventStatus();
		void ClearBuff(_Object *Player, uint32_t BuffID);
		void UpdateBuff(_Object *Player, uint32_t BuffID, int64_t Level);
		void Slap(ae::NetworkIDType PlayerID);
		void Mute(uint32_t AccountID, bool Value);
		void Ban(uint32_t AccountID, const std::string &TimeFromNow);
		bool StartLog(ae::NetworkIDType PlayerID);
		void SendInventoryFullMessage(ae::_Peer *Peer, const _Item *Item, int Type);
		void SendStatusEffects(_Object *Player);
		void RemoveObject(_Object *Object);
		void UpdateParties();
		void UpdateEventCount();
		void AddParty(const std::string &PartyName, uint32_t CharacterID, _Object *Object);
		void RemoveParty(const std::string &PartyName, uint32_t CharacterID, _Object *Object);
		void SendSpawnPoint(_Object *Player);
		void ApplyLeavePenalty(ae::NetworkIDType NetworkID);

		// Packet handling
		void HandleLoginInfo(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleCharacterListRequest(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleCharacterPlay(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleCharacterCreate(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleCharacterDelete(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleInputCommand(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleRestart(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleInventoryMove(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleInventoryPrivilege(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleInventoryTransfer(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleInventoryUse(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleInventorySplit(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleInventoryDelete(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleVendorExchange(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleTraderAccept(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleSkillAdjust(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleEnchanterBuy(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleDisenchanterSell(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleChatMessage(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleTradeRequest(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleTradeCancel(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleTradeGold(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleTradeAccept(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandlePartyName(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleActionUse(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleActionBarChanged(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleBattleFinished(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandlePlayerStatus(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleClearBuff(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleSwapBuff(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleBlacksmithUpgrade(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleBlacksmithUpgradeSet(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleMinigamePay(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleMinigameGetPrize(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleJoin(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleExit(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleCommand(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleUpdateID(ae::_Buffer &Data, ae::_Peer *Peer);
		void HandleSyncRequest(ae::_Buffer &Data, ae::_Peer *Peer);

		// Parameters
		bool DevMode{false};
		bool Hardcore{false};
		bool NoPVP{false};
		bool AddBot{false};

		// State
		double ShutdownTime{0.0};
		double Time{0.0};
		double SaveTime{0.0};
		double BotTime{0.0};
		double PartyTime{0.0};
		int ActiveCount{0};
		uint16_t TimeSteps{0};
		bool Dedicated{false};
		bool Done{false};
		bool StartShutdownTimer{false};
		bool StartDisconnect{false};
		bool StartShutdown{false};
		bool AI{true};
		bool PenalizeSinglePlayer{false};

		// Log
		ae::_LogFile Log;

		// Stats
		const _Stats *Stats{nullptr};
		_Save *Save{nullptr};

		// Network
		std::unique_ptr<ae::_ServerNetwork> Network{nullptr};

		// Scripting
		_Scripting *Scripting{nullptr};

		// Objects
		ae::_Manager<_Object> *ObjectManager{nullptr};
		ae::_Manager<_Map> *MapManager{nullptr};
		ae::_Manager<_Battle> *BattleManager{nullptr};
		std::unordered_map<std::string, std::map<uint32_t, _Object *>> Parties;
		std::vector<_BattleEvent> BattleEvents;
		std::vector<_RebirthEvent> RebirthEvents;
		ae::NetworkIDType SinglePlayerNetworkID{0};

	private:

		_Object *CreatePlayer(ae::_Peer *Peer, uint32_t CharacterID);
		_Object *CreateBot();
		bool ValidatePeer(ae::_Peer *Peer);
		bool CheckAccountUse(ae::_Peer *Peer);
		void AddBattleSummons(_Battle *Battle, int Side, _Object *JoinPlayer=nullptr, bool Join=false);
		void StartBattle(_BattleEvent &BattleEvent);
		void StartRebirth(_RebirthEvent &RebirthEvent);
		int GetEventPhase();

		void HandleConnect(ae::_NetworkEvent &Event);
		void HandleDisconnect(ae::_NetworkEvent &Event);
		void HandlePacket(ae::_Buffer &Data, ae::_Peer *Peer);

		void SendMap(ae::_Peer *Peer, ae::NetworkIDType MapID);
		void SendItem(ae::_Peer *Peer, const _Item *Item, int Count);
		void SendPlayerInfo(ae::_Peer *Peer, bool ClearRecentItems=true);
		void SendCharacterList(ae::_Peer *Peer);
		void SendTradeInformation(_Object *Sender, _Object *Receiver);
		void SendTradePlayerInventory(_Object *Player);
		void SendClearWait(_Object *Player);

		// Threading
		std::thread *Thread{nullptr};
		ae::_Buffer PingPacket{1024};

		// Chat
		std::vector<std::string> SlapRegexes;
		std::vector<const _Item *> KeyUnlocks;
		std::vector<const _Item *> RelicUnlocks;
};
