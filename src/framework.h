/******************************************************************************
* choria
* Copyright (c) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <ae/log.h>
#include <cstdint>

// Forward Declarations
union SDL_Event;

namespace ae {
	class _Console;
	class _FrameLimit;
	class _State;
}

// Classes
class _Framework {

	public:

		enum StateType {
			INIT,
			UPDATE,
			CLOSE
		};

		// Setup
		void Init(int ArgumentCount, char **Arguments);
		void Close();

		// Update functions
		void Update();
		void Render();

		ae::_State *GetState() { return State; }
		void ChangeState(ae::_State *RequestedState);
		double GetTimeStepAccumulator() const { return TimeStepAccumulator; }

		// Console
		ae::_Console *Console;

		// State
		ae::_LogFile Log;
		bool Done;
		bool IgnoreNextInputEvent;

	private:

		int GlobalKeyHandler(const SDL_Event &Event);
		void HandleCommand(ae::_Console *Console);
		void LoadAssets();

		// Assets
		bool Load4kAssets;

		// States
		StateType FrameworkState;
		ae::_State *State;
		ae::_State *RequestedState;

		// Time
		ae::_FrameLimit *FrameLimit;
		uint64_t Timer;
		double TimeStep;
		double TimeStepAccumulator;

};

extern _Framework Framework;
