cmake_minimum_required(VERSION 3.10)

# set project
project(luac)

# set flags
add_definitions("-DLUA_USE_LINUX")

# set include directories
include_directories("../../ext/lua")

# add source code
file(GLOB_RECURSE SRC_LUA
	luac.c
	../../ext/lua/*.c
	../../ext/lua/*.h
)

# build binary
add_executable(luac ${SRC_LUA})

# link libraries
target_link_libraries(luac m dl)

# set output path to build directory
set(EXECUTABLE_OUTPUT_PATH .)
