-- Spring Water --

Script_Spring = {

	Activate = function(self, Level, Object, Change)
		Change.Health = math.floor(Level * (Object.HealPower * 0.01))
		Change.Mana = math.floor(Level * (Object.ManaPower * 0.01))
		Change.Buff = Buff_Spring.Pointer
		Change.BuffLevel = math.floor(25 * (Object.HealPower + Object.ManaPower - 100) * 0.01)
		Change.BuffDuration = 10

		return Change
	end,

	PlaySound = function(self, Object)
		Audio.Play("swamp0.ogg", 0.5)
	end,
}

-- Lava --

Script_Lava = {

	BaseDamage = 10,

	Activate = function(self, Level, Object, Change)
		if Object.LavaProtection == true then
			Change.ClearBuff = Buff_Invis.Pointer
			return Change
		end

		Level = math.ceil(Level * Object.DifficultyModifier)
		local Damage = math.ceil(self.BaseDamage * Level * Object.GetDamageReduction(DamageTypes["Fire"]))

		Change.Health = -Damage
		Change.Buff = Buff_Burning.Pointer
		Change.BuffLevel = Level
		Change.BuffDuration = 6
		if Object.HasBuff(Buff_Healing.Pointer) then
			Change.ClearBuff = Buff_Healing.Pointer
		else
			Change.ClearBuff = Buff_Invis.Pointer
		end

		return Change
	end,

	PlaySound = function(self, Object)
		if Object.LavaProtection == true then
			return
		end

		Audio.Play("flame0.ogg")
	end,
}

-- Boss --

Script_Boss = {

	Activate = function(self, Level, Object, Change)
		Change.Battle = Level

		return Change
	end,
}

-- Fall --

Script_Fall = {

	BaseDamage = 1000,
	HealthMultiplier = 5,

	GetLevel = function(self, Object)
		return math.floor(self.BaseDamage * (1 - Object.FallResist * 0.01) * Object.DifficultyModifier)
	end,

	Activate = function(self, Level, Object, Change)
		local BuffLevel = self:GetLevel(Object)

		Change.Health = -self.HealthMultiplier * BuffLevel
		if Change.Health < 0 then
			Change.Buff = Buff_Bleeding.Pointer
			Change.BuffLevel = BuffLevel
			Change.BuffDuration = 5
		end
		Change.MapChange = Level

		return Change
	end,

	PlaySound = function(self, Object)
		if Object.FallResist >= 100 then
			Audio.Play("swoop" .. Random.GetInt(0, 1) .. ".ogg")
		else
			Audio.Play("crunch" .. Random.GetInt(0, 1) .. ".ogg")
		end
	end,
}

-- Slow --

Script_Slow = {

	Activate = function(self, Level, Object, Change)
		if Object.Flippers == false then
			Change.Buff = Buff_Slowed.Pointer
			Change.BuffLevel = Level
			Change.BuffDuration = 5
		end

		if Object.HasBuff(Buff_Burning.Pointer) then
			Change.ClearBuff = Buff_Burning.Pointer
		else
			Change.ClearBuff = Buff_Invis.Pointer
		end

		return Change
	end,

	PlaySound = function(self, Object)
		if Object.Flippers == false then
			Audio.Play("swamp0.ogg", 0.5)
		end
	end,
}

-- Freezing --

Script_Freezing = {

	BaseDamage = 5,

	Activate = function(self, Level, Object, Change)
		if Object.FreezeProtection == true then
			Change.ClearBuff = Buff_Invis.Pointer
			return Change
		end

		Level = math.ceil(Level * Object.DifficultyModifier)
		local Damage = math.ceil(self.BaseDamage * Level * Object.GetDamageReduction(DamageTypes["Cold"]))

		Change.Health = -Damage
		Change.Buff = Buff_Freezing.Pointer
		Change.BuffLevel = Level
		Change.BuffDuration = 10
		if Object.HasBuff(Buff_Healing.Pointer) then
			Change.ClearBuff = Buff_Healing.Pointer
		else
			Change.ClearBuff = Buff_Invis.Pointer
		end

		return Change
	end,

	PlaySound = function(self, Object)
		if Object.FreezeProtection == true then
			return
		end

		Audio.Play("ice" .. Random.GetInt(0, 1) .. ".ogg")
	end,
}

-- Spikes --

Script_Spikes = {

	Activate = function(self, Level, Object, Change)
		if Object.Mountain == false then
			Level = math.ceil(Level * Object.DifficultyModifier)
			Change.Health = -Level * 5
			Change.Buff = Buff_Bleeding.Pointer
			Change.BuffLevel = Level
			Change.BuffDuration = 5
		end

		return Change
	end,

	PlaySound = function(self, Object)
		if Object.Mountain == false then
			Audio.Play("crunch" .. Random.GetInt(0, 1) .. ".ogg")
		end
	end,
}
