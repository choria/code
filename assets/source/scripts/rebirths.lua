-- Rebirth --

Base_Rebirth = Base_Object:New({

	Scale = 1,
	Type = "Rebirth",
	TierAttribute = "RebirthTier",
	GrowthAttribute = "RebirthGrowth",
	IntroText = "Sacrifice everything to rebirth anew",
	ForfeitText = "Forfeit all items, unlocks, keys, gold, experience and learned skills for:",
	InteractText = "rebirths",

	GetRiteLevel = function(self, Source, Value)
		if self.Type == "Evolve" then
			return math.min(Source.Transforms * TRANSFORM_RITE_SCALE + Source.Evolves + Source.RebirthEvolution + 1, Value)
		elseif self.Type == "Transform" then
			return math.min((Source.Transforms + 1) * TRANSFORM_RITE_SCALE, Value)
		end

		return Value
	end,

	GetBonus = function(self, Source, MoreInfo)
		local Value = Source[self.TierAttribute]
		if self.GrowthAttribute then
			Value = Value * (1 + Source[self.GrowthAttribute])
		end

		Value = Value * self.Scale

		return MoreInfo and Value or math.floor(Value)
	end,

	GetBonusText = function(self, Source, MoreInfo)
		if MoreInfo then
			return self:GetBonus(Source, true)
		else
			return FormatSI(self:GetBonus(Source, false))
		end
	end,

	CanUse = function(self, Level, Source)
		return Source[self.TierAttribute] > 0
	end,

	Use = function(self, Level, Duration, Source, Target, Result, Priority)
		Result.Target[self.Type] = 1
		Result.Target[self.Attribute] = Source[self.TierAttribute]

		return Result
	end,

	GetInfo = function(self, Source, Item)

		-- Get new rite levels
		local RebirthWisdom = self:GetRiteLevel(Source, Source.RebirthWisdom) + 1
		local RebirthWealth = self:GetRiteLevel(Source, Source.RebirthWealth)
		local RebirthKnowledge = self:GetRiteLevel(Source, Source.RebirthKnowledge)
		local RebirthPrivilege = self:GetRiteLevel(Source, Source.RebirthPrivilege)
		local RebirthEnchantment = self:GetRiteLevel(Source, Source.RebirthEnchantment)
		local RebirthPassage = self:GetRiteLevel(Source, Source.RebirthPassage)

		-- Get new rebirth numbers
		local Rebirths = Source.Rebirths
		local Evolves = Source.Evolves
		local Transforms = Source.Transforms
		local PainDifficulty = 0
		if self.Type == "Rebirth" then
			Rebirths = Rebirths + 1 + Source.RebirthGrowth
			PainDifficulty = Source.EternalPain
		elseif self.Type == "Evolve" then
			Evolves = Evolves + 1 + Source.RebirthEvolution
			Rebirths = 0
		elseif self.Type == "Transform" then
			Transforms = Transforms + 1
			Evolves = 0
			Rebirths = 0
		end

		-- Calculate new difficulty
		local NewDifficulty = 100 + Rebirths * REBIRTH_DIFFICULTY + Evolves * EVOLVE_DIFFICULTY + Transforms * TRANSFORM_DIFFICULTY + PainDifficulty
		if Item == Item_EternalPain then
			NewDifficulty = NewDifficulty + Item:GetBonus(Source)
		end

		-- Get text
		local GoldPercent = RebirthWealth * Item_RiteWealth.Multiplier
		local Gold = math.min(math.floor(GoldPercent * 0.01 * Source.Experience), MAX_GOLD)
		local WisdomPlural = RebirthWisdom ~= 1 and "s" or ""
		local PassagePlural = RebirthPassage ~= 1 and "s" or ""
		local PrivilegePlural = RebirthPrivilege ~= 1 and "s" or ""
		local PrivilegeText = RebirthPrivilege > 0 and "[c green]" .. RebirthPrivilege .. "[c white] item" .. PrivilegePlural .. " in your trade bag\n\n" or "\n"
		local UpgradeText = self.UpgradeText or self:GetUpgradeText(Source, Item)

		-- Set text
		return
			"[c gray]" .. self.IntroText .. "\n\n" ..
			self.ForfeitText .. "\n\n" ..
			"Permanent [c green]" .. self:GetBonusText(Source, Item.MoreInfo) .. UpgradeText ..
			"\n\n[c yellow]You will keep\n" ..
			"[c green]" .. RebirthKnowledge .. "[c white] of your highest level skills\n" ..
			PrivilegeText ..
			"[c yellow]You will start with\n" ..
			"[c green]" .. RebirthEnchantment .. "[c white] extra max skill levels\n" ..
			"[c green]" .. FormatSI(NewDifficulty) .. "%[c white] difficulty\n" ..
			"[c green]" .. RebirthWisdom .. "[c white] character level" .. WisdomPlural .. "\n" ..
			"[c green]" .. RebirthPassage .. "[c white] key" .. PassagePlural .. "\n" ..
			"[c green]" .. FormatSI(Gold) .. "[c white] gold ([c green]" .. GoldPercent .. "%[c white])\n\n" ..
			"[c yellow]Warning\nYou will only be able to interact with players that have the same number of " .. self.InteractText
	end,

	PlaySound = function(self)
		Audio.Play("rebirth.ogg")
	end,
})

-- Base Rebirth Token --

Base_RebirthToken = Base_Object:New({

	Difficulty = REBIRTH_DIFFICULTY,
	Tiers = 1,
	Type = "rebirth",
	TierAttribute = "RebirthTier",
	TierText = "character levels",

	GetTier = function(self, Source, MoreInfo)
		if MoreInfo then
			return Source[self.TierAttribute]
		else
			return FormatSI(Source[self.TierAttribute])
		end
	end,

	GetTierIncrease = function(self, Source)
		return REBIRTH_PROGRESS_START - Source.RebirthProgress * REBIRTH_PROGRESS_SCALE
	end,

	GetExtraTierText = function(self, Source)
		return ""
	end,

	GetResetText = function(self, Source)
		return ""
	end,

	GetInfo = function(self, Source, Item)
		return
			"You are in " .. self.Type .. " tier [c green]" .. self:GetTier(Source, Item.MoreInfo) .. "\n\n" ..
			"Every [c green]" .. self:GetTierIncrease(Source) .. "[c white] " .. self.TierText .. " gives [c green]" .. self.Tiers .. "[c white] tier, which increases your " .. self.Type .. " stat bonus\n\n" ..
			self:GetExtraTierText(Source) ..
			"Every " .. self.Type .. " adds [c green]" .. self.Difficulty .. "%[c white] difficulty" ..
			self:GetResetText(Source)
	end,
})

-- Rebirth Token --

Item_RebirthToken = Base_RebirthToken:New({

})

-- Evolve Token --

Item_EvolveToken = Base_RebirthToken:New({

	Type = "evolve",
	TierAttribute = "EvolveTier",
	TierText = "rebirths",
	Difficulty = EVOLVE_DIFFICULTY,

	GetTierIncrease = function(self, Source)
		return 10
	end,

	GetExtraTierText = function(self, Source)
		local RebirthTierIncrease = 1
		local RiteLevelCapIncrease = 1

		return "Evolving increases your rebirth tier by [c green]" .. RebirthTierIncrease .. "[c white] and your starting rite level cap by [c green]" .. RiteLevelCapIncrease .. "\n\n"
	end,

	GetResetText = function(self, Source)
		local RiteLevel = Source.Transforms * TRANSFORM_RITE_SCALE + Source.Evolves + 1

		return "\n\n[c yellow]Evolving resets your rebirths\n[c yellow]All rites will be reduced to level [c green]" .. RiteLevel
	end,
})

-- Transform Token --

Item_TransformToken = Base_RebirthToken:New({

	Type = "transform",
	TierAttribute = "TransformTier",
	TierText = "evolves",
	Difficulty = TRANSFORM_DIFFICULTY,

	GetTierIncrease = function(self, Source)
		return 10
	end,

	GetExtraTierText = function(self, Source)
		return "Transforming increases your rebirth tier by [c green]" .. TRANSFORM_REBIRTH_TIERS .. "[c white], evolve tier by [c green]1[c white], and your starting rite level cap by [c green]" .. TRANSFORM_RITE_SCALE .. "\n\n"
	end,

	GetResetText = function(self, Source)
		local RiteLevels = (Source.Transforms + 1) * TRANSFORM_RITE_SCALE

		return "\n\n[c yellow]Transforming resets your evolves/rebirths\n[c yellow]All rites will be reduced to level [c green]" .. RiteLevels
	end,
})

-- Eternal Strength

Item_EternalStrength = Base_Rebirth:New({

	Attribute = "EternalStrength",
	UpgradeText = "%[c white] damage power bonus",
})

-- Eternal Guard

Item_EternalGuard = Base_Rebirth:New({

	Attribute = "EternalGuard",

	GetUpgradeText = function(self, Source, Item)
		local DamageBlock
		local Armor
		if Item.MoreInfo then
			DamageBlock = self:GetBonus(Source, true)
			Armor = RoundDown1(self:GetBonus(Source, true) / ETERNAL_GUARD_DIVISOR)
		else
			DamageBlock = FormatSI(self:GetBonus(Source, false))
			Armor = FormatSI(math.floor((self:GetBonus(Source, true) / ETERNAL_GUARD_DIVISOR)))
		end

		return "[c white] damage block and [c green]" .. Armor .. "[c white] armor bonus"
	end,
})

-- Eternal Fortitude

Item_EternalFortitude = Base_Rebirth:New({

	Attribute = "EternalFortitude",
	UpgradeText = "%[c white] max health and heal power bonus",
})

-- Eternal Spirit

Item_EternalSpirit = Base_Rebirth:New({

	Attribute = "EternalSpirit",
	UpgradeText = "%[c white] max mana and mana power bonus",
})

-- Eternal Wisdom

Item_EternalWisdom = Base_Rebirth:New({

	Attribute = "EternalWisdom",
	UpgradeText = "%[c white] experience bonus",
})

-- Eternal Wealth

Item_EternalWealth = Base_Rebirth:New({

	Attribute = "EternalWealth",
	UpgradeText = "%[c white] gold bonus",
})

-- Eternal Knowledge

Item_EternalKnowledge = Base_Rebirth:New({

	Attribute = "EternalKnowledge",
	Scale = 1.0 / ETERNAL_KNOWLEDGE_DIVISOR,

	GetUpgradeText = function(self, Source, Item)
		local Plural = self:GetBonus(Source) ~= 1 and "s" or ""

		return "[c white] extra skill point" .. Plural
	end,
})

-- Eternal Pain

Item_EternalPain = Base_Rebirth:New({

	Attribute = "EternalPain",
	UpgradeText = "%[c white] difficulty increase",
})

-- Eternal Protection

Item_EternalProtection = Base_Rebirth:New({

	Attribute = "EternalProtection",
	UpgradeText = "%[c white] resistance bonus",
	Scale = 1.0 / ETERNAL_PROTECTION_DIVISOR,
})

-- Evolve --

Base_Evolve = Base_Rebirth:New({

	Type = "Evolve",
	TierAttribute = "EvolveTier",
	GrowthAttribute = "RebirthEvolution",
	IntroText = "Evolve into a higher form",
	ForfeitText = "Forfeit all rebirths, items, unlocks, keys, gold, experience and learned skills for:",
	InteractText = "evolves and rebirths",

	PlaySound = function(self)
		Audio.Play("sparkle0.ogg")
	end,
})

-- Eternal Alacrity

Item_EternalAlacrity = Base_Evolve:New({

	Attribute = "EternalAlacrity",
	Scale = ETERNAL_ALACRITY_SCALE,
	UpgradeText = "%[c white] battle speed bonus\n\n[c yellow]Max battle speed is " .. MAX_BATTLE_SPEED .. "%",
})

-- Eternal Command

Item_EternalCommand = Base_Evolve:New({

	Attribute = "EternalCommand",
	Scale = ETERNAL_COMMAND_SCALE,
	UpgradeText = "%[c white] summon battle speed bonus\n\n[c yellow]Max summon battle speed is " .. MAX_BATTLE_SPEED .. "%",
})

-- Eternal Ward

Item_EternalWard = Base_Evolve:New({

	Attribute = "EternalWard",
	Scale = ETERNAL_WARD_SCALE,
	UpgradeText = "%[c white] max resistance bonus\n\n[c yellow]Applies to summons\n[c yellow]Max resistance is " .. MAX_RESISTANCE .. "%",
})

-- Eternal Impatience

Item_EternalImpatience = Base_Evolve:New({

	Attribute = "EternalImpatience",
	Scale = ETERNAL_IMPATIENCE_SCALE,
	UpgradeText = "%[c white] cooldown reduction bonus\n\n[c yellow]Minimum cooldown is " .. MIN_COOLDOWN .. " seconds",
})

-- Eternal Charisma

Item_EternalCharisma = Base_Evolve:New({

	Attribute = "EternalCharisma",
	Scale = ETERNAL_CHARISMA_SCALE,
	UpgradeText = "%[c white] vendor discount bonus\n\n[c yellow]Max vendor discount is " .. MAX_VENDOR_DISCOUNT .. "%",
})

-- Transform --

Base_Transform = Base_Rebirth:New({

	Type = "Transform",
	TierAttribute = "TransformTier",
	GrowthAttribute = false,
	IntroText = "Transform into a higher being",
	ForfeitText = "Forfeit all rebirths, evolves, items, unlocks, keys, gold, experience and learned skills for:",
	InteractText = "transforms, evolves, and rebirths",

	PlaySound = function(self)
		Audio.Play("warp0.ogg")
	end,
})

-- Eternal Hell

Item_EternalHell = Base_Transform:New({

	Attribute = "EternalHell",
	Scale = ETERNAL_HELL_SCALE,
	UpgradeText = "[c white] difficulty multiplier bonus",
})

-- Eternal Malice

Item_EternalMalice = Base_Transform:New({

	Attribute = "EternalMalice",
	Scale = ETERNAL_MALICE_SCALE,
	UpgradeText = "[c white] target count bonus",
})

-- Eternal Deceit

Item_EternalDeceit = Base_Transform:New({

	Attribute = "EternalDeceit",
	Scale = ETERNAL_DECEIT_SCALE,
	UpgradeText = "[c white] set requirement bonus",
})

-- Rites --

Base_Rite = Base_Object:New({

	ExponentScale = 0.0,
	LinearScale = 1.0,

	GetRiteText = function(self, UpgradeText)
		return "Permanently " .. UpgradeText
	end,

	PlaySound = function(self)
		Audio.Play("unlock" .. Random.GetInt(0, 1) .. ".ogg", 0.85)
	end,

	GetUpgradedPrice = function(self, Source, Upgrades)
		local Index = Source.GetInventoryItemCount(self.Item.Pointer) + Upgrades

		local Cost = self.Item.Cost ^ (1 + Index * self.ExponentScale) + self.Item.Cost * (Index * self.LinearScale)
		if Cost > MAX_GOLD then
			return MAX_GOLD
		end

		-- Round to nearest 1000
		return math.floor(Cost / 1000) * 1000
	end,
})

-- Rite of Wealth

Item_RiteWealth = Base_Rite:New({

	Multiplier = REBIRTH_WEALTH_MULTIPLIER,
	ExponentScale = 0.01,
	LinearScale = 1.0,

	GetInfo = function(self, Source, Item)
		return self:GetRiteText("increase the amount of experience converted into gold after rebirth by [c green]" .. self:GetPercent(Item.Level) .. "%[c white]")
	end,

	GetPercent = function(self, Level)
		return Level * self.Multiplier
	end,

	GetCost = function(self, Source)
		return self:GetUpgradedPrice(Source, Source.RebirthWealth)
	end,

	Use = function(self, Level, Duration, Source, Target, Result, Priority)
		Result.Target.RebirthWealth = Level

		return Result
	end,
})

-- Rite of Wisdom

Item_RiteWisdom = Base_Rite:New({

	ExponentScale = 0.01,
	LinearScale = 1.0,

	GetInfo = function(self, Source, Item)
		local AddedText = ""
		if not self:CanBuy(Source) then
			AddedText = "\n\n[c red]Max wisdom attained"
		end

		return self:GetRiteText("increase the starting level after rebirth by [c green]" .. Item.Level .. "[c white]" .. AddedText)
	end,

	GetCost = function(self, Source)
		return self:GetUpgradedPrice(Source, Source.RebirthWisdom)
	end,

	CanBuy = function(self, Source)
		if Source.RebirthWisdom < MAX_LEVEL - 1 then
			return true
		end

		return false
	end,

	Use = function(self, Level, Duration, Source, Target, Result, Priority)
		if self:CanBuy(Source) then
			Result.Target.RebirthWisdom = Level
		end

		return Result
	end,
})

-- Rite of Knowledge

Item_RiteKnowledge = Base_Rite:New({

	ExponentScale = 0.05,
	LinearScale = 1.0,

	GetInfo = function(self, Source, Item)
		local AddedText = ""
		if not self:CanBuy(Source) then
			AddedText = "\n\n[c red]Max knowledge attained"
		end

		return self:GetRiteText(
			"increase the number of learned skills carried over after rebirth by [c green]" .. Item.Level .. "\n\n" ..
			"Skills will have a max level of [c green]" .. Source.RebirthEnchantment + DEFAULT_MAX_SKILL_LEVEL .. AddedText)
	end,

	GetCost = function(self, Source)
		return self:GetUpgradedPrice(Source, Source.RebirthKnowledge)
	end,

	CanBuy = function(self, Source)
		if Source.RebirthKnowledge < MAX_SKILLS then
			return true
		end

		return false
	end,

	Use = function(self, Level, Duration, Source, Target, Result, Priority)
		Result.Target.RebirthKnowledge = Level

		return Result
	end,
})

-- Rite of Power

Item_RitePower = Base_Rite:New({

	ExponentScale = 0.01,
	LinearScale = 1.0,
	EvolveScale = 1,
	TransformScale = 10,

	GetLevel = function(self, Source, Level)
		return Level + self.EvolveScale * Source.Evolves + self.TransformScale * Source.Transforms
	end,

	GetInfo = function(self, Source, Item)
		return self:GetRiteText("increase your rebirth tier bonus by [c green]" .. self:GetLevel(Source, Item.Level) .. "[c white]")
	end,

	GetCost = function(self, Source)
		return self:GetUpgradedPrice(Source, Source.RebirthPower)
	end,

	Use = function(self, Level, Duration, Source, Target, Result, Priority)
		Result.Target.RebirthPower = self:GetLevel(Source, Level)

		return Result
	end,
})

-- Rite of Progress

Item_RiteProgress = Base_Rite:New({

	ExponentScale = 0.022,
	LinearScale = 1.0,
	Max = 49,

	GetInfo = function(self, Source, Item)
		local AddedText = ""
		if not self:CanBuy(Source) then
			AddedText = "\n\n[c red]Max progress attained"
		end

		return self:GetRiteText("reduce the character levels required for a rebirth tier by [c green]" .. REBIRTH_PROGRESS_SCALE .. "[c white]" .. AddedText)
	end,

	GetCost = function(self, Source)
		return self:GetUpgradedPrice(Source, Source.RebirthProgress)
	end,

	CanBuy = function(self, Source)
		if Source.RebirthProgress < self.Max then
			return true
		end

		return false
	end,

	Use = function(self, Level, Duration, Source, Target, Result, Priority)
		if self:CanBuy(Source) then
			Result.Target.RebirthProgress = Level
		end

		return Result
	end,
})

-- Rite of Girth

Item_RiteGirth = Base_Rite:New({

	ExponentScale = 0.12,
	LinearScale = 4.0,

	GetInfo = function(self, Source, Item)
		local AddedText = ""
		if not self:CanBuy(Source) then
			AddedText = "\n\n[c red]Max girth attained"
		end

		return self:GetRiteText("increase the belt size after rebirth by [c green]" .. Item.Level .. "[c white]" .. AddedText)
	end,

	GetCost = function(self, Source)
		return self:GetUpgradedPrice(Source, Source.RebirthGirth)
	end,

	CanBuy = function(self, Source)
		if Source.RebirthGirth < MAX_BELT_SIZE - DEFAULT_BELTSIZE then
			return true
		end

		return false
	end,

	Use = function(self, Level, Duration, Source, Target, Result, Priority)
		if self:CanBuy(Target) then
			Result.Target.RebirthGirth = Level
		end

		return Result
	end,
})

-- Rite of Proficiency

Item_RiteProficiency = Base_Rite:New({

	ExponentScale = 0.05,
	LinearScale = 2.0,

	GetInfo = function(self, Source, Item)
		local AddedText = ""
		if not self:CanBuy(Source) then
			AddedText = "\n\n[c red]Max proficiency attained"
		end

		return self:GetRiteText("increase the skill bar size after rebirth by [c green]" .. Item.Level .. "[c white]" .. AddedText)
	end,

	GetCost = function(self, Source)
		return self:GetUpgradedPrice(Source, Source.RebirthProficiency)
	end,

	CanBuy = function(self, Source)
		if Source.RebirthProficiency < MAX_SKILLBAR_SIZE - DEFAULT_SKILLBARSIZE then
			return true
		end

		return false
	end,

	Use = function(self, Level, Duration, Source, Target, Result, Priority)
		if self:CanBuy(Source) then
			Result.Target.RebirthProficiency = Level
		end

		return Result
	end,
})

-- Rite of Insight

Item_RiteInsight = Base_Rite:New({

	ExponentScale = 0.03,
	LinearScale = 1.0,

	GetInfo = function(self, Source, Item)
		local AddedText = ""
		if not self:CanBuy(Source) then
			AddedText = "\n\n[c red]Max insight attained"
		end

		return self:GetRiteText("increase the starting skill point unlocks after rebirth by [c green]" .. Item.Level .. "[c white]" .. AddedText)
	end,

	GetCost = function(self, Source)
		return self:GetUpgradedPrice(Source, Source.RebirthInsight)
	end,

	CanBuy = function(self, Source)
		if Source.RebirthInsight < MAX_SKILL_UNLOCKS then
			return true
		end

		return false
	end,

	Use = function(self, Level, Duration, Source, Target, Result, Priority)
		if self:CanBuy(Source) then
			Result.Target.RebirthInsight = Level
		end

		return Result
	end,
})

-- Rite of Passage

Item_RitePassage = Base_Rite:New({

	Keys = {
		{ "Crab Key",         1.0e5  },
		{ "Graveyard Key",    2.5e5  },
		{ "Storage Key",      5.0e5  },
		{ "Witch Key",        7.5e5  },
		{ "Library Key",      1.0e6  },
		{ "Cellar Key",       2.5e6  },
		{ "Hideout Key",      5.0e6  },
		{ "Dungeon Key",      7.5e6  },
		{ "Tower Key",        1.0e7  },
		{ "Lighthouse Key",   2.5e7  },
		{ "Passage Key",      5.0e7  },
		{ "City Key",         7.5e7  },
		{ "Building Key",     1.0e8  },
		{ "Bridge Key",       2.5e8  },
		{ "Prisoner Key",     5.0e8  },
		{ "Lost Key",         7.5e8  },
		{ "Swamp Key",        1.0e9  },
		{ "Spider Key",       2.5e9  },
		{ "Chamber Key",      5.0e9  },
		{ "Ancient Key",      7.5e9  },
		{ "Timeless Key",     1.0e10 },
		{ "Black Runestone",  2.5e10 },
		{ "Blue Runestone",   5.0e10 },
		{ "Red Runestone",    7.5e10 },
		{ "Green Runestone",  1.0e11 },
		{ "Icy Key",          1.0e12 },
		{ "Frozen Key",       1.0e13 },
		{ "Eternal Key",      1.0e14 },
		{ "Dark Chamber Key", 1.0e15 },
		{ "Dead Key",         1.0e16 },
		{ "Chore Key",        1.0e18 },
	},

	GetInfo = function(self, Source, Item)
		local AddedText = ""
		if not self:CanBuy(Source) then
			AddedText = "\n\n[c red]Max passage attained"
		else
			AddedText = "\n\n[c yellow]Start with the " .. self.Keys[Source.RebirthPassage + 1][1]
		end

		return self:GetRiteText("increase the number of keys unlocked after rebirth by [c green]" .. Item.Level .. AddedText)
	end,

	GetCost = function(self, Source)
		local KeyIndex = math.max(1, math.min(Source.RebirthPassage + 1, #self.Keys))

		return self.Keys[KeyIndex][2]
	end,

	CanBuy = function(self, Source)
		if Source.RebirthPassage < #self.Keys then
			return true
		end

		return false
	end,

	Use = function(self, Level, Duration, Source, Target, Result, Priority)
		if self:CanBuy(Source) then
			Result.Target.RebirthPassage = Level
		end

		return Result
	end,
})

-- Rite of Enchantment

Item_RiteEnchantment = Base_Rite:New({

	ExponentScale = 0.015,
	LinearScale = 1.0,

	GetInfo = function(self, Source, Item)
		local AddedText = ""
		if not self:CanBuy(Source) then
			AddedText = "\n\n[c red]Max enchantment attained"
		end

		return self:GetRiteText("increase the max level for starting skills after rebirth by [c green]" .. Item.Level .. "[c white]" .. AddedText)
	end,

	GetCost = function(self, Source)
		return self:GetUpgradedPrice(Source, Source.RebirthEnchantment)
	end,

	CanBuy = function(self, Source)
		if Source.RebirthEnchantment < MAX_SKILL_LEVEL - DEFAULT_MAX_SKILL_LEVEL then
			return true
		end

		return false
	end,

	Use = function(self, Level, Duration, Source, Target, Result, Priority)
		if self:CanBuy(Source) then
			Result.Target.RebirthEnchantment = Level
		end

		return Result
	end,
})

-- Rite of Privilege

Item_RitePrivilege = Base_Rite:New({

	ExponentScale = 0.05,
	LinearScale = 2.0,
	Max = 10,

	GetInfo = function(self, Source, Item)
		local Level = math.min(Source.RebirthPrivilege + 1, self.Max)
		local ItemStack = REBIRTH_PRIVILEGE_ITEM_STACK * Level
		local EquipmentStack = REBIRTH_PRIVILEGE_EQUIPMENT_STACK * Level
		local AddedText = ""
		if not self:CanBuy(Source) then
			AddedText = "\n\n[c red]Max privilege attained"
		end

		return self:GetRiteText(
			"increase the number of items and summons carried over after rebirth by [c green]" .. Item.Level ..
			"[c white]" .. AddedText .. "\n\n" ..
			"[c yellow]Items must be placed in your trade bag\n\n" ..
			"[c yellow]Items limited to a stack size of " .. ItemStack .. "\n" ..
			"[c yellow]Equipment limited to a stack size of " .. EquipmentStack
		)
	end,

	GetCost = function(self, Source)
		return self:GetUpgradedPrice(Source, Source.RebirthPrivilege)
	end,

	CanBuy = function(self, Source)
		if Source.RebirthPrivilege < self.Max then
			return true
		end

		return false
	end,

	Use = function(self, Level, Duration, Source, Target, Result, Priority)
		if self:CanBuy(Source) then
			Result.Target.RebirthPrivilege = Level
		end

		return Result
	end,
})

-- Rite of Soul

Item_RiteSoul = Base_Rite:New({

	Max = 100,

	GetInfo = function(self, Source, Item)
		local AddedText = ""
		if not self:CanUse(1, Source) then
			AddedText = "\n\n[c red]Max soul attained"
		end

		return self:GetRiteText("decrease boss cooldowns by [c green]" .. Item.Level .. "%[c white]" .. AddedText)
	end,

	GetCost = function(self, Source)
		return self:GetUpgradedPrice(Source, Source.RebirthSoul)
	end,

	CanUse = function(self, Level, Source)
		if Source.RebirthSoul < self.Max then
			return true
		end

		return false
	end,

	Use = function(self, Level, Duration, Source, Target, Result, Priority)
		if self:CanUse(Level, Source, Target) then
			Result.Target.RebirthSoul = Level
		end

		return Result
	end,
})

-- Rite of Growth

Item_RiteGrowth = Base_Rite:New({

	ExponentScale = 0.04,
	LinearScale = 10.0,

	GetInfo = function(self, Source, Item)
		local AddedText = ""
		if not self:CanBuy(Source) then
			AddedText = "\n\n[c red]Max growth attained"
		else
			AddedText = "\n\n[c yellow]Max growth is " .. MAX_GROWTH
		end

		return self:GetRiteText("increase the number of rebirths per rebirth by [c green]" .. Item.Level .. AddedText)
	end,

	CanBuy = function(self, Source)
		if Source.RebirthGrowth < MAX_GROWTH then
			return true
		end

		return false
	end,

	GetCost = function(self, Source)
		return self:GetUpgradedPrice(Source, Source.RebirthGrowth)
	end,

	Use = function(self, Level, Duration, Source, Target, Result, Priority)
		Result.Target.RebirthGrowth = Level

		return Result
	end,
})

-- Rite of Evolution

Item_RiteEvolution = Base_Rite:New({

	GetInfo = function(self, Source, Item)
		local AddedText = ""
		if not self:CanUse(1, Source) then
			AddedText = "\n\n[c red]Max evolution attained"
		else
			AddedText = "\n\n[c yellow]Max evolution is " .. MAX_EVOLUTION
		end

		return self:GetRiteText("increase the number of evolves per evolve by [c green]" .. Item.Level .. AddedText)
	end,

	GetCost = function(self, Source)
		return self:GetUpgradedPrice(Source, Source.RebirthEvolution)
	end,

	CanUse = function(self, Level, Source)
		if Source.RebirthEvolution < MAX_EVOLUTION then
			return true
		end

		return false
	end,

	Use = function(self, Level, Duration, Source, Target, Result, Priority)
		if self:CanUse(Level, Source, Target) then
			Result.Target.RebirthEvolution = Level
		end

		return Result
	end,
})

-- Rite of Traversal

Item_RiteTraversal = Base_Rite:New({

	Items = {
		{ "Dimensionality",     5^0 * 1e14 },
		{ "Flippers",           5^1 * 1e14 },
		{ "Mountain Climbers",  5^2 * 1e14 },
		{ "Winged Shoes",       5^3 * 1e14 },
		{ "Fire Walkers",       5^4 * 1e14 },
		{ "Ice Walkers",        5^5 * 1e14 },
	},

	GetInfo = function(self, Source, Item)
		local AddedText = ""
		if not self:CanBuy(Source) then
			AddedText = "\n\n[c red]Max traversal attained"
		else
			AddedText = "\n\n[c yellow]Start with " .. self.Items[Source.RebirthTraversal + 1][1]
		end

		return self:GetRiteText("increase the number of starting relics after rebirth by [c green]" .. Item.Level .. AddedText)
	end,

	GetCost = function(self, Source)
		local Count = Source.GetInventoryItemCount(self.Item.Pointer)
		local Index = math.max(1, math.min(Source.RebirthTraversal + Count + 1, #self.Items))

		return self.Items[Index][2]
	end,

	CanBuy = function(self, Source)
		if Source.RebirthTraversal < #self.Items then
			return true
		end

		return false
	end,

	Use = function(self, Level, Duration, Source, Target, Result, Priority)
		if self:CanBuy(Source) then
			Result.Target.RebirthTraversal = Level
		end

		return Result
	end,
})
