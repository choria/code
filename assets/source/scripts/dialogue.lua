
-- Vendors

Vendor_Dialogue = {
	['ham_trainer'] = [[
		I can teach you some new [c yellow]skills[c white]... for a price of course!

		If you need another skill slot, talk to my blind son in [c yellow]cellar[c white].
	]],

	['ham_supplies'] = [[
		I have the finest supplies in all of... [c yellow]Hamlen[c white]!

		Of course, this map here ain't looking too hot. Say, why don't you visit my good friend [c yellow]Johm[c white] out by the [c yellow]Sandy Cave[c white]. He should be able to get it all fixed up for you.

		Also, don't forget to stock up on [c yellow]torches[c white]. You wouldn't want to get lost in a cave without one!
	]],

	['ham_weapons'] = [[
		Each one of my weapons has a unique [c yellow]power[c white].

		Make sure to match it up with the right [c yellow]armor set[c white] to fully unlock its abilities. Looks better, too!

		If you feel like it needs a good sharpening, head over to the town [c yellow]blacksmith[c white] to upgrade it.

		Don't forget to give the practice [c yellow]dummy[c white] a couple of whacks afterwards!
	]],

	['ham_armor'] = [[
		Buy a full set of armor today!

		Best to keep each piece nice and shiny.
	]],

	['ham_gambler'] = [[
		Trying to get into that [c yellow]graveyard[c white] by the swamp? My crazy brother [c yellow]Pete[c white] might still have the key. He likes to hang around that old withered [c yellow]tree[c white] behind the graveyard.

		Although, if you get lost looking for him, you could always try gambling for my spooky [c yellow]map[c white]! Heh heh!
	]],

	['fem_vendor'] = [[
		The [c yellow]library[c white] contains a vast wealth of knowledge.

		Only the initiated may enter.

		Seek out [c yellow]Tas[c white] in the [c yellow]graveyard[c white].
	]],

	['san_supplies'] = [[
		Just some basic, [c yellow]dusty[c white] supplies. Nothing fancy here.
	]],

	['san_trainer'] = [[
		Are you ready to take your combat to the next level?

		Let me teach you some new powerful melee [c yellow]skills[c white].

		You should talk with [c yellow]Erb[c white] up in the old [c yellow]Ruined City[c white] if you really want to master your weapons arts.
	]],

	['san_weapons'] = [[
		These are the best weapons you'll find on this side of the river.

		Don't forget to pair it with a [c yellow]set[c white] of armor next door.
	]],

	['san_armor'] = [[
		Beware the old [c yellow]Sentinels[c white] in the [c yellow]Ruined City[c white]. They don't move fast but they sure pack a wallop!

		You're better off buying my armor to protect yourself against them. I promise to clean the dust off if you do!
	]],

	['graveyard_trainer'] = [[
		Have you seen ole' [c yellow]Rog[c white] wandering around here?

		He usually takes a break from the [c yellow]coffin[c white] life to scare unwary visitors and to shoo away the occasional [c yellow]grave robber[c white].
	]],

	['wym_vendor'] = [[
		Careful around the swamps here... that ain't your grandma's [c yellow]lava[c white] out there!

		[c yellow]Gurm[c white] is deep down into the swamp, and he's got the right [c yellow]tool[c white] for your survival. Bring him [c yellow]Steel [c yellow]Boots[c white] and plenty of [c yellow]Lava [c yellow]Sludge[c white] and he'll get you fixed right up.

		Buy my trusty [c yellow]map[c white] and you'll find him at the [c orange]orange[c white] mark.
	]],
}

-- Traders

Trader_Dialogue = {
	['soothing_salve'] = [[
		These [c yellow]slimy globs[c white] are quite effective for bandaging your bleeding wounds. I can never get enough of them since I'm always getting snagged on those damn [c yellow]thorns[c white] in the caves.

		If you're running low, I hear there's a big [c yellow]cave[c white] full of slimes somewhere.
	]],

	['firebomb'] = [[
		Are your weapons ineffective against the ghastly [c yellow]ghosts[c white] in the graveyard?

		Try lighting them up with these [c yellow]firebombs[c white].
	]],

	['cellar_key'] = [[
		Psst! Hey, over here!

		Have you seen that great big [c yellow]bat[c white] down in the caves?

		Well, sooner or later you'll need to get down into that dark [c yellow]cellar[c white], and I got your key right here!
	]],

	['throwing_knives'] = [[
		Best stay away from the beaches around these parts... those [c yellow]crabs[c white] have mighty pincers!

		Of course, my [c yellow]throwing knives[c white] are of the finest quality and [c yellow]pierce[c white] right through their chitin.
	]],

	['poison'] = [[
		Tired of those pesky healers? Throw some of my [c yellow]poison[c white] at them!

		In fact, keep throwing cause the poison [c yellow]stacks[c white]! Ha ha!
	]],

	['spider_sword'] = [[
		I've infused the power of [c yellow]spiders[c white] into this mighty [c yellow]sword[c white]!

		I call it... the [c yellow]Spider Sword[c white]! Pretty good, right?

		Give those bastards a taste of their own medicine, ha ha!
	]],

	['better_map'] = [[
		Hey there, I'm [c yellow]Johm[c white]. Let me fix up that map for you. Careful handling that [c yellow]fire dust[c white] though... you don't want to get it in your eyes!

		I wish I hadn't lost my mountain climbing boots. Of course [c yellow]Sach[c white] has another pair if you're looking for some. Last I heard she's up north of [c yellow]Sanna[c white] by the [c yellow]Ruined City[c white].
	]],

	['flippers'] = [[
		Don't bother with that water by [c yellow]Lighthouse[c white] without my stylish blue [c yellow]flippers[c white]!

		You'll never be [c yellow]slow[c white] in water again!
	]],

	['sandy_map'] = [[
		Oh boy, mapping out that [c yellow]Sandy Cave[c white] was real fun, lemme tell ya! Hope you can read my [c red]scribbles[c white] ha ha!

		If you see [c yellow]Bursk[c white] in there, good luck trying to talk to him. He set up some sort of tomfoolery in that room.

		I fell through an [c yellow]illusory wall[c white] in another room. Couldn't remember where though. It lead to some [c yellow]slimy[c white] cavern from what I could tell.
	]],

	['slimy_map'] = [[
		You'll need [c yellow]Mountain Climbers[c white] or [c yellow]Flippers[c white] to get down to the [c yellow]Slimy Cave[c white]. Of course, I've heard of other sneaky ways in.

		The [c yellow]lava[c white] down there isn't so bad. Nothing like the [c yellow]lava[c white] over by [c yellow]Wymig[c white]!

		What you gotta watch out for are those damned evil [c yellow]venomous [c yellow]bats[c white], with their big gnarly [c yellow]fangs[c white]! They don't like hanging around water too much though.
	]],

	['dark_potion'] = [[
		It's... much... too... bright... down... here...
	]],

	['tower_key'] = [[
		The [c yellow]Tower of Femwood[c white] is a very dangerous and haunted place.

		Try not to [c yellow]fall[c white] off the bridge at the top. It's not a very soft landing at the bottom!
	]],

	['invisibility_potion'] = [[
		Trying to sneak past a well-guarded area? Why not test one of my [c yellow]invisibility[c white] potions!

		Don't go running through [c yellow]water[c white] though. You'll splash it all over yourself and ruin the effect!
	]],

	['graveyard_key'] = [[
		Hey uh.. you wouldn't happen to have any [c yellow]swamp globs[c white], would ya? Heh heh! They help cool me down after a nice [c yellow]lava[c white] bath in the caves!

		I could trade ya this key for 'em, but I'd steer clear of the [c yellow]graveyard[c white] in any case. It's haunted I tell ya!

		If you are crazy enough to venture in, try visiting [c yellow]Rog's [c yellow]headstone[c white], wherever it was...
	]],

	['gash'] = [[
		Have you visited the [c yellow]Raj's [c yellow]Hideout[c white] yet?

		We have the best gambling around. You can usually pick off a few tokens from all those damn [c yellow]goblins[c white] running around down there.
	]],

	['dark_ring'] = [[
		The [c yellow]graveyard[c white] sure is a good place to find corpses, isn't it?

		Make sure to pay my [c yellow]headstone[c white] a visit. Heh heh!
	]],

	['battle_potion'] = [[
		Ever wanted to just get into [c yellow]fight[c white] without having to shuffle around to agitate all those monsters?

		Try my special brew. You can [c yellow]stand still[c white] and let the monsters come right to you!

		Use it in [c yellow]lava[c white] and you'll see what I mean!
	]],

	['trolls_ring'] = [[
		Alright, you got me!

		I'm sure [c yellow]Biff[c white] over in [c yellow]Sanna[c white] would love this [c yellow]ring[c white]. Make sure to dangle it in front of him for a while before trading it. I love seeing him get worked up!
	]],

	['beggar'] = [[
		Boy, sure is hot out here. I could really go for a drink right about now...

		Spare a [c yellow]token[c white] and help an old fella out?
	]],

	['lucky_amulet'] = [[
		Are you ready to take your [c yellow]gambling[c white] addiction to the next level?

		Nah, I'm just jokin' around. But seriously, you should try out my [c yellow]Lucky Amulet[c white].

		I never leave home without it!
	]],

	['witch_key'] = [[
		Hey I got a key for ya! Well, it's not the [c yellow]City key[c white], but it's really good, I promise!

		Talk to old [c yellow]Sae[c white] in the [c yellow]Lighthouse[c white] if you're thinking about exploring these ruins.
	]],

	['potion_of_haste'] = [[
		Ever felt the need to [c yellow]jolt[c white] your party into action? Slip this one into their tea, heh heh!

		Nevermind the [c yellow]color[c white]... it's real good, I swear!
	]],

	['venom_blade'] = [[
		If you're heading up to the [c yellow]Ruined City[c white] you best prepare yourself. Those [c yellow]Ants[c white] are mighty smart and only go for the weaklings in your group.

		If you want to tilt the odds, use my [c yellow]Venom Blade[c white] against those damned [c yellow]Skeleton Priests[c white]!
	]],

	['taunt'] = [[
		There's nothing I hate more in this world than a dumb [c yellow]mage[c white], with their dumb hat and their dumb wand!

		That's why I love [c yellow]aggravating[c white] 'em and making em' forgot all about their dumb spells! Ha ha!
	]],

	['cold_ring'] = [[
		Welcome to the [c yellow]Femwood [c yellow]Library[c white]

		I assumed you showed your library card at the door, yes? Ha ha!
	]],

	['ghostly_potion'] = [[
		If you really want to sneak around up here, you'll want the best potions... something that makes you [c yellow]ghostly[c white]!

		Of course if you can't handle the [c yellow]heat[c white], should you really be mucking around in the kitchen?
	]],

	['elusive_potion'] = [[
		I've seen hundreds of adventurers fall off this old rickety [c yellow]bridge[c white] in my time. Never gets old, heh heh!

		Although, sometimes they walk out onto that [c yellow]spot[c white] over there and just vanish into thin air! No screaming or nothing!

		Never really wanted to try it myself, though.
	]],

	['fire_ring'] = [[
		Sooner or later, you'll need to splash around in hot [c yellow]lava[c white], gathering up all that [c yellow]sludge[c white] and what not.

		Trust me, you're going to want to have these wonderful [c yellow]rings[c white].
	]],

	['poison_ring'] = [[
		Poison can be quite deadly in these caves, especially if you don't [c yellow]treat[c white] it right away with a bat [c yellow]fang[c white].

		I'd stick to the [c yellow]water[c white] if you know what's good for ya!
	]],

	['large_torch'] = [[
		I got the best [c yellow]torches[c white] you can find... well, for lighting up the place at least.
	]],

	['building_key'] = [[
		Those old [c yellow]Sentinels[c white] hold the powers of the ancient [c yellow]Runestones[c white].

		Maybe someday I'll make these fragments [c yellow]whole[c white].
	]],

	['flamuss'] = [[
		Well done on passing the trial.

		You have been granted audience to the legendary [c yellow]Flamuss[c white]!
	]],

	['pickpocket'] = [[
		Who doesn't like an extra bit of [c yellow]coin[c white]?

		The real deal though is [c yellow]hunting[c white] other adventurers, heh heh.

		Talk to [c yellow]Lan[c white] if you're up for it. She's usually hiding in the [c yellow]Chamber[c white], waiting for unsuspecting victims.
	]],
}
