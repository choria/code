
-- Game
Game = {

	-- Constants
	ColdDuration = 4.0,
	BleedDuration = 5.0,
	FireDuration = 6.0,
	PoisonDuration = 10.0,
	HealDuration = 5.0,
	ManaDuration = 10.0,
	SanctuaryDuration = 10.0,
	DayLength = 24 * 60,
	DayBegins = 6 * 60,
	NightBegins = 22 * 60,
	NoonTime = 12 * 60,

	-- Determines if the clock is at night
	IsNight = function(self, Clock)
		return Clock < self.DayBegins or Clock >= self.NightBegins
	end,

	-- Get days until event
	DaysUntilEvent = function(self, Clock, EventTime)
		local TimeUntilNoon = self.NoonTime - Clock
		if TimeUntilNoon <= 0 then
			TimeUntilNoon = TimeUntilNoon + self.DayLength
		end

		return math.ceil(math.max(EventTime - TimeUntilNoon, 0) / self.DayLength)
	end,

	-- Set difficulty based on game time
	GetDifficulty = function(self, Clock, EventActive)

		local Added = 0
		local Multiplier = 1.0

		-- Hardest at midnight, easiest at noon
		local Delta = math.abs(Clock - self.NoonTime)
		local Distance = 0
		if Delta > self.NoonTime then
			Distance = self.DayLength - Delta
		else
			Distance = Delta
		end
		local DistanceMultiplier = Distance / self.NoonTime

		-- Check event
		if EventActive then

			-- Get difficulty multiplier
			Multiplier = 3.0 * DistanceMultiplier + 1.1
		else

			-- Make nighttime more difficult
			if self:IsNight(Clock) then
				Added = 50
				Multiplier = 0.5 * DistanceMultiplier + 1.05
			end
		end

		return Added, Multiplier
	end,
}

require("scripts/utils")
require("scripts/objects")
require("scripts/growth")
require("scripts/battle")
require("scripts/buffs")
require("scripts/procs")
require("scripts/skills")
require("scripts/attacks")
require("scripts/passives")
require("scripts/spells")
require("scripts/summons")
require("scripts/sets")
require("scripts/items")
require("scripts/rebirths")
require("scripts/ai")
require("scripts/bot")
require("scripts/scripts")
