-- Linear = ax + b --

Growth_Linear = Base_Object:New({

	A = 0,
	B = 0,

	Result = function(self, Level, RoundFunction)
		return (RoundFunction or math.floor)(self.A * (Level - 1) + self.B)
	end,

	ModifiedResult = function(self, Level, Scalar, Constant, RoundFunction)
		return (RoundFunction or math.floor)(Scalar * (self.A * (Level - 1) + self.B + (Constant or 0)))
	end,
})

-- Polynomial = ax^e + bx + c --

Growth_Polynomial = Base_Object:New({

	A = 0,
	B = 0,
	C = 0,
	E = 0,

	Result = function(self, Level, RoundFunction)
		return (RoundFunction or math.floor)(self.A * (Level - 1) ^ self.E + self.B * (Level - 1) + self.C)
	end,

	ModifiedResult = function(self, Level, Scalar, Constant, RoundFunction)
		return (RoundFunction or math.floor)(Scalar * (self.A * (Level - 1) ^ self.E + self.B * (Level - 1) + self.C + (Constant or 0)))
	end,
})

-- Diminishing = ax/(b + x) + c --

Growth_Diminishing = Base_Object:New({

	A = 0,
	B = 1,
	C = 0,

	Result = function(self, Level, RoundFunction)
		return (RoundFunction or math.floor)(self.A * (Level - 1) / ((Level - 1) + self.B) + self.C)
	end,

	ModifiedResult = function(self, Level, Scalar, Constant, RoundFunction)
		return (RoundFunction or math.floor)(Scalar * (self.A * (Level - 1) / ((Level - 1) + self.B) + self.C + (Constant or 0)))
	end,
})
