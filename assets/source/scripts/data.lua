
-- Additional item data that is loaded after the database
Item_Data = {

-- Amulets

	-- Mythical Amulet
	[231] = {
		AllSkills = 1,
	},

	-- Lava Amulet
	[316] = {
		MaxFireResist = 5,
	},

	-- Chilly Amulet
	[355] = {
		MaxColdResist = 5,
	},

	-- Hangman's Noose
	[422] = {
		Cursed = true,
	},

-- Rings

	-- Attack Power Ring
	[366] = {
		AttackPower = 100,
	},

	-- Elusive One
	[273] = {
		Initiative = 10,
		Delete = false,
	},

	-- Greater Ring of Pain
	[367] = {
		Cursed = true,
	},

	-- Hungering Ring
	[286] = {
		Cursed = true,
	},

-- Relics

	-- Greed
	[376] = {
		Endure = true,
	},

	-- Wisdom
	[377] = {
		Endure = true,
	},

	-- Suffering
	[379] = {
		Endure = true,
	},

	-- Dimensionality
	[371] = {
		Delete = false,
	},

	-- Flippers
	[387] = {
		Delete = false,
	},

	-- Mountain Climbers
	[388] = {
		Delete = false,
	},

-- One-Handed Weapons

	-- Flamuss
	[266] = {
		GoldGained = 5,
	},

	-- Shining Star
	[290] = {
		ExperienceGained = 5,
	},

	-- Bone Wand
	[374] = {
		SummonPower = 20,
	},

-- Two-Handed Weapons

	-- Dark Staff
	[212] = {
		SummonPower = 15,
	},

	-- Demon Stick
	[213] = {
		SummonPower = 25,
	},

	-- Diabolic Staff
	[317] = {
		SummonPower = 35,
	},

-- Shields

	-- Hand of Zog
	[344] = {
		Cursed = true,
		AttackPower = -100,
	},

-- Spells

	-- Flay
	[44] = {
		BuffPriority = 1,
	},

	-- Fracture
	[46] = {
		BuffPriority = 1,
	},

-- Boots

	-- Dimensional Slippers
	[263] = {
		Delete = false,
	},

}
