function RoundDown1(Value) return math.floor(Value * 10) / 10.0 end
function RoundDown2(Value) return math.floor(Value * 100) / 100.0 end
function RoundDown3(Value) return math.floor(Value * 1000) / 1000.0 end

function FormatSI(Value, RoundValue)
	local Number = math.abs(Value)
	if Number >= 1e18 then
		return RoundDown2(Value * 1e-18) .. "E"
	elseif Number >= 1e15 then
		return RoundDown2(Value * 1e-15) .. "P"
	elseif Number >= 1e12 then
		return RoundDown2(Value * 1e-12) .. "T"
	elseif Number >= 1e9 then
		return RoundDown2(Value * 1e-9) .. "G"
	elseif Number >= 1e6 then
		return RoundDown2(Value * 1e-6) .. "M"
	elseif Number >= 1e3 then
		return RoundDown2(Value * 1e-3) .. "K"
	end

	if RoundValue then
		return RoundDown1(Value)
	else
		return Value
	end
end

function FormatDuration(Time, RoundDown)
	local Seconds
	local RoundedSeconds
	if RoundDown == true then
		Time = math.floor(Time)
		Seconds = Time % 60
		RoundedSeconds = math.floor(Seconds)
	else
		Seconds = Time % 60
		RoundedSeconds = math.floor(Seconds * 100.0 + 0.5) / 100.0
	end

	if Seconds == 0 then
		Seconds = ""
	else
		Seconds = RoundedSeconds .. "s"
	end

	if Time < 60 then
		return RoundedSeconds .. "s"
	elseif Time < 3600 then
		return math.floor(Time / 60) .. "m" .. Seconds
	else

		local Minutes = math.floor(Time / 60 % 60)
		if Minutes == 0 then
			Minutes = ""
		else
			Minutes = Minutes .. "m"
		end

		return math.floor(Time / 3600) .. "h" .. Minutes .. Seconds
	end
end

-- Print table key value pairs
function Dump(Object)
	if type(Object) == 'table' then
		local Text = '{\n'
		for Key, Value in pairs(Object) do
			if type(Key) ~= 'number' then
				Key = '"'.. Key ..'"'
			end

			Text = Text .. '['.. Key ..'] = ' .. Dump(Value) .. ',\n'
		end

		return Text .. '}'
	else
		return '"' .. tostring(Object) .. '"'
	end
end
