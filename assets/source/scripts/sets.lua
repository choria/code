-- Base Set --

Base_Set = Base_Object:New({

	GetAddedInfo = function(self, Source, Item)
		return "[c light_green]Added " .. self.Item.Set.Name .. " Set Bonus\n" .. self:GetSetInfo(Source, Item.SetLevel, Item.MaxSetLevel, Item.MoreInfo)
	end,

	GetInfo = function(self, Source, Item)
		return self:GetAddedInfo(Source, Item)
	end,

	GetSetInfo = function(self, Source, Upgrades, MaxUpgrades, MoreInfo)

		-- Sort attributes by name
		local SortedAttributes = {}
		for Key in pairs(self.Attributes) do
			table.insert(SortedAttributes, Key)
		end
		table.sort(SortedAttributes)

		local Text = ""
		for i, Key in pairs(SortedAttributes) do
			local Label = Key:gsub("([a-z])([A-Z])", "%1 %2")
			local Value = self.Attributes[Key][1]
			local MaxValue = self.Attributes[Key][2]

			-- Show range or upgraded value
			Text = Text .. "[c white]" .. Label .. "[c white] "
			if MoreInfo == true then
				Text = Text .. "[c light_green]" .. self:GetDisplayValue(Key, Value, MaxValue, Upgrades, MaxUpgrades, true) .. "[c white] (" .. Value .. " - " .. MaxValue ..  ")\n"
			else
				Text = Text .. self:GetDisplayValue(Key, Value, MaxValue, Upgrades, MaxUpgrades, false) .. "\n"
			end
		end

		return Text
	end,

	GetDisplayValue = function(self, Key, Value, MaxValue, Upgrades, MaxUpgrades, Fractions)
		local UpgradedValue = self:GetUpgradedValue(Key, Value, MaxValue, Upgrades, MaxUpgrades, Fractions)

		local Sign = "+"
		if UpgradedValue < 0 then
			Sign = ""
		end

		local PercentPosition = string.find(Value, "%%")
		local Percent = ""
		if PercentPosition ~= nil then
			Percent = "%"
		end

		return Sign .. UpgradedValue .. Percent
	end,

	GetUpgradedValue = function(self, Key, Value, MaxValue, Upgrades, MaxUpgrades, Fractions)
		Value = string.gsub(Value, "%%", "")
		Value = tonumber(Value)
		if MaxUpgrades == 0 then
			return Value
		end

		MaxValue = string.gsub(MaxValue, "%%", "")
		MaxValue = tonumber(MaxValue)
		local Range = MaxValue - Value;

		local UpgradeProgression = Upgrades / MaxUpgrades
		local UpgradedValue = Value + Range * UpgradeProgression
		if not Fractions then
			UpgradedValue = math.floor(UpgradedValue)
		end

		return UpgradedValue
	end,

	SetStats = function(self, Object, Upgrades, MaxUpgrades, Change)
		for Key, Value in pairs(self.Attributes) do
			Change[Key] = self:GetUpgradedValue(Key, Value[1], Value[2], Upgrades, MaxUpgrades)
		end

		return Change
	end,

	Attributes = {}
})

-- Base Bonus

Set_Cloth = Base_Set:New({
	Attributes = {
		BattleSpeed = { "5%", "20%" },
		BleedPower = { "10%", "50%" },
		BleedResist = { "10%", "25%" },
		ConsumeChance = { "-5%", "-15%" },
		Evasion = { "5%", "15%" },
		Initiative = { "20%", "40%" },
		MaxHealth = { "10", "100" },
	},
})

Set_Black = Base_Set:New({
	Attributes = {
		BattleSpeed = { "15%", "35%" },
		BleedPower = { "25%", "100%" },
		BleedResist = { "15%", "45%" },
		ConsumeChance = { "-10%", "-25%" },
		Evasion = { "10%", "30%" },
		Initiative = { "30%", "50%" },
		MaxBleedResist = { "0%", "5%" },
		MaxHealth = { "50", "200" },
	},
})

Set_Elusive = Base_Set:New({
	Attributes = {
		BattleSpeed = { "25%", "75%" },
		BleedPower = { "50%", "250%" },
		BleedResist = { "25%", "75%" },
		ConsumeChance = { "-15%", "-50%" },
		Evasion = { "20%", "50%" },
		Initiative = { "40%", "75%" },
		MaxBleedResist = { "5%", "10%" },
		MaxHealth = { "125", "500" },
		TargetCount = { "0", "1" },
	},
})

Set_Mage = Base_Set:New({
	Attributes = {
		AllSkills = { "0", "1" },
		ElementalResist = { "5%", "25%" },
		ManaRegen = { "2", "4" },
		MaxHealth = { "5", "50" },
		MaxMana = { "50", "150" },
	},
})

Set_Wizard = Base_Set:New({
	Attributes = {
		AllSkills = { "0.5", "2" },
		ElementalResist = { "15%", "40%" },
		ManaRegen = { "3", "8" },
		MaxElementalResist = { "0%", "5%" },
		MaxHealth = { "25", "100" },
		MaxMana = { "100", "300" },
	},
})

Set_Arcane = Base_Set:New({
	Attributes = {
		AllSkills = { "1", "5" },
		Cooldowns = { "-5%", "-25%" },
		ElementalResist = { "25%", "60%" },
		ManaRegen = { "5", "20" },
		MaxElementalResist = { "5%", "10%" },
		MaxHealth = { "50", "250" },
		MaxMana = { "200", "750" },
		TargetCount = { "0", "1" },
	},
})

Set_Leather = Base_Set:New({
	Attributes = {
		Armor = { "2", "10" },
		ColdResist = { "10%", "25%" },
		HealPower = { "5%", "15%" },
		MaxHealth = { "25", "250" },
		PhysicalPower = { "10%", "50%" },
	},
})

Set_ReinforcedLeather = Base_Set:New({
	Attributes = {
		Armor = { "6", "20" },
		ColdResist = { "15%", "50%" },
		HealPower = { "10%", "25%" },
		MaxColdResist = { "0%", "5%" },
		MaxHealth = { "125", "500" },
		PhysicalPower = { "25%", "100%" },
	},
})

Set_Warriors = Base_Set:New({
	Attributes = {
		Armor = { "13", "45" },
		ColdResist = { "25%", "75%" },
		FireResist = { "15%", "35%" },
		HealPower = { "15%", "75%" },
		MaxColdResist = { "5%", "10%" },
		MaxHealth = { "250", "1250" },
		PhysicalPower = { "50%", "250%" },
		TargetCount = { "0", "1" },
	},
})

Set_Bronze = Base_Set:New({
	Attributes = {
		Armor = { "5", "15" },
		AllResist = { "10%", "20%" },
		DamageBlock = { "15", "50" },
		MaxHealth = { "50", "400" },
		DamageReturn = { "10%", "25%" },
	},
})

Set_Iron = Base_Set:New({
	Attributes = {
		AllResist = { "15%", "35%" },
		Armor = { "10", "30" },
		DamageBlock = { "25", "250" },
		MaxAllResist = { "0%", "5%" },
		MaxHealth = { "200", "800" },
		DamageReturn = { "15%", "50%" },
	},
})

Set_Steel = Base_Set:New({
	Attributes = {
		AllResist = { "20%", "50%" },
		Armor = { "20", "75" },
		DamageBlock = { "125", "500" },
		HitChance = { "10%", "35%" },
		MaxAllResist = { "5%", "10%" },
		MaxHealth = { "400", "2000" },
		DamageReturn = { "50%", "100%" },
		TargetCount = { "0", "1" },
	},
})

Set_Dark = Base_Set:New({
	Attributes = {
		SummonLimit = { "1", "2" },
	},
})

Set_Health = Base_Set:New({
	Attributes = {
		HealPower = { "10%", "50%" },
	},
})

Set_Magic = Base_Set:New({
	Attributes = {
		ManaPower = { "10%", "50%" },
	},
})

Set_Warm = Base_Set:New({
	Attributes = {
		HealPower = { "25%", "100%" },
		HealthBonus = { "0%", "15%" },
	},
})

Set_Glowing = Base_Set:New({
	Attributes = {
		ManaPower = { "25%", "100%" },
		ManaBonus = { "0%", "15%" },
	},
})

Set_Trolls = Base_Set:New({
	Attributes = {
		HealPower = { "50%", "100%" },
		HealthBonus = { "5%", "25%" },
	},
})

Set_Jem = Base_Set:New({
	Attributes = {
		AttackPower = { "25%", "50%" },
		BleedPower = { "25%", "50%" },
		PoisonPower = { "25%", "50%" },
	},
})

Set_Pain = Base_Set:New({
	Attributes = {
		BossCooldowns = { "-10%", "-50%" },
		MonsterCount = { "25%", "50%" },
	},
})

Set_Suffering = Base_Set:New({
	Attributes = {
		BossCooldowns = { "-25%", "-75%" },
		MonsterCount = { "50%", "100%" },
	},
})

Set_Fire = Base_Set:New({
	Attributes = {
		FirePower = { "25%", "75%" },
	},
})

Set_Cold = Base_Set:New({
	Attributes = {
		ColdPower = { "25%", "75%" },
	},
})

Set_Lightning = Base_Set:New({
	Attributes = {
		LightningPower = { "25%", "75%" },
	},
})

Set_Poison = Base_Set:New({
	Attributes = {
		PoisonPower = { "25%", "75%" },
	},
})

Set_Bleed = Base_Set:New({
	Attributes = {
		BleedPower = { "25%", "75%" },
	},
})

Set_Mundane = Base_Set:New({
	Attributes = {
		PhysicalPower = { "15%", "50%" },
	},
})

-- Added Bonus --
-- Names need to start with SetBonus for the tooltip description to work correctly --

-- One-Handed Weapons --

SetBonus_MageWand = Base_Set:New({
	Attributes = {
		SpellDamage = { "10%", "25%" },
	},
})

SetBonus_WizardWand = Base_Set:New({
	Attributes = {
		SpellDamage = { "15%", "50%" },
	},
})

SetBonus_ArcaneWand = Base_Set:New({
	Attributes = {
		SpellDamage = { "25%", "125%" },
	},
})

SetBonus_BoneWand = Base_Set:New({
	Attributes = {
		PoisonPower = { "50%", "200%" },
		SummonBattleSpeed = { "10%", "25%" },
		SummonLimit = { "0.5", "2" },
	},
})

SetBonus_ShortSword = Base_Set:New({
	Attributes = {
		AttackPower = { "15%", "30%" },
	},
})

SetBonus_Sword = Base_Set:New({
	Attributes = {
		AttackPower = { "20%", "35%" },
	},
})

SetBonus_SpiderSword = Base_Set:New({
	Attributes = {
		BattleSpeed = { "10%", "25%" },
	},
})

SetBonus_Scimitar = Base_Set:New({
	Attributes = {
		AttackPower = { "25%", "35%" },
	},
})

SetBonus_VenomBlade = Base_Set:New({
	Attributes = {
		AttackPower = { "25%", "35%" },
		PoisonPower = { "25%", "50%" },
	},
})

SetBonus_SilverEdge = Base_Set:New({
	Attributes = {
		AttackPower = { "25%", "60%" },
	},
})

SetBonus_LightBlade = Base_Set:New({
	Attributes = {
		AttackPower = { "30%", "75%" },
	},
})

SetBonus_Flamuss = Base_Set:New({

	Attributes = {
		AttackPower = { "25%", "100%" },
		FirePower = { "25%", "100%" },
	},

	GetInfo = function(self, Source, Item)
		return "[c yellow]Grants freeze immunity\n\n" .. self:GetAddedInfo(Source, Item)
	end,

	Stats = function(self, Item, Object, Change)
		Change.FreezeProtection = true

		return Change
	end,
})

SetBonus_Mace = Base_Set:New({
	Attributes = {
		AttackPower = { "20%", "50%" },
	},
})

SetBonus_MorningStar = Base_Set:New({
	Attributes = {
		AttackPower = { "25%", "65%" },
	},
})

SetBonus_FlangedMace = Base_Set:New({
	Attributes = {
		AttackPower = { "30%", "80%" },
	},
})

SetBonus_ShiningStar = Base_Set:New({
	Attributes = {
		AttackPower = { "40%", "100%" },
	},
})

-- Two-Handed Weapons --

SetBonus_BambooStick = Base_Set:New({
	Attributes = {
		AttackPower = { "5%", "25%" },
		Initiative = { "5%", "15%" },
	},
})

SetBonus_FightingStick = Base_Set:New({
	Attributes = {
		AttackPower = { "10%", "35%" },
		Initiative = { "10%", "25%" },
	},
})

SetBonus_Quarterstaff = Base_Set:New({
	Attributes = {
		AttackPower = { "15%", "50%" },
		Initiative = { "15%", "35%" },
	},
})

SetBonus_MysticStaff = Base_Set:New({
	Attributes = {
		AttackPower = { "25%", "75%" },
		Initiative = { "25%", "50%" },
	},
})

SetBonus_BlessedStaff = Base_Set:New({
	Attributes = {
		HealPower = { "15%", "50%" },
		HealthBonus = { "5%", "15%" },
	},
})

SetBonus_HolyStaff = Base_Set:New({
	Attributes = {
		HealPower = { "25%", "75%" },
		HealthBonus = { "10%", "30%" },
	},
})

SetBonus_LightStaff = Base_Set:New({
	Attributes = {
		HealPower = { "50%", "200%" },
		HealthBonus = { "15%", "50%" },
	},
})

SetBonus_DarkStaff = Base_Set:New({
	Attributes = {
		SummonBattleSpeed = { "5%", "15%" },
		SummonLimit = { "0", "1" },
	},
})

SetBonus_DemonStick = Base_Set:New({
	Attributes = {
		SummonBattleSpeed = { "10%", "30%" },
		SummonLimit = { "0.5", "2" },
	},
})

SetBonus_DiabolicStaff = Base_Set:New({
	Attributes = {
		SummonBattleSpeed = { "15%", "50%" },
		SummonLimit = { "1", "3" },
	},
})

SetBonus_Axe = Base_Set:New({
	Attributes = {
		AttackPower = { "20%", "40%" },
	},
})

SetBonus_BattleAxe = Base_Set:New({
	Attributes = {
		AttackPower = { "25%", "50%" },
	},
})

SetBonus_Claymore = Base_Set:New({
	Attributes = {
		AttackPower = { "30%", "75%" },
	},
})

SetBonus_Greatsword = Base_Set:New({
	Attributes = {
		AttackPower = { "35%", "100%" },
	},
})

SetBonus_Icebrand = Base_Set:New({

	Attributes = {
		AttackPower = { "50%", "150%" },
	},

	GetInfo = function(self, Source, Item)
		return "[c yellow]Grants lava immunity\n\n" .. self:GetAddedInfo(Source, Item)
	end,

	Stats = function(self, Item, Object, Change)
		Change.LavaProtection = true

		return Change
	end,
})

-- Off-Hand Weapons --

SetBonus_SmallKnife = Base_Set:New({
	Attributes = {
		BleedPower = { "15%", "30%" },
	},
})

SetBonus_Dagger = Base_Set:New({
	Attributes = {
		BleedPower = { "20%", "40%" },
	},
})

SetBonus_Stiletto = Base_Set:New({
	Attributes = {
		BleedPower = { "25%", "50%" },
	},
})

SetBonus_SwiftKnife = Base_Set:New({
	Attributes = {
		BleedPower = { "30%", "75%" },
	},
})

SetBonus_MoonBlade = Base_Set:New({
	Attributes = {
		BleedPower = { "50%", "150%" },
		AttackPower = { "15%", "35%" },
	},
})

SetBonus_Bloodletter = Base_Set:New({
	Attributes = {
		BleedPower = { "25%", "50%" },
		HealPower = { "10%", "25%" },
	},
})

-- Shields --

SetBonus_Gauntlet = Base_Set:New({
	Attributes = {
		AttackPower = { "10%", "25%" },
		DamageBlock = { "5", "25" },
	},
})

SetBonus_BlackGauntlet = Base_Set:New({
	Attributes = {
		AttackPower = { "15%", "50%" },
		DamageBlock = { "10", "40" },
	},
})

SetBonus_ElusiveGauntlet = Base_Set:New({
	Attributes = {
		AttackPower = { "25%", "75%" },
		DamageBlock = { "20", "100" },
	},
})

SetBonus_MageBook = Base_Set:New({
	Attributes = {
		ManaPower = { "10%", "25%" },
	},
})

SetBonus_WizardBook = Base_Set:New({
	Attributes = {
		ManaPower = { "15%", "50%" },
	},
})

SetBonus_ArcaneBook = Base_Set:New({
	Attributes = {
		ManaPower = { "25%", "125%" },
	},
})

SetBonus_LeatherBuckler = Base_Set:New({
	Attributes = {
		DamageBlock = { "5", "25" },
	},
})

SetBonus_ReinforcedBuckler = Base_Set:New({
	Attributes = {
		DamageBlock = { "15", "50" },
	},
})

SetBonus_WarriorsBuckler = Base_Set:New({
	Attributes = {
		DamageBlock = { "25", "125" },
	},
})

SetBonus_BronzeShield = Base_Set:New({
	Attributes = {
		ShieldDamage = { "5%", "25%" },
	},
})

SetBonus_IronShield = Base_Set:New({
	Attributes = {
		ShieldDamage = { "15%", "50%" },
	},
})

SetBonus_SteelShield = Base_Set:New({
	Attributes = {
		ShieldDamage = { "25%", "100%" },
	},
})

SetBonus_HandOfZog = Base_Set:New({
	Attributes = {
		ManaShield = { "5%", "25%" },
	},
})

-- Helmet --

SetBonus_Cap = Base_Set:New({
	Attributes = {
		HealPower = { "5%", "25%" },
	},
})

-- Boots --

SetBonus_DimensionalSlippers = Base_Set:New({

	Attributes = {
		Evasion = { "1%", "5%" },
	},

	GetInfo = function(self, Source, Item)
		return "[c yellow]Grants diagonal movement\n\n" .. self:GetAddedInfo(Source, Item)
	end,

	Stats = function(self, Item, Object, Change)
		Change.DiagonalMovement = true

		return Change
	end,
})

SetBonus_LavaBoots = Base_Set:New({

	Attributes = {
		MoveSpeed = { "5%", "15%" },
	},

	GetInfo = function(self, Source, Item)
		return "[c yellow]Grants lava immunity\n\n" .. self:GetAddedInfo(Source, Item)
	end,

	Stats = function(self, Item, Object, Change)
		Change.LavaProtection = true

		return Change
	end,
})

-- Amulets --

SetBonus_SoothingAmulet = Base_Set:New({
	Attributes = {
		HealthBonus = { "5%", "10%" },
	},
})

SetBonus_BrilliantAmulet = Base_Set:New({
	Attributes = {
		ManaBonus = { "5%", "10%" },
	},
})
