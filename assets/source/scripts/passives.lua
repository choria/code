-- Base Passive

Base_Passive = Base_Skill:New({

	Power = Growth_Polynomial:New({ E = 3.764, A = 0.0001, B = 5, C = 25 }),
	Duration = Growth_Linear:New({ A = 90 / 49.0, B = 10 }),

	GetInfo = function(self, Source, Item)
		local LowerName = string.lower(self.AttributeName)
		return
			"Increase " .. LowerName .. " power by [c green]" .. self.Power:Result(Item.Level) .. "%[c white]\n" ..
			"Increase " .. LowerName .. " duration by [c green]" .. self.Duration:Result(Item.Level) .. "%"
	end,

	Stats = function(self, Level, Object, Change)
		Change[self.AttributeName .. "Power"] = self.Power:Result(Level)
		Change[self.AttributeName .. "Duration"] = self.Duration:Result(Level)

		return Change
	end,
})

-- Toughness --

Skill_Toughness = Base_Passive:New({

	Health = Growth_Linear:New({ A = 50, B = 50 }),
	Armor = Growth_Linear:New({ A = 45 / 49.0, B = 5 }),

	GetInfo = function(self, Source, Item)
		return
			"Increase max HP by [c green]" .. self.Health:Result(Item.Level) .. "\n" ..
			"[c white]Increase armor by [c green]" .. self.Armor:Result(Item.Level)
	end,

	Stats = function(self, Level, Object, Change)
		Change.MaxHealth = self.Health:Result(Level)
		Change.Armor = self.Armor:Result(Level)

		return Change
	end,
})

-- Arcane Mastery --

Skill_ArcaneMastery = Base_Passive:New({

	Mana = Growth_Linear:New({ A = 40, B = 40 }),

	GetInfo = function(self, Source, Item)
		return
			"Increase max MP by [c light_blue]" .. self.Mana:Result(Item.Level) .. "\n" ..
			"[c white]Increase mana power by [c green]" .. self.Power:Result(Item.Level) .. "%\n" ..
			"Increase mana duration by [c green]" .. self.Duration:Result(Item.Level) .. "%"
	end,

	Stats = function(self, Level, Object, Change)
		Change.MaxMana = self.Mana:Result(Level)
		Change.ManaPower = self.Power:Result(Level)
		Change.ManaDuration = self.Duration:Result(Level)

		return Change
	end,
})

-- Evasion --

Skill_Evasion = Base_Passive:New({

	Evasion = Growth_Linear:New({ A = 40 / 49.0, B = 10 }),
	BattleSpeed = Growth_Linear:New({ A = 25 / 49.0, B = 5 }),

	GetInfo = function(self, Source, Item)
		return
			"Increase evasion by [c green]" .. self.Evasion:Result(Item.Level) .. "%" .. "\n" ..
			"[c white]Increase battle speed by [c green]" .. self.BattleSpeed:Result(Item.Level, Item.MoreInfo and RoundDown1) .. "%"
	end,

	Stats = function(self, Level, Object, Change)
		Change.Evasion = self.Evasion:Result(Level)
		Change.BattleSpeed = self.BattleSpeed:Result(Level)

		return Change
	end,
})

-- Mana Shield --

Skill_ManaShield = Base_Passive:New({

	ManaShield = Growth_Diminishing:New({ A = 200, B = 100, C = 5 }),
	AttackPower = Growth_Linear:New({ A = 0, B = -100 }),

	GetInfo = function(self, Source, Item)
		return
			"Convert [c green]" .. self.ManaShield:Result(Item.Level) .. "%[c white] of attack damage taken to mana drain\n" ..
			"Decrease attack power by [c green]" .. -self.AttackPower:Result(Item.Level) .. "%[c white]"
	end,

	Stats = function(self, Level, Object, Change)
		Change.ManaShield = self.ManaShield:Result(Level)
		Change.AttackPower = self.AttackPower:Result(Level)

		return Change
	end,
})

-- Physical Mastery --

Skill_PhysicalMastery = Base_Passive:New({

	GetInfo = function(self, Source, Item)
		return "Increase physical power by [c green]" .. self.Power:Result(Item.Level) .. "%[c white]"
	end,

	Stats = function(self, Level, Object, Change)
		Change.PhysicalPower = self.Power:Result(Level)

		return Change
	end,
})

-- Summon Mastery --

Skill_SummonMastery = Base_Passive:New({

	SummonLimit = Growth_Linear:New({ A = 3 / 49.0, B = 1 }),

	GetInfo = function(self, Source, Item)
		return
			"Increase summon power by [c green]" .. self.Power:Result(Item.Level) .. "%\n" ..
			"Increase summon limit by [c green]" .. self.SummonLimit:Result(Item.Level, Item.MoreInfo and RoundDown2)
	end,

	Stats = function(self, Level, Object, Change)
		Change.SummonPower = self.Power:Result(Level)
		Change.SummonLimit = self.SummonLimit:Result(Level)

		return Change
	end,
})

-- Consume Mastery --

Skill_ConsumeMastery = Base_Passive:New({

	ConsumeTargets = Growth_Linear:New({ A = 4 / 49.0, B = 1 }),
	Power = Growth_Polynomial:New({ B = 5, C = 25 }),

	GetInfo = function(self, Source, Item)
		return
			"Increase consumable power by [c green]" .. self.Power:Result(Item.Level) .. "%\n" ..
			"Increase consumable target count by [c green]" .. self.ConsumeTargets:Result(Item.Level, Item.MoreInfo and RoundDown2)
	end,

	Stats = function(self, Level, Object, Change)
		Change.ConsumePower = self.Power:Result(Level)
		Change.ConsumeTargets = self.ConsumeTargets:Result(Level)

		return Change
	end,
})

-- Fire Mastery --

Skill_FireMastery = Base_Passive:New({
	AttributeName = "Fire",
})

-- Cold Mastery --

Skill_ColdMastery = Base_Passive:New({
	AttributeName = "Cold",
})

-- Lightning Mastery --

Skill_LightningMastery = Base_Passive:New({
	AttributeName = "Lightning",
})

-- Bleed Mastery --

Skill_BleedMastery = Base_Passive:New({
	AttributeName = "Bleed",
})

-- Poison Mastery --

Skill_PoisonMastery = Base_Passive:New({
	AttributeName = "Poison",
})

-- Heal Mastery --

Skill_HealMastery = Base_Passive:New({
	AttributeName = "Heal",
})
