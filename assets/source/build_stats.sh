#!/bin/bash

# constants
DB="../../working/stats/stats.db"

# check programs
type sqlite3 >/dev/null 2>&1 || {
	echo >&2 "'sqlite3' is not installed!"
	exit 1
}

# script options
set -e

# build db
mkdir -p "$(dirname "$DB")"
rm -f "$DB"
sqlite3 "$DB" < stats/stats.txt
