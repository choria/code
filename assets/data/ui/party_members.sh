#!/bin/bash

offset_y=0
for i in {0..6}; do

read -r -d '' temp << EOF
<element id="image_hud_party_portrait$i" offset_y="$offset_y" size_x="50" size_y="50" alignment_x="0" alignment_y="0">
	<element id="label_hud_party_name$i" font="hud_tiny" offset_x="56" offset_y="14" alignment_x="0" alignment_y="3" format="true"/>
	<element id="element_hud_party_health$i" offset_x="56" offset_y="20" size_x="80" size_y="12" alignment_x="0" alignment_y="0">
		<element id="image_hud_party_health_bar_empty$i" texture="textures/hud_repeat/health_empty.webp" size_x="100%" size_y="100%" alignment_x="0" alignment_y="0"/>
		<element id="image_hud_party_health_bar_full$i" texture="textures/hud_repeat/health_full.webp" size_x="100%" size_y="100%" alignment_x="0" alignment_y="0"/>
	</element>
	<element id="element_hud_party_mana$i" offset_x="56" offset_y="34" size_x="80" size_y="12" alignment_x="0" alignment_y="0">
		<element id="image_hud_party_mana_bar_empty$i" texture="textures/hud_repeat/mana_empty.webp" size_x="100%" size_y="100%" alignment_x="0" alignment_y="0"/>
		<element id="image_hud_party_mana_bar_full$i" texture="textures/hud_repeat/mana_full.webp" size_x="100%" size_y="100%" alignment_x="0" alignment_y="0"/>
	</element>
	<element id="image_hud_party_gold$i" texture="textures/hud/gold.webp" offset_x="130" offset_y="8" size_x="24" size_y="24">
		<element id="label_hud_party_gold$i" font="hud_micro" offset_x="28" offset_y="16" alignment_x="0" alignment_y="3"/>
	</element>
	<element id="image_hud_party_direction$i" texture="textures/hud/arrow.webp" offset_x="216" offset_y="8" size_x="24" size_y="24"/>
</element>
EOF

	echo "$temp"
	offset_y=$((offset_y+60))
done
