#!/bin/bash

for i in {0..10}; do

read -r -d '' out << EOF
<element id="element_hud_relics_$i" size_x="100" size_y="100" style="style_relic_bg" hover_style="style_relic_hover" clickable="true" index="$i">
	<element id="image_hud_relics_$i" size_x="64" size_y="64"/>
</element>
EOF

	echo "$out"
done
