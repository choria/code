#!/bin/bash

# check parameters
in="$1"
keyword="$2"
if [ -z "$in" ] || [ -z "$keyword" ]; then
	echo "Usage: $0 log_path keyword"
	exit 1
fi

# count items in matching keyword
grep -E "$keyword" "$in" | grep "[0-9]\+x [^(]*" -o | sort | uniq -c | gawk '
BEGIN {
	FS = " "
	OFS = "\t"
}
{
	# get item name from line
	item = $0
	sub($1 " " $2, "", item)
	sub("^ *", "", item)

	# get count
	add = $1
	if($2 == "5x")
		add *= 5;

	# add to total
	sum[item] += add;
}
END {

	# print all totals
	for(item in sum) {
		print sum[item], item }
}
' | sort -n
