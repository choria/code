#!/bin/bash
# Show min/max attribute for each item

# change to script dir
cd "$(dirname "${BASH_SOURCE[0]}")" || exit

# constants
db=../../../working/stats/stats.db
scale=0.2
negative_scale=0.25

# columns from item table
declare -A attributes
attributes["mindamage"]="MinDamage"
attributes["maxdamage"]="MaxDamage"
attributes["armor"]="Armor"
attributes["block"]="DamageBlock"
attributes["res"]="Resist"
attributes["pierce"]="Pierce"
attributes["maxhealth"]="MaxHealth"
attributes["maxmana"]="MaxMana"
attributes["healthregen"]="HealthRegen"
attributes["manaregen"]="ManaRegen"
attributes["battlespeed"]="BattleSpeed"
attributes["movespeed"]="MoveSpeed"
attributes["evasion"]="Evasion"
attributes["spell_damage"]="SpellDamage"

mapfile -t ids < <(sqlite3 "$db" "SELECT id FROM item WHERE itemtype_id NOT IN (1, 11, 13, 15, 16)")
for id in "${ids[@]}"; do
	sqlite3 "$db" "SELECT id, name FROM item WHERE id = $id"
	for column in "${!attributes[@]}"; do
		label=${attributes[$column]}
		if [ "$column" == "res" ]; then
			sqlite3 "$db" "\
				SELECT \
					dt.name || 'Resist' AS name, \
					$column AS start, \
					($column + $column * a.upgrade_scale * $scale * maxlevel) AS end \
				FROM \
					item i, \
					attribute a, \
					damagetype dt \
				WHERE \
					i.id = $id AND \
					a.name = '$label' AND \
					dt.id = i.restype_id AND \
					start <> 0 AND \
					end <> 0 \
			"
		else
			sqlite3 "$db" "\
				SELECT \
					'$label' AS name, \
					$column AS start, \
					CASE WHEN $column >= 0 THEN
						($column + $column * a.upgrade_scale * $scale * maxlevel) \
					ELSE
						MIN(0, $column + -$column * a.upgrade_scale * $scale * $negative_scale * maxlevel) \
					END AS end
				FROM \
					item i, \
					attribute a \
				WHERE \
					i.id = $id AND \
					a.name = '$label' AND \
					end <> 0 AND \
					start <> 0 \
			"
		fi
	done
	echo
done
