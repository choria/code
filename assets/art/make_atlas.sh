#!/bin/bash

# check programs
type magick >/dev/null 2>&1 || {
	echo >&2 "imagemagick is not installed!"
	exit 1
}

# change to script dir
cd "$(dirname "${BASH_SOURCE[0]}")" || exit

# create export directory
mkdir -p export
rm -f export/*.png

# create a one-pixel border for each texture that wraps around
for f in tiles/*.png; do
	magick "$f" -compose copy -write mpr:tile +delete -size 130x130 -tile-offset -1-1 tile:mpr:tile "export/t_$(basename "$f")"
done

# count tiles
count=$(find tiles -type f -iname "*.png" | wc -l)

# calculate number of columns the atlas should use
cols=$(echo "$count" | awk '{ x = sqrt($1); print x%1 ? int(x)+1 : x }')

# create atlas from all tiles
out="export/main.png"
magick montage -background transparent -geometry 130x130 -tile "$cols"x export/* "$out"

# move to source texture directory
mv "$out" ../source/textures/atlas/

# clean
rm -f export/*.png
