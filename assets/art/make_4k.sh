#!/bin/bash

# check programs
for program in "magick" "inkscape" "cwebp" "xmllint" "parallel"; do
	type $program >/dev/null 2>&1 || {
		echo >&2 "'$program' is not installed!"
		exit 1
	}
done

# script options
set -e

# change to script dir
cd "$(dirname "${BASH_SOURCE[0]}")" || exit

# init
rm -rf "4k"

# create texture directories
dest="4k/textures"
mkdir -p "$dest/buffs"
mkdir -p "$dest/builds"
mkdir -p "$dest/buttonbar"
mkdir -p "$dest/hud"
mkdir -p "$dest/items"
mkdir -p "$dest/keys"
mkdir -p "$dest/menu"
mkdir -p "$dest/minigames"
mkdir -p "$dest/models"
mkdir -p "$dest/monsters"
mkdir -p "$dest/objects"
mkdir -p "$dest/portraits"
mkdir -p "$dest/skills"
mkdir -p "$dest/status"

# make logo
./make_images.sh vector/logo.svgz 2
mv export/*.webp "$dest/menu/"

# make textures
for f in buffs builds buttonbar minigames models monsters objects portraits skills status; do
	./make_images.sh vector/$f.svgz 2
	mv export/*.webp "$dest/$f/"
done

# make hud
for f in hud hud24 hud48; do
	./make_images.sh vector/$f.svgz 2
	mv export/*.webp "$dest/hud/"
done

# make items
for f in armors boots consumables helms jewelry keys maps potions rebirth shields tradables unlocks weapons; do
	./make_images.sh vector/$f.svgz 2
	mv export/*.webp "$dest/items/"
done

# copy hud textures
cp ../source/textures/hud/body.webp "$dest/hud/"
cp ../source/textures/hud/experience_notch.webp "$dest/hud/"

# create slots
magick -size "$((6*128))x$((7*128))" tile:"$dest/hud/slot.webp" -define webp:lossless=true -define webp:method=6 -quality 100 "$dest/hud/inventory_slots.webp"
magick -size "$((5*128))x$((7*128))" tile:"$dest/hud/slot.webp" -define webp:lossless=true -define webp:method=6 -quality 100 "$dest/hud/vendor_slots.webp"

# pack textures
pushd 4k || exit
mkdir -p temp
rm -f temp/*
for f in textures/*; do
	../../../ext/ae/scripts/pack.py ./ "$f"

	pack=$(basename "$f")
	mv -v "${pack}.bin" "temp/${pack}_4k"
done
popd || exit

# move 4k textures into working directory
mv 4k/temp/* ../../working/textures
rm -fd 4k/temp
