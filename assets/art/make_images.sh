#!/bin/bash

# check programs
for program in "magick" "inkscape" "cwebp" "xmllint" "parallel"; do
	type $program >/dev/null 2>&1 || {
		echo >&2 "'$program' is not installed!"
		exit 1
	}
done

# script options
set -e

# function to optimize and rename file
worker_img() {
	cwebp -quiet -exact -z 9 "$1" -o "$2"
}

export -f worker_img

# get parameters
file=$1
scale=$2

# get file
if [ -z "$file" ]; then
	echo "No input .svg file"
	exit
fi

# get scale
if [ -z "$scale" ]; then
	scale=1
fi
echo "$file"
echo "calculating..."

# calculate density
density=$((96 * scale))

# get size of each image
width=$(identify -density 96 -format "%[fx:w/10]" "$file")
height=$(identify -density 96 -format "%[fx:h/10]" "$file")
width=$((width * scale))
height=$((height * scale))

# get list of objects
names=$(xmllint --xpath "//*[local-name()='svg']/*[local-name()='metadata']//*[local-name()='description']/text()" "$file")

# create export directory
mkdir -p export
rm -f export/*.{png,webp}

# export images
echo "exporting..."
temp_export="export/export.png"
inkscape "${file}" -d "${density}" -y 0 -o "${temp_export}" 2>/dev/null
magick "${temp_export}" -crop "${width}x${height}" +repage export/_out.png

# name files and optimize in parallel
echo "compressing..."
i=0
for name in $names; do
	oldname="export/_out-${i}.png"
	newname="export/${name}.webp"

	echo worker_img \""$oldname"\" \""$newname"\"
	i=$((i+1))
done | parallel

# clean up
rm -f export/_*
rm -f export/*.png
rm -f export/none.webp
